from django.urls import path
from .views import drawing_list, drawing_create, drawing_update, drawing_delete, drawing_home

urlpatterns = [
    path('drawing/', drawing_list, name='drawing_list'),
    path('drawing/home', drawing_home, name='drawing_home'),
    path('drawing/create', drawing_create, name='drawing_create'),
    path('drawing/<int:pk>/update', drawing_update, name='drawing_update'),
    path('drawing/<int:pk>/delete', drawing_delete, name='drawing_delete'),
]
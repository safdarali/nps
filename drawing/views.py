from django.shortcuts import render, get_object_or_404

from chart_of_accounts.models import ChartOfAccount
from taxes.models import Tax
from nps.custom_decorators.permission_required import permission_required
from .models import Drawing
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import DrawingForm
from general_journal.models import GeneralJournal


def drawing_home(request):
    total_drawing = Drawing.objects.count()
    return render(request, 'drawing/drawing_home.html',
                  {'total_drawing': total_drawing})


@permission_required('drawing.list_drawing')
def drawing_list(request):
    drawing = Drawing.objects.all()
    total_drawing = Drawing.objects.count()
    return render(request, 'drawing/drawing_list.html',
                  {'drawing': drawing,
                   'total_drawing': total_drawing})


def save_drawing_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            drawing = Drawing.objects.all()
            data['html_fee_paid_list'] = render_to_string(
                'drawing/includes/partial_drawing_list.html', {
                    'drawing': drawing
                })
            data['total_drawing'] = Drawing.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_drawing_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            amount = form.cleaned_data['amount']
            from_bank = form.cleaned_data['from_bank']
            to_owner = form.cleaned_data['to_owner']
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=to_owner.id, duty_id=duty.id, voucher_type='DRAWING',
                                               voucher_number=id,
                                               debit=amount, credit=0, naration=f'To {from_bank}')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=from_bank.id, duty_id=duty.id, voucher_type='DRAWING',
                                               voucher_number=id,
                                               debit=0, credit=amount, naration=f'By {to_owner}')
            general_journal_2.save()

            form.instance.status = 2
            form.save()
            # general journal entry end
            data['form_is_valid'] = True
            drawing = Drawing.objects.all()
            data['html_drawing_list'] = render_to_string(
                'drawing/includes/partial_drawing_list.html', {
                    'drawing': drawing
                })
            data['total_drawing'] = Drawing.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('drawing.add_drawing')
def drawing_create(request):
    if request.method == 'POST':
        form = DrawingForm(request.POST)
    else:
        form = DrawingForm()
    return save_drawing_form(request, form,
                                           'drawing/includes/partial_drawing_create.html')


@permission_required('drawing.change_drawing')
def drawing_update(request, pk):
    drawing = get_object_or_404(Drawing, pk=pk)
    if request.method == 'POST':
        form = DrawingForm(request.POST, instance=drawing)
    else:
        form = DrawingForm(instance=drawing)
    return update_drawing_form(request, form,
                                             'drawing/includes/partial_drawing_update.html')


@permission_required('drawing.delete_drawing')
def drawing_delete(request, pk):
    drawing = get_object_or_404(Drawing, pk=pk)
    data = dict()
    if request.method == 'POST':
        drawing.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='FFP').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        drawing = Drawing.objects.all()
        data['html_drawing_list'] = render_to_string(
            'drawing/includes/partial_drawing_list.html', {
                'drawing': drawing
            })
        data['total_drawing'] = Drawing.objects.count()
    else:
        context = {'drawing': drawing}
        data['html_form'] = render_to_string('drawing/includes/partial_drawing_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

from django import forms

from duties.models import Duty
from .models import Drawing
from chart_of_accounts.models import ChartOfAccount


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld


class DrawingForm(forms.ModelForm):
    class Meta:
        model = Drawing
        fields = '__all__'
        exclude = ('status',)

    def __init__(self, *args, **kwargs):
        super(DrawingForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['from_bank'].empty_label = 'From Bank'
        self.fields['from_bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['to_owner'].empty_label = 'To Account'
        self.fields['to_owner'].queryset = ChartOfAccount.objects.filter(account_type=18)
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)






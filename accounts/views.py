from allauth.account.views import PasswordChangeView
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse_lazy


class LoginAfterPasswordChangeView(PasswordChangeView):
    @property
    def success_url(self):
        return reverse_lazy('account_login')

login_after_password_change = login_required(LoginAfterPasswordChangeView.as_view())


def privileges_required(request):
    data = dict()
    if request.method == 'GET':
        data['form_is_valid'] = True  # This is just to play along with the existing code
        data['html_form'] = render_to_string('partial_privileges_required.html', request=request,)
    return JsonResponse(data)




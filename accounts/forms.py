from django import forms

from .choices import *
from .models import NpsUser


class SignupForm(forms.Form):
    username = forms.CharField(max_length=50, label='User Name')
    email = forms.CharField(max_length=254, label='Email')
    password1 = forms.CharField(max_length=254, label='Password1', widget=forms.PasswordInput)
    password2 = forms.CharField(max_length=254, label='Password2', widget=forms.PasswordInput)

    class Meta:
        model = NpsUser
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)

    def signup(self, request, user):
        user.username = self.cleaned_data['username']
        user.email = self.cleaned_data['email']
        user.password1 = self.cleaned_data['password1']
        user.password2 = self.cleaned_data['password2']
        user.save()

from django import forms
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from .models import NpsUser
from .forms import SignupForm
from django.contrib.auth.models import PermissionsMixin


class UserCreationForm(forms.ModelForm):
    password = forms.CharField(max_length=254, label='Password', widget=forms.PasswordInput)

    class Meta:
        model = NpsUser
        fields = '__all__'

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.username = self.cleaned_data['username']
        user.email = self.cleaned_data['email']
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances
    add_form = UserCreationForm
    list_display = ('username', 'email', 'role',)
    ordering = ("email",)

    fieldsets = (
        ('User basic info.', {'fields': ('username', 'email', 'password', )}),
        ('Permissions', {'fields': ('is_superuser', 'is_staff', 'is_active','groups', 'user_permissions')}),
        ('Role', {'fields': ('role',)}),
    )
    add_fieldsets = (
        ('User basic info.', {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password', )}
         ),
        ('Permissions', {'fields': ('is_superuser', 'is_staff', 'is_active', 'groups', 'user_permissions')}),
        ('Role', {'fields': ('role',)}),
    )

    filter_horizontal = ()


admin.site.register(NpsUser, CustomUserAdmin)

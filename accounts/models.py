from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from .choices import *


class UserManager(BaseUserManager):
    def create_user(self, email, username=None, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=UserManager.normalize_email(email),
            username=username

        )

        user.set_password(password)
        user.is_active = False
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """

        if not email:
            raise ValueError('Users must have an email address')

        if not username:
            raise ValueError('Users must have an username')

        user = self.create_user(email,
            password=password,
            username=username
        )
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save(using=self._db)
        return user


class NpsUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='email address', unique=True)
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    is_superuser = models.BooleanField(default=False, null=False, verbose_name='Is supperuser')
    is_staff = models.BooleanField(default=False, null=False, verbose_name='Is staff')
    is_active = models.BooleanField(default=False,
                                    help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.',
                                    verbose_name='Is active')
    role = models.PositiveIntegerField(choices=Role, default=MANAGER)
    username = models.CharField(verbose_name='User name', max_length=50, null=False, blank=False, unique=True)
    objects = UserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    class Meta:
        verbose_name = 'nps_user'
        verbose_name_plural = 'nps_users'

    def __str__(self):
        return '%s' % self.username


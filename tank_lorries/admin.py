from django import forms
from django.contrib import admin
from django.contrib.admin import TabularInline, StackedInline
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from tank_lorries.models import ChamberChart, LorryChamber, Lorry


class LorryChamberInline(admin.TabularInline):
    extra = 0
    model = LorryChamber


class LorryAdmin(admin.ModelAdmin):
    inlines = [LorryChamberInline]


admin.site.register(Lorry, LorryAdmin)


class ChamberChartResource(resources.ModelResource):
    class Meta:
        model = ChamberChart


class ChamberChartAdmin(ImportExportModelAdmin):
    resource_class = ChamberChartResource
    list_display = ('lorry',
                    'lorrychamber',
                    'dip',
                    'litre',)
    search_fields = ('=dip',)


admin.site.register(ChamberChart, ChamberChartAdmin)

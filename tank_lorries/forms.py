from django import forms
from tank_lorries.models import TankLorry, Lorry


class TankLorryForm(forms.ModelForm):
    class Meta:
        model = TankLorry
        fields = '__all__'


# class LorryForm(forms.ModelForm):
#     class Meta:
#         model = Lorry
#         fields = '__all__'



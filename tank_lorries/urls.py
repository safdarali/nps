from django.urls import path
from .views import tl_create, get_tl_info, tl_list, tl_new, tl_update

urlpatterns = [
    path('tank_lorries/', tl_list, name='tl_list'),
    path('tank_lorries/create', tl_create, name='tl_create'),
    path('tank_lorries/new', tl_new, name='tl_new'),
    path('tank_lorries/<int:pk>/update', tl_update, name='tl_update'),
    path('tank_lorries/get_tl_info', get_tl_info, name='get_tl_info'),

    # path('lorries/', lorry_list, name='lorry_list'),
    # path('lorries/create', lorry_create, name='lorry_create'),
    # path('lorries/new', lorry_new, name='lorry_new'),
    # path('lorries/<int:pk>/update', lorry_update, name='lorry_update'),
    # path('lorries/get_lorry_info', get_lorry_info, name='get_lorry_info'),
]
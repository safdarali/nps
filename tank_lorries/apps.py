from django.apps import AppConfig


class TankLorriesConfig(AppConfig):
    name = 'tank_lorries'

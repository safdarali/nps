from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse
from .forms import TankLorryForm
from .models import TankLorry, Lorry


# Create your views here.


def tl_list(request):
    lorry = TankLorry.objects.all()
    return render(request, 'tank_lorries/tl_list.html', {'lorry': lorry})


def save_tl_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            data['tl_id'] = form.instance.id
            data['tl_no'] = form.cleaned_data['tl_no']
            data['tl_carriage_name'] = form.cleaned_data['tl_carriage_name']
            data['tl_driver_phone'] = form.cleaned_data['tl_driver_phone']
            data['tl_driver_name'] = form.cleaned_data['tl_driver_name']

        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tl_create(request):
    if request.method == 'POST':
        form = TankLorryForm(request.POST)
    else:
        form = TankLorryForm()
    return save_tl_form(request, form, 'tank_lorries/includes/partial_tl_create.html')


def save_new_tl_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            lorry = TankLorry.objects.all()
            data['html_tl_list'] = render_to_string('tank_lorries/includes/partial_tl_list.html', {
                'lorry': lorry
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tl_new(request):
    if request.method == 'POST':
        form = TankLorryForm(request.POST)
    else:
        form = TankLorryForm()
    return save_new_tl_form(request, form, 'tank_lorries/includes/partial_tl_create.html')


def tl_update(request, pk):
    lorry = get_object_or_404(TankLorry, pk=pk)
    if request.method == 'POST':
        form = TankLorryForm(request.POST, instance=lorry)
    else:
        form = TankLorryForm(instance=lorry)
    return save_new_tl_form(request, form, 'tank_lorries/includes/partial_tl_update.html')


def get_tl_info(request):
    data = dict()
    if request.method == 'GET':
        tanklorry = request.GET.get('tanklorry')
        lorry = TankLorry.objects.filter(id=tanklorry)[0]
        data['tl_carriage_name'] = lorry.tl_carriage_name
        data['tl_driver_phone'] = lorry.tl_driver_phone
        data['tl_driver_name'] = lorry.tl_driver_name
    return JsonResponse(data)



# Lorry


# def lorry_list(request):
#     lorry = Lorry.objects.all()
#     return render(request, 'lorries/lorry_list.html', {'lorry': lorry})
#
#
# def save_lorry_form(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             form.save()
#             data['form_is_valid'] = True
#             data['lorry_id'] = form.instance.id
#             data['lorry_no'] = form.cleaned_data['lorry_no']
#             data['lorry_name'] = form.cleaned_data['lorry_name']
#         else:
#             data['form_is_valid'] = False
#     context = {'form': form}
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
# def lorry_create(request):
#     if request.method == 'POST':
#         form = TankLorryForm(request.POST)
#     else:
#         form = TankLorryForm()
#     return save_lorry_form(request, form, 'lorries/includes/partial_lorry_create.html')
#
#
# def save_new_lorry_form(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             form.save()
#             data['form_is_valid'] = True
#             lorry = Lorry.objects.all()
#             data['html_lorry_list'] = render_to_string('lorries/includes/partial_lorry_list.html', {
#                 'lorry': lorry
#             })
#         else:
#             data['form_is_valid'] = False
#     context = {'form': form}
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
# def lorry_new(request):
#     if request.method == 'POST':
#         form = TankLorryForm(request.POST)
#     else:
#         form = TankLorryForm()
#     return save_new_lorry_form(request, form, 'lorries/includes/partial_lorry_create.html')
#
#
# def lorry_update(request, pk):
#     lorry = get_object_or_404(Lorry, pk=pk)
#     if request.method == 'POST':
#         form = TankLorryForm(request.POST, instance=lorry)
#     else:
#         form = TankLorryForm(instance=lorry)
#     return save_new_lorry_form(request, form, 'lorries/includes/partial_lorry_update.html')
#
#
# def get_lorry_info(request):
#     data = dict()
#     if request.method == 'GET':
#         lorry = request.GET.get('lorry')
#         lorry = Lorry.objects.filter(id=lorry)[0]
#         data['lorry_name'] = lorry.lorry_carriage_name
#     return JsonResponse(data)
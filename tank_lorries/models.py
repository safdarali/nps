from django.db import models

# Create your models here.
from nps.models import ModelWithTimeStamp


class TankLorry(ModelWithTimeStamp):

    tl_no = models.CharField(verbose_name='TL No', max_length=20, unique=True)
    tl_carriage_name = models.CharField(verbose_name=' Carriage name', max_length=100)
    tl_driver_name = models.CharField(verbose_name=' Driver name', max_length=100, default='')
    tl_driver_phone = models.CharField(verbose_name=' Driver phone', max_length=20)

    def __str__(self):
        return self.tl_no


class Lorry(ModelWithTimeStamp):
    lorry_name = models.CharField(verbose_name=' Lorry name', max_length=100)
    lorry_no = models.CharField(verbose_name='Lorry No', max_length=20, unique=True)

    def __str__(self):
        return self.lorry_name


class LorryChamber(ModelWithTimeStamp):
    lorry = models.ForeignKey(Lorry, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='lorry')
    chamber = models.IntegerField(verbose_name='chamber', default=None)

    def __str__(self):
        return str(self.chamber)
    # def __str__(self):
    #     return f'id:{self.id}, chamber:{self.chamber} lorry: {self.lorry}'


class ChamberChart(ModelWithTimeStamp):
    lorry = models.ForeignKey(Lorry, on_delete=models.CASCADE, null=True, blank=True, default=None, db_index=True, verbose_name='lorry')
    lorrychamber = models.ForeignKey(LorryChamber, on_delete=models.CASCADE, null=True, blank=True, default=None, db_index=True, verbose_name='chamber')
    dip = models.IntegerField(verbose_name='Dip', default=0, db_index=True)
    litre = models.IntegerField(verbose_name='Litre')

    def __str__(self):
        return str(self.dip)




from django.apps import AppConfig


class BankExpenseConfig(AppConfig):
    name = 'bank_expense'

from django.shortcuts import render, get_object_or_404

from chart_of_accounts.models import ChartOfAccount
from taxes.models import Tax
from nps.custom_decorators.permission_required import permission_required
from .models import BankExpense
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import BankExpenseForm
from general_journal.models import GeneralJournal


def bank_expense_home(request):
    total_bank_expense = BankExpense.objects.count()
    return render(request, 'bank_expense/bank_expense_home.html',
                  {'total_bank_expense': total_bank_expense})


@permission_required('bank_expense.list_bank_expense')
def bank_expense_list(request):
    bank_expense = BankExpense.objects.all()
    total_bank_expense = BankExpense.objects.count()
    return render(request, 'bank_expense/bank_expense_list.html',
                  {'bank_expense': bank_expense,
                   'total_bank_expense': total_bank_expense})


def save_bank_expense_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            bank_expense = BankExpense.objects.all()
            data['html_fee_paid_list'] = render_to_string(
                'bank_expense/includes/partial_bank_expense_list.html', {
                    'bank_expense': bank_expense
                })
            data['total_bank_expense'] = BankExpense.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_bank_expense_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            amount = form.cleaned_data['amount']
            from_bank = form.cleaned_data['from_bank']
            to_expense = ChartOfAccount.objects.get(id=50240004)
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=to_expense.id, duty_id=duty.id, voucher_type='BE',
                                               voucher_number=id,
                                               debit=amount, credit=0, naration=f'To {from_bank}')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=from_bank.id, duty_id=duty.id, voucher_type='BE',
                                               voucher_number=id,
                                               debit=0, credit=amount, naration=f'By {to_expense.account_name}')
            general_journal_2.save()

            form.instance.status = 2
            form.save()
            # general journal entry end
            data['form_is_valid'] = True
            bank_expense = BankExpense.objects.all()
            data['html_bank_expense_list'] = render_to_string(
                'bank_expense/includes/partial_bank_expense_list.html', {
                    'bank_expense': bank_expense
                })
            data['total_bank_expense'] = BankExpense.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('bank_expense.add_bank_expense')
def bank_expense_create(request):
    if request.method == 'POST':
        form = BankExpenseForm(request.POST)
    else:
        form = BankExpenseForm()
    return save_bank_expense_form(request, form,
                                           'bank_expense/includes/partial_bank_expense_create.html')


@permission_required('bank_expense.change_bank_expense')
def bank_expense_update(request, pk):
    bank_expense = get_object_or_404(BankExpense, pk=pk)
    if request.method == 'POST':
        form = BankExpenseForm(request.POST, instance=bank_expense)
    else:
        form = BankExpenseForm(instance=bank_expense)
    return update_bank_expense_form(request, form,
                                             'bank_expense/includes/partial_bank_expense_update.html')


@permission_required('bank_expense.delete_bank_expense')
def bank_expense_delete(request, pk):
    bank_expense = get_object_or_404(BankExpense, pk=pk)
    data = dict()
    if request.method == 'POST':
        bank_expense.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='FFP').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        bank_expense = BankExpense.objects.all()
        data['html_bank_expense_list'] = render_to_string(
            'bank_expense/includes/partial_bank_expense_list.html', {
                'bank_expense': bank_expense
            })
        data['total_bank_expense'] = BankExpense.objects.count()
    else:
        context = {'bank_expense': bank_expense}
        data['html_form'] = render_to_string('bank_expense/includes/partial_bank_expense_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

from django.urls import path
from .views import bank_expense_list, bank_expense_create, bank_expense_update, bank_expense_delete, bank_expense_home

urlpatterns = [
    path('bank_expense/', bank_expense_list, name='bank_expense_list'),
    path('bank_expense/home', bank_expense_home, name='bank_expense_home'),
    path('bank_expense/create', bank_expense_create, name='bank_expense_create'),
    path('bank_expense/<int:pk>/update', bank_expense_update, name='bank_expense_update'),
    path('bank_expense/<int:pk>/delete', bank_expense_delete, name='bank_expense_delete'),
]
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect


@login_required
def dashboard(request):
    if request.user.role == 2:
        return redirect('customer_home')
    else:
        return render(request, 'dashboard.html')
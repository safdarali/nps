import base64
import json

from django.contrib import messages
from django.core.files.base import ContentFile

from duties.models import Duty
from general_journal.models import GeneralJournal, TankGeneralJournal
from meters.models import Meter
from purchases.models import LubesStock, FuelPurchases
from taxes.models import Tax
from nps.custom_decorators.permission_required import permission_required
from django.utils.decorators import method_decorator
from chart_of_accounts.models import ChartOfAccount
from django.db.models import Sum
from django.shortcuts import get_object_or_404, render
from django.db import transaction
from products.models import Product
from tanks.models import TankChart1, TankChart2
from .models import LubeSalem, LubeSales, FuelSalem, TankFuelSalem, FuelSales, TanksLedger, TankFuelSales, \
    SaleTransition
from .forms import LubeSalemForm, LubeSalesForm, LubeSaleFormSet, FuelSalemForm, FuelSalesForm, FuelSaleFormSet, \
    TankFuelSalemForm, TankFuelSalesForm, TankFuelSaleFormSet
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
import pendulum
from easy_pdf.views import PDFTemplateView, PDFTemplateResponseMixin


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld.id


def LubeSaleDelete(request, pk):
    lubes_sale = get_object_or_404(LubeSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        lubes_sale.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        lubes_sales = LubeSalem.objects.all()
        data['html_lubes_sale_list'] = render_to_string('sales/includes/partial_lube_sale_list.html', {
            'lubes_sales': lubes_sales
        })
        data['total_sales'] = LubeSalem.objects.count()
    else:
        context = {'lubes_sale': lubes_sale}
        data['html_form'] = render_to_string('sales/includes/partial_fuel_sale_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


def lube_sale_home(request):
    total_sales = LubeSalem.objects.count()
    return render(request, 'sales/lube_sales_home.html', {'total_sales': total_sales})


class LubeSaleDetail(DetailView):
    template_name = 'sales/includes/lube_sale_detail.html'
    context_object_name = 'lubes_sale'
    model = LubeSalem


class LubeSaleList(ListView):
    template_name = 'sales/lube_sales_list.html'
    context_object_name = 'lubes_sales'
    model = LubeSalem

    def get_context_data(self, **kwargs):
        context = super(LubeSaleList, self).get_context_data(**kwargs)
        context['total_sales'] = LubeSalem.objects.count()
        return context


class LubeSaleCreate(CreateView):
    template_name = 'sales/includes/lube_sale_create.html'
    model = LubeSalem
    form_class = LubeSalemForm

    @method_decorator(permission_required('sales.add_lubesalem'))
    def dispatch(self, *args, **kwargs):
        return super(LubeSaleCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(LubeSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_context_data(self, **kwargs):
        data = super(LubeSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['lubes_sale'] = LubeSaleFormSet(self.request.POST)
            data['total_sales'] = LubeSalem.objects.count()
        else:
            data['lubes_sale'] = LubeSaleFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['lubes_sale']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()
        return super(LubeSaleCreate, self).form_valid(form)


class LubeSaleUpdate(UpdateView):
    model = LubeSalem
    form_class = LubeSalesForm
    template_name = 'sales/includes/lube_sale_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_form_kwargs(self):
        kwargs = super(LubeSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = LubeSalemForm
        form = self.get_form(form_class)
        lubes_sale = LubeSaleFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, lubes_sale=lubes_sale))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = LubeSalemForm
        form = self.get_form(form_class)
        lubes_sale = LubeSaleFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and lubes_sale.is_valid():
            return self.form_valid(form, lubes_sale)
        return self.form_invalid(form, lubes_sale)

    def form_valid(self, form, lubes_sale):
        id = form.instance.id
        self.object = form.save()
        lubes_sale.instance = self.object
        id = form.instance.id
        duty = form.cleaned_data['duty']
        net_amount = self.object.net_amount
        net_discount = self.object.net_discount
        lubes_sale.save()
        ls = LubeSales.objects.filter(lubesalem=id)
        short_products = dict()
        list_short_products = []
        for i in ls:
            if i.product is not None:
                product = i.product
                qty_sold = i.qty_sold
                lstock = LubesStock.objects.filter(product_id=product.id).exclude(qty=0).aggregate(total_qty=Sum('qty'))
                if lstock['total_qty'] is None:
                    lstock['total_qty'] = 0
                if qty_sold > lstock['total_qty']:
                    short_products['product'] = product.product_name
                    short_products['present_qty'] = lstock['total_qty']
                    short_products['sold_qty'] = qty_sold
            list_short_products.append(short_products.copy())
        if short_products:
            return self.render_to_response(
                self.get_context_data(form=form, lubes_sale=lubes_sale, short_products=list_short_products))
        else:
            # Lube stock entery
            for i in ls:
                if i.product is not None:
                    product = i.product
                    qty_sold = i.qty_sold
                    condition = True
                    cost = 0
                    total_cost = 0
                    while condition:
                        stock = LubesStock.objects.filter(product_id=product.id).exclude(qty=0).first()
                        in_stock = stock.qty
                        if qty_sold <= in_stock:
                            stock.qty = stock.qty - qty_sold
                            stock.save()
                            cost = qty_sold * stock.cost_price
                            condition = False
                        else:
                            remaining = qty_sold - in_stock
                            stock.qty = stock.qty - in_stock
                            stock.save()
                            cost = in_stock * stock.cost_price
                            qty_sold = remaining
                            condition = True
                        total_cost += cost

                    lsale = ls.get(product_id=product.id)
                    lsale.profit = lsale.amount - total_cost
                    lsale.save()
            lsm = LubeSalem.objects.get(id=id)
            lsm.status = 2
            lsm.save()
        # general journal entry
        lube_account = ChartOfAccount.objects.get(id=40010002).account_name
        cash_in_hand_account = ChartOfAccount.objects.get(id=30010001).account_name
        lube_sale_discount_account = ChartOfAccount.objects.get(id=50090007).account_name
        if net_discount > 0:
            general_2 = GeneralJournal(
                chartofaccount_id=40010002, duty_id=duty.id, voucher_type='LS', voucher_number=id, debit=0,
                credit=net_amount + net_discount, naration=f'By {cash_in_hand_account} & {lube_sale_discount_account}')
            general_2.save()

            general_1 = GeneralJournal(
                chartofaccount_id=30010001, duty_id=duty.id, voucher_type='LS', voucher_number=id, debit=net_amount,
                credit=0, naration=f'To {lube_account}')
            general_1.save()

            general_3 = GeneralJournal(
                chartofaccount_id=50090007, duty_id=duty.id, voucher_type='LS', voucher_number=id, debit=net_discount,
                credit=0, naration=f'To {lube_account}')
            general_3.save()
        else:
            general_2 = GeneralJournal(
                chartofaccount_id=40010002, duty_id=duty.id, voucher_type='LS', voucher_number=id, debit=0,
                credit=net_amount, naration=f'By {cash_in_hand_account}')
            general_2.save()

            general_1 = GeneralJournal(
                chartofaccount_id=30010001, duty_id=duty.id, voucher_type='LS', voucher_number=id, debit=net_amount,
                credit=0, naration=f'To {lube_account}')
            general_1.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, lubes_sale):
        return self.render_to_response(self.get_context_data(form=form, lubes_sale=lubes_sale))


class GetLubesProductPriceAndStock(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        id = request.GET.get('id')
        if id is not None:
            product = Product.objects.get(pk=id)
            ls = LubesStock.objects.filter(product_id=id).aggregate(total_stock=Sum('qty'))
            data['price'] = product.sale_price
            data['available_stock'] = ls['total_stock']

        return JsonResponse(data)


# Fuel sale


def FuelSaleDelete(request, pk):
    fuels_sale = get_object_or_404(FuelSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        fuels_sale.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        fuels_sales = FuelSalem.objects.all()
        data['html_fuels_sale_list'] = render_to_string('sales/includes/partial_fuel_sale_list.html', {
            'fuels_sales': fuels_sales
        })
        data['total_sales'] = FuelSalem.objects.count()
    else:
        context = {'fuels_sale': fuels_sale}
        data['html_form'] = render_to_string('sales/includes/partial_fuel_sale_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


def fuel_sale_home(request):
    total_sales = FuelSalem.objects.count()
    return render(request, 'sales/fuel_sales_home.html', {'total_sales': total_sales})


class FuelSaleDetail(DetailView):
    template_name = 'sales/includes/fuel_sale_detail.html'
    context_object_name = 'fuels_sale'
    model = FuelSalem


class FuelSaleList(ListView):
    template_name = 'sales/fuel_sales_list.html'
    context_object_name = 'fuels_sales'
    model = FuelSalem

    def get_context_data(self, **kwargs):
        context = super(FuelSaleList, self).get_context_data(**kwargs)
        context['total_sales'] = FuelSalem.objects.count()
        return context


class FuelSaleCreate(CreateView):
    template_name = 'sales/includes/fuel_sale_create.html'
    model = FuelSalem
    form_class = FuelSalemForm

    @method_decorator(permission_required('sales.add_fuelsalem'))
    def dispatch(self, *args, **kwargs):
        return super(FuelSaleCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FuelSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_context_data(self, **kwargs):
        data = super(FuelSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['fuels_sale'] = FuelSaleFormSet(self.request.POST)
            data['total_sales'] = FuelSalem.objects.count()
        else:
            data['fuels_sale'] = FuelSaleFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['fuels_sale']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(FuelSaleCreate, self).form_valid(form)


class FuelSaleUpdate(UpdateView):
    model = FuelSalem
    form_class = FuelSalesForm
    template_name = 'sales/includes/fuel_sale_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    @method_decorator(permission_required('sales.change_fuelsalem'))
    def dispatch(self, *args, **kwargs):
        return super(FuelSaleUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FuelSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = FuelSalemForm
        form = self.get_form(form_class)
        fuels_sale = FuelSaleFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, fuels_sale=fuels_sale))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = FuelSalemForm
        form = self.get_form(form_class)
        fuels_sale = FuelSaleFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and fuels_sale.is_valid():
            return self.form_valid(form, fuels_sale)
        return self.form_invalid(form, fuels_sale)

    def form_valid(self, form, fuels_sale):
        self.object = form.save()
        fuels_sale.instance = self.object
        id = form.instance.id
        duty = form.cleaned_data['duty'].id
        product_id = form.cleaned_data['product'].id
        net_amount = form.cleaned_data['net_amount']
        fuels_sale.save()
        fs = FuelSales.objects.filter(fuelsalem=id)
        for i in fs:
            if i.qty_sold != 0 and i.qty_sold is not None:
                meter = i.meter
                qty_sold = i.qty_sold
                test_reading = i.test_reading
                # tank ledger entry
                tank_ledger = TanksLedger(duty_id=duty, meter_id=meter.id, tank_id=meter.tank_id, opening_stock=0,
                                          received_stock=0, meter_sale=qty_sold, test_reading=test_reading, dip=0,
                                          physical_stock=0, trans_type='MS', trans_number=id)
                tank_ledger.save()
                # tank ledger entry end
        # TRANSITION ENTRY
        sale_transition = SaleTransition(duty_id=duty, product_id=product_id, unallocated=net_amount, allocated=0,
                                         balance=net_amount)
        sale_transition.save()
        # TRANSITION ENTRY END
        fs = FuelSalem.objects.get(id=id)
        fs.status = 2
        fs.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, fuels_sale):
        return self.render_to_response(self.get_context_data(form=form, fuels_sale=fuels_sale))


class GetFuelsProductPriceAndStock(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        d = []
        current_readings = []
        id = request.GET.get('id')
        if id is not None:
            product = Product.objects.get(pk=id)
            # ll = FuelsLedger.objects.filter(product_id=id).last()
            data['price'] = product.sale_price
            data['meter_count'] = product.meter_set.all().count()
            meters_vals = product.meter_set.values('id')
            for i in range(len(meters_vals)):
                d.append(meters_vals[i]['id'])
                try:
                    current_readings.append(
                        FuelSales.objects.values('current_reading').filter(meter_id=meters_vals[i]['id']).last()[
                            'current_reading'])
                except:
                    current_readings.append(0)

            data['meters'] = d
            data['current_readings'] = current_readings

        return JsonResponse(data)


# Tank Fuel sale

def get_chart(request):
    data = dict()
    if request.method == 'GET':
        tank_id = request.GET.get('tank', None)
        current_dip = request.GET.get('current_dip', None)
        if tank_id == '1':
            data['litre'] = TankChart1.objects.filter(dip=current_dip)[0].litre
        else:
            data['litre'] = TankChart2.objects.filter(dip=current_dip)[0].litre

    return JsonResponse(data)


def TankFuelSaleDelete(request, pk):
    tankfuels_sale = get_object_or_404(TankFuelSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        tankfuels_sale.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        tankfuels_sales = TankFuelSalem.objects.all()
        data['html_tankfuels_sale_list'] = render_to_string('sales/includes/partial_tankfuel_sale_list.html', {
            'tankfuels_sales': tankfuels_sales
        })
        data['total_sales'] = TankFuelSalem.objects.count()
    else:
        context = {'tankfuels_sale': tankfuels_sale}
        data['html_form'] = render_to_string('sales/includes/partial_tankfuel_sale_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


def tankfuel_sale_home(request):
    total_sales = TankFuelSalem.objects.count()
    return render(request, 'sales/tankfuel_sales_home.html', {'total_sales': total_sales})


class TankFuelSaleDetail(DetailView):
    template_name = 'sales/includes/tankfuel_sale_detail.html'
    context_object_name = 'tankfuels_sale'
    model = TankFuelSalem


class TankFuelSaleList(ListView):
    template_name = 'sales/tankfuel_sales_list.html'
    context_object_name = 'tankfuels_sales'
    model = TankFuelSalem

    def get_context_data(self, **kwargs):
        context = super(TankFuelSaleList, self).get_context_data(**kwargs)
        context['total_sales'] = TankFuelSalem.objects.count()
        return context


class TankFuelSaleCreate(CreateView):
    template_name = 'sales/includes/tankfuel_sale_create.html'
    model = TankFuelSalem
    form_class = TankFuelSalemForm

    @method_decorator(permission_required('sales.add_tankfuelsalem'))
    def dispatch(self, *args, **kwargs):
        return super(TankFuelSaleCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(TankFuelSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_context_data(self, **kwargs):
        data = super(TankFuelSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['tankfuels_sale'] = TankFuelSaleFormSet(self.request.POST)
            data['total_sales'] = TankFuelSalem.objects.count()
        else:
            data['tankfuels_sale'] = TankFuelSaleFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['tankfuels_sale']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(TankFuelSaleCreate, self).form_valid(form)


class TankFuelSaleUpdate(UpdateView):
    model = TankFuelSalem
    form_class = TankFuelSalesForm
    template_name = 'sales/includes/tankfuel_sale_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_form_kwargs(self):
        kwargs = super(TankFuelSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = TankFuelSalemForm
        form = self.get_form(form_class)
        tankfuels_sale = TankFuelSaleFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, tankfuels_sale=tankfuels_sale))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = TankFuelSalemForm
        form = self.get_form(form_class)
        tankfuels_sale = TankFuelSaleFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and tankfuels_sale.is_valid():
            return self.form_valid(form, tankfuels_sale)
        return self.form_invalid(form, tankfuels_sale)

    def form_valid(self, form, tankfuels_sale):
        self.object = form.save()
        tankfuels_sale.instance = self.object
        id = form.instance.id
        duty = form.cleaned_data['duty']
        tankfuels_sale.save()
        duty_count = Duty.objects.count()
        if duty_count > 1:
            latest_duty = Duty.objects.latest('id')
            prev_duty = Duty.objects.filter(id__lt=latest_duty.id).order_by('-id').first()
        else:
            prev_duty = Duty.objects.latest('date')

        tfss = TankFuelSales.objects.filter(tankfuelsalem=id)
        for i in tfss:
            if i.qty_sold != 0 and i.qty_sold is not None:
                tank = i.tank
                current_dip = i.current_dip
                # opening stock management
                ps = 0
                try:
                    physical_stock = \
                        TanksLedger.objects.filter(duty_id=prev_duty.id, tank_id=tank.id).first()
                    ps = physical_stock.physical_stock
                except:
                    ps = 0
                # tank ledger entry
                tank_ledger = TanksLedger(duty_id=duty.id, tank_id=tank.id, opening_stock=ps, received_stock=0,
                                          meter_sale=0, test_reading=0, dip=current_dip,
                                          physical_stock=0, trans_type='TS', trans_number=id)
                tank_ledger.save()

                tl_current_duty_count = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id)
                if tl_current_duty_count.count() == 1:
                    tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id)[0]
                    tl_obj = TanksLedger.objects.get(id=tl.id)
                    ps = 0
                    try:
                        physical_stock = TanksLedger.objects.filter(duty_id=prev_duty.id, tank_id=tank.id).first()
                        ps = physical_stock.physical_stock
                        tl_obj.physical_stock = ps - i.qty_sold
                        tl_obj.save()
                    except:
                        ps = 0
                        tl_obj.physical_stock = ps - i.qty_sold
                        tl_obj.save()
                else:
                    tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id).first()
                    tl_obj = TanksLedger.objects.get(id=tl.id)
                    tl_obj.physical_stock = tl_obj.physical_stock - i.qty_sold
                    tl_obj.save()

                # tank ledger entry end
        tfsm = TankFuelSalem.objects.get(id=id)
        tfsm.status = 2
        tfsm.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, tankfuels_sale):
        return self.render_to_response(self.get_context_data(form=form, tankfuels_sale=tankfuels_sale))


class GetTankFuelsProductPriceAndStock(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        d = []
        previous_dip = []
        previous_stock = []
        purchase = []
        id = request.GET.get('id')
        duty_id = request.GET.get('duty_id')
        duty = Duty.objects.get(id=duty_id)
        if id is not None:
            try:
                tsm = TankFuelSalem.objects.filter(product_id=id).last()
                tss = tsm.tankfuelsales_set.all()
            except:
                pass
            product = Product.objects.get(pk=id)
            data['tank_count'] = product.tank_set.all().count()
            tanks_vals = product.tank_set.values('id')

            for i in range(len(tanks_vals)):
                d.append(tanks_vals[i]['id'])
                try:
                    previous_dip.append(tss.filter(tank_id=tanks_vals[i]['id'])[0].current_dip)
                    previous_stock.append(tss.filter(tank_id=tanks_vals[i]['id'])[0].current_stock)
                except:
                    previous_dip = 0
                    previous_stock = 0
                try:
                    purchase.append(
                        FuelPurchases.objects.filter(product_id=id, date__year=duty.date.year,
                                                     date__month=duty.date.month,
                                                     date__day=duty.date.day, tank_id=tanks_vals[i]['id']).aggregate(
                            purchased_stock=Sum('purchased_stock')))
                except:
                    purchase.append({"purchased_stock": '0'})
            data['tanks'] = d
            data['previous_dip'] = previous_dip
            data['previous_stock'] = previous_stock
            data['purchase'] = purchase

        return JsonResponse(data)


class GetSaleTransition(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        duty = request.GET.get('duty')
        # if duty is not None:
        sale_transition = SaleTransition.objects.filter(duty_id=duty)
        context = {'sale_transition': sale_transition}
        data['html_sale_transition_table'] = render_to_string('sales/includes/partial_sale_transition_add.html',
                                             context,
                                             request=request,
                                             )
        return JsonResponse(data)

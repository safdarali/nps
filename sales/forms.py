from django import forms
from django.forms.models import inlineformset_factory
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty, DutyMen
from meters.models import Meter
from products.models import Product
from tanks.models import Tank
from .models import LubeSalem, LubeSales, FuelSalem, FuelSales, TankFuelSalem, TankFuelSales


def get_latest_duty_date():
    ld = Duty.objects.latest('date')
    return ld


class LubeSalesForm(forms.ModelForm):
    class Meta:
        model = LubeSales
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(LubeSalesForm, self).__init__(*args, **kwargs)
        if self.instance.product_id:
            self.fields['product'].queryset = Product.objects.filter(id=self.instance.product_id)
            self.fields['product'].empty_label = None
        else:
            self.fields['product'].queryset = Product.objects.filter(product_type=1)
            self.fields['product'].empty_label = 'Please, choose a Product'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


LubeSaleFormSet = inlineformset_factory(LubeSalem, LubeSales, form=LubeSalesForm, extra=1)


class LubeSalemForm(forms.ModelForm):
    class Meta:
        model = LubeSalem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(LubeSalemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class FuelSalesForm(forms.ModelForm):
    class Meta:
        model = FuelSales
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(FuelSalesForm, self).__init__(*args, **kwargs)
        if self.instance.meter_id:
            self.fields['meter'].queryset = Meter.objects.filter(id=self.instance.meter_id)
            # self.fields['meter'].empty_label = None
        else:
            self.fields['meter'].queryset = Meter.objects.all()
            # self.fields['meter'].empty_label = None
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


FuelSaleFormSet = inlineformset_factory(FuelSalem, FuelSales, form=FuelSalesForm, extra=1)


class FuelSalemForm(forms.ModelForm):
    class Meta:
        model = FuelSalem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(FuelSalemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type=4)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class TankFuelSalesForm(forms.ModelForm):
    class Meta:
        model = TankFuelSales
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(TankFuelSalesForm, self).__init__(*args, **kwargs)
        if self.instance.tank_id:
            self.fields['tank'].queryset = Tank.objects.filter(id=self.instance.tank_id)
            # self.fields['meter'].empty_label = None
        else:
            self.fields['tank'].queryset = Tank.objects.all()
            # self.fields['meter'].empty_label = None
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


TankFuelSaleFormSet = inlineformset_factory(TankFuelSalem, TankFuelSales, form=TankFuelSalesForm, extra=1)


class TankFuelSalemForm(forms.ModelForm):
    class Meta:
        model = TankFuelSalem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(TankFuelSalemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type__in=[4,5])

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)



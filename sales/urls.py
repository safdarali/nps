from django.urls import path
from .views import LubeSaleList, LubeSaleCreate, LubeSaleDetail, LubeSaleUpdate, LubeSaleDelete, \
    GetLubesProductPriceAndStock, \
    FuelSaleList, FuelSaleCreate, FuelSaleDetail, FuelSaleUpdate, FuelSaleDelete, GetFuelsProductPriceAndStock, \
    lube_sale_home, TankFuelSaleList, TankFuelSaleCreate, TankFuelSaleDetail, TankFuelSaleUpdate, TankFuelSaleDelete, \
    GetTankFuelsProductPriceAndStock, get_chart, GetSaleTransition

urlpatterns = [
    path('lubes_sales', LubeSaleList.as_view(), name='lubes_sale_list'),
    path('lubes_sales/create', LubeSaleCreate.as_view(), name='lubes_sale_create'),
    path('lubes_sales/<int:pk>/detail', LubeSaleDetail.as_view(), name="lubes_sale_detail"),
    path('lubes_sales/<int:pk>/update', LubeSaleUpdate.as_view(), name="lubes_sale_update"),
    path('lubes_sales/<int:pk>/delete', LubeSaleDelete, name="lubes_sale_delete"),
    path('lubes_sales/get_price', GetLubesProductPriceAndStock.as_view(), name='get_lubes_price'),
    path('lubes_sales/home', lube_sale_home, name='lube_sale_home'),

    path('fuels_sales', FuelSaleList.as_view(), name='fuels_sale_list'),
    path('fuels_sales/create', FuelSaleCreate.as_view(), name='fuels_sale_create'),
    path('fuels_sales/<int:pk>/detail', FuelSaleDetail.as_view(), name="fuels_sale_detail"),
    path('fuels_sales/<int:pk>/update', FuelSaleUpdate.as_view(), name="fuels_sale_update"),
    path('fuels_sales/<int:pk>/delete', FuelSaleDelete, name="fuels_sale_delete"),
    path('fuels_sales/get_price', GetFuelsProductPriceAndStock.as_view(), name='get_fuels_price'),

    path('tankfuels_sales', TankFuelSaleList.as_view(), name='tankfuels_sale_list'),
    path('tankfuels_sales/create', TankFuelSaleCreate.as_view(), name='tankfuels_sale_create'),
    path('tankfuels_sales/<int:pk>/detail', TankFuelSaleDetail.as_view(), name="tankfuels_sale_detail"),
    path('tankfuels_sales/<int:pk>/update', TankFuelSaleUpdate.as_view(), name="tankfuels_sale_update"),
    path('tankfuels_sales/<int:pk>/delete', TankFuelSaleDelete, name="tankfuels_sale_delete"),
    path('tankfuels_sales/get_previous_dip', GetTankFuelsProductPriceAndStock.as_view(), name='get_previous_dip'),
    path('tankfuels_sales/get_chart', get_chart, name='tankfuel_get_chart'),
    path('sale_transition', GetSaleTransition.as_view(), name='sale_transition'),
    ]
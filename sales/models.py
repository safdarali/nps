from decimal import Decimal

from autosequence import AutoSequenceField
from django.db import models

from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty, DutyMen
from django.conf import settings
from django.utils import timezone

from meters.models import Meter
from nps.models import ModelWithTimeStamp
from products.models import Product
from tanks.models import Tank


class LubeSalem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, db_index=True, blank=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),
                                     verbose_name='Invoice amount')
    net_discount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,
                                       default=Decimal('0.000'), verbose_name='Net discount')
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,
                                      db_index=True)


class LubeSales(ModelWithTimeStamp):
    lubesalem = models.ForeignKey(LubeSalem, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Product',
                                db_index=True)
    qty_present = models.IntegerField(null=False, blank=False, verbose_name='Qty present', default=0)
    qty_sold = models.IntegerField(null=False, blank=False, verbose_name='Qty sold', default=1)
    price = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),
                                verbose_name='Price / item')
    discount = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),
                                   verbose_name='Discount / item')
    amount = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),
                                 verbose_name='Amount')
    profit = models.DecimalField(max_digits=10, decimal_places=3, null=True, blank=True, default=Decimal('0.000'),
                                 verbose_name='Profit')

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.product


class FuelSalem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Product', db_index=True)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Net amount')
    net_qty_sold = models.IntegerField(null=False, blank=False, verbose_name='Net qty sold', default=0)
    net_test_reading = models.IntegerField(null=False, blank=False, verbose_name='Net test reading', default=0)
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,
                                      db_index=True)

    class Meta:
        unique_together = ('product', 'duty',)

    def __str__(self):
        return self.product.product_name


class FuelSales(ModelWithTimeStamp):
    fuelsalem = models.ForeignKey(FuelSalem, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    meter = models.ForeignKey(Meter, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Meter', db_index=True)
    current_reading = models.IntegerField(null=False, blank=False, verbose_name='Current reading', default=0)
    test_reading = models.IntegerField(null=False, blank=False, verbose_name='Test reading', default=0)
    previous_reading = models.IntegerField(null=False, blank=False, verbose_name='Previous reading', default=0)
    qty_sold = models.IntegerField(null=False, blank=False, verbose_name='Qty sold', default=0)
    price = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Price / litre')
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Amount')

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.meter.meter_name


class TankFuelSalem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Product',
                                db_index=True)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    net_qty_sold = models.IntegerField(null=False, blank=False, verbose_name='Net qty sold', default=0)
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,
                                      db_index=True)

    class Meta:
        unique_together = ('product', 'duty',)

    def __str__(self):
        return self.product.product_name


class TankFuelSales(ModelWithTimeStamp):
    tankfuelsalem = models.ForeignKey(TankFuelSalem, on_delete=models.CASCADE, null=True, blank=True, db_index=True)
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Tank',
                             db_index=True)
    current_dip = models.IntegerField(null=False, blank=False, verbose_name='Current dip', default=0)
    current_stock = models.IntegerField(null=False, blank=False, verbose_name='Current stock', default=0)
    previous_dip = models.IntegerField(null=False, blank=False, verbose_name='Previous dip', default=0)
    previous_stock = models.IntegerField(null=False, blank=False, verbose_name='Previous stock', default=0)
    stock_received = models.IntegerField(null=False, blank=False, verbose_name='Stock received', default=0)
    qty_sold = models.IntegerField(null=False, blank=False, verbose_name='Qty. sold', default=0)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.tank.tank_name


class TanksLedger(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, db_index=True)
    meter = models.ForeignKey(Meter, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Meter',
                              db_index=True)
    tank = models.ForeignKey(Tank, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Tank', db_index=True)
    opening_stock = models.IntegerField(null=False, blank=False, verbose_name='Opening stock', default=0)
    received_stock = models.IntegerField(null=False, blank=False, verbose_name='Received stock', default=0)
    meter_sale = models.IntegerField(null=False, blank=False, verbose_name='Meter sale', default=0)
    test_reading = models.IntegerField(null=False, blank=False, verbose_name='Test reading', default=0)
    dip = models.IntegerField(null=False, blank=False, verbose_name='Dip', default=0)
    physical_stock = models.IntegerField(null=False, blank=False, verbose_name='Physical stock', default=0)
    trans_type = models.CharField(max_length=15, verbose_name='Trans?', null=True, blank=True)
    trans_number = models.CharField(max_length=15, verbose_name='Trans#', null=True, blank=True)

    class Meta:
        ordering = ('duty',)

    def __str__(self):
        return self.tank.tank_name


class SaleTransition(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, db_index=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Product')
    unallocated = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Unallocated')
    allocated = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Allocated')
    balance = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Balance')

    def __str__(self):
        return str(self.duty)

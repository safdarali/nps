from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import TankFuelSalem, TankFuelSales, FuelSalem, FuelSales, TanksLedger, SaleTransition


class TankFuelSalemResource(resources.ModelResource):
    class Meta:
        model = TankFuelSalem


class TankFuelSalemAdmin(ImportExportModelAdmin):
    resource_class = TankFuelSalemResource
    fields = ['user', 'product', 'duty', 'duty_person', 'net_qty_sold']


admin.site.register(TankFuelSalem, TankFuelSalemAdmin)


class TankFuelSalesResource(resources.ModelResource):
    class Meta:
        model = TankFuelSales


class TankFuelSalesAdmin(ImportExportModelAdmin):
    resource_class = TankFuelSalesResource
    fields = ['tankfuelsalem',
              'tank',
              'current_dip',
              'current_stock',
              'previous_dip',
              'previous_stock',
              'stock_received',
              'qty_sold']


admin.site.register(TankFuelSales, TankFuelSalesAdmin)


class FuelSalemResource(resources.ModelResource):
    class Meta:
        model = FuelSalem


class FuelSalemAdmin(ImportExportModelAdmin):
    resource_class = FuelSalemResource
    fields = ['user',
              'product',
              'duty',
              'duty_person',
              'date',
              'net_amount',
              'net_qty_sold',
              'net_test_reading',
              ]


admin.site.register(FuelSalem, FuelSalemAdmin)


class FuelSalesResource(resources.ModelResource):
    class Meta:
        model = FuelSales


class FuelSalesAdmin(ImportExportModelAdmin):
    resource_class = FuelSalesResource
    fields = ['fuelsalem',
              'meter',
              'current_reading',
              'test_reading',
              'previous_reading',
              'qty_sold',
              'price',
              'amount',
              ]


admin.site.register(FuelSales, FuelSalesAdmin)


class TanksLedgerResource(resources.ModelResource):
    class Meta:
        model = TanksLedger


class TanksLedgerAdmin(ImportExportModelAdmin):
    resource_class = TanksLedgerResource
    fields = ['duty',
              'meter',
              'tank',
              'opening_stock',
              'received_stock',
              'meter_sale',
              'test_reading',
              'dip',
              'physical_stock',
              'trans_type',
              'trans_number']
    list_display = ['duty',
                    'meter',
                    'tank',
                    'opening_stock',
                    'received_stock',
                    'meter_sale',
                    'test_reading',
                    'dip',
                    'physical_stock',
                    'trans_type',
                    'trans_number']


admin.site.register(TanksLedger, TanksLedgerAdmin)


class SaleTransitionResource(resources.ModelResource):
    class Meta:
        model = SaleTransition


class SaleTransitionAdmin(ImportExportModelAdmin):
    resource_class = SaleTransitionResource
    fields = ['duty',
              'product',
              'unallocated',
              'allocated',
              'balance',
              ]
    list_display = ('duty', 'product', 'unallocated', 'allocated', 'balance')


admin.site.register(SaleTransition, SaleTransitionAdmin)

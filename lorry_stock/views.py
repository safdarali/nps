from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import TemplateView, ListView

from duties.models import Duty
from lorry_stock.forms import LorryToTankForm, TankToLorryForm
from lorry_stock.models import LorryToTank, LorryStock, LorryStockHistory, TankToLorry
from sales.models import TanksLedger
from tank_lorries.models import LorryChamber

 # LORRY TO TANK
class LorryStockTransaction(TemplateView):
    template_name = 'lorry_stock_to_tank/lorry_stock_transaction_home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['un_posted_lorry_to_tank'] = LorryToTank.objects.filter(status=1)
        return context


def save_lorry_to_tank_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            un_posted_lorry_to_tank = LorryToTank.objects.filter(status=1)
            data['html_lorry_to_tank'] = render_to_string('lorry_stock_to_tank/includes/partial_lorry_to_tank_unposted_list.html', {'un_posted_lorry_to_tank': un_posted_lorry_to_tank })
            # data['total_drawing'] = Drawing.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def lorry_to_tank_create(request):
    if request.method == 'POST':
        form = LorryToTankForm(request.POST)
    else:
        form = LorryToTankForm()
    return save_lorry_to_tank_form(request, form,'lorry_stock_to_tank/includes/partial_lorry_to_tank_create.html')


def load_chamber(request):
    lorry_id = request.GET.get('lorry_id')
    chambers = LorryChamber.objects.filter(lorry_id=lorry_id).order_by('chamber')
    return render(request, 'lorry_stock_to_tank/includes/chamber_dropdown_list_options.html', {'chambers': chambers})


def show_all_posted_lorry_to_tank(request):
    posted_lorry_to_tank = LorryToTank.objects.filter(status=2)
    return render(request, 'lorry_stock_to_tank/includes/partial_lorry_to_tank_posted_list.html', {'posted_lorry_to_tank': posted_lorry_to_tank})


def update_lorry_to_tank_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()

            duty_count = Duty.objects.count()
            if duty_count > 1:
                latest_duty = Duty.objects.latest('id')
                prev_duty = Duty.objects.filter(id__lt=latest_duty.id).order_by('-id').first()
            else:
                prev_duty = Duty.objects.latest('id')

            id = form.instance.id
            tank = form.cleaned_data['tank']
            lorry = form.cleaned_data['lorry']
            chamber = form.cleaned_data['chamber']
            product = form.cleaned_data['product']
            duty = form.cleaned_data['duty']
            lorry_shifted_stock = form.cleaned_data['lorry_shifted_stock']
            lorry_final_stock = form.cleaned_data['lorry_final_stock']
            lorry_final_dip = form.cleaned_data['lorry_final_dip']
            tank_previous_stock = form.cleaned_data['tank_previous_stock']
            tank_final_dip = form.cleaned_data['tank_final_dip']
            tank_final_stock = form.cleaned_data['tank_final_stock']
            tank_ledger = TanksLedger(duty_id=duty.id, tank_id=tank.id, opening_stock=0,
                                      received_stock=lorry_shifted_stock, meter_sale=0, test_reading=0, dip=tank_final_dip,
                                      physical_stock=0, trans_type='LT', trans_number=id)
            tank_ledger.save()
            tl_current_duty_count = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id)
            if tl_current_duty_count.count() == 1:
                tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id)[0]
                tl_obj = TanksLedger.objects.get(id=tl.id)
                ps = 0
                try:
                    physical_stock = TanksLedger.objects.filter(duty_id=prev_duty.id, tank_id=tank.id).first()
                    ps = physical_stock.physical_stock
                    tl_obj.physical_stock = ps + lorry_shifted_stock
                    tl_obj.save()
                except:
                    ps = 0
                    tl_obj.physical_stock = ps + lorry_shifted_stock
                    tl_obj.save()
            else:
                tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id).first()
                tl_obj = TanksLedger.objects.get(id=tl.id)
                tl_obj.physical_stock = tl_obj.physical_stock + lorry_shifted_stock
                tl_obj.save()
            # lorry stock update
            ls = LorryStock.objects.filter(lorry_id=lorry.id, chamber_id=chamber.id, product_id=product.id)
            if ls.count() > 0:
                ls_object = LorryStock.objects.get(id=ls[0].id)
                ls_object.stock = ls_object.stock - lorry_shifted_stock
                ls_object.save()
            else:
                ls_object = LorryStock(lorry_id=lorry.id, chamber_id=chamber.id, product_id=product.id, stock=lorry_final_stock)
                ls_object.save()
            # lorry stock history
            lorry_stock_history = LorryStockHistory(duty_id=duty.id, lorry_id=lorry.id, chamber_id=chamber.id,
                                                    product_id=product.id, voucher_type='LT', voucher_number=id,
                                                    dip=lorry_final_dip, stock_debit=0,
                                                    stock_credit=lorry_shifted_stock, comment=f'{product.product_name} LT')

            lorry_stock_history.save()
            form.instance.status = 2
            form.save()
            # general journal entry end
            data['form_is_valid'] = True
            data['total_lorry_to_tank'] = LorryToTank.objects.count()
            un_posted_lorry_to_tank = LorryToTank.objects.filter(status=1)
            data['html_lorry_to_tank'] = render_to_string('lorry_stock_to_tank/includes/partial_lorry_to_tank_unposted_list.html',{'un_posted_lorry_to_tank': un_posted_lorry_to_tank})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def lorry_to_tank_update(request, pk):
    lorry_to_tank = get_object_or_404(LorryToTank, pk=pk)
    if request.method == 'POST':
        form = LorryToTankForm(request.POST, instance=lorry_to_tank)
    else:
        form = LorryToTankForm(instance=lorry_to_tank)
    return update_lorry_to_tank_form(request, form,'lorry_stock_to_tank/includes/partial_lorry_to_tank_update.html')



def lorry_to_tank_delete(request, pk):
    lorry_to_tank = get_object_or_404(LorryToTank, pk=pk)
    data = dict()
    if request.method == 'POST':
        lorry_to_tank.delete()
        data['form_is_valid'] = True
        un_posted_lorry_to_tank = LorryToTank.objects.filter(status=1)
        data['html_lorry_to_tank'] = render_to_string(
            'lorry_stock_to_tank/includes/partial_lorry_to_tank_unposted_list.html', {
                'un_posted_lorry_to_tank': un_posted_lorry_to_tank
            })
        data['total_lorry_to_tank'] = LorryToTank.objects.count()
    else:
        context = {'lorry_to_tank': lorry_to_tank}
        data['html_form'] = render_to_string('lorry_stock_to_tank/includes/partial_lorry_to_tank_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# TANK TO LORRY
class TankStockTransaction(TemplateView):
    template_name = 'tank_stock_to_lorry/tank_stock_transaction_home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['un_posted_tank_to_lorry'] = TankToLorry.objects.filter(status=1)
        return context


def save_tank_to_lorry_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            un_posted_tank_to_lorry = TankToLorry.objects.filter(status=1)
            data['html_tank_to_lorry'] = render_to_string('tank_stock_to_lorry/includes/partial_tank_to_lorry_unposted_list.html', {'un_posted_tank_to_lorry': un_posted_tank_to_lorry })
            # data['total_drawing'] = Drawing.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tank_to_lorry_create(request):
    if request.method == 'POST':
        form = TankToLorryForm(request.POST)
    else:
        form = TankToLorryForm()
    return save_tank_to_lorry_form(request, form,'tank_stock_to_lorry/includes/partial_tank_to_lorry_create.html')


def load_chamber(request):
    lorry_id = request.GET.get('lorry_id')
    chambers = LorryChamber.objects.filter(lorry_id=lorry_id).order_by('chamber')
    return render(request, 'tank_stock_to_lorry/includes/chamber_dropdown_list_options.html', {'chambers': chambers})


def show_all_posted_lorry_to_tank(request):
    posted_lorry_to_tank = LorryToTank.objects.filter(status=2)
    return render(request, 'lorry_stock_to_tank/includes/partial_lorry_to_tank_posted_list.html', {'posted_lorry_to_tank': posted_lorry_to_tank})


def show_all_posted_tank_to_lorry(request):
    posted_tank_to_lorry = TankToLorry.objects.filter(status=2)
    return render(request, 'tank_stock_to_lorry/includes/partial_tank_to_lorry_posted_list.html', {'posted_tank_to_lorry': posted_tank_to_lorry})


def update_tank_to_lorry_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()

            duty_count = Duty.objects.count()
            if duty_count > 1:
                latest_duty = Duty.objects.latest('id')
                prev_duty = Duty.objects.filter(id__lt=latest_duty.id).order_by('-id').first()
            else:
                prev_duty = Duty.objects.latest('id')

            id = form.instance.id
            tank = form.cleaned_data['tank']
            lorry = form.cleaned_data['lorry']
            chamber = form.cleaned_data['chamber']
            product = form.cleaned_data['product']
            duty = form.cleaned_data['duty']
            lorry_shifted_stock = form.cleaned_data['lorry_shifted_stock']
            lorry_final_stock = form.cleaned_data['lorry_final_stock']
            lorry_final_dip = form.cleaned_data['lorry_final_dip']
            tank_previous_stock = form.cleaned_data['tank_previous_stock']
            tank_final_dip = form.cleaned_data['tank_final_dip']
            tank_final_stock = form.cleaned_data['tank_final_stock']
            tank_ledger = TanksLedger(duty_id=duty.id, tank_id=tank.id, opening_stock=0,
                                      received_stock=0, meter_sale=lorry_shifted_stock, test_reading=0, dip=tank_final_dip,
                                      physical_stock=0, trans_type='TL', trans_number=id)
            tank_ledger.save()
            tl_current_duty_count = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id)
            if tl_current_duty_count.count() == 1:
                tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id)[0]
                tl_obj = TanksLedger.objects.get(id=tl.id)
                ps = 0
                try:
                    physical_stock = TanksLedger.objects.filter(duty_id=prev_duty.id, tank_id=tank.id).first()
                    ps = physical_stock.physical_stock
                    tl_obj.physical_stock = ps - lorry_shifted_stock
                    tl_obj.save()
                except:
                    ps = 0
                    tl_obj.physical_stock = ps - lorry_shifted_stock
                    tl_obj.save()
            else:
                tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=tank.id).first()
                tl_obj = TanksLedger.objects.get(id=tl.id)
                tl_obj.physical_stock = tl_obj.physical_stock - lorry_shifted_stock
                tl_obj.save()
            # lorry stock update
            ls = LorryStock.objects.filter(lorry_id=lorry.id, chamber_id=chamber.id, product_id=product.id)
            if ls.count() > 0:
                ls_object = LorryStock.objects.get(id=ls[0].id)
                ls_object.stock = ls_object.stock + lorry_shifted_stock
                ls_object.save()
            else:
                ls_object = LorryStock(lorry_id=lorry.id, chamber_id=chamber.id, product_id=product.id, stock=lorry_final_stock)
                ls_object.save()
            # lorry stock history
            tank_stock_history = LorryStockHistory(duty_id=duty.id, lorry_id=lorry.id, chamber_id=chamber.id,
                                                    product_id=product.id, voucher_type='TL', voucher_number=id,
                                                    dip=lorry_final_dip, stock_debit=lorry_shifted_stock,
                                                    stock_credit=0, comment=f'{product.product_name} TL')

            tank_stock_history.save()
            form.instance.status = 2
            form.save()
            # general journal entry end
            data['form_is_valid'] = True
            data['total_tank_to_lorry'] = TankToLorry.objects.count()
            un_posted_tank_to_lorry = TankToLorry.objects.filter(status=1)
            data['html_tank_to_lorry'] = render_to_string('tank_stock_to_lorry/includes/partial_tank_to_lorry_unposted_list.html',{'un_posted_tank_to_lorry': un_posted_tank_to_lorry})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tank_to_lorry_update(request, pk):
    tank_to_lorry = get_object_or_404(TankToLorry, pk=pk)
    if request.method == 'POST':
        form = TankToLorryForm(request.POST, instance=tank_to_lorry)
    else:
        form = TankToLorryForm(instance=tank_to_lorry)
    return update_tank_to_lorry_form(request, form,'tank_stock_to_lorry/includes/partial_tank_to_lorry_update.html')


def tank_to_lorry_delete(request, pk):
    tank_to_lorry = get_object_or_404(TankToLorry, pk=pk)
    data = dict()
    if request.method == 'POST':
        tank_to_lorry.delete()
        data['form_is_valid'] = True
        un_posted_tank_to_lorry = TankToLorry.objects.filter(status=1)
        data['html_tank_to_lorry'] = render_to_string(
            'tank_stock_to_lorry/includes/partial_tank_to_lorry_unposted_list.html', {
                'un_posted_tank_to_lorry': un_posted_tank_to_lorry
            })
        data['total_tank_to_lorry'] = TankToLorry.objects.count()
    else:
        context = {'tank_to_lorry': tank_to_lorry}
        data['html_form'] = render_to_string('tank_stock_to_lorry/includes/partial_tank_to_lorry_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class GetLorryStock(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        lorry = request.GET.get('lorry')
        chamber = request.GET.get('chamber')
        product = request.GET.get('product')
        lorry_stock = 0
        if lorry and chamber and product is not None:
            try:
                lorry_stock = LorryStock.objects.filter(lorry_id=lorry, chamber_id=chamber, product_id=product)[0].stock
            except:
                lorry_stock = 0

            data['lorry_stock'] = lorry_stock

        return JsonResponse(data)



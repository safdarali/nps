from decimal import Decimal

from django.db import models

from duties.models import Duty
from nps.models import ModelWithTimeStamp
from products.models import Product
from tank_lorries.models import Lorry, LorryChamber
from tanks.models import Tank


class LorryStockHistory(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',
                             db_index=True)
    lorry = models.ForeignKey(Lorry, db_index=True, on_delete=models.CASCADE, verbose_name='Lorry',null=True, blank=True, default=None)
    chamber = models.ForeignKey(LorryChamber, db_index=True, on_delete=models.CASCADE, verbose_name='Chamber',null=True, blank=True, default=None)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, related_name='Product', db_index=True)
    voucher_type = models.CharField(max_length=15, db_index=True, verbose_name='Voucher type', null=True, blank=True)
    voucher_number = models.CharField(max_length=15, db_index=True, verbose_name='Voucher number', null=True,blank=True)
    dip = models.IntegerField(verbose_name='Dip', null=False, blank=False, default=0)
    stock_debit = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Stock debit',default=Decimal('0.000'))
    stock_credit = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Stock credit',default=Decimal('0.000'))
    comment = models.CharField(max_length=200, verbose_name='Comment', null=True, blank=True)

    class Meta:
        ordering = ['duty']

    def __str__(self):
        return str(self.duty.date)


class LorryStock(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',
                             db_index=True)
    lorry = models.ForeignKey(Lorry, db_index=True, on_delete=models.CASCADE, verbose_name='Lorry', null=True,blank=True, default=None, related_name='lorry_stock_lorry')
    chamber = models.ForeignKey(LorryChamber, db_index=True, on_delete=models.CASCADE, verbose_name='Chamber',null=True, blank=True, default=None, related_name='lorry_stock_chamber')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, db_index=True, related_name='lorry_stock_product')
    stock = models.DecimalField(verbose_name='Stock', decimal_places=1, max_digits=10, null=False,blank=False, default=Decimal('0.0'))

    def __str__(self):
        return self.product.product_name

STATUS = (
    (1, 'not posted'),
    (2, 'posted')
)


class LorryToTank(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Duty')
    lorry = models.ForeignKey(Lorry, db_index=True, on_delete=models.CASCADE, verbose_name='Lorry', null=True,blank=True, default=None, related_name='lorry_to_tank_lorry')
    chamber = models.ForeignKey(LorryChamber, db_index=True, on_delete=models.CASCADE, verbose_name='Chamber',null=True, blank=True, default=None, related_name='lorry_to_tank_chamber')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, related_name='lorry_to_tank_product', db_index=True, verbose_name='Product')
    lorry_previous_dip = models.IntegerField(verbose_name='Previous dip', null=False, blank=False, default=0)
    lorry_previous_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Previous stock',default=Decimal('0.000'))
    lorry_final_dip = models.IntegerField(verbose_name='Final dip', null=False, blank=False, default=0)
    lorry_final_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Final stock',default=Decimal('0.000'))
    lorry_shifted_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Shifted stock',default=Decimal('0.000'))
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Tank',db_index=True)
    tank_previous_dip = models.IntegerField(verbose_name='Previous dip', null=False, blank=False, default=0)
    tank_previous_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Previous stock',default=Decimal('0.000'))
    tank_final_dip = models.IntegerField(verbose_name='Final dip', null=False, blank=False, default=0)
    tank_final_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Final stock',default=Decimal('0.000'))
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1)

    def __str__(self):
        return str(self.duty.date)


class TankToLorry(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Duty')
    lorry = models.ForeignKey(Lorry, db_index=True, on_delete=models.CASCADE, verbose_name='Lorry', null=True,blank=True, default=None, related_name='tank_to_lorry_lorry')
    chamber = models.ForeignKey(LorryChamber, db_index=True, on_delete=models.CASCADE, verbose_name='Chamber',null=True, blank=True, default=None, related_name='tank_to_lorry_chamber')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, related_name='tank_to_lorry_product', db_index=True, verbose_name='Product')
    lorry_previous_dip = models.IntegerField(verbose_name='Previous dip', null=False, blank=False, default=0)
    lorry_previous_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Previous stock',default=Decimal('0.000'))
    lorry_final_dip = models.IntegerField(verbose_name='Final dip', null=False, blank=False, default=0)
    lorry_final_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Final stock',default=Decimal('0.000'))
    lorry_shifted_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Shifted stock',default=Decimal('0.000'))
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Tank', related_name='tank_to_lorry_tank',db_index=True)
    tank_previous_dip = models.IntegerField(verbose_name='Previous dip', null=False, blank=False, default=0)
    tank_previous_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Previous stock',default=Decimal('0.000'))
    tank_final_dip = models.IntegerField(verbose_name='Final dip', null=False, blank=False, default=0)
    tank_final_stock = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Final stock',default=Decimal('0.000'))
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1)

    def __str__(self):
        return str(self.duty.date)



    

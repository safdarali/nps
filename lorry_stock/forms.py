from django import forms

from duties.models import Duty
from lorry_stock.models import LorryToTank, TankToLorry
from products.models import Product
from tank_lorries.models import Lorry, LorryChamber


def get_latest_duty_date():
    ld = Duty.objects.latest('date')
    return ld

class LorryToTankForm(forms.ModelForm):
    class Meta:
        model = LorryToTank
        fields = '__all__'
        exclude = ('status',)

    def __init__(self, *args, **kwargs):
        super(LorryToTankForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['product'].empty_label = None
        self.fields['product'].queryset = Product.objects.filter(product_type=4)
        self.fields['chamber'].queryset = LorryChamber.objects.none()

        if 'lorry' in self.data:
            try:
                lorry_id = int(self.data.get('lorry'))
                self.fields['chamber'].queryset = LorryChamber.objects.filter(lorry_id=lorry_id).order_by('chamber')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['chamber'].queryset = self.instance.lorry.lorrychamber_set.order_by('chamber')



class TankToLorryForm(forms.ModelForm):
    class Meta:
        model = TankToLorry
        fields = '__all__'
        exclude = ('status',)

    def __init__(self, *args, **kwargs):
        super(TankToLorryForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['product'].empty_label = None
        self.fields['product'].queryset = Product.objects.filter(product_type=4)
        self.fields['chamber'].queryset = LorryChamber.objects.none()

        if 'lorry' in self.data:
            try:
                lorry_id = int(self.data.get('lorry'))
                self.fields['chamber'].queryset = LorryChamber.objects.filter(lorry_id=lorry_id).order_by('chamber')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['chamber'].queryset = self.instance.lorry.lorrychamber_set.order_by('chamber')


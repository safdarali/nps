from django.urls import path

from lorry_stock.views import LorryStockTransaction, lorry_to_tank_create, load_chamber, lorry_to_tank_update, \
   lorry_to_tank_delete, show_all_posted_lorry_to_tank, TankStockTransaction, tank_to_lorry_create, \
   tank_to_lorry_update, tank_to_lorry_delete, show_all_posted_tank_to_lorry, GetLorryStock

urlpatterns = [
   path('lorry_stock_transaction', LorryStockTransaction.as_view(), name='lorry_stock_transaction_home'),
   path('lorry_to_tank_create', lorry_to_tank_create, name='lorry_to_tank_create'),
   path('load_chambers', load_chamber, name='load_chambers'),
   path('lorry_to_tank_update/<int:pk>/update', lorry_to_tank_update, name='lorry_to_tank_update'),
   path('lorry_to_tank_delete/<int:pk>/delete', lorry_to_tank_delete, name='lorry_to_tank_delete'),
   path('show_all_posted_lorry_to_tank', show_all_posted_lorry_to_tank, name='show_all_posted_lorry_to_tank'),

   path('tank_stock_transaction', TankStockTransaction.as_view(), name='tank_stock_transaction_home'),
   path('get_lorry_stock', GetLorryStock.as_view(), name='get_lorry_stock'),
   path('tank_to_lorry_create', tank_to_lorry_create, name='tank_to_lorry_create'),
   path('load_chambers', load_chamber, name='load_chambers'),
   path('tank_to_lorry_update/<int:pk>/update', tank_to_lorry_update, name='tank_to_lorry_update'),
   path('tank_to_lorry_delete/<int:pk>/delete', tank_to_lorry_delete, name='tank_to_lorry_delete'),
   path('show_all_posted_tank_to_lorry', show_all_posted_tank_to_lorry, name='show_all_posted_tank_to_lorry'),

]
from django.apps import AppConfig


class LorryStockConfig(AppConfig):
    name = 'lorry_stock'

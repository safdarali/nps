
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from lorry_stock.models import LorryStockHistory, LorryStock, LorryToTank, TankToLorry


class LorryStockHistoryResource(resources.ModelResource):
    class Meta:
        model = LorryStockHistory


class LorryStockHistoryAdmin(ImportExportModelAdmin):
    resource_class = LorryStockHistoryResource
    list_display = ['duty','lorry', 'chamber', 'product', 'voucher_type', 'voucher_number','dip','stock_debit','stock_credit','comment']
    list_filter = ['lorry','product','voucher_type']


admin.site.register(LorryStockHistory, LorryStockHistoryAdmin)


class LorryStockResource(resources.ModelResource):
    class Meta:
        model = LorryStock


class LorryStockAdmin(ImportExportModelAdmin):
    resource_class = LorryStockResource
    list_display = ['lorry', 'chamber','product','stock']


admin.site.register(LorryStock, LorryStockAdmin)


class LorryToTankResource(resources.ModelResource):
    class Meta:
        model = LorryToTank


class LorryToTankAdmin(ImportExportModelAdmin):
    resource_class = LorryToTankResource
    list_display = ['duty', 'lorry', 'chamber', 'product', 'lorry_previous_dip','lorry_previous_stock','lorry_final_dip', 'lorry_final_stock','lorry_shifted_stock','tank', 'tank_previous_dip', 'tank_previous_stock', 'tank_final_dip', 'tank_final_stock']


admin.site.register(LorryToTank, LorryToTankAdmin)

# TANK TO LORRY


class TankToLorryResource(resources.ModelResource):
    class Meta:
        model = TankToLorry


class TankToLorryAdmin(ImportExportModelAdmin):
    resource_class = TankToLorryResource
    list_display = ['duty', 'lorry', 'chamber', 'product', 'lorry_previous_dip','lorry_previous_stock','lorry_final_dip', 'lorry_final_stock','lorry_shifted_stock','tank', 'tank_previous_dip', 'tank_previous_stock', 'tank_final_dip', 'tank_final_stock']


admin.site.register(TankToLorry, TankToLorryAdmin)

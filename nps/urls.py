"""nps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from allauth.account.views import LoginView
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


from accounts.views import login_after_password_change, privileges_required
from nps.views import login_success

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/password/change/', login_after_password_change, name='account_change_password'),
    path('privileges_required/', privileges_required, name="privileges_required"),
    path('accounts/', include('allauth.urls')),
    path('', include('dashboard.urls')),
    path('', include('products.urls')),
    path('', include('purchases.urls')),
    # path('', include('tanks.urls')),
    # path('', include('vehicles.urls')),
    path('', include('meters.urls')),
    path('', include('rent_received.urls')),
    path('', include('chart_of_accounts.urls')),
    path('', include('tank_lorries.urls')),
    path('', include('duties.urls')),
    path('', include('sales.urls')),
    path('', include('cash.urls')),
    path('', include('reports.urls')),
    # path('', include('customers.urls')),
    path('', include('expenses.urls')),
    path('', include('taxes.urls')),
    path('', include('discounts.urls')),
    path('', include('customer_limits.urls')),
    path('', include('bank_to_bank_transfer.urls')),
    path('', include('drawing.urls')),
    path('', include('bank_expense.urls')),
    path('', include('milli_short_recovery.urls')),
    path('', include('lorry_stock.urls')),
    path('login_success/', login_success, name='login_success'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

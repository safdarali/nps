from django.shortcuts import redirect


def login_success(request):
    if request.user.role == 2:

        return redirect("customer_home")
    else:
        return redirect("dashboard")
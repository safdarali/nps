from django.contrib import messages
from functools import wraps
from django.shortcuts import redirect


def permission_required(permissions):
    def inner_render(fn):
        @wraps(fn)
        def wrapped(request, *args, **kwargs):
            if request.user.is_authenticated and request.user.has_perm(permissions):
                return fn(request, *args, **kwargs)
            else:
                return redirect('privileges_required')
        return wrapped
    return inner_render

from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Discount


class DiscountResource(resources.ModelResource):
    class Meta:
        model = Discount


class DiscountAdmin(ImportExportModelAdmin):
    resource_class = DiscountResource
    list_display = ('customer', 'product', 'discount')


admin.site.register(Discount, DiscountAdmin)
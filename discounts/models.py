from decimal import Decimal

from django.db import models

# Create your models here.
from chart_of_accounts.models import ChartOfAccount
from nps.models import ModelWithTimeStamp
from products.models import Product


class Discount(ModelWithTimeStamp):
    customer = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=False, blank=False, related_name='Customer')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True,default=None)
    discount = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False,default=Decimal('0.000'),verbose_name='Discount/L')

    class Meta:
        unique_together = (('customer', 'product'),)

    def __str__(self):
        return self.customer.account_name
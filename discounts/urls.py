from django.urls import path
from .views import discount_list, discount_create, discount_update, discount_delete

urlpatterns = [
    path('discounts/', discount_list, name='discount_list'),
    path('discounts/create', discount_create, name='discount_create'),
    path('discounts/<int:pk>/update', discount_update, name='discount_update'),
    path('discounts/<int:pk>/delete', discount_delete, name='discount_delete'),
]
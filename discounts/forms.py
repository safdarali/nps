from django import forms

from chart_of_accounts.models import ChartOfAccount
from products.models import Product
from .models import Discount


class DiscountForm(forms.ModelForm):
    class Meta:
        model = Discount
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DiscountForm, self).__init__(*args, **kwargs)
        self.fields['customer'].empty_label = 'Select an agency customer'
        self.fields['customer'].queryset = ChartOfAccount.objects.filter(account_type=7)
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type__in=[4, 5])
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)

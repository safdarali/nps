from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import Discount
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import DiscountForm


def discount_list(request):
    discounts = Discount.objects.all()
    return render(request, 'discounts/discount_list.html', {'discounts': discounts})


def save_discount_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            discounts = Discount.objects.all()
            data['html_discount_list'] = render_to_string('discounts/includes/partial_discount_list.html', {'discounts': discounts})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('discounts.add_discount')
def discount_create(request):
    if request.method == 'POST':
        form = DiscountForm(request.POST)
    else:
        form = DiscountForm()
    return save_discount_form(request, form, 'discounts/includes/partial_discount_create.html')


@permission_required('discounts.change_discount')
def discount_update(request, pk):
    discount = get_object_or_404(Discount, pk=pk)
    if request.method == 'POST':
        form = DiscountForm(request.POST, instance=discount)
    else:
        form = DiscountForm(instance=discount)
    return save_discount_form(request, form, 'discounts/includes/partial_discount_update.html')


@permission_required('discounts.delete_discount')
def discount_delete(request, pk):
    discount = get_object_or_404(Discount, pk=pk)
    data = dict()
    if request.method == 'POST':
        discount.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        discounts = Discount.objects.all()
        data['html_discount_list'] = render_to_string('discounts/includes/partial_discount_list.html', {
            'discounts': discounts
        })
    else:
        context = {'discount': discount}
        data['html_form'] = render_to_string('discounts/includes/partial_discount_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import Tax
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import TaxForm


def tax_list(request):
    tax = Tax.objects.first()
    return render(request, 'taxes/tax_list.html', {'tax': tax})


def save_tax_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            tax = Tax.objects.first()
            data['html_tax_list'] = render_to_string('taxes/includes/partial_tax_list.html', {'tax': tax})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('taxes.add_tax')
def tax_create(request):
    if request.method == 'POST':
        form = TaxForm(request.POST)
    else:
        form = TaxForm()
    return save_tax_form(request, form, 'taxes/includes/partial_tax_create.html')


@permission_required('taxes.change_tax')
def tax_update(request, pk):
    tax = get_object_or_404(Tax, pk=pk)
    if request.method == 'POST':
        form = TaxForm(request.POST, instance=tax)
    else:
        form = TaxForm(instance=tax)
    return save_tax_form(request, form, 'taxes/includes/partial_tax_update.html')


@permission_required('taxes.delete_tax')
def tax_delete(request, pk):
    tax = get_object_or_404(Tax, pk=pk)
    data = dict()
    if request.method == 'POST':
        tax.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        tax = Tax.objects.first()
        data['html_tax_list'] = render_to_string('taxes/includes/partial_tax_list.html', {
            'tax': tax
        })
    else:
        context = {'tax': tax}
        data['html_form'] = render_to_string('taxes/includes/partial_tax_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

from decimal import Decimal

from django.db import models

# Create your models here.
from nps.models import ModelWithTimeStamp


class Tax(ModelWithTimeStamp):
    sales_tax = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Sales tax')
    extra_tax = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False,default=Decimal('0.000'),verbose_name='Extra tax')
    further_tax = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False,default=Decimal('0.000'),verbose_name='Further tax')
    sum_taxes = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Sum of taxes')
    cc_tax = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Club card tax')
    withholding_tax = models.DecimalField(max_digits=10, decimal_places=5, null=False, blank=False,default=Decimal('0.00000'),verbose_name='Withholding tax')
    extra_swaping_return_tax = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Extra swaping return tax')

    def __str__(self):
        return str(self.sum_taxes)



from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from taxes.models import Tax


class TaxResource(resources.ModelResource):
    class Meta:
        model = Tax


class TaxAdmin(ImportExportModelAdmin):
    resource_class = TaxResource
    list_display = ('sum_taxes', 'sales_tax', 'extra_tax', 'further_tax', 'withholding_tax', 'further_tax')


admin.site.register(Tax, TaxAdmin)

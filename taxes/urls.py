from django.urls import path
from .views import tax_list, tax_create, tax_update, tax_delete

urlpatterns = [
    path('taxes/', tax_list, name='tax_list'),
    path('taxes/create', tax_create, name='tax_create'),
    path('taxes/<int:pk>/update', tax_update, name='tax_update'),
    path('taxes/<int:pk>/delete', tax_delete, name='tax_delete'),
]
from decimal import Decimal

from django.db import models
from django.utils import timezone

from chart_of_accounts.models import ChartOfAccount
from nps.models import ModelWithTimeStamp


class MilliShortRecovery(ModelWithTimeStamp):
    company_name = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, db_index=True)
    from_date = models.DateTimeField(default=timezone.now, null=True, blank=True, verbose_name='From Date')
    to_date = models.DateTimeField(default=timezone.now, null=True, blank=True, verbose_name='To Date')
    amount = models.DecimalField(max_digits=15, decimal_places=3, verbose_name='Amount')
    percent = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Percent')
    percentage = models.DecimalField(max_digits=15, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Percentage')

    def __str__(self):
        return self.company_name.account_name
from django.urls import path

from .views import milli_short_recovery_list, milli_short_recovery_get, milli_short_recovery_post

urlpatterns = [
    path('milli_short_recovery/milli_short_recovery_list', milli_short_recovery_list, name='milli_short_recovery_list'),
    path('milli_short_recovery/<int:pk>/<int:product_id>/milli_short_recovery_get', milli_short_recovery_get, name='milli_short_recovery_get'),
    path('milli_short_recovery/milli_short_recovery_post', milli_short_recovery_post, name='milli_short_recovery_post'),

]

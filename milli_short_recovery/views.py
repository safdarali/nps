from datetime import datetime

from django.db.models import Sum, F, Case, Value, DecimalField, When, CharField, Count
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.template.loader import render_to_string
from django.utils.dateparse import parse_date
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from pendulum import parse

from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty
from general_journal.models import GeneralJournal
from milli_short_recovery.models import MilliShortRecovery
from purchases.models import FuelPurchases, FuelPurchasem


def get_latest_duty_id():
    ld = Duty.objects.latest('id').id
    return ld


def milli_short_recovery_list(request):
    milli_short_list = FuelPurchasem.objects.filter(fuelpurchases__milli_short__gt=0, milli_short_recovery_status=1).values(
        'supplier__account_name', 'supplier_id', 'fuelpurchases__product__product_name','fuelpurchases__product_id').annotate(total_nos=Count('fuelpurchases__amount'))
    milli_short_recovery = MilliShortRecovery.objects.all()
    total_milli_shorts = milli_short_recovery.count()
    return render(request, 'milli_short_recovery/milli_short_list.html',
                  {'milli_short_list': milli_short_list, 'milli_short_recovery': milli_short_recovery,
                   'total_milli_shorts': total_milli_shorts})


def milli_short_recovery_get(request, pk, product_id):
    data = dict()
    milli_short_recovery_detail = FuelPurchases.objects.filter(milli_short__gt=0, fuelpurchasem__milli_short_recovery_status=1, fuelpurchasem__supplier_id=pk, product_id=product_id).values(
        'fuelpurchasem__duty__date','fuelpurchasem__supplier__account_name', 'fuelpurchasem__supplier_id', 'product__product_name', 'milli_short','amount', 'fuelpurchasem_id'). \
        annotate(unit_price=F('amount') / F('purchased_stock'), litre = Case(
        When(purchased_stock=8000, then=F('milli_short') * 5.5),
        When(purchased_stock=16000, then=F('milli_short') * 11.0),
        default=Value(0.000),
        output_field=DecimalField()
    ), total_amount=F('litre') * (F('amount')/F('purchased_stock')))

    account = ChartOfAccount.objects.get(id=pk)

    data['milli_short_recovery_detail'] = render_to_string(
        'milli_short_recovery/includes/partial_milli_short_update.html',
        {'milli_short_recovery_detail': milli_short_recovery_detail, 'account': account})
    return JsonResponse(data)


@csrf_exempt
def milli_short_recovery_post(request):
    data = dict()
    invoices_ids = request.POST.getlist('invoices_ids')
    company_id = request.POST.get('company_id')
    company = ChartOfAccount.objects.get(id=company_id)
    sum_total_amount = request.POST.get('sum_total_amount')
    percent = request.POST.get('percent')
    percentage = request.POST.get('percentage')
    account = ChartOfAccount.objects.get(id=40130001).account_name
    if len(invoices_ids) == 1:
        from_date = FuelPurchasem.objects.get(id=invoices_ids[0]).duty.date
        to_date = from_date
    elif len(invoices_ids) > 1:
        from_date = FuelPurchasem.objects.get(id=invoices_ids[0]).duty.date
        to_date = FuelPurchasem.objects.get(id=invoices_ids[-1]).duty.date
    milli_short_recovery = MilliShortRecovery(company_name=company, amount=sum_total_amount,
                                                                       percent=percent, percentage=percentage,
                                                                       from_date=from_date, to_date=to_date)
    milli_short_recovery.save()

    general_journal_1 = GeneralJournal(chartofaccount_id=company_id, duty_id=get_latest_duty_id(), voucher_type='MSR',
                                       voucher_number=milli_short_recovery.id,
                                       debit=percentage, credit=0, naration=f'To {account}')
    general_journal_1.save()
    general_journal_2 = GeneralJournal(chartofaccount_id=40130001, duty_id=get_latest_duty_id(), voucher_type='MSR',
                                       voucher_number=milli_short_recovery.id,
                                       debit=0, credit=percentage, naration=f'By {company.account_name}')
    general_journal_2.save()
    FuelPurchasem.objects.filter(id__in=invoices_ids).update(milli_short_recovery_status=2)
    return JsonResponse(data)

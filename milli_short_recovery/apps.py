from django.apps import AppConfig


class MilliShortRecoveryConfig(AppConfig):
    name = 'milli_short_recovery'

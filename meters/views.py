from django.contrib.auth.decorators import permission_required
from django.shortcuts import render, get_object_or_404
from .models import Meter
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import MeterForm


def meter_list(request):
    meters = Meter.objects.all()
    return render(request, 'meters/meter_list.html', {'meters': meters})

def save_meter_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            meters = Meter.objects.all()
            data['html_meter_list'] = render_to_string('meters/includes/partial_meter_list.html', {
                'meters': meters
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('meters.add_meter')
def meter_create(request):
    if request.method == 'POST':
        form = MeterForm(request.POST)
    else:
        form = MeterForm()
    return save_meter_form(request, form, 'meters/includes/partial_meter_create.html')


@permission_required('meters.change_meter')
def meter_update(request, pk):
    meter = get_object_or_404(Meter, pk=pk)
    if request.method == 'POST':
        form = MeterForm(request.POST, instance=meter)
    else:
        form = MeterForm(instance=meter)
    return save_meter_form(request, form, 'meters/includes/partial_meter_update.html')


@permission_required('meters.delete_meter')
def meter_delete(request, pk):
    meter = get_object_or_404(Meter, pk=pk)
    data = dict()
    if request.method == 'POST':
        meter.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        meters = Meter.objects.all()
        data['html_meter_list'] = render_to_string('meters/includes/partial_meter_list.html', {
            'meters': meters
        })
    else:
        context = {'meter': meter}
        data['html_form'] = render_to_string('meters/includes/partial_meter_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

from django import forms

from products.models import Product
from .models import Meter


class MeterForm(forms.ModelForm):
    class Meta:
        model = Meter
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MeterForm, self).__init__(*args, **kwargs)
        self.fields['tank'].empty_label = 'Select a tank'
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type__in=[4, 5])



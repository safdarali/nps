# Generated by Django 3.0.7 on 2020-09-06 10:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('products', '0001_initial'),
        ('tanks', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Meter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('meter_name', models.CharField(max_length=50)),
                ('meter_description', models.TextField()),
                ('product', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='products.Product', verbose_name='Product')),
                ('tank', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tanks.Tank', verbose_name='Tank')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

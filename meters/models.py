from django.db import models

from nps.models import ModelWithTimeStamp
from products.models import Product
from tanks.models import Tank


class Meter(ModelWithTimeStamp):
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, verbose_name='Tank')
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, verbose_name='Product', null=True, blank=True, default=None)
    meter_name = models.CharField(max_length=50)
    meter_description = models.TextField()

    def __str__(self):
        return self.meter_name


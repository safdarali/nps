from django.urls import path
from .views import meter_list, meter_create, meter_update, meter_delete

urlpatterns = [
    path('meters/', meter_list, name='meter_list'),
    path('meters/create', meter_create, name='meter_create'),
    path('meters/<int:pk>/update', meter_update, name='meter_update'),
    path('meters/<int:pk>/delete', meter_delete, name='meter_delete'),
]
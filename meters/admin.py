from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from meters.models import Meter


class MeterResource(resources.ModelResource):
    class Meta:
        model = Meter


class MeterAdmin(ImportExportModelAdmin):
    resource_class = MeterResource
    list_display = ('meter_name',
              'tank',
              'product',
              'meter_description')
    search_fields = ('meter_name',)


admin.site.register(Meter, MeterAdmin)

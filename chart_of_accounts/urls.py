from django.urls import path
from .views import main_account_list, main_account_create, main_account_update, main_account_delete, sub_account_list, sub_account_create, sub_account_update, sub_account_delete, \
    chart_of_account_list, chart_of_account_create, chart_of_account_update, chart_of_account_delete

urlpatterns = [
    # main accounts
    path('main_accounts/', main_account_list, name='main_account_list'),
    path('main_accounts/create', main_account_create, name='main_account_create'),
    path('main_accounts/<int:pk>/update', main_account_update, name='main_account_update'),
    path('main_accounts/<int:pk>/delete', main_account_delete, name='main_account_delete'),
    # sub accounts
    path('sub_accounts/', sub_account_list, name='sub_account_list'),
    path('sub_accounts/create', sub_account_create, name='sub_account_create'),
    path('sub_accounts/<int:pk>/update', sub_account_update, name='sub_account_update'),
    path('sub_accounts/<int:pk>/delete', sub_account_delete, name='sub_account_delete'),
    # chart of  accounts
    path('chart_of_accounts/', chart_of_account_list, name='chart_of_account_list'),
    path('chart_of_accounts/create', chart_of_account_create, name='chart_of_account_create'),
    path('chart_of_accounts/<int:pk>/update', chart_of_account_update, name='chart_of_account_update'),
    path('chart_of_accounts/<int:pk>/delete', chart_of_account_delete, name='chart_of_account_delete'),
]

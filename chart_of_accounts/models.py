from decimal import Decimal

from django.conf import settings
from django.db import models

from nps.models import ModelWithTimeStamp
from products.models import Product
from .choices import *


class MainAccount(models.Model):
    main_account_name = models.SmallIntegerField(verbose_name='Main account name', choices=main_accouts, unique=True)

    def __str__(self):
        return self.get_main_account_name_display()


class SubAccount(models.Model):
    mainaccount = models.ForeignKey(MainAccount, on_delete=models.CASCADE, verbose_name='Main account', null=True,
                                    blank=True, default=None)
    sub_account_name = models.SmallIntegerField(verbose_name='Sub account name', choices=sub_accounts, unique=True)

    def __str__(self):
        return str(self.get_sub_account_name_display())


class ChartOfAccount(ModelWithTimeStamp):
    SUPPLIER = 1
    PERSONAL = 2
    PRODUCT = 3
    REAL = 4
    EMPLOYEE = 5
    BANK = 6
    AGENCY_CUSTOMER = 7
    FUEL_PRODUCT = 8
    EXPENSE = 9
    COUNTER_SALES = 10
    LUBES_PURCHASES = 11
    LUBES_SALES = 12
    UNIFORM_PURCHASES = 13
    INCOME = 14
    VEHICLE = 15
    CLUB_CARD_CUSTOMER = 16
    CASH = 17
    OWNER = 18

    AC_TYPE = (
        ('', 'Select account type'),
        (SUPPLIER, 'Supplier'),
        (PERSONAL, 'Personal'),
        (EMPLOYEE, 'Employee'),
        (BANK, 'Bank'),
        (PRODUCT, 'Product'),
        (AGENCY_CUSTOMER, 'Agency customer'),
        (FUEL_PRODUCT, 'Fuel'),
        (REAL, 'Real'),
        (EXPENSE, 'Expense'),
        (COUNTER_SALES, 'Counter Sales'),
        (LUBES_PURCHASES, 'Lubes purchases'),
        (LUBES_SALES, 'Lubes sales'),
        (UNIFORM_PURCHASES, 'Uniform purchases'),
        (INCOME, 'Income'),
        (VEHICLE, 'Vehicle'),
        (CLUB_CARD_CUSTOMER, 'Club card customer'),
        (CASH, 'Cash'),
        (OWNER, 'Owner'),

    )

    BY_CARD_PAYER = 1
    CARD_DEPOSITOR = 2
    BY_BANK_PAYER = 3
    HYBRID = 4

    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Product',null=True, blank=True, default=None)
    account_type = models.SmallIntegerField(choices=AC_TYPE, null=True, blank=True, verbose_name="Account type")
    subaccount = models.ForeignKey(SubAccount, on_delete=models.CASCADE, verbose_name='Sub account', null=True)
    account_name = models.CharField(max_length=70, verbose_name='Account name', unique=True)
    address = models.TextField(verbose_name='Address', null=True, blank=True)
    phone_number = models.CharField(max_length=15, verbose_name='Phone number', null=True, blank=True)
    is_account_active = models.BooleanField(verbose_name='is account active?', choices=is_account_active, default=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True, default=None)

    def __str__(self):
        return self.account_name

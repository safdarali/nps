from django import forms
from .models import MainAccount, SubAccount, ChartOfAccount
from django.utils.safestring import mark_safe


class MainAccountForm(forms.ModelForm):
    class Meta:
        model = MainAccount
        fields = '__all__'


class SubAccountForm(forms.ModelForm):
    class Meta:
        model = SubAccount
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SubAccountForm, self).__init__(*args, **kwargs)
        self.fields['mainaccount'].empty_label = 'Select a main account'


class ChartOfAccountForm(forms.ModelForm):
    class Meta:
        model = ChartOfAccount
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ChartOfAccountForm, self).__init__(*args, **kwargs)
        self.fields['subaccount'].empty_label = 'Select a sub account'
        self.fields['address'].widget.attrs['rows'] = 3

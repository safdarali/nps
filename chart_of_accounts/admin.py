from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import MainAccount, SubAccount, ChartOfAccount


class MainAccountResource(resources.ModelResource):
    class Meta:
        model = MainAccount


class MainAccountAdmin(ImportExportModelAdmin):
    resource_class = MainAccountResource
    fields = ['main_account_name']


admin.site.register(MainAccount, MainAccountAdmin)


class SubAccountResource(resources.ModelResource):
    class Meta:
        model = SubAccount


class SubAccountAdmin(ImportExportModelAdmin):
    resource_class = SubAccountResource
    fields = ['mainaccount', 'sub_account_name']


admin.site.register(SubAccount, SubAccountAdmin)


class ChartOfAccountResource(resources.ModelResource):
    class Meta:
        model = ChartOfAccount


class ChartOfAccountAdmin(ImportExportModelAdmin):
    search_fields = ['account_name',]
    resource_class = ChartOfAccountResource



admin.site.register(ChartOfAccount, ChartOfAccountAdmin)

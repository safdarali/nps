from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import MainAccount, SubAccount, ChartOfAccount
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import MainAccountForm, SubAccountForm, ChartOfAccountForm
from django.db.models import Max


# main accounts


def main_account_list(request):
    main_accounts = MainAccount.objects.all()
    sub_accounts = SubAccount.objects.all()
    return render(request, 'chart_of_accounts/main_account_list.html',
                  {'main_accounts': main_accounts, 'sub_accounts': sub_accounts})


def save_main_account_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            main_accounts = MainAccount.objects.all()
            data['html_main_account_list'] = render_to_string(
                'chart_of_accounts/includes/partial_main_account_list.html', {
                    'main_accounts': main_accounts
                })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('chart_of_accounts.add_mainaccount')
def main_account_create(request):
    if request.method == 'POST':
        form = MainAccountForm(request.POST)
    else:
        form = MainAccountForm()
    return save_main_account_form(request, form, 'chart_of_accounts/includes/partial_main_account_create.html')


def main_account_update(request, pk):
    main_account = get_object_or_404(MainAccount, pk=pk)
    if request.method == 'POST':
        form = MainAccountForm(request.POST, instance=main_account)
    else:
        form = MainAccountForm(instance=main_account)
    return save_main_account_form(request, form, 'chart_of_accounts/includes/partial_main_account_update.html')


def main_account_delete(request, pk):
    main_account = get_object_or_404(MainAccount, pk=pk)
    data = dict()
    if request.method == 'POST':
        main_account.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        main_accounts = MainAccount.objects.all()
        data['html_main_account_list'] = render_to_string('chart_of_accounts/includes/partial_main_account_list.html', {
            'main_accounts': main_accounts
        })
    else:
        context = {'main_account': main_account}
        data['html_form'] = render_to_string('chart_of_accounts/includes/partial_main_account_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# sub account


def sub_account_list(request):
    sub_accounts = SubAccount.objects.all()
    return render(request, 'chart_of_accounts/main_account_list.html', {'sub_accounts': sub_accounts})


def save_sub_account_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            sub_account_name = form.cleaned_data['sub_account_name']
            main_account = form.cleaned_data['mainaccount'].id
            if int(str(sub_account_name)[:1]) != main_account:
                form.add_error('sub_account_name', 'This sub account is not related to selected main account!')
                data['form_is_valid'] = False
            else:
                subaccount = SubAccount(id=sub_account_name, mainaccount_id=main_account,
                                        sub_account_name=sub_account_name)
                subaccount.save()
                data['form_is_valid'] = True
            sub_accounts = SubAccount.objects.all()
            data['html_sub_account_list'] = render_to_string('chart_of_accounts/includes/partial_sub_account_list.html',
                                                             {
                                                                 'sub_accounts': sub_accounts
                                                             })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('chart_of_accounts.add_subaccount')
def sub_account_create(request):
    if request.method == 'POST':
        form = SubAccountForm(request.POST)
    else:
        form = SubAccountForm()
    return save_sub_account_form(request, form, 'chart_of_accounts/includes/partial_sub_account_create.html')

@permission_required('chart_of_accounts.change_subaccount')
def sub_account_update(request, pk):
    sub_account = get_object_or_404(SubAccount, pk=pk)
    if request.method == 'POST':
        form = SubAccountForm(request.POST, instance=sub_account)
    else:
        form = SubAccountForm(instance=sub_account)
    return save_sub_account_form(request, form, 'chart_of_accounts/includes/partial_sub_account_update.html')


@permission_required('chart_of_accounts.delete_mainaccount')
def sub_account_delete(request, pk):
    sub_account = get_object_or_404(SubAccount, pk=pk)
    data = dict()
    if request.method == 'POST':
        sub_account.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        sub_account = SubAccount.objects.all()
        data['html_sub_account_list'] = render_to_string('chart_of_accounts/includes/partial_sub_account_list.html', {
            'sub_accounts': sub_account
        })
    else:
        context = {'sub_account': sub_account}
        data['html_form'] = render_to_string('chart_of_accounts/includes/partial_sub_account_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# chart of accounts


def chart_of_account_list(request):
    #
    # if request.method == 'GET':
    #     q = request.GET.get('q',None)
    #     if q is not None:
    #         search = ChartOfAccount.objects.filter(account_type=q)
    #         if search:
    #             context = {'search': search}
    #             return render(request, 'chart_of_accounts/chart_of_account_list.html', {'search': search})
    #     else:
    #         search = ChartOfAccount.objects.all()
    #         return render(request, 'chart_of_accounts/chart_of_account_list.html', {'search': search})
    #
    # else:
    #     search = ChartOfAccount.objects.all()
    #     return render(request, 'chart_of_accounts/chart_of_account_list.html', {'search': search})

    chart_of_accounts = ChartOfAccount.objects.all()
    return render(request, 'chart_of_accounts/chart_of_account_list.html', {'chart_of_accounts': chart_of_accounts})


def save_chart_of_account_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():

            sub_account_name = form.cleaned_data['subaccount'].id
            account_name = form.cleaned_data['account_name']
            address = form.cleaned_data['address']
            phone_number = form.cleaned_data['phone_number']
            is_account_active = form.cleaned_data['is_account_active']
            account_type = form.cleaned_data['account_type']
            chart_of_account_max_id = ChartOfAccount.objects.filter(id__startswith=sub_account_name).aggregate(
                Max('id'))

            if chart_of_account_max_id['id__max'] is not None:
                data['form_is_valid'] = True
                chart_of_account_obj = ChartOfAccount(id=chart_of_account_max_id['id__max'] + 1,
                                                      subaccount_id=sub_account_name,
                                                      account_name=account_name,
                                                      address=address, phone_number=phone_number,
                                                      is_account_active=is_account_active, account_type=account_type)
                chart_of_account_obj.save()
            else:
                data['form_is_valid'] = True
                new_id = sub_account_name * 10000 + 1
                chart_of_account_obj = ChartOfAccount(id=new_id, subaccount_id=sub_account_name,
                                                      account_name=account_name,
                                                      address=address, phone_number=phone_number,
                                                      is_account_active=is_account_active, account_type=account_type)
                chart_of_account_obj.save()

            chart_of_accounts = ChartOfAccount.objects.all()
            data['html_chart_of_account_list'] = render_to_string(
                'chart_of_accounts/includes/partial_chart_of_account_list.html',
                {'chart_of_accounts': chart_of_accounts})
    else:
        data['form_is_valid'] = False

    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_chart_of_account_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            chart_of_accounts = ChartOfAccount.objects.all()
            data['html_chart_of_account_list'] = render_to_string(
                'chart_of_accounts/includes/partial_chart_of_account_list.html', {
                    'chart_of_accounts': chart_of_accounts
                })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('chart_of_accounts.add_chartofaccount')
def chart_of_account_create(request):
    if request.method == 'POST':
        form = ChartOfAccountForm(request.POST)
    else:
        form = ChartOfAccountForm()
    return save_chart_of_account_form(request, form, 'chart_of_accounts/includes/partial_chart_of_account_create.html')


@permission_required('chart_of_accounts.add_chartofaccount')
def chart_of_account_update(request, pk):
    chart_of_account = get_object_or_404(ChartOfAccount, pk=pk)
    if request.method == 'POST':
        form = ChartOfAccountForm(request.POST, instance=chart_of_account)
    else:
        form = ChartOfAccountForm(instance=chart_of_account)
    return update_chart_of_account_form(request, form,
                                        'chart_of_accounts/includes/partial_chart_of_account_update.html')


@permission_required('chart_of_accounts.delete_chartofaccount')
def chart_of_account_delete(request, pk):
    chart_of_account = get_object_or_404(ChartOfAccount, pk=pk)
    data = dict()
    if request.method == 'POST':
        chart_of_account.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        chart_of_accounts = ChartOfAccount.objects.all()
        data['html_chart_of_account_list'] = render_to_string(
            'chart_of_accounts/includes/partial_chart_of_account_list.html', {
                'chart_of_accounts': chart_of_accounts
            })
    else:
        context = {'chart_of_account': chart_of_account}
        data['html_form'] = render_to_string('chart_of_accounts/includes/partial_chart_of_account_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

# from django.urls import path
#
# from customers.views import customer_ledger, customer_home, customer_invoices, CcsDetail
#
# urlpatterns = [
#     path('customer', customer_home, name='customer_home'),
#     path('customer/ledger', customer_ledger, name='customer_ledger'),
#     path('customer/invoices', customer_invoices, name='customer_invoices'),
#     path('customer/invoices/detail/<int:pk>/detail', CcsDetail.as_view(), name="customer_sale_detail"),
# ]
# from django.contrib.auth.decorators import login_required
# from django.db.models import Sum, F
# from django.shortcuts import render, redirect
# from django.views.generic import DetailView
#
# from chart_of_accounts.models import ChartOfAccount
# from general_journal.models import GeneralJournal
#
#
# @login_required
# def customer_ledger(request):
#     data = dict()
#     if request.method == 'GET':
#         coa = ChartOfAccount.objects.get(user=request.user)
#         rec_number = GeneralJournal.objects.filter(chartofaccount_id=coa.id).count()
#         # [: 2] befone aggregate
#         previous_balance = GeneralJournal.objects.filter(chartofaccount_id=coa.id).aggregate(
#             difference=Sum('debit') - Sum('credit'))
#         if previous_balance['difference'] is not None:
#             pb = previous_balance['difference']
#         else:
#             pb = 0
#
#         tbs = GeneralJournal.objects.filter(chartofaccount_id=coa.id).values(
#             'date', 'voucher_type',
#             'voucher_number', 'naration',
#             'debit', 'credit').annotate(difference=F('debit') - F('credit')).order_by('date')
#         balance = pb
#         for t in tbs:
#             balance = balance + t['difference']
#             t.update({'balance': balance})
#         data = {'tbs': tbs, 'account_id': coa.id, 'account_name':coa.account_name, 'previous_balance': pb}
#     return render(request, 'customer/ledger.html', data)
#
#
# @login_required
# def customer_invoices(request):
#     data = dict()
#     if request.method == 'GET':
#         coa = ChartOfAccount.objects.get(user=request.user)
#         clubcardsalem = ClubcardSalem.objects.filter(company_name_id=coa.id)
#         data = {'clubcardsalem':clubcardsalem, 'account_name':coa.account_name, 'account_id':coa.id,'total_invoices':clubcardsalem.count()}
#     return render(request, 'customer/clubcard_invoices.html', data)
#
#
# class CcsDetail(DetailView):
#     template_name = 'customer/detail_clubcard_sale.html'
#     context_object_name = 'clubcard_sale'
#     model = ClubcardSalem
#
#     def get_context_data(self, **kwargs):
#         context = super(CcsDetail, self).get_context_data(**kwargs)
#         coa = ChartOfAccount.objects.get(user=self.request.user)
#         context['account_name'] = coa.account_name
#         return context
#
# @login_required
# def customer_home(request):
#     return render(request, template_name='customer/customer_home.html')

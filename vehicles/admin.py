from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from vehicles.models import Vehicle


class VehicleResource(resources.ModelResource):
    class Meta:
        model = Vehicle


class VehicleAdmin(ImportExportModelAdmin):
    resource_class = VehicleResource
    fields = ('vehicle_name',
              'vehicle_description',)


admin.site.register(Vehicle, VehicleAdmin)

from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import Vehicle
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import VehicleForm


def vehicle_list(request):
    vehicles = Vehicle.objects.all()
    return render(request, 'vehicles/vehicle_list.html', {'vehicles': vehicles})


def save_vehicle_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            vehicles = Vehicle.objects.all()
            data['html_vehicle_list'] = render_to_string('vehicles/includes/partial_vehicle_list.html', {
                'vehicles': vehicles
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('vehicles.add_vehicle')
def vehicle_create(request):
    if request.method == 'POST':
        form = VehicleForm(request.POST)
    else:
        form = VehicleForm()
    return save_vehicle_form(request, form, 'vehicles/includes/partial_vehicle_create.html')


@permission_required('vehicles.change_vehicle')
def vehicle_update(request, pk):
    vehicle = get_object_or_404(Vehicle, pk=pk)
    if request.method == 'POST':
        form = VehicleForm(request.POST, instance=vehicle)
    else:
        form = VehicleForm(instance=vehicle)
    return save_vehicle_form(request, form, 'vehicles/includes/partial_vehicle_update.html')


@permission_required('vehicles.delete_vehicle')
def vehicle_delete(request, pk):
    vehicle = get_object_or_404(Vehicle, pk=pk)
    data = dict()
    if request.method == 'POST':
        vehicle.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        vehicles = Vehicle.objects.all()
        data['html_vehicle_list'] = render_to_string('vehicles/includes/partial_vehicle_list.html', {
            'vehicles': vehicles
        })
    else:
        context = {'vehicle': vehicle}
        data['html_form'] = render_to_string('vehicles/includes/partial_vehicle_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

from django.db import models

from nps.models import ModelWithTimeStamp


class Vehicle(ModelWithTimeStamp):
    vehicle_name = models.CharField(max_length=50)
    vehicle_description = models.TextField()


from django.db import transaction
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import DeleteView, UpdateView, CreateView, ListView, DetailView

from duties.models import Duty
from .models import Expense, Expensem, Expenses
from django.http import JsonResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from .forms import ExpenseForm, ExpensesForm, ExpensemForm, ExpenseFormSet
from general_journal.models import GeneralJournal
from chart_of_accounts.models import ChartOfAccount


def get_latest_duty_date():
    ld = Duty.objects.latest('date')
    return ld.id


def expense_list(request):
    total_expenses = Expense.objects.all().count()
    expenses = Expense.objects.all()
    return render(request, 'expenses/expense_list.html', {'total_expenses': total_expenses, 'expenses': expenses})


def expense_home(request):
    total_expense = Expense.objects.count()
    return render(request, 'expenses/expense_home.html', {'total_expenses': total_expense})


def save_expense_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            debit_acc = form.cleaned_data['expense_account'].id
            amount = form.cleaned_data['amount']
            credit_acc = form.cleaned_data['bank']
            duty = form.cleaned_data['duty']
            if credit_acc is not None:
                credit_acc = credit_acc.id
                debit_naration = 'To bank account'
            else:
                credit_acc = 30010001
                debit_naration = 'To cash in hand account'

            general_journal_1 = GeneralJournal(chartofaccount_id=debit_acc, voucher_type='EXP', voucher_number=id,
                                               debit=amount, credit=0, naration=debit_naration, duty_id=duty.id)
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=credit_acc, voucher_type='EXP', voucher_number=id,
                                               debit=0, credit=amount, naration='By expense account', duty_id=duty.id)
            general_journal_2.save()
            # general journal entry end
            data['form_is_valid'] = True
            expenses = Expense.objects.all().count()
            data['html_expense_list'] = render_to_string('expenses/expense_home.html', {
                'total_expenses': expenses
            })
            data['total_expenses'] = Expense.objects.all().count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_expense_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            debit_acc = form.cleaned_data['expense_account'].id
            amount = form.cleaned_data['amount']
            credit_acc = form.cleaned_data['bank']
            date = form.cleaned_data['date']
            if credit_acc is not None:
                credit_acc = credit_acc.id
                debit_naration = 'To bank account'
            else:
                credit_acc = 30010001
                debit_naration = 'To cash in hand account'
            g1 = GeneralJournal.objects.filter(voucher_type='EXP', voucher_number=id).order_by('id').first()
            g2 = GeneralJournal.objects.filter(voucher_type='EXP', voucher_number=id).order_by('id').last()
            g1.debit = amount
            g1.chartofaccount_id = debit_acc
            g1.date = date
            g1.naration = debit_naration
            g1.save()

            g2.credit = amount
            g2.chartofaccount_id = credit_acc
            g2.date = date
            g2.save()
            # general journal entry end
            data['form_is_valid'] = True
            expenses = Expense.objects.all()
            data['html_expense_list'] = render_to_string('expenses/includes/partial_expense_list.html', {
                'expenses': expenses
            })
            data['total_expenses'] = Expense.objects.all().count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def expense_create(request):
    if request.method == 'POST':
        form = ExpenseForm(request.POST)
    else:
        form = ExpenseForm()
    return save_expense_form(request, form, 'expenses/includes/partial_expense_create.html')


def expense_update(request, pk):
    expense = get_object_or_404(Expense, pk=pk)
    if request.method == 'POST':
        form = ExpenseForm(request.POST, instance=expense)
    else:
        form = ExpenseForm(instance=expense)
    return update_expense_form(request, form, 'expenses/includes/partial_expense_update.html')


def expense_delete(request, pk):
    expense = get_object_or_404(Expense, pk=pk)
    data = dict()
    if request.method == 'POST':
        expense.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='EXP').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        expenses = Expense.objects.all()
        data['html_expense_list'] = render_to_string('expenses/includes/partial_expense_list.html', {
            'expenses': expenses
        })
    else:
        context = {'expense': expense}
        data['html_form'] = render_to_string('expenses/includes/partial_expense_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# duty expenses
def duty_expense_home(request):
    total_duty_expense = Expensem.objects.count()
    return render(request, 'expenses/duty_expense_home.html', {'total_duty_expense': total_duty_expense})


class ExpenseDutyDetail(DetailView):
    template_name = 'expenses/includes/detail_duty_expense.html'
    context_object_name = 'expense'
    model = Expensem


class ExpenseDelete(DeleteView):
    model = Expensem
    template_name = 'expenses/includes/partial_duty_expense_delete.html'
    success_url = reverse_lazy('list_duty_expense')


class ExpenseDutyDelete(DeleteView):
    model = Expensem
    template_name = 'expenses/includes/partial_duty_expense_delete.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})


class ExpenseDutyUpdate(UpdateView):
    model = Expensem
    form_class = ExpensesForm
    template_name = 'expenses/includes/duty_expense_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_form_kwargs(self):
        kwargs = super(ExpenseDutyUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = ExpensemForm
        form = self.get_form(form_class)
        expense = ExpenseFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, expense=expense))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = ExpensemForm
        form = self.get_form(form_class)
        expense = ExpenseFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and expense.is_valid():
            return self.form_valid(form, expense)
        return self.form_invalid(form, expense)

    def form_valid(self, form, expense):
        self.object = form.save()
        expense.instance = self.object
        id = form.instance.id
        expense.save()
        net_amount = form.cleaned_data['net_amount']
        duty = form.cleaned_data['duty']
        exps = Expenses.objects.filter(expensem_id=id)
        # general journal entry
        for i in exps:
            if i.expense_account is not None:
                general_journal_1 = GeneralJournal(chartofaccount_id=i.expense_account.id, duty_id=duty.id,
                                                   voucher_type='EXP',
                                                   voucher_number=id, debit=i.amount, credit=0,
                                                   naration='To cash in hand account')
                general_journal_1.save()
        general_journal_2 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id, voucher_type='EXP',
                                           voucher_number=id, debit=0, credit=net_amount, naration='By expense account')
        general_journal_2.save()
                # general journal entry end
        expm = Expensem.objects.get(id=id)
        expm.status = 2
        expm.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, expense):
        return self.render_to_response(self.get_context_data(form=form, expense=expense))


class ExpenseUpdate(UpdateView):
    model = Expensem
    form_class = ExpensesForm
    template_name = 'expenses/includes/duty_expense_update.html'
    success_url = '/expenses/duty/list'

    def get_form_kwargs(self):
        kwargs = super(ExpenseUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = ExpensemForm
        form = self.get_form(form_class)
        expense = ExpenseFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, expense=expense))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = ExpensemForm
        form = self.get_form(form_class)
        expense = ExpenseFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and expense.is_valid():
            return self.form_valid(form, expense)
        return self.form_invalid(form, expense)

    def form_valid(self, form, expense):
        self.object = form.save()
        expense.instance = self.object
        id = form.instance.id
        expense.save()
        net_amount = form.cleaned_data['net_amount']
        duty = form.cleaned_data['duty']
        exps = Expenses.objects.filter(expensem_id=id)
        # general journal entry
        for i in exps:
            if i.expense_account is not None:
                general_journal_1 = GeneralJournal(chartofaccount_id=i.expense_account.id, duty_id=duty.id, voucher_type='EXP',
                                                   voucher_number=id, debit=i.amount, credit=0, naration='To cash in hand account')
                general_journal_1.save()
        general_journal_2 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id, voucher_type='EXP',
                                           voucher_number=id, debit=0,credit=net_amount, naration='By expense account')
        general_journal_2.save()
                # general journal entry end
        expm = Expensem.objects.get(id=id)
        expm.status = 2
        expm.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, expense):
        return self.render_to_response(self.get_context_data(form=form, expense=expense))


class ExpenseDutyCreate(CreateView):
    template_name = 'expenses/includes/duty_expense_create.html'
    model = Expensem
    form_class = ExpensemForm

    # @method_decorator(privilegesRequired)
    def dispatch(self, *args, **kwargs):
        return super(ExpenseDutyCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(ExpenseDutyCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_context_data(self, **kwargs):
        data = super(ExpenseDutyCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['expense'] = ExpenseFormSet(self.request.POST)
        else:
            data['expense'] = ExpenseFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        expense = context['expense']
        with transaction.atomic():
            self.object = form.save()
            if expense.is_valid():
                expense.instance = self.object
                expense.user = self.request.user
                expense.save()

        return super(ExpenseDutyCreate, self).form_valid(form)

    # def form_invalid(self, form, expense):
    #     return self.render_to_response(self.get_context_data(form=form, expense=expense))


class ExpenseCreate(CreateView):
    template_name = 'expenses/includes/duty_expense_create.html'
    model = Expensem
    form_class = ExpensemForm

    # @method_decorator(privilegesRequired)
    def dispatch(self, *args, **kwargs):
        return super(ExpenseCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(ExpenseCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('list_duty_expense')

    def get_context_data(self, **kwargs):
        data = super(ExpenseCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['expense'] = ExpenseFormSet(self.request.POST)
        else:
            data['expense'] = ExpenseFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        expense = context['expense']
        with transaction.atomic():
            self.object = form.save()
            if expense.is_valid():
                expense.instance = self.object
                expense.user = self.request.user
                expense.save()

        return super(ExpenseCreate, self).form_valid(form)

    # def form_invalid(self, form, expense):
    #     return self.render_to_response(self.get_context_data(form=form, expense=expense))


class ExpenseList(ListView):
    template_name = 'expenses/list_duty_expense.html'
    context_object_name = 'expenses'
    model = Expensem

    # @method_decorator(privilegesRequired)
    def dispatch(self, *args, **kwargs):
        return super(ExpenseList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        queryset = super(ExpenseList, self).get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ExpenseList, self).get_context_data(**kwargs)
        context['total_duty_expenses'] = Expensem.objects.count()
        return context


# class ExpenseDutyList(ListView):
#     template_name = 'expenses/list_duty_expense.html'
#     context_object_name = 'expenses'
#     model = Expensem
#
#     # @method_decorator(privilegesRequired)
#     def dispatch(self, *args, **kwargs):
#         return super(ExpenseList, self).dispatch(*args, **kwargs)
#
#     def get_queryset(self):
#         queryset = super(ExpenseList, self).get_queryset()
#         queryset = queryset.filter(status=1)
#         return queryset
#
#     def get_context_data(self, **kwargs):
#         context = super(ExpenseList, self).get_context_data(**kwargs)
#         context['total_duty_expenses'] = Expensem.objects.count()
#         return context

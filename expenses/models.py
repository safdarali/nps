from decimal import Decimal
from django.db import models
from django.utils import timezone
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty, DutyMen
from nps import settings
from nps.models import ModelWithTimeStamp


class Expense(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    expense_account = models.ForeignKey(ChartOfAccount, on_delete=models.SET_NULL, null=True, related_name='expense_account',db_index=True)
    bank = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, blank=True, default=None, related_name='expense_bank')
    amount = models.DecimalField(max_digits=15, decimal_places=3, verbose_name='Amount')

    class Meta:
        permissions = (('list_expense', 'Can list expense'),)


class Expensem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True,db_index=True)
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Net amount')
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,db_index=True)

    class Meta:
        permissions = (('list_expensem', 'Can list duty expense'),)


class Expenses(ModelWithTimeStamp):
    expensem = models.ForeignKey(Expensem, on_delete=models.CASCADE,db_index=True)
    expense_account = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Expense head')
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Expense amount')
    description = models.TextField(verbose_name='Description', null=True, blank=True,)
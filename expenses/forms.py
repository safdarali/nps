from django import forms
from django.forms import inlineformset_factory, Textarea

from duties.models import Duty, DutyMen
from .models import Expense, Expensem, Expenses
from chart_of_accounts.models import ChartOfAccount


def get_latest_duty_date():
    ld = Duty.objects.latest('date')
    return ld


class ExpenseForm(forms.ModelForm):
    class Meta:
        model = Expense
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ExpenseForm, self).__init__(*args, **kwargs)
        self.fields['bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['expense_account'].empty_label = 'Select an expense account'
        self.fields['expense_account'].queryset = ChartOfAccount.objects.filter(id__startswith=5)


class ExpensemForm(forms.ModelForm):
    class Meta:
        model = Expensem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ExpensemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class ExpensesForm(forms.ModelForm):
    class Meta:
        model = Expenses
        fields = '__all__'
        widgets = {
            'description': Textarea(attrs={'rows': 1, 'cols': 30}),
        }

    def __init__(self, *args, **kwargs):
        super(ExpensesForm, self).__init__(*args, **kwargs)
        if self.instance.expense_account_id:
            self.fields['expense_account'].queryset = ChartOfAccount.objects.filter(id=self.instance.expense_account_id)
            self.fields['expense_account'].empty_label = None
        else:
            self.fields['expense_account'].empty_label = 'Select an expense head'
            self.fields['expense_account'].queryset = ChartOfAccount.objects.filter(account_type__in=[5, 9])
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


ExpenseFormSet = inlineformset_factory(Expensem, Expenses, form=ExpensesForm, extra=1)
from django.urls import path
from .views import expense_list, expense_create, expense_update, expense_delete, expense_home, ExpenseCreate, \
    ExpenseList, ExpenseUpdate, ExpenseDelete, duty_expense_home, ExpenseDutyDetail

urlpatterns = [
    path('expenses/', expense_list, name='expense_list'),
    path('expenses/home', expense_home, name='expense_home'),
    path('expenses/create', expense_create, name='expense_create'),
    path('expenses/<int:pk>/update', expense_update, name='expense_update'),
    path('expenses/<int:pk>/delete', expense_delete, name='expense_delete'),

    path('expenses/duty/home', duty_expense_home, name='duty_expense_home'),
    path('expenses/duty/create', ExpenseCreate.as_view(), name='create_duty_expense'),
    path('expenses/duty/list', ExpenseList.as_view(), name='list_duty_expense'),
    path('expenses/duty/update/<int:pk>', ExpenseUpdate.as_view(), name="update_duty_expense"),
    path('expenses/duty/delete/<int:pk>', ExpenseDelete.as_view(), name="delete_duty_expense"),
    path('expenses/duty/<int:pk>/detail', ExpenseDutyDetail.as_view(), name="detail_duty_expense"),
]
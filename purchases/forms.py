from django import forms
from django.forms.models import inlineformset_factory
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty
from products.models import Product
from tank_lorries.models import TankLorry, Lorry, LorryChamber
from tanks.models import Tank
from .models import Lubepurchasem, Lubepurchases, FuelPurchases, FuelPurchasem, FuelPurchase, FuelPurchasesByLorry, \
    FuelPurchasemByLorry


def get_latest_duty_date():
    ld = Duty.objects.latest('date')
    return ld


class LubepurchasesForm(forms.ModelForm):
    class Meta:
        model = Lubepurchases
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(LubepurchasesForm, self).__init__(*args, **kwargs)
        if self.instance.product_id:
            self.fields['product'].queryset = Product.objects.filter(id=self.instance.product_id)
            self.fields['product'].empty_label = None
        else:
            self.fields['product'].queryset = Product.objects.filter(product_type=1)
            self.fields['product'].empty_label = 'Please, choose a Product'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


LubepurchaseFormSet = inlineformset_factory(Lubepurchasem, Lubepurchases, form=LubepurchasesForm, extra=1, can_delete=True)


class LubepurchasemForm(forms.ModelForm):
    class Meta:
        model = Lubepurchasem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(LubepurchasemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['supplier'].queryset = ChartOfAccount.objects.filter(account_type=1)
        self.fields['supplier'].empty_label = 'Please, choose a Supplier'

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class FuelPurchasesForm(forms.ModelForm):
    class Meta:
        model = FuelPurchases
        fields = '__all__'
        widgets = {
            'previous_stock': forms.TextInput(attrs={'readonly': 'readonly'}),
            'current_dipless_stock': forms.TextInput(attrs={'readonly': 'readonly'}),
            'current_dip_stock': forms.TextInput(attrs={'readonly': 'readonly'}),
            'milli_short': forms.TextInput(attrs={'readonly': 'readonly'}),
            'stock_difference': forms.TextInput(attrs={'readonly': 'readonly'}),
            'tl_carriage_name': forms.TextInput(attrs={'readonly': 'readonly'}),
            'tl_driver_phone': forms.TextInput(attrs={'readonly': 'readonly'}),
        }

    def __init__(self, *args, **kwargs):
        super(FuelPurchasesForm, self).__init__(*args, **kwargs)
        self.fields['tank'].empty_label = 'Tank'
        self.fields['tank'].queryset = Tank.objects.filter(id__in=[1, 2, 3])
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type=4)
        self.fields['tank'].widget.attrs['style'] = 'width:100px;'

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


FuelPurchaseFormSet = inlineformset_factory(FuelPurchasem, FuelPurchases, form=FuelPurchasesForm, extra=1, can_delete=True)


class FuelPurchasemForm(forms.ModelForm):
    class Meta:
        model = FuelPurchasem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(FuelPurchasemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['supplier'].empty_label = 'Select a supplier'
        self.fields['supplier'].queryset = ChartOfAccount.objects.filter(account_type=1)
        self.fields['tanklorry'].empty_label = 'Select a Tank lorry number'
        self.fields['tanklorry'].queryset = TankLorry.objects.all()
        self.fields['tl_decanted_by'].queryset = ChartOfAccount.objects.filter(account_type=5)
        self.fields['tl_decanted_by'].empty_label = 'Select a person'
        self.fields['tl_cleared_by'].queryset = ChartOfAccount.objects.filter(account_type=5)
        self.fields['tl_cleared_by'].empty_label = 'Select a person'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class FPForm(forms.ModelForm):
    class Meta:
        model = FuelPurchase
        fields = '__all__'
        exclude = ('user, status',)

    def __init__(self, *args, **kwargs):
        super(FPForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['tank'].queryset = Tank.objects.filter(id__in=[4,5])
        self.fields['tank'].empty_label = 'Select a tank'
        self.fields['supplier'].empty_label = 'Select a supplier'
        self.fields['supplier'].queryset = ChartOfAccount.objects.filter(account_type=1)
        self.fields['decanted_by'].queryset = ChartOfAccount.objects.filter(account_type=5)
        self.fields['decanted_by'].empty_label = 'Select a person'
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type=5)
        self.fields['status'].widget = forms.HiddenInput()
        self.fields['user'].widget = forms.HiddenInput()
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class FuelPurchasesByLorryForm(forms.ModelForm):
    class Meta:
        model = FuelPurchasesByLorry
        fields = '__all__'
        widgets = {
            'milli_short': forms.TextInput(attrs={'readonly': 'readonly'}),
        }

    def __init__(self, *args, **kwargs):
        super(FuelPurchasesByLorryForm, self).__init__(*args, **kwargs)
        self.fields['product'].empty_label = 'Product'
        self.fields['product'].queryset = Product.objects.filter(product_type=4)
        if self.instance.lorry_chamber_id:
            self.fields['lorry_chamber'].queryset = LorryChamber.objects.filter(id=self.instance.lorry_chamber_id)
        else:
            self.fields['lorry_chamber'].queryset = LorryChamber.objects.all()

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


FuelPurchaseByLorryFormSet = inlineformset_factory(FuelPurchasemByLorry, FuelPurchasesByLorry, form=FuelPurchasesByLorryForm, extra=1)


class FuelPurchasemByLorryForm(forms.ModelForm):
    class Meta:
        model = FuelPurchasemByLorry
        fields = '__all__'
        widgets = {
            'supplier_invoice_no': forms.TextInput(attrs={'placeholder': 'Enter Invoice #'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(FuelPurchasemByLorryForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['supplier'].empty_label = 'Select Supplier'
        self.fields['supplier'].queryset = ChartOfAccount.objects.filter(account_type=1)
        self.fields['lorry'].empty_label = 'Select Lorry'
        self.fields['driver_name'].empty_label = 'Select Driver'
        self.fields['driver_name'].queryset = ChartOfAccount.objects.filter(account_type=5)
        self.fields['lorry'].queryset = Lorry.objects.all()
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)
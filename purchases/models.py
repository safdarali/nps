from django.db import models
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty
from nps.models import ModelWithTimeStamp
from products.models import Product
from django.utils import timezone
from tanks.models import Tank
from decimal import Decimal
from tank_lorries.models import TankLorry, Lorry, LorryChamber
from django.conf import settings


class FuelPurchasem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    MILLI_SHORT_RECOVERY_STATUS = (
        (1, 'Un-recovered'),
        (2, 'Recovered')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    supplier = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, related_name='supplier',db_index=True)
    supplier_invoice_no = models.CharField(verbose_name='Invoice#', max_length=25, default='')
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    invoice_date = models.DateTimeField(default=timezone.now, null=False, blank=False, verbose_name='Invoice date')
    tanklorry = models.ForeignKey(TankLorry, on_delete=models.CASCADE, blank=True, null=True, default=None,
                                  verbose_name='Tank lorry',db_index=True)
    tl_carriage_name = models.CharField(verbose_name=' Carriage name', max_length=100, null=True)
    tl_driver_name = models.CharField(verbose_name='Driver name', max_length=120, null=True)
    tl_driver_phone = models.CharField(verbose_name='Driver phone', max_length=20, null=True)
    tl_reporting_time = models.TimeField(verbose_name='Reporting time')
    tl_decantation_time = models.TimeField(verbose_name='Decant time')
    tl_checkout_time = models.TimeField(verbose_name='Checkout time')
    tl_decanted_by = models.ForeignKey(ChartOfAccount, verbose_name='Decanted by', on_delete=models.CASCADE,
                                       related_name='tl_decanted_by')
    tl_cleared_by = models.ForeignKey(ChartOfAccount, verbose_name='TL cleared by', on_delete=models.CASCADE,
                                      related_name='tl_cleared_by')
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,
                                     verbose_name='Net amount', default=Decimal('0.000'))
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,db_index=True)
    milli_short_recovery_status = models.SmallIntegerField(verbose_name='Milli short status',
                                                           choices=MILLI_SHORT_RECOVERY_STATUS, default=None, null=True,
                                                           blank=True)

    class Meta:
        permissions = (('list_fuelpurchase', 'Can list fuelpurchase'),('delete_fuelpurchase', 'Can delete fuelpurchase'))


class FuelPurchases(ModelWithTimeStamp):
    fuelpurchasem = models.ForeignKey(FuelPurchasem, on_delete=models.CASCADE, null=True, blank=True,db_index=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, related_name='products',db_index=True)
    tl_chamber = models.CharField(verbose_name='TL chamber', max_length=20, default=0)
    tl_dip = models.IntegerField(verbose_name='TL refinery dip', null=False, blank=False, default=0)
    tl_dip_observed = models.IntegerField(verbose_name='TL dip received', null=False, blank=False, default=0)
    milli_short = models.IntegerField(verbose_name='Milli short', null=False, blank=False, default=0)
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Tank',db_index=True)
    previous_dip = models.IntegerField(verbose_name='Dip before decantation', null=False, blank=False, default=0)
    previous_stock = models.DecimalField(verbose_name='Stock before decantation', decimal_places=1, max_digits=10,
                                         null=False, blank=False, default=Decimal('0.0'))
    purchased_stock = models.DecimalField(verbose_name='Purchased stock', decimal_places=1, max_digits=10, null=False,
                                          blank=False, default=Decimal('0.0'))
    current_dipless_stock = models.DecimalField(verbose_name='Physical stock', decimal_places=1, max_digits=10,
                                                null=False, blank=False, default=Decimal('0.0'))
    current_dip = models.IntegerField(verbose_name='Dip after decantation', null=False, blank=False, default=0)
    current_dip_stock = models.DecimalField(verbose_name='Stock after decantation', decimal_places=1, max_digits=10,
                                            null=False, blank=False, default=Decimal('0.0'))
    sale_during_decantation = models.DecimalField(verbose_name='Sale during decantation', decimal_places=1,
                                                  max_digits=10, null=False, blank=False, default=Decimal('0.0'))
    stock_difference = models.DecimalField(verbose_name='Stock difference', decimal_places=1, max_digits=10, null=False,
                                           blank=False, default=Decimal('0.0'))
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Amount',
                                 default=Decimal('0.000'))

    def __str__(self):
        return self.product


class FuelPurchasemByLorry(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    supplier = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, related_name='supplier_by_lorry',db_index=True, verbose_name='Supplier')
    supplier_invoice_no = models.CharField(verbose_name='Invoice #', max_length=25, default='')
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    lorry = models.ForeignKey(Lorry, on_delete=models.CASCADE, blank=True, null=True, default=None,
                                  verbose_name='Lorry',db_index=True)
    driver_name = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, related_name='driver',db_index=True, verbose_name='Driver Name')
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,
                                     verbose_name='Total Amount', default=Decimal('0.000'))
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,db_index=True)


    class Meta:
        permissions = (('list_fuelpurchase_by_lorry', 'Can list fuelpurchase_by_lorry'),('delete_fuelpurchase_by_lorry', 'Can delete fuelpurchase by lorry'))


class FuelPurchasesByLorry(ModelWithTimeStamp):
    fuelpurchasembylorry = models.ForeignKey(FuelPurchasemByLorry, on_delete=models.CASCADE, null=True, blank=True,db_index=True, related_name='fuel_purchases_by_lorry')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, related_name='products_by_lorry',db_index=True, verbose_name='Product')
    lorry_chamber = models.ForeignKey(LorryChamber,on_delete=models.SET_NULL,verbose_name='Chamber',null=True, blank=True)
    chamber_dip_purchased = models.IntegerField(verbose_name='Purchased Dip', null=False, blank=False, default=0)
    chamber_dip_received = models.IntegerField(verbose_name='Received Dip', null=False, blank=False, default=0)
    milli_short = models.IntegerField(verbose_name='Milli Short', null=False, blank=False, default=0)
    purchased_stock = models.DecimalField(verbose_name='Purchased Stock', decimal_places=1, max_digits=10, null=False, blank=False, default=Decimal('0.0'))
    received_stock = models.DecimalField(verbose_name='Received Stock', decimal_places=1, max_digits=10, null=False, blank=False, default=Decimal('0.0'))
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Amount',
                                 default=Decimal('0.000'))

    def __str__(self):
        return self.product


class Lubepurchasem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    supplier = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True,db_index=True)
    supplier_invoice_no = models.CharField(verbose_name='Invoice#', max_length=25, default='')
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    invoice_date = models.DateTimeField(default=timezone.now, null=False, blank=False, verbose_name='Invoice date')
    net_amount = models.DecimalField(max_digits=20, decimal_places = 3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Net amount')
    tax_net_amount = models.DecimalField(max_digits=10, decimal_places = 3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Tax net amount')
    net_amount_without_tax = models.DecimalField(max_digits=10, decimal_places = 3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Net amount without tax')
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True,db_index=True)

    class Meta:
        permissions = (('list_lubepurchasem', 'Can list lubepurchasem'),)


class Lubepurchases(ModelWithTimeStamp):
    lubepurchasem = models.ForeignKey(Lubepurchasem, on_delete=models.CASCADE, null=True, blank=True,db_index=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Product',db_index=True)
    box_purchased = models.IntegerField(null=False, blank=False, verbose_name='Qty. purchased', default=1)
    box_price = models.DecimalField(max_digits=10, decimal_places = 3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Price')
    amount_without_tax = models.DecimalField(max_digits=10, decimal_places = 3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Amount without tax')
    tax_amount = models.DecimalField(max_digits=10, decimal_places = 3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Tax amount')
    amount = models.DecimalField(max_digits=10, decimal_places = 3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Amount',db_index=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.product


class LubesStock(ModelWithTimeStamp):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Product',db_index=True)
    purchase_ref = models.IntegerField(null=False, verbose_name='Purchase ref#')
    cost_price = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Cost price')
    purchased_stock = models.IntegerField(default=0, verbose_name='Purchased stock')
    qty = models.IntegerField(default=0, verbose_name='Quantity')

    class Meta:
        permissions = (('list_lubesstock', 'Can list lubesstock'),)


class FuelPurchase(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='')
    supplier = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, related_name='fp_suppliers',db_index=True)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    decanted_by = models.ForeignKey(ChartOfAccount, verbose_name='Decanted by', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, related_name='fp_products',db_index=True)
    purchased_stock = models.DecimalField(verbose_name='Purchased stock', decimal_places=1, max_digits=10, null=False, blank=False, default=Decimal('0.0'))
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, null=True, verbose_name='Tank',db_index=True)
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Amount', default=Decimal('0.000'))
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1, null=True, blank=True,db_index=True)

    def __str__(self):
        return self.product



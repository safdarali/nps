from django.urls import path
from .views import fuel_purchase_home, get_chart, LubepurchaseList, LubepurchaseCreate, LubepurchaseDetail, \
    LubepurchaseUpdate, LubepurchaseDelete, GetProductPrice, lube_purchase_home, FuelpurchaseList, FuelpurchaseDetail, \
    FuelPurchaseCreate, FuelPurchaseUpdate, FuelPurchaseDelete, GetTax, fp_list, fp_create, fp_update, fp_delete, \
    load_tank, fp_home, fp_create_home, fp_detail, FuelpurchaseByLorryList, get_chart_by_lorry, \
    fuel_purchase_by_lorry_home, FuelpurchaseByLorryDetail, FuelPurchaseByLorryCreate, FuelPurchaseByLorryUpdate, \
    FuelPurchaseByLorryDelete, GetLorryChambers

urlpatterns = [
    path('fuel_purchases/', FuelpurchaseList.as_view(), name='fuels_purchase_list'),
    path('fuel_purchases/get_chart', get_chart, name='fuel_get_chart'),
    path('fuel_purchases/home', fuel_purchase_home, name='fuels_purchase_home'),
    path('fuel_purchases/<int:pk>/detail', FuelpurchaseDetail.as_view(), name="fuels_purchase_detail"),
    path('fuel_purchases/create', FuelPurchaseCreate.as_view(), name='fuels_purchase_create'),
    path('fuel_purchases/<int:pk>/update', FuelPurchaseUpdate.as_view(), name='fuels_purchase_update'),
    path('fuel_purchases/<int:pk>/delete', FuelPurchaseDelete.as_view(), name='fuels_purchase_delete'),

    path('fuel_purchases_by_lorry/', FuelpurchaseByLorryList.as_view(), name='fuels_purchase_by_lorry_list'),
    path('fuel_purchases_by_lorry/get_chart', get_chart_by_lorry, name='fuel_get_chart_by_lorry'),
    path('fuel_purchases_by_lorry/home', fuel_purchase_by_lorry_home, name='fuels_purchase_by_lorry_home'),
    path('fuel_purchases_by_lorry/<int:pk>/detail', FuelpurchaseByLorryDetail.as_view(), name="fuels_purchase_by_lorry_detail"),
    path('fuel_purchases_by_lorry/create', FuelPurchaseByLorryCreate.as_view(), name='fuels_purchase_by_lorry_create'),
    path('fuel_purchases_by_lorry/<int:pk>/update', FuelPurchaseByLorryUpdate.as_view(), name='fuels_purchase_by_lorry_update'),
    path('fuel_purchases_by_lorry/<int:pk>/delete', FuelPurchaseByLorryDelete.as_view(), name='fuels_purchase_by_lorry_delete'),
    path('fuel_purchases_by_lorry/get_lorry', GetLorryChambers.as_view(), name='get_lorry'),

    path('lubes_purchases', LubepurchaseList.as_view(), name='lubes_purchase_list'),
    path('lubes_purchases/create', LubepurchaseCreate.as_view(), name='lubes_purchase_create'),
    path('lubes_purchases/home', lube_purchase_home, name='lube_purchase_home'),
    path('lubes_purchases/<int:pk>/detail', LubepurchaseDetail.as_view(), name="lubes_purchase_detail"),
    path('lubes_purchases/<int:pk>/update', LubepurchaseUpdate.as_view(), name="lubes_purchase_update"),
    path('lubes_purchases/<int:pk>/delete', LubepurchaseDelete, name="lubes_purchase_delete"),
    path('lubes_purchases/get_price', GetProductPrice.as_view(), name='get_price'),
    path('lubes_purchases/fetch_tax', GetTax.as_view(), name='fetch_tax'),

    path('fps/', fp_list, name='fp_list'),
    path('fps/home', fp_home, name='fp_home'),
    path('fps/create', fp_create, name='fp_create'),
    path('fps/create/home', fp_create_home, name='fp_create_home'),
    path('fps/<int:pk>/update', fp_update, name='fp_update'),
    path('fps/<int:pk>/delete', fp_delete, name='fp_delete'),
    path('fps/load_tank/', load_tank, name='load_tank'),
    path('fps/detail/<int:pk>', fp_detail, name='fp_detail'),
]
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from purchases.models import LubesStock


class LubesStockResource(resources.ModelResource):
    class Meta:
        model = LubesStock


class LubesStockAdmin(ImportExportModelAdmin):
    resource_class = LubesStockResource
    list_display = ['product', 'created_at', 'purchase_ref', 'cost_price','purchased_stock','qty']
    search_fields = ['product']


admin.site.register(LubesStock, LubesStockAdmin)
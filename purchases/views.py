import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404

from chart_of_accounts.models import ChartOfAccount
from general_journal.models import GeneralJournal, TankGeneralJournal
from lorry_stock.models import LorryStock, LorryStockHistory
from products.models import Product
from sales.models import TanksLedger
from tank_lorries.models import Lorry, LorryChamber, ChamberChart
from taxes.models import Tax
from nps.custom_decorators.permission_required import permission_required
from .models import FuelPurchasem, Lubepurchases, FuelPurchases, FuelPurchase, FuelPurchasemByLorry, \
    FuelPurchasesByLorry
from django.http import JsonResponse, HttpResponse
from django.template.loader import render_to_string
from .forms import FuelPurchasemForm, LubepurchasemForm, LubepurchaseFormSet, LubepurchasesForm, FuelPurchaseFormSet, \
    FuelPurchasesForm, FPForm, FuelPurchaseByLorryFormSet, FuelPurchasemByLorryForm, FuelPurchasesByLorryForm
from .models import Lubepurchasem
from django.db import transaction
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from tanks.models import TankChart1, TankChart2, Tank
from django.core.exceptions import ObjectDoesNotExist
from purchases.models import LubesStock


def get_chart(request):
    data = dict()
    if request.method == 'GET':
        tank_id = request.GET.get('tank')
        previous_dip = request.GET.get('previous_dip', None)
        if tank_id == '1':
            data['litre'] = TankChart1.objects.get(dip=previous_dip).litre
        else:
            data['litre'] = TankChart2.objects.get(dip=previous_dip).litre

    return JsonResponse(data)


def fuel_purchase_home(request):
    fph = FuelPurchasem.objects.all()
    total_purchases = fph.count()
    not_posted = fph.filter(status=1).count()
    return render(request, 'purchases/fuel_purchases_home.html',
                  {'total_purchases': total_purchases, 'not_posted': not_posted})


class FuelpurchaseList(ListView):
    template_name = 'purchases/fuel_purchases_list.html'
    context_object_name = 'fuel_purchases'
    model = FuelPurchasem

    @method_decorator(permission_required('purchases.list_fuelpurchase'))
    def dispatch(self, *args, **kwargs):
        return super(FuelpurchaseList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FuelpurchaseList, self).get_context_data(**kwargs)
        context['total_fuel_purchases'] = FuelPurchasem.objects.count()
        return context


class FuelpurchaseDetail(DetailView):
    template_name = 'purchases/includes/detail_fuel_purchase.html'
    context_object_name = 'fuel_purchase'
    model = FuelPurchasem


class FuelPurchaseCreate(CreateView):
    template_name = 'purchases/includes/fuel_purchase_create.html'
    model = FuelPurchasem
    form_class = FuelPurchasemForm

    @method_decorator(permission_required('purchases.add_fuelpurchasem'))
    def dispatch(self, *args, **kwargs):
        return super(FuelPurchaseCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FuelPurchaseCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('fuels_purchase_detail', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        data = super(FuelPurchaseCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['fuels_purchase'] = FuelPurchaseFormSet(self.request.POST)
            data['total_fuel_purchases'] = FuelPurchasem.objects.count()
        else:
            data['fuels_purchase'] = FuelPurchaseFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['fuels_purchase']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(FuelPurchaseCreate, self).form_valid(form)


class FuelPurchaseUpdate(UpdateView):
    model = FuelPurchasem
    form_class = FuelPurchasesForm
    template_name = 'purchases/includes/fuel_purchase_update.html'
    success_url = '/fuel_purchases'

    def dispatch(self, *args, **kwargs):
        return super(FuelPurchaseUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FuelPurchaseUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = FuelPurchasemForm
        form = self.get_form(form_class)
        fuels_purchase = FuelPurchaseFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, fuels_purchase=fuels_purchase))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = FuelPurchasemForm
        form = self.get_form(form_class)
        fuels_purchase = FuelPurchaseFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and fuels_purchase.is_valid():
            return self.form_valid(form, fuels_purchase)
        return self.form_invalid(form, fuels_purchase)

    def form_valid(self, form, fuels_purchase):
        self.object = form.save()
        fuels_purchase.instance = self.object
        id = self.object.id
        fuels_purchase.save()
        # general journal entry
        id = form.instance.id
        credit_acc = form.cleaned_data['supplier'].id
        net_amount = form.cleaned_data['net_amount']
        duty = form.cleaned_data['duty']
        fp = FuelPurchases.objects.filter(fuelpurchasem=id)
        for i in fp:
            if i.product is not None:
                # tank ledger entry
                tank_ledger = TanksLedger(duty_id=duty.id, tank_id=i.tank.id, opening_stock=0,
                                          received_stock=i.purchased_stock, meter_sale=0, test_reading=0, dip=i.current_dip,
                                          physical_stock=0, trans_type='FP', trans_number=id)
                tank_ledger.save()
                tl = TanksLedger.objects.filter(duty_id=duty.id, tank_id=i.tank.id).first()
                tl_obj = TanksLedger.objects.get(id=tl.id)
                tl_obj.physical_stock = tl_obj.physical_stock + i.current_dipless_stock
                tl_obj.save()

                # tank ledger entry end
                # general journal entry
                if i.purchased_stock > 0:
                    general_journal_1 = GeneralJournal(
                        chartofaccount=ChartOfAccount.objects.filter(product_id=i.product.id,
                                                                     subaccount_id=5001)[0], duty_id=duty.id,
                        voucher_type='FP',
                        voucher_number=id, debit=i.amount, credit=0,
                        naration='To supplier account')
                    general_journal_1.save()

        general_journal_2 = GeneralJournal(chartofaccount_id=credit_acc, duty_id=duty.id, voucher_type='FP',
                                           voucher_number=id, debit=0, credit=net_amount,
                                           naration='By fuel purchase account')
        general_journal_2.save()
        # general journal entry end
        fpm = FuelPurchasem.objects.get(id=id)
        fpm.status = 2
        if i.milli_short > 0:
            fpm.milli_short_recovery_status = 1
        fpm.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, fuels_purchase):
        return self.render_to_response(self.get_context_data(form=form, fuels_purchase=fuels_purchase))


class FuelPurchaseDelete(LoginRequiredMixin, DeleteView):
    template_name = 'purchases/includes/partial_fuel_purchase_delete.html'
    model = FuelPurchasem
    success_url = reverse_lazy('fuels_purchase_list')

    @method_decorator(permission_required('purchases.delete_fuelpurchase'))
    def dispatch(self, *args, **kwargs):
        return super(FuelPurchaseDelete, self).dispatch(*args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        gj = GeneralJournal.objects.filter(voucher_number=self.object.id, voucher_type='FP')
        gj.delete()
        self.object.delete()

        return HttpResponseRedirect(success_url)


# def update_fuel_purchase_form(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             form.save()
#             # general journal entry
#             id = form.instance.id
#             credit_acc  = form.cleaned_data['supplier'].id
#             amount = form.cleaned_data['amount']
#             debit_acc = form.cleaned_data['product'].id
#             date = form.cleaned_data['date']
#             g1 = GeneralJournal.objects.filter(voucher_type='FP', voucher_number=id).order_by('id').first()
#             g1.debit = amount
#             g1.chartofaccount_id = debit_acc
#             g1.date = date
#             g1.save()
#             g2 = GeneralJournal.objects.filter(voucher_type='FP', voucher_number=id).order_by('id').last()
#             g2.credit = amount
#             g2.chartofaccount_id = credit_acc
#             g2.date = date
#             g2.save()
#             # general journal entry end
#             data['form_is_valid'] = True
#             purchases = FuelPurchase.objects.all()
#             data['html_fuel_purchases_list'] = render_to_string('purchases/includes/partial_purchase_list.html', {
#                 'purchases': purchases
#             })
#             data['total_purchases'] = FuelPurchase.objects.count()
#         else:
#             data['form_is_valid'] = False
#     context = {'form': form}
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)
#
#
# @permission_required('purchases.add_fuelpurchase')
# def fuel_purchase_create(request):
#     if request.method == 'POST':
#         form = FuelPurchaseForm(request.POST)
#     else:
#         form = FuelPurchaseForm()
#     return save_fuel_purchase_form(request, form, 'purchases/includes/partial_fuel_purchase_create.html')
#
#
# @permission_required('purchases.change_fuelpurchase')
# def fuel_purchase_update(request, pk):
#     purchase = get_object_or_404(FuelPurchase, pk=pk)
#     if request.method == 'POST':
#         form = FuelPurchaseForm(request.POST, instance=purchase)
#     else:
#         form = FuelPurchaseForm(instance=purchase)
#     return update_fuel_purchase_form(request, form, 'purchases/includes/partial_purchase_update.html')
#
#
# @permission_required('purchases.delete_fuelpurchase')
# def fuel_purchase_delete(request, pk):
#     purchase = get_object_or_404(FuelPurchase, pk=pk)
#     data = dict()
#     if request.method == 'POST':
#         purchase.delete()
#         data['form_is_valid'] = True  # This is just to play along with the existing code
#         GeneralJournal.objects.filter(voucher_number=pk, voucher_type ='FP').delete()
#         purchases = FuelPurchase.objects.all()
#         data['html_fuel_purchases_list'] = render_to_string('purchases/includes/partial_purchase_list.html', {
#             'purchases': purchases
#         })
#         data['total_purchases'] = FuelPurchase.objects.count()
#     else:
#         context = {'purchase': purchase}
#         data['html_form'] = render_to_string('purchases/includes/partial_fuel_purchase_delete.html',
#                                              context,
#                                              request=request,
#                                              )
#     return JsonResponse(data)


# ----------------lubes purchases------------------

def lube_purchase_home(request):
    total_purchases = Lubepurchasem.objects.count()
    return render(request, 'purchases/lube_purchase_home.html', {'total_purchases': total_purchases})


@permission_required('purchases.delete_lubepurchasem')
def LubepurchaseDelete(request, pk):
    lubes_purchase = get_object_or_404(Lubepurchasem, pk=pk)
    data = dict()
    if request.method == 'POST':
        lubes_purchase.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='LP').delete()
        LubesStock.objects.filter(purchase_ref=pk).delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        lubes_purchases = Lubepurchasem.objects.all()
        data['html_lubes_purchase_list'] = render_to_string('purchases/includes/partial_lube_purchase_list.html', {
            'lubes_purchases': lubes_purchases
        })
        data['total_purchases'] = Lubepurchasem.objects.count()
    else:
        context = {'lubes_purchase': lubes_purchase}
        data['html_form'] = render_to_string('purchases/includes/partial_lube_purchase_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class LubepurchaseDetail(DetailView):
    template_name = 'purchases/includes/detail_lube_purchase.html'
    context_object_name = 'lubes_purchase'
    model = Lubepurchasem


class LubepurchaseList(ListView):
    template_name = 'purchases/lube_purchases_list.html'
    context_object_name = 'lubes_purchases'
    model = Lubepurchasem

    @method_decorator(permission_required('purchases.list_lubepurchasem'))
    def dispatch(self, *args, **kwargs):
        return super(LubepurchaseList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LubepurchaseList, self).get_context_data(**kwargs)
        context['total_purchases'] = Lubepurchasem.objects.count()
        return context


class LubepurchaseCreate(CreateView):
    template_name = 'purchases/includes/lube_purchase_create.html'
    model = Lubepurchasem
    form_class = LubepurchasemForm

    @method_decorator(permission_required('purchases.add_lubepurchasem'))
    def dispatch(self, *args, **kwargs):
        return super(LubepurchaseCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(LubepurchaseCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('lubes_purchase_detail', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        data = super(LubepurchaseCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['lubes_purchase'] = LubepurchaseFormSet(self.request.POST)
            data['total_purchases'] = Lubepurchasem.objects.count()
        else:
            data['lubes_purchase'] = LubepurchaseFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['lubes_purchase']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()
        return super(LubepurchaseCreate, self).form_valid(form)


class LubepurchaseUpdate(UpdateView):
    model = Lubepurchasem
    form_class = LubepurchasesForm
    template_name = 'purchases/includes/lube_purchase_update.html'
    success_url = '/lubes_purchases'

    @method_decorator(permission_required('purchases.change_lubepurchasem'))
    def dispatch(self, *args, **kwargs):
        return super(LubepurchaseUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(LubepurchaseUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = LubepurchasemForm
        form = self.get_form(form_class)
        lubes_purchase = LubepurchaseFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, lubes_purchase=lubes_purchase))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = LubepurchasemForm
        form = self.get_form(form_class)
        lubes_purchase = LubepurchaseFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and lubes_purchase.is_valid():
            return self.form_valid(form, lubes_purchase)
        return self.form_invalid(form, lubes_purchase)

    def form_valid(self, form, lubes_purchase):
        id = form.instance.id
        self.object = form.save()
        lubes_purchase.instance = self.object
        lubes_purchase.save()
        lp = Lubepurchases.objects.filter(lubepurchasem_id=id)
        all_products = Product.objects.filter(product_type=1)
        # general journal entry

        duty = form.cleaned_data['duty']
        amount = form.cleaned_data['net_amount']
        supplier_acc = form.cleaned_data.get('supplier', None)
        general_journal_1 = GeneralJournal(chartofaccount_id=50010005, duty_id=duty.id, voucher_type='LP',
                                           voucher_number=id, debit=amount, credit=0,
                                           naration='To supplier account')
        general_journal_1.save()
        general_journal_2 = GeneralJournal(chartofaccount_id=supplier_acc.id, duty_id=duty.id, voucher_type='LP',
                                           voucher_number=id, debit=0, credit=amount,
                                           naration='By lube purchase account')
        general_journal_2.save()
        # general journal entry end
        # Lube Stock entery
        for i in lp:
            if i.product is not None:
                user = self.request.user
                product = i.product
                product_obj = all_products.get(id=product.id)
                units_per_box = product_obj.units_per_box
                box_purchased = i.box_purchased
                units = box_purchased * units_per_box
                total_cost_price = i.amount / units
                try:
                    obj = LubesStock.objects.get(purchase_ref=id, product=product)
                    obj.qty = units
                    obj.purchased_stock = units
                    obj.save()
                except LubesStock.DoesNotExist:
                    obj = LubesStock(user=user, purchase_ref=id, product=product, date=duty.date,
                                     cost_price=total_cost_price, purchased_stock=units, qty=units)
                    obj.save()
        lpm = Lubepurchasem.objects.get(id=id)
        lpm.status = 2
        lpm.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, lubes_purchase):
        return self.render_to_response(self.get_context_data(form=form, lubes_purchase=lubes_purchase))


class GetProductPrice(ListView):
    def get(self, request, *args, **kwargs):
        id = request.GET.get('id')
        if id is not None:
            product_obj = Product.objects.filter(pk=id)
            tax_obj = Tax.objects.first()
            product = []
            item = dict()
            item["id"] = product_obj[0].id
            item["price"] = str(product_obj[0].purchase_price_per_box)
            item["units_per_box"] = str(product_obj[0].units_per_box)
            item["sum_taxes"] = str(tax_obj.sum_taxes)
            item["withholding_tax"] = str(tax_obj.withholding_tax)
            product.append(item)
            return HttpResponse(json.dumps(product))


class GetTax(ListView):
    def get(self, request, *args, **kwargs):
        tax_obj = Tax.objects.first()
        tax = []
        item = dict()
        item["sum_taxes"] = str(tax_obj.sum_taxes)
        item["withholding_tax"] = str(tax_obj.withholding_tax)
        tax.append(item)
        return HttpResponse(json.dumps(tax))


# FP tank 4 and tank 5 purchases


def fp_list(request):
    fps = FuelPurchase.objects.all()
    return render(request, 'fps/fp_list.html', {'fps': fps})


def save_fp_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            obj = form.save(commit=False)
            obj.user = request.user
            obj.save()
            data['form_is_valid'] = True
            fps = FuelPurchase.objects.all()
            data['html_fp_list'] = render_to_string('fps/includes/partial_fp_list.html', {'fps': fps})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def save_fp_form_home(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            obj = form.save(commit=False)
            obj.user = request.user
            obj.save()
            data['form_is_valid'] = True
            fps = FuelPurchase.objects.all()
            fps_count = fps.count()
            not_posted = fps.filter(status=1).count()
            data['fps_count'] = fps_count
            data['not_posted'] = not_posted
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('fps.add_fp')
def fp_create(request):
    if request.method == 'POST':
        form = FPForm(request.POST)
    else:
        form = FPForm()
    return save_fp_form(request, form, 'fps/includes/partial_fp_create.html')


@permission_required('fps.change_fp')
def fp_update(request, pk):
    fp = get_object_or_404(FuelPurchase, pk=pk)
    if request.method == 'POST':
        form = FPForm(request.POST, instance=fp)
    else:
        form = FPForm(instance=fp)
    return update_fp_form(request, form, 'fps/includes/partial_fp_update.html')


def update_fp_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            amount = form.cleaned_data['amount']
            supplier = form.cleaned_data['supplier'].id
            product = form.cleaned_data['product']
            duty = form.cleaned_data['duty']
            purchased_stock = form.cleaned_data['purchased_stock']
            general_journal_1 = GeneralJournal(
                chartofaccount=ChartOfAccount.objects.filter(product_id=product.id, subaccount_id=5001)[0],
                duty_id=duty.id,
                voucher_type='FP %s' % product.product_name,
                voucher_number=id, debit=amount, credit=0,
                naration='To supplier account')
            # SP supplier payment
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=supplier,  duty_id=duty.id,
                                               voucher_type='FP %s' % product.product_name,
                                               voucher_number=id, debit=0, credit=amount,
                                               naration='By fuel purchase account')
            general_journal_2.save()
            general_journal_3 = TankGeneralJournal(product_id=product.id, duty_id=duty.id,
                                                   voucher_type='FP', voucher_number=id,
                                                   debit=purchased_stock,
                                                   credit=0, naration='%s purchased' % product.product_name)
            general_journal_3.save()
            # general journal entry end
            fp = FuelPurchase.objects.get(id=id)
            fp.status = 2
            fp.save()
            data['form_is_valid'] = True
            fps = FuelPurchase.objects.all()
            data['html_fp_list'] = render_to_string('fps/includes/partial_fp_list.html', {'fps': fps})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('fps.delete_fp')
def fp_delete(request, pk):
    fp = get_object_or_404(FuelPurchase, pk=pk)
    data = dict()
    if request.method == 'POST':
        fp.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        fps = FuelPurchase.objects.all()
        data['html_fp_list'] = render_to_string('fps/includes/partial_fp_list.html', {
            'fps': fps
        })
    else:
        context = {'fp': fp}
        data['html_form'] = render_to_string('fps/includes/partial_fp_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


def load_tank(request):
    product_id = request.GET.get('product')
    tank = Tank.objects.filter(product_id=product_id)[0]
    return render(request, 'fps/includes/tank_dropdown_list_options.html', {'tank': tank})


def fp_create_home(request):
    if request.method == 'POST':
        form = FPForm(request.POST)
    else:
        form = FPForm()
    return save_fp_form_home(request, form, 'fps/includes/partial_fp_create.html')


def fp_home(request):
    fps = FuelPurchase.objects.all()
    fps_count = fps.count()
    not_posted = fps.filter(status=1).count()
    return render(request, 'fps/fp_home.html', {'fps_count': fps_count, 'not_posted': not_posted})


def fp_detail(request, pk):
    if request.method == 'GET':
        fp = FuelPurchase.objects.get(pk=pk)
        return render(request, 'fps/includes/fp_detail.html', {'fp': fp})


# Lorry

def get_chart_by_lorry(request):
    data = dict()
    if request.method == 'GET':
        lorry_id = request.GET.get('lorry_id')
        lorry_chamber_id = request.GET.get('lorry_chamber_id')
        chamber_dip_received = request.GET.get('chamber_dip_received')
        lorry = Lorry.objects.get(id=lorry_id)
        chamber = lorry.lorrychamber_set.all().get(id=lorry_chamber_id)
        litre = ChamberChart.objects.filter(lorry_id=lorry_id, lorrychamber_id=lorry_chamber_id,dip=chamber_dip_received)[0].litre

        data['litre'] = litre

    return JsonResponse(data)


def fuel_purchase_by_lorry_home(request):
    fpbylorry = FuelPurchasemByLorry.objects.all()
    total_purchases_by_lorry = fpbylorry.count()
    not_posted = fpbylorry.filter(status=1).count()
    return render(request, 'purchases/fuel_purchases_by_lorry_home.html',
                  {'total_purchases_by_lorry': total_purchases_by_lorry, 'not_posted': not_posted})


class FuelpurchaseByLorryList(ListView):
    template_name = 'purchases/fuel_purchases_by_lorry_list.html'
    context_object_name = 'fuel_purchases_by_lorry'
    model = FuelPurchasemByLorry

    @method_decorator(permission_required('purchases.list_fuelpurchase_by_lorry'))
    def dispatch(self, *args, **kwargs):
        return super(FuelpurchaseByLorryList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FuelpurchaseByLorryList, self).get_context_data(**kwargs)
        context['total_fuel_purchases_by_lorry'] = FuelPurchasemByLorry.objects.count()
        return context


class FuelpurchaseByLorryDetail(DetailView):
    template_name = 'purchases/includes/detail_fuel_purchase_by_lorry.html'
    context_object_name = 'fuel_purchase_by_lorry'
    model = FuelPurchasemByLorry


class FuelPurchaseByLorryCreate(CreateView):
    template_name = 'purchases/includes/fuel_purchase_by_lorry_create.html'
    model = FuelPurchasemByLorry
    form_class = FuelPurchasemByLorryForm

    @method_decorator(permission_required('purchases.add_fuelpurchasem_by_lorry'))
    def dispatch(self, *args, **kwargs):
        return super(FuelPurchaseByLorryCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FuelPurchaseByLorryCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('fuels_purchase_by_lorry_detail', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        data = super(FuelPurchaseByLorryCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['fuels_purchase_by_lorry'] = FuelPurchaseByLorryFormSet(self.request.POST)
            data['total_fuel_purchases_by_lorry'] = FuelPurchasemByLorry.objects.count()
        else:
            data['fuels_purchase_by_lorry'] = FuelPurchaseByLorryFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['fuels_purchase_by_lorry']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(FuelPurchaseByLorryCreate, self).form_valid(form)


class FuelPurchaseByLorryUpdate(UpdateView):
    model = FuelPurchasemByLorry
    form_class = FuelPurchasesByLorryForm
    template_name = 'purchases/includes/fuel_purchase_by_lorry_update.html'
    success_url = '/fuel_purchases_by_lorry'

    @method_decorator(permission_required('purchases.change_lubepurchasem_by_lorry'))
    def dispatch(self, *args, **kwargs):
        return super(FuelPurchaseByLorryUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FuelPurchaseByLorryUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = FuelPurchasemByLorryForm
        form = self.get_form(form_class)
        fuels_purchase_by_lorry = FuelPurchaseByLorryFormSet(instance=self.object)
        return self.render_to_response(
            self.get_context_data(form=form, fuels_purchase_by_lorry=fuels_purchase_by_lorry))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = FuelPurchasemByLorryForm
        form = self.get_form(form_class)
        fuels_purchase_by_lorry = FuelPurchaseByLorryFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and fuels_purchase_by_lorry.is_valid():
            return self.form_valid(form, fuels_purchase_by_lorry)
        return self.form_invalid(form, fuels_purchase_by_lorry)

    def form_valid(self, form, fuels_purchase_by_lorry):
        self.object = form.save()
        fuels_purchase_by_lorry.instance = self.object
        id = self.object.id
        fuels_purchase_by_lorry.save()
     # stock and general journal entries
        id = form.instance.id
        credit_acc = form.cleaned_data['supplier']
        net_amount = form.cleaned_data['net_amount']
        duty = form.cleaned_data['duty']
        lorry = form.cleaned_data['lorry']
        supplier = ChartOfAccount.objects.get(id=credit_acc.id).account_name
        fp = FuelPurchasesByLorry.objects.filter(fuelpurchasembylorry_id=id)
        for i in fp:
            if i.product is not None:
                # lorry stock history entry
                try:
                    lorry_stock = LorryStock.objects.filter(lorry_id=lorry.id, chamber_id=i.lorry_chamber_id, product_id=i.product_id).values('id')[0]
                    lorry_stock_obj = LorryStock.objects.get(id=lorry_stock['id'])
                    lorry_stock_obj.stock = lorry_stock_obj.stock + i.received_stock
                    lorry_stock_obj.save()
                except:
                    ls = LorryStock(lorry_id=lorry.id, chamber_id=i.lorry_chamber_id, product_id=i.product_id, stock=i.received_stock)
                    ls.save()


                lorry_stock_history = LorryStockHistory(duty_id=duty.id, lorry_id=lorry.id, chamber_id=i.lorry_chamber_id,
                                                        product_id=i.product_id, voucher_type='FP', voucher_number=id,
                                                        dip=i.chamber_dip_purchased, stock_debit=i.purchased_stock,
                                                        stock_credit=0, comment=f'{i.product.product_name} purchase')

                lorry_stock_history.save()

        # general journal entry
                if i.purchased_stock > 0:
                    general_journal_1 = GeneralJournal(chartofaccount=ChartOfAccount.objects.filter(product_id=i.product.id,subaccount_id=5001)[0], duty_id=duty.id,
                        voucher_type='FP',
                        voucher_number=id, debit=i.amount, credit=0,
                        naration=f'To {supplier}')
                    general_journal_1.save()

        general_journal_2 = GeneralJournal(chartofaccount_id=credit_acc.id, duty_id=duty.id, voucher_type='FP',
                                           voucher_number=id, debit=0, credit=net_amount,
                                           naration='By fuel purchase account')
        general_journal_2.save()
        # general journal entries end
        fpbylorry = FuelPurchasemByLorry.objects.get(id=id)
        fpbylorry.status = 2
        fpbylorry.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, fuels_purchase_by_lorry):
        return self.render_to_response(
            self.get_context_data(form=form, fuels_purchase_by_lorry=fuels_purchase_by_lorry))


class FuelPurchaseByLorryDelete(LoginRequiredMixin, DeleteView):
    template_name = 'purchases/includes/partial_fuel_purchase_by_lorry_delete.html'
    model = FuelPurchasemByLorry
    success_url = reverse_lazy('fuels_purchase_by_lorry_list')

    @method_decorator(permission_required('purchases.delete_fuelpurchase_by_lorry'))
    def dispatch(self, *args, **kwargs):
        return super(FuelPurchaseByLorryDelete, self).dispatch(*args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()

        return HttpResponseRedirect(success_url)


class GetLorryChambers(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        d = []
        id = request.GET.get('id')
        if id is not None:
            lorry = Lorry.objects.get(id=id)
            data['chamber_count'] = lorry.lorrychamber_set.all().count()
            chamber_vals = lorry.lorrychamber_set.values('id')
            for i in range(len(chamber_vals)):
                d.append(chamber_vals[i]['id'])
            # getting stock of chambers
            lorry_stock = None
            try:
                lorry_stock = LorryStock.objects.filter(lorry_id=lorry.id).values('product__product_name',
                                                                                  'chamber__chamber', 'stock')
            except:
                lorry_stock = None
            data['chambers'] = d
            data['lorry_stock'] = list(lorry_stock)

        return JsonResponse(data)


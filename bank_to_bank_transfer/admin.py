from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from bank_to_bank_transfer.models import BankToBank


class BankToBankResource(resources.ModelResource):
    class Meta:
        model = BankToBank


class BankToBankAdmin(ImportExportModelAdmin):
    resource_class = BankToBankResource
    list_display = ('created_at', 'from_bank', 'to_bank', 'description')


admin.site.register(BankToBank, BankToBankAdmin)
from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import BankToBank
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import BankToBankForm
from general_journal.models import GeneralJournal


def bank_to_bank_transfer_home(request):
    total_bank_to_bank_transfer = BankToBank.objects.count()
    return render(request, 'bank_to_bank_transfer/bank_to_bank_transfer_home.html',
                  {'total_bank_to_bank_transfer': total_bank_to_bank_transfer})


@permission_required('bank_to_bank_transfer.list_bank_to_bank_transfer')
def bank_to_bank_transfer_list(request):
    bank_to_bank_transfer = BankToBank.objects.all()
    total_bank_to_bank_transfer = BankToBank.objects.count()
    return render(request, 'bank_to_bank_transfer/bank_to_bank_transfer_list.html',
                  {'bank_to_bank_transfer': bank_to_bank_transfer,
                   'total_bank_to_bank_transfer': total_bank_to_bank_transfer})


def save_bank_to_bank_transfer_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            bank_to_bank_transfer = BankToBank.objects.all()
            data['html_fee_paid_list'] = render_to_string(
                'bank_to_bank_transfer/includes/partial_bank_to_bank_transfer_list.html', {
                    'bank_to_bank_transfer': bank_to_bank_transfer
                })
            data['total_bank_to_bank_transfer'] = BankToBank.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_bank_to_bank_transfer_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            amount = form.cleaned_data['amount']
            debit_acc = form.cleaned_data['to_bank']
            credit_acc = form.cleaned_data['from_bank']
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=debit_acc.id, duty_id=duty.id, voucher_type='BTBT',
                                               voucher_number=id,
                                               debit=amount, credit=0, naration=f'To {credit_acc} account')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=credit_acc.id, duty_id=duty.id, voucher_type='BTBT',
                                               voucher_number=id,
                                               debit=0, credit=amount, naration=f'By {debit_acc} account')
            general_journal_2.save()
            form.instance.status = 2
            form.save()
            # general journal entry end
            data['form_is_valid'] = True
            bank_to_bank_transfer = BankToBank.objects.all()
            data['html_bank_to_bank_transfer_list'] = render_to_string(
                'bank_to_bank_transfer/includes/partial_bank_to_bank_transfer_list.html', {
                    'bank_to_bank_transfer': bank_to_bank_transfer
                })
            data['total_bank_to_bank_transfer'] = BankToBank.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('bank_to_bank_transfer.add_bank_to_bank_transfer')
def bank_to_bank_transfer_create(request):
    if request.method == 'POST':
        form = BankToBankForm(request.POST)
    else:
        form = BankToBankForm()
    return save_bank_to_bank_transfer_form(request, form,
                                           'bank_to_bank_transfer/includes/partial_bank_to_bank_transfer_create.html')


@permission_required('bank_to_bank_transfer.change_bank_to_bank_transfer')
def bank_to_bank_transfer_update(request, pk):
    bank_to_bank_transfer = get_object_or_404(BankToBank, pk=pk)
    if request.method == 'POST':
        form = BankToBankForm(request.POST, instance=bank_to_bank_transfer)
    else:
        form = BankToBankForm(instance=bank_to_bank_transfer)
    return update_bank_to_bank_transfer_form(request, form,
                                             'bank_to_bank_transfer/includes/partial_bank_to_bank_transfer_update.html')


@permission_required('bank_to_bank_transfer.delete_bank_to_bank_transfer')
def bank_to_bank_transfer_delete(request, pk):
    bank_to_bank_transfer = get_object_or_404(BankToBank, pk=pk)
    data = dict()
    if request.method == 'POST':
        bank_to_bank_transfer.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='FFP').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        bank_to_bank_transfer = BankToBank.objects.all()
        data['html_bank_to_bank_transfer_list'] = render_to_string(
            'bank_to_bank_transfer/includes/partial_bank_to_bank_transfer_list.html', {
                'bank_to_bank_transfer': bank_to_bank_transfer
            })
        data['total_bank_to_bank_transfer'] = BankToBank.objects.count()
    else:
        context = {'bank_to_bank_transfer': bank_to_bank_transfer}
        data['html_form'] = render_to_string('bank_to_bank_transfer/includes/partial_bank_to_bank_transfer_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

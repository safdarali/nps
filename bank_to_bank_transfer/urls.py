from django.urls import path
from .views import bank_to_bank_transfer_list, bank_to_bank_transfer_create, bank_to_bank_transfer_update, bank_to_bank_transfer_delete, bank_to_bank_transfer_home

urlpatterns = [
    path('bank_to_bank_transfer/', bank_to_bank_transfer_list, name='bank_to_bank_transfer_list'),
    path('bank_to_bank_transfer/home', bank_to_bank_transfer_home, name='bank_to_bank_transfer_home'),
    path('bank_to_bank_transfer/create', bank_to_bank_transfer_create, name='bank_to_bank_transfer_create'),
    path('bank_to_bank_transfer/<int:pk>/update', bank_to_bank_transfer_update, name='bank_to_bank_transfer_update'),
    path('bank_to_bank_transfer/<int:pk>/delete', bank_to_bank_transfer_delete, name='bank_to_bank_transfer_delete'),
]
from django.db import models
from chart_of_accounts.models import ChartOfAccount
from django.utils import timezone

from duties.models import Duty
from nps.models import ModelWithTimeStamp

STATUS = (
    (1, 'not posted'),
    (2, 'posted')
)


class BankToBank(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    from_bank = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, verbose_name='From bank', related_name='from_bank')
    to_bank = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, verbose_name='To bank', related_name='to_bank')
    amount = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='Amount')
    description = models.TextField(verbose_name='Description')
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1)

    def __str__(self):
        return f'From {self.from_bank.account_name} To {self.to_bank.account_name}'

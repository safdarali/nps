from django.apps import AppConfig


class BankToBankTransferConfig(AppConfig):
    name = 'bank_to_bank_transfer'

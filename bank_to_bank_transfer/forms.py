from django import forms

from duties.models import Duty
from .models import BankToBank
from chart_of_accounts.models import ChartOfAccount


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld


class BankToBankForm(forms.ModelForm):
    class Meta:
        model = BankToBank
        fields = '__all__'
        exclude = ('status',)

    def __init__(self, *args, **kwargs):
        super(BankToBankForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['from_bank'].empty_label = 'From'
        self.fields['from_bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['to_bank'].empty_label = 'To'
        self.fields['to_bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)






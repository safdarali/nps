from django.apps import AppConfig


class GeneralJournalConfig(AppConfig):
    name = 'general_journal'

from django.db import models
from decimal import Decimal
from chart_of_accounts.models import SubAccount
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty
from nps.models import ModelWithTimeStamp
from products.models import Product


class GeneralJournal(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',db_index=True)
    chartofaccount = models.ForeignKey(ChartOfAccount,  db_index=True, on_delete=models.CASCADE, verbose_name='Account', null=True, blank=True, default=None)
    voucher_type = models.CharField(max_length=15,db_index=True, verbose_name='Voucher type', null=True, blank=True)
    voucher_number = models.CharField(max_length=15,db_index=True, verbose_name='Voucher number', null=True, blank=True)
    debit = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Debit', default=Decimal('0.000'))
    credit = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, verbose_name='Credit', default=Decimal('0.000'))
    naration = models.CharField(max_length=200, verbose_name='Narration')

    class Meta:
        ordering = ['created_at']


class TankGeneralJournal(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',
                             db_index=True)
    product = models.ForeignKey(Product,db_index=True, on_delete=models.CASCADE, verbose_name='Product', null=True, blank=True, default=None)
    voucher_type = models.CharField(max_length=15,db_index=True, verbose_name='Voucher type', null=True, blank=True)
    voucher_number = models.CharField(max_length=15,db_index=True, verbose_name='Voucher number', null=True, blank=True)
    debit = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, verbose_name='Debit', default=Decimal('0.000'))
    credit = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, verbose_name='Credit', default=Decimal('0.000'))
    naration = models.CharField(max_length=100, verbose_name='Narration')


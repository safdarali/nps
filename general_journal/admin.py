from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import GeneralJournal, TankGeneralJournal


class GeneralJournalResource(resources.ModelResource):
    class Meta:
        model = GeneralJournal


class GeneralJournalAdmin(ImportExportModelAdmin):
    resource_class = GeneralJournalResource
    list_display = ('created_at', 'chartofaccount', 'voucher_type', 'voucher_number','debit','credit','naration')
    list_filter = ['voucher_type', 'created_at']
    search_fields = ['chartofaccount__account_name','voucher_number']


admin.site.register(GeneralJournal, GeneralJournalAdmin)


class TankGeneralJournalResource(resources.ModelResource):
    class Meta:
        model = TankGeneralJournal


class TankGeneralJournalAdmin(ImportExportModelAdmin):
    resource_class = TankGeneralJournalResource
    list_display = ('created_at', 'product', 'voucher_type', 'voucher_number','debit','credit','naration')


admin.site.register(TankGeneralJournal, TankGeneralJournalAdmin)
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import TankChart1, TankChart2, Tank


class TankResource(resources.ModelResource):
    class Meta:
        model = Tank


class TankAdmin(ImportExportModelAdmin):
    resource_class = TankResource
    fields = ('tank_name',
              'product',
              'tank_description',
              )


admin.site.register(Tank, TankAdmin)


class TankChart1Resource(resources.ModelResource):
    class Meta:
        model = TankChart1
        fields = ('id', 'tank1', 'dip', 'litre')


class TankChart1Admin(ImportExportModelAdmin):
    resource_class = TankChart1Resource
    list_display = ['id', 'tank1', 'dip', 'litre']
    search_fields = ['=dip']


admin.site.register(TankChart1, TankChart1Admin)


class TankChart2Resource(resources.ModelResource):
    class Meta:
        model = TankChart2
        fields = ('id', 'tank2', 'dip', 'litre')


class TankChart2Admin(ImportExportModelAdmin):
    resource_class = TankChart2Resource
    list_display = ['id', 'tank2', 'dip', 'litre']
    search_fields = ['=dip']


admin.site.register(TankChart2, TankChart2Admin)



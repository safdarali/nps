from django import forms

from products.models import Product
from .models import Tank


class TankForm(forms.ModelForm):
    class Meta:
        model = Tank
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(TankForm, self).__init__(*args, **kwargs)
        self.fields['product'].empty_label = 'Select a product'
        self.fields['product'].queryset = Product.objects.filter(product_type__in=[4, 5])


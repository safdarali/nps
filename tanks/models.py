from django.db import models

from nps.models import ModelWithTimeStamp
from products.models import Product


class Tank(ModelWithTimeStamp):
    tank_name = models.CharField(max_length=50, unique=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, verbose_name='Product', null=True,
                                blank=True, default=None)
    tank_description = models.TextField()

    def __str__(self):
        return self.tank_name


class TankChart1(ModelWithTimeStamp):
    tank1 = models.ForeignKey(Tank, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Tank1')
    dip = models.IntegerField(verbose_name='Dip', default=0)
    litre = models.IntegerField(verbose_name='Litre')


class TankChart2(ModelWithTimeStamp):
    tank2 = models.ForeignKey(Tank, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Tank2')
    dip = models.IntegerField(verbose_name='Dip', default=0)
    litre = models.IntegerField(verbose_name='Litre')



from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import Tank
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import TankForm


def tank_list(request):
    tanks = Tank.objects.all()
    return render(request, 'tanks/tank_list.html', {'tanks': tanks})


def save_tank_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            tanks = Tank.objects.all()
            data['html_tank_list'] = render_to_string('tanks/includes/partial_tank_list.html', {
                'tanks': tanks
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@permission_required('tanks.add_tank')
def tank_create(request):
    if request.method == 'POST':
        form = TankForm(request.POST)
    else:
        form = TankForm()
    return save_tank_form(request, form, 'tanks/includes/partial_tank_create.html')

@permission_required('tanks.change_tank')
def tank_update(request, pk):
    tank = get_object_or_404(Tank, pk=pk)
    if request.method == 'POST':
        form = TankForm(request.POST, instance=tank)
    else:
        form = TankForm(instance=tank)
    return save_tank_form(request, form, 'tanks/includes/partial_tank_update.html')

@permission_required('tanks.delete_tank')
def tank_delete(request, pk):
    tank = get_object_or_404(Tank, pk=pk)
    data = dict()
    if request.method == 'POST':
        tank.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        tanks = Tank.objects.all()
        data['html_tank_list'] = render_to_string('tanks/includes/partial_tank_list.html', {
            'tanks': tanks
        })
    else:
        context = {'tank': tank}
        data['html_form'] = render_to_string('tanks/includes/partial_tank_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

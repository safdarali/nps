from django import forms
from django.forms.models import inlineformset_factory
from chart_of_accounts.models import ChartOfAccount
from .models import Duty, DutyMen


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld


class DutymenForm(forms.ModelForm):
    class Meta:
        model = DutyMen
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DutymenForm, self).__init__(*args, **kwargs)
        if self.instance.duty_person_id:
            self.fields['duty_person'].queryset = ChartOfAccount.objects.filter(id=self.instance.duty_person_id)
            self.fields['duty_person'].empty_label = None
        else:
            self.fields['duty_person'].queryset = ChartOfAccount.objects.filter(account_type=5)
            self.fields['duty_person'].empty_label = 'Please, choose a duty person'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


DutyFormSet = inlineformset_factory(Duty, DutyMen, form=DutymenForm, extra=1)


class DutyForm(forms.ModelForm):
    class Meta:
        model = Duty
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(DutyForm, self).__init__(*args, **kwargs)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)
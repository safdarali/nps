from django.urls import path

from expenses.views import ExpenseDutyCreate, ExpenseDutyUpdate, ExpenseDutyDelete
from .views import DutyList, DutyCreate, DutyUpdate, DutyDelete, DutyDetail, DutyLatest, LatestDutyUpdate, \
    LatestDutyDelete, LatestDutyCreate, LatestLubeSaleDelete, LatestFuelSaleDelete, LatestTankFuelSaleDelete, \
    duty_fuel_vehicle_income_create, duty_fuel_vehicle_income_update, \
    duty_fuel_vehicle_income_delete, DutySummery

urlpatterns = [
    path('duties', DutyList.as_view(), name='duty_list'),
    path('duties/create', DutyCreate.as_view(), name='duty_create'),
    path('duties/latest/create', LatestDutyCreate.as_view(), name='latest_duty_create'),
    path('duties/<int:pk>/update', DutyUpdate.as_view(), name="duty_update"),
    path('duties/<int:pk>/latest/update', LatestDutyUpdate.as_view(), name="latest_duty_update"),
    path('duties/<int:pk>/delete', DutyDelete, name="duty_delete"),
    path('duties/<int:pk>/lubes/delete', LatestLubeSaleDelete.as_view(), name="latest_lube_sale_delete"),
    path('duties/<int:pk>/fuels/delete', LatestFuelSaleDelete.as_view(), name="latest_fuel_sale_delete"),
    path('duties/<int:pk>/tankfuel/delete', LatestTankFuelSaleDelete.as_view(), name="latest_tank_fuel_sale_delete"),
    path('duties/<int:pk>/latest/delete', LatestDutyDelete, name="latest_duty_delete"),
    path('duties/<int:pk>/detail', DutyDetail.as_view(), name="duty_detail"),
    path('duties/latest/<random_duty>', DutyLatest.as_view(), name="duty_latest"),
    path('duties/duty_summery/<random_duty>', DutySummery.as_view(), name="duty_summery"),

    path('duties/fuel_vehicle_income/create', duty_fuel_vehicle_income_create, name='duty_fuel_vehicle_income_create'),
    path('duties/fuel_vehicle_income/<int:pk>/update', duty_fuel_vehicle_income_update, name='duty_fuel_vehicle_income_update'),
    path('duties/fuel_vehicle_income/<int:pk>/delete', duty_fuel_vehicle_income_delete, name='duty_fuel_vehicle_income_delete'),

    path('duties/expense/create', ExpenseDutyCreate.as_view(), name='duty_expense_create'),
    path('duties/expense/<int:pk>/update', ExpenseDutyUpdate.as_view(), name='duty_expense_update'),
    path('duties/expense/<int:pk>/delete', ExpenseDutyDelete.as_view(), name='duty_expense_delete'),
]

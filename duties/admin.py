from django.contrib import admin
from .models import Duty, DutyMen
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from duties.models import BbfHistory


class DutyMenAdmin(admin.TabularInline):
    model = DutyMen
    extra = 1


class DutyAdmin(admin.ModelAdmin):
   inlines = [DutyMenAdmin,]


admin.site.register(Duty, DutyAdmin)


class BbfHistoryResource(resources.ModelResource):
    class Meta:
        model = BbfHistory


class BbfHistoryAdmin(ImportExportModelAdmin):
    resource_class = BbfHistoryResource
    list_display = ('duty', 'bbf', 'hc')


admin.site.register(BbfHistory, BbfHistoryAdmin)
from datetime import datetime

from django.db.models import Sum
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView, DetailView, DeleteView
from django.shortcuts import get_object_or_404, redirect, render
from django.http import JsonResponse
from django.template.loader import render_to_string
from easy_pdf.views import PDFTemplateView

from cash.forms import FuelVehicleIncomeForm
from cash.models import NetSalem, CreditSalem, BankDeposition, FuelVehicleIncome, SupplierPayment
from chart_of_accounts.models import ChartOfAccount
from expenses.models import Expense, Expensem
from general_journal.models import GeneralJournal, TankGeneralJournal
from products.models import Product
from purchases.models import FuelPurchasem, FuelPurchase
from sales.models import LubeSalem, FuelSalem, TankFuelSalem, TanksLedger
from nps.custom_decorators.permission_required import permission_required
from .forms import DutyForm, DutymenForm, DutyFormSet
from .models import Duty, BbfHistory
from django.db import transaction
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


def save_fuel_vehicle_income_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            id = form.instance.id
            amount = form.cleaned_data['amount']
            vehicle = form.cleaned_data['vehicle'].id
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id, voucher_type='FVI',
                                               voucher_number=id,
                                               debit=amount, credit=0, naration='To fuel vehicle income')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=vehicle, duty_id=duty.id, voucher_type='FVI',
                                               voucher_number=id,
                                               debit=0, credit=amount, naration='By cash in hand account')
            general_journal_2.save()
            # general journal entry end
            data['form_is_valid'] = True
            latest_duty = Duty.objects.latest('id')
            fuel_vehicle_incomes = FuelVehicleIncome.objects.filter(duty_id=latest_duty.id)
            data['html_fuel_vehicle_income_list'] = render_to_string(
                'duties/includes/partial_fuel_vehicle_income_list.html', {
                    'fuel_vehicle_incomes': fuel_vehicle_incomes
                })
            data['total_fuel_vehicle_incomes'] = FuelVehicleIncome.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_fuel_vehicle_income_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            id = form.instance.id
            # general journal entry
            amount = form.cleaned_data['amount']
            vehicle = form.cleaned_data['vehicle'].id
            date = form.cleaned_data['date']
            g1 = GeneralJournal.objects.filter(voucher_type='FVI', voucher_number=id).order_by('id').first()
            g1.debit = amount
            g1.date = date
            g1.save()
            g2 = GeneralJournal.objects.filter(voucher_type='FVI', voucher_number=id).order_by('id').last()
            g2.credit = amount
            g2.chartofaccount_id = vehicle
            g2.date = date
            g2.save()
            # general journal entry end
            data['form_is_valid'] = True
            latest_duty = Duty.objects.latest('id')
            fuel_vehicle_incomes = FuelVehicleIncome.objects.filter(duty_id=latest_duty.id)
            data['html_fuel_vehicle_income_list'] = render_to_string(
                'duties/includes/partial_fuel_vehicle_income_list.html', {
                    'fuel_vehicle_incomes': fuel_vehicle_incomes
                })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


# @permission_required('cash.add_fuel_vehicle_income')
def duty_fuel_vehicle_income_create(request):
    if request.method == 'POST':
        form = FuelVehicleIncomeForm(request.POST)
    else:
        form = FuelVehicleIncomeForm()
    return save_fuel_vehicle_income_form(request, form, 'duties/includes/partial_fuel_vehicle_income_create.html')


# @permission_required('fuel_vehicle_incomes.change_fuel_vehicle_income')
def duty_fuel_vehicle_income_update(request, pk):
    fuel_vehicle_income = get_object_or_404(FuelVehicleIncome, pk=pk)
    if request.method == 'POST':
        form = FuelVehicleIncomeForm(request.POST, instance=fuel_vehicle_income)
    else:
        form = FuelVehicleIncomeForm(instance=fuel_vehicle_income)
    return update_fuel_vehicle_income_form(request, form, 'duties/includes/partial_fuel_vehicle_income_update.html')


# @permission_required('fuel_vehicle_incomes.delete_fuel_vehicle_income')
def duty_fuel_vehicle_income_delete(request, pk):
    fuel_vehicle_income = get_object_or_404(FuelVehicleIncome, pk=pk)
    data = dict()
    if request.method == 'POST':
        fuel_vehicle_income.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='FVI').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        latest_duty = Duty.objects.latest('date')
        fuel_vehicle_incomes = FuelVehicleIncome.objects.filter(duty_id=latest_duty.id)
        data['html_fuel_vehicle_income_list'] = render_to_string(
            'duties/includes/partial_fuel_vehicle_income_list.html', {
                'fuel_vehicle_incomes': fuel_vehicle_incomes
            })
    else:
        context = {'fuel_vehicle_income': fuel_vehicle_income}
        data['html_form'] = render_to_string('duties/includes/partial_fuel_vehicle_income_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# # fuel vehicle income end



def DutyDelete(request, pk):
    duty = get_object_or_404(Duty, pk=pk)
    data = dict()
    if request.method == 'POST':
        duty.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        duties = Duty.objects.all()
        data['html_duty_list'] = render_to_string('duties/includes/partial_duty_list.html', {
            'duties': duties
        })
        data['total_duties'] = Duty.objects.count()
    else:
        context = {'duty': duty}
        data['html_form'] = render_to_string('duties/includes/partial_duty_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


@permission_required('duties.delete_duty')
def LatestDutyDelete(request, pk):
    duty = get_object_or_404(Duty, pk=pk)
    data = dict()
    if request.method == 'POST':
        duty.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        d = Duty.objects.latest('date')
        data['html_duty_list'] = render_to_string('duties/includes/partial_duty_latest.html', {
            'duty': d
        })
        data['total_duties'] = Duty.objects.count()
    else:
        context = {'duty': duty}
        data['html_form'] = render_to_string('duties/includes/partial_latest_duty_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class DutyList(ListView):
    template_name = 'duties/duty_list.html'
    context_object_name = 'duties'
    model = Duty

    def get_context_data(self, **kwargs):
        context = super(DutyList, self).get_context_data(**kwargs)
        context['total_duties'] = Duty.objects.count()
        return context


class DutyCreate(CreateView):
    template_name = 'duties/includes/duty_create.html'
    model = Duty
    form_class = DutyForm

    @method_decorator(permission_required('duties.add_duty'))
    def dispatch(self, *args, **kwargs):
        return super(DutyCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(DutyCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_list')

    def get_context_data(self, **kwargs):
        data = super(DutyCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['duty'] = DutyFormSet(self.request.POST)
            data['total_duties'] = Duty.objects.count()
        else:
            data['duty'] = DutyFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['duty']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()
        return super(DutyCreate, self).form_valid(form)


class LatestDutyCreate(CreateView):
    template_name = 'duties/includes/duty_create.html'
    model = Duty
    form_class = DutyForm

    def get_form_kwargs(self):
        kwargs = super(LatestDutyCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_context_data(self, **kwargs):
        data = super(LatestDutyCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['duty'] = DutyFormSet(self.request.POST)
            data['total_duties'] = Duty.objects.count()
        else:
            data['duty'] = DutyFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['duty']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(LatestDutyCreate, self).form_valid(form)


class DutyUpdate(UpdateView):
    model = Duty
    form_class = DutymenForm
    template_name = 'duties/includes/duty_update.html'
    success_url = '/duties'

    @method_decorator(permission_required('duties.change_duty'))
    def dispatch(self, *args, **kwargs):
        return super(DutyUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(DutyUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = DutyForm
        form = self.get_form(form_class)
        duty = DutyFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, duty=duty))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = DutyForm
        form = self.get_form(form_class)
        duty = DutyFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and duty.is_valid():
            return self.form_valid(form, duty)
        return self.form_invalid(form, duty)

    def form_valid(self, form, duty):
        self.object = form.save()
        duty.instance = self.object
        duty.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, duty):
        return self.render_to_response(self.get_context_data(form=form, duty=duty))


class LatestDutyUpdate(UpdateView):
    model = Duty
    form_class = DutymenForm
    template_name = 'duties/includes/duty_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_form_kwargs(self):
        kwargs = super(LatestDutyUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = DutyForm
        form = self.get_form(form_class)
        duty = DutyFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, duty=duty))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = DutyForm
        form = self.get_form(form_class)
        duty = DutyFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and duty.is_valid():
            return self.form_valid(form, duty)
        return self.form_invalid(form, duty)

    def form_valid(self, form, duty):
        self.object = form.save()
        duty.instance = self.object
        duty.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, duty):
        return self.render_to_response(self.get_context_data(form=form, duty=duty))


class DutyDetail(DetailView):
    template_name = 'duties/includes/detail_duty.html'
    context_object_name = 'duty'
    model = Duty


class DutyLatest(ListView):
    template_name = 'duties/duty_latest.html'
    context_object_name = 'duty'
    model = Duty

    def get_queryset(self, **kwargs):
        queryset = super(DutyLatest, self).get_queryset()
        try:
            random_duty = self.kwargs.get('random_duty', None)
            queryset = queryset.get(id=random_duty)
        except:
            queryset = queryset.latest('date')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(DutyLatest, self).get_context_data(**kwargs)

        try:
            random_duty = self.kwargs.get('random_duty', None)
            latest_duty = Duty.objects.get(id=random_duty)
        except:
            latest_duty = Duty.objects.latest('date')

        context['fuel_vehicle_incomes'] = FuelVehicleIncome.objects.filter(duty_id=latest_duty.id)
        context['duty_expenses'] = Expensem.objects.filter(duty_id=latest_duty.id)
        context['duty_bank_deposition'] = BankDeposition.objects.filter(duty_id=latest_duty.id)
        context['total_duties'] = Duty.objects.count()
        context['all_duties'] = Duty.objects.all()

        return context


class LatestLubeSaleDelete(DeleteView):
    template_name = 'duties/includes/partial_latest_lube_sale_delete.html'
    model = LubeSalem

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})


class LatestFuelSaleDelete(DeleteView):
    template_name = 'duties/includes/partial_latest_fuel_sale_delete.html'
    model = FuelSalem

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    @method_decorator(permission_required('sales.delete_fuelsalem'))
    def dispatch(self, *args, **kwargs):
        return super(LatestFuelSaleDelete, self).dispatch(*args, **kwargs)


class LatestTankFuelSaleDelete(DeleteView):
    template_name = 'duties/includes/partial_latest_tank_fuel_sale_delete.html'
    model = TankFuelSalem

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    @method_decorator(permission_required('sales.delete_tankfuelsalem'))
    def dispatch(self, *args, **kwargs):
        return super(LatestTankFuelSaleDelete, self).dispatch(*args, **kwargs)


class DutySummery(PDFTemplateView):
    template_name = 'duties/duty_summery.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        try:
            random_duty = self.kwargs.get('random_duty', None)
            latest_duty = Duty.objects.get(id=random_duty)
        except:
            latest_duty = Duty.objects.latest('date')

        # ====================== VARIABLE INITIALIZED AND DATA FETCHING======================

        previous_bbf = 0
        data = dict()
        chart_of_accounts = ChartOfAccount.objects.all()
        general_journal = GeneralJournal.objects.all()


        # ====================== TANKS GAIN LOSS AND TANK SALE ======================

        start_of_month = latest_duty.date.replace(day=1)
        gain_loss_of_duty = TanksLedger.objects.filter(duty=latest_duty).values('duty__date').annotate(
            gain_loss=Sum('physical_stock') - (
                    Sum('opening_stock') + Sum('received_stock') - Sum('meter_sale') + Sum('test_reading')))
        tank_ledgers_mtd = TanksLedger.objects.filter(duty__date__range=(start_of_month, latest_duty.date)).values(
            'tank__tank_name').order_by('tank__tank_name').annotate(gain_loss=Sum('physical_stock') - (
                Sum('opening_stock') + Sum('received_stock') - Sum('meter_sale') + Sum('test_reading')))

        tanksalem = TankFuelSalem.objects.order_by('tankfuelsales__tank__tank_name').filter(duty=latest_duty, tankfuelsales__tank__product__product_type=4).values('tankfuelsales__tank__tank_name',
                                                                          'tankfuelsales__current_dip',
                                                                          'tankfuelsales__current_stock')
        for tsm in tanksalem:
            try:
                tank = gain_loss_of_duty.filter(tank__tank_name=tsm['tankfuelsales__tank__tank_name'])[0]
                tsm.update({'gain_loss': tank['gain_loss']})
            except:
                tsm.update({'gain_loss': 0})

            # ====================== add mtd ======================

        for tsm in tanksalem:
            try:
                tank = tank_ledgers_mtd.filter(tank__tank_name=tsm['tankfuelsales__tank__tank_name'])[0]
                tsm.update({'mtd': tank['gain_loss']})
            except:
                tsm.update({'mtd': 0})

        # ====================== METER SALE ======================

        fuelsalem = FuelSalem.objects.filter(duty=latest_duty)
        total_fuelsalem = fuelsalem.aggregate(total_fuelsalem=Sum('net_amount'))
        total_fuelsales_testing = fuelsalem.annotate(total_fuelsales_testing=Sum('fuelsales__test_reading'),
                                                     total_fuelsales_qty_sold=Sum('fuelsales__qty_sold')).order_by(
            'fuelsales__fuelsalem')
        fs_sums = fuelsalem.values('product__product_name').annotate(total_amount=Sum('fuelsales__amount'))
        fs_total = fs_sums.aggregate(fs_total=Sum('total_amount'))

        #  ====================== FUEL PURCHASE ======================

        fuelpurchasem = FuelPurchasem.objects.filter(duty__date=latest_duty.date).\
            values('fuelpurchases__tank__tank_name', 'fuelpurchases__purchased_stock', 'fuelpurchases__milli_short',
                   'fuelpurchases__previous_dip','fuelpurchases__previous_stock', 'fuelpurchases__current_dip', 'fuelpurchases__current_dip_stock',
                   'fuelpurchases__sale_during_decantation','fuelpurchases__stock_difference', 'fuelpurchases__amount').order_by('fuelpurchases__tank__tank_name')
        fuelpurchasem_total = fuelpurchasem.aggregate(fuelpurchasem_total=Sum('net_amount'))

        #  ====================== SUPPLIER PAYMENT ======================
        basic_data_suppliers = chart_of_accounts.filter(account_type=1,
                                                             generaljournal__duty_id=latest_duty.id,
                                                             generaljournal__voucher_type__in=['SPCP','SPCP/B'])
        basic_data_suppliers_2 = basic_data_suppliers.filter(generaljournal__duty_id=latest_duty.id, generaljournal__voucher_type__exact='SPCP')
        suppliers_ids = basic_data_suppliers.values_list('id', flat=True)
        supplier_balance = chart_of_accounts.filter(id__in=suppliers_ids,
                                                         generaljournal__voucher_type__in=['SPCP','SPCP/B']).annotate(
            balance=Sum('generaljournal__debit') - Sum('generaljournal__credit'))
        supplier_general = basic_data_suppliers.values('id', 'account_name').annotate(
            debit=Sum('generaljournal__debit'), credit=Sum('generaljournal__credit'))
        supplier_general_2 = basic_data_suppliers_2.values('id', 'account_name').annotate(
            debit=Sum('generaljournal__debit'), credit=Sum('generaljournal__credit'))
        for sg in supplier_general:
            balance = supplier_balance.filter(id=sg['id'])[0]
            sg.update({'balance': balance.balance})
        supplier_general_sums = supplier_general.aggregate(debit_sum=Sum('debit'), credit_sum=Sum('credit'))
        supplier_general_sums_2 = supplier_general_2.aggregate(debit_sum=Sum('debit'), credit_sum=Sum('credit'))
        supplier_debit_sum = supplier_general_sums['debit_sum']
        supplier_credit_sum = supplier_general_sums['credit_sum']
        supplier_debit_sum_2 = supplier_general_sums_2['debit_sum']
        #  ====================== AGENCY AND HYBRID CUSTOMER ======================
        #  ====================== Recovery ======================
        basic_data_agency = chart_of_accounts.filter(account_type=7, generaljournal__duty_id=latest_duty.id,
                                                          generaljournal__voucher_type='CR&CS')
        agency_general_recovery = basic_data_agency.values('id', 'account_name').annotate(
            credit=Sum('generaljournal__credit'))
        credit_sum_recovery = agency_general_recovery.aggregate(credit_sum=Sum('credit'))

        #  ====================== Sale ======================
        basic_data_agency_sale = chart_of_accounts.filter(account_type=7, generaljournal__duty_id=latest_duty.id)
        customers_ids = basic_data_agency_sale.values_list('id', flat=True)
        agency_balance_sale = chart_of_accounts.filter(id__in=customers_ids).annotate(
            balance=Sum('generaljournal__debit') - Sum('generaljournal__credit'))
        agency_general_sale = basic_data_agency_sale.values('id', 'account_name').annotate(
            debit=Sum('generaljournal__debit'))
        for ag in agency_general_sale:
            balance = agency_balance_sale.filter(id=ag['id'])[0]
            ag.update({'balance': balance.balance})
        agency_general_sums_sale = agency_general_sale.aggregate(debit_sum=Sum('debit'))
        debit_sum_sale = agency_general_sums_sale['debit_sum']

        #  ====================== LUBE SALE ======================

        l_sold = LubeSalem.objects.filter(duty=latest_duty)
        lube_products = Product.objects.filter(product_type=1).values('id', 'product_name').annotate(
            lubesstock_sum=Sum('lubesstock__qty'))
        for product in lube_products:
            try:
                lsd = l_sold.filter(lubesales__product_id=product['id']).values('lubesales__product_id').annotate(
                    qty_sold_sum=Sum('lubesales__qty_sold'), amount_sum=Sum('lubesales__amount'),
                    discount_sum=Sum('lubesales__discount'))[0]
                product.update({'qty_sold_sum': lsd['qty_sold_sum'], 'amount_sum': lsd['amount_sum'],
                                'opening_stock': product['lubesstock_sum'] + lsd['qty_sold_sum'],
                                'discount_sum': lsd['discount_sum']})
            except:
                product.update({'qty_sold_sum': '0', 'amount_sum': '0', 'discount_sum': '0', 'opening_stock': '0'})
        l_sale_sums = lube_products.filter(lubesales__lubesalem__duty=latest_duty).aggregate(
            total_discount_sum=Sum('lubesales__discount'), total_amount_sum=Sum('lubesales__amount'))
        total_discount_sum = l_sale_sums['total_discount_sum']
        total_amount_sum = l_sale_sums['total_amount_sum']

        # ====================== DUTY EXPENSES AND SALES EXPENSE======================

        expensem = Expensem.objects.filter(duty=latest_duty).values('expenses__description', 'expenses__amount')
        expensem_sums = expensem.aggregate(total_amount_sum=Sum('expenses__amount'))
        total_expense_amount_sum = expensem_sums['total_amount_sum']

        sales_discount_expense = general_journal.filter(chartofaccount_id=50090004, duty_id=latest_duty.id).values('naration', 'debit')
        total_sales_discount_expense = sales_discount_expense.aggregate(sum_sales_discount_expense=Sum('debit'))
        total_sales_discount_expense_sum = total_sales_discount_expense['sum_sales_discount_expense']


        #  ====================== BBF FETCHING ======================

        p_bbf = BbfHistory.objects.filter(duty_id__lt=latest_duty.id).order_by('-duty_id')[:1]
        if p_bbf.exists():
            previous_bbf = p_bbf[0].bbf

        #  ====================== GRAND TOTALS-I ======================


        grand_fuel_sale = fs_total['fs_total']
        grand_agency_recovery = credit_sum_recovery['credit_sum']
        grand_lube_sale = total_amount_sum
        if grand_fuel_sale is None:
            grand_fuel_sale = 0
        if grand_agency_recovery is None:
            grand_agency_recovery = 0
        if grand_lube_sale is None:
            grand_lube_sale = 0
        grand_cash_in =  grand_fuel_sale + grand_agency_recovery + grand_lube_sale +  previous_bbf

        #  ====================== BANK DEPOSITION ======================

        bd = BankDeposition.objects.filter(duty=latest_duty)
        bd_sums = bd.aggregate(total_amount=Sum('amount'))
        total_bd_sums = bd_sums['total_amount']

        #  ====================== GRAND TOTALS-II ======================
        if total_bd_sums is None:
            total_bd_sums = 0
        if supplier_debit_sum is None:
            supplier_debit_sum = 0
        if supplier_debit_sum_2 is None:
            supplier_debit_sum_2 = 0
        if debit_sum_sale is None:
            debit_sum_sale = 0
        if total_expense_amount_sum is None:
            total_expense_amount_sum = 0

        if total_sales_discount_expense_sum is None:
            total_sales_discount_expense_sum = 0
        # if total_fuel_equivalent is None:
        #     total_fuel_equivalent = 0
        grand_cash_out = total_bd_sums + supplier_debit_sum_2 + debit_sum_sale + \
                         total_expense_amount_sum  + total_sales_discount_expense_sum

        #  ====================== Cash in hand ======================

        cash_in_hand = grand_cash_in - grand_cash_out

        #  ====================== BBF HISTORY SAVING ======================
        last_duty = Duty.objects.last()
        if latest_duty.id == last_duty.id:
            bbfhistory, created = BbfHistory.objects.get_or_create(duty=latest_duty)
            bbfhistory.bbf = cash_in_hand
            bbfhistory.save()

        #  ====================== DATA DICTIONARY FOR TEMPLATE ======================

        data = {
            'latest_duty': latest_duty, 'tanksalem': tanksalem, 'fuelsalem': fuelsalem,
            'total_fuelsalem': total_fuelsalem['total_fuelsalem'],
            'fuelpurchasem': fuelpurchasem, 'fuelpurchasem_total': fuelpurchasem_total['fuelpurchasem_total'],
            'total_fuelsales_testing': total_fuelsales_testing,
            'total_discount_sum': total_discount_sum,
            'total_amount_sum': total_amount_sum,
            'expensem': expensem, 'total_expense_amount_sum': total_expense_amount_sum, 'sales_discount_expense':sales_discount_expense,
             'all_expenses_sum':total_expense_amount_sum + total_sales_discount_expense_sum   ,'total_sales_discount_expense_sum':total_sales_discount_expense_sum, 'fs_sums': fs_sums,
            'fs_total': fs_total['fs_total'], 'grand_cash_in': grand_cash_in,
            'bd': bd, 'total_bd_sums': total_bd_sums, 'previous_bbf': previous_bbf,
            'supplier_general': supplier_general,
            'supplier_debit_sum': supplier_debit_sum, 'supplier_credit_sum': supplier_credit_sum,'supplier_debit_sum_2':supplier_debit_sum_2,
             'grand_cash_out': grand_cash_out,
            'cash_in_hand': cash_in_hand, 'debit_sum_sale': debit_sum_sale, 'agency_general_sale': agency_general_sale,
            'lube_sales': lube_products, 'credit_sum_recovery': credit_sum_recovery['credit_sum'],
            'agency_general_recovery': agency_general_recovery

        }
        context["data"] = data

        return self.render_to_response(context)

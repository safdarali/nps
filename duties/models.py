from decimal import Decimal
from django.db import models
from chart_of_accounts.models import ChartOfAccount
from django.utils import timezone
from meters.models import Meter

# Create your models here.
from nps.models import ModelWithTimeStamp

SHIFT =(
    (1, 'Day'),
    (2, 'Night')
)
SHIFT_PARTION =(
    (11, 'Day I'),
    (12, 'Day II'),
    (21, 'Night I'),
    (22, 'Night II'),
)


class Duty(ModelWithTimeStamp):
    date = models.DateField(default=timezone.now, null=False, blank=False, verbose_name='Date', db_index=True,unique=True)
    shift = models.SmallIntegerField(null=True, blank=True, verbose_name='Shift', choices=SHIFT)
    shift_partion = models.SmallIntegerField(null=True, blank=True, verbose_name='Shift Partion', choices=SHIFT_PARTION)

    class Meta:
        unique_together = ('date', 'shift','shift_partion',)

    def __str__(self):
        return f'{self.date}, {self.get_shift_display()}, {self.get_shift_partion_display()}'


class DutyMen(models.Model):
    duty = models.ForeignKey(Duty, verbose_name='Duty', on_delete=models.CASCADE, blank=True, null=True)
    duty_person = models.ForeignKey(ChartOfAccount, verbose_name='Duty person', on_delete=models.SET_NULL, null=True, blank=True)
    duty_at = models.ForeignKey(Meter, verbose_name='Duty at', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return '%s (%s)' % (self.duty_person, self.duty_at)


class BbfHistory(ModelWithTimeStamp):
    duty = models.OneToOneField(Duty, verbose_name='Duty', on_delete=models.CASCADE, blank=True, null=True,db_index=True)
    bbf = models.DecimalField(verbose_name='Balance Brought Forward', default=Decimal('0.000'), decimal_places=3, max_digits=20,db_index=True)
    hc = models.DecimalField(verbose_name='Hard Cash', default=Decimal('0.000'), decimal_places=3, max_digits=20,db_index=True)

    def __str__(self):
        return '%s' % self.duty.date

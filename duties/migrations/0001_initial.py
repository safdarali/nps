# Generated by Django 3.0.7 on 2020-09-06 10:01

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('meters', '__first__'),
        ('chart_of_accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Duty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('date', models.DateField(db_index=True, default=django.utils.timezone.now, unique=True, verbose_name='Date')),
                ('shift', models.SmallIntegerField(blank=True, choices=[(1, 'Day'), (2, 'Night')], null=True, verbose_name='Shift')),
                ('shift_partion', models.SmallIntegerField(blank=True, choices=[(11, 'Day I'), (12, 'Day II'), (21, 'Night I'), (22, 'Night II')], null=True, verbose_name='Shift Partion')),
            ],
            options={
                'unique_together': {('date', 'shift', 'shift_partion')},
            },
        ),
        migrations.CreateModel(
            name='DutyMen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duty', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty', verbose_name='Duty')),
                ('duty_at', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='meters.Meter', verbose_name='Duty at')),
                ('duty_person', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='chart_of_accounts.ChartOfAccount', verbose_name='Duty person')),
            ],
        ),
        migrations.CreateModel(
            name='BbfHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('bbf', models.DecimalField(db_index=True, decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Balance Brought Forward')),
                ('hc', models.DecimalField(db_index=True, decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Hard Cash')),
                ('duty', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty', verbose_name='Duty')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

from django.urls import path
from .views import rent_received_list, rent_received_create, rent_received_update, rent_received_delete, rent_received_home

urlpatterns = [
    path('rent_received/', rent_received_list, name='rent_received_list'),
    path('rent_received/home', rent_received_home, name='rent_received_home'),
    path('rent_received/create', rent_received_create, name='rent_received_create'),
    path('rent_received/<int:pk>/update', rent_received_update, name='rent_received_update'),
    path('rent_received/<int:pk>/delete', rent_received_delete, name='rent_received_delete'),
]
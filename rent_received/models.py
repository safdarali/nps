from django.db import models
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty
from nps.models import ModelWithTimeStamp


class Rentreceived(ModelWithTimeStamp):
    MONTHS = (
        ('', "Select a month"),
        (1, "January"),
        (2, "February"),
        (3, "March"),
        (4, "April"),
        (5, "May"),
        (6, "June"),
        (7, "Jully"),
        (8, "August"),
        (9, "September"),
        (10, "October"),
        (11, "November"),
        (12, "December")
    )
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Duty',
                             db_index=True)
    bank = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True)
    rent_cheque_no = models.CharField(max_length=20, verbose_name='Cheques no')
    rent_receiving_date = models.DateTimeField(null=False, blank=False, verbose_name='Receiving date')
    rent_receiving_month = models.SmallIntegerField(choices=MONTHS, blank=False, verbose_name=' Rent receiving month')
    rent_bank_deposition_date = models.DateTimeField(null=False, blank=False, verbose_name='Bank deposition date')
    rent_amount = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='Rent amount')
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1, null=True, blank=True)
    class Meta:
        permissions = (('list_rentreceived', 'Can list rentreceived'),)
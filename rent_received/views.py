from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import Rentreceived
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import RentreceivedForm
from general_journal.models import GeneralJournal
from chart_of_accounts.models import ChartOfAccount


def rent_received_home(request):
    total_rent_received = Rentreceived.objects.count()
    return render(request, 'rent_receiveds/rent_received_home.html', {'total_rent_receiveds': total_rent_received})


@permission_required('rent_received.list_rentreceived')
def rent_received_list(request):
    rent_receiveds = Rentreceived.objects.all()
    total_rent_received = Rentreceived.objects.count()
    return render(request, 'rent_receiveds/rent_received_list.html', {'rent_receiveds': rent_receiveds,'total_rent_receiveds': total_rent_received})


def save_rent_received_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            rent_receiveds =  Rentreceived.objects.all()
            data['html_rent_received_list'] = render_to_string('rent_receiveds/includes/partial_rent_received_list.html', {
                'rent_receiveds': rent_receiveds
            })
            data['total_rent_receiveds'] = Rentreceived.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_rent_received_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            debit_acc = form.cleaned_data['bank'].id
            rent_amount = form.cleaned_data['rent_amount']
            duty = form.cleaned_data['duty']
            credit_acc = 40060001
            general_journal_1 = GeneralJournal(chartofaccount_id=debit_acc, duty_id=duty.id, voucher_type='RR',
                                               voucher_number=id, debit=rent_amount, credit=0,
                                               naration='To rent from company account ')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=credit_acc,  duty_id=duty.id, voucher_type='RR',
                                               voucher_number=id, debit=0, credit=rent_amount,
                                               naration='By bank account')
            general_journal_2.save()
            # general journal entry end
            rr = Rentreceived.objects.get(id=id)
            rr.status = 2
            rr.save()
            data['form_is_valid'] = True
            rent_receiveds =  Rentreceived.objects.all()
            data['html_rent_received_list'] = render_to_string('rent_receiveds/includes/partial_rent_received_list.html', {
                'rent_receiveds': rent_receiveds
            })
            data['total_rent_receiveds'] = Rentreceived.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('rent_received.add_rentreceived')
def rent_received_create(request):
    if request.method == 'POST':
        form = RentreceivedForm(request.POST)
    else:
        form = RentreceivedForm()
    return save_rent_received_form(request, form, 'rent_receiveds/includes/partial_rent_received_create.html')


@permission_required('rent_received.change_rentreceived')
def rent_received_update(request, pk):
    rent_received = get_object_or_404(Rentreceived, pk=pk)
    if request.method == 'POST':
        form = RentreceivedForm(request.POST, instance=rent_received)
    else:
        form = RentreceivedForm(instance=rent_received)
    return update_rent_received_form(request, form, 'rent_receiveds/includes/partial_rent_received_update.html')


@permission_required('rent_received.delete_rentreceived')
def rent_received_delete(request, pk):
    rent_received = get_object_or_404(Rentreceived, pk=pk)
    data = dict()
    if request.method == 'POST':
        rent_received.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='RR').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        rent_receiveds = Rentreceived.objects.all()
        data['html_rent_received_list'] = render_to_string('rent_receiveds/includes/partial_rent_received_list.html', {
            'rent_receiveds': rent_receiveds
        })
        data['total_rent_receiveds'] = Rentreceived.objects.count()
    else:
        context = {'rent_received': rent_received}
        data['html_form'] = render_to_string('rent_receiveds/includes/partial_rent_received_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

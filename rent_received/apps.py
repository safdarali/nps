from django.apps import AppConfig


class RentReceivedConfig(AppConfig):
    name = 'rent_received'

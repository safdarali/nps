from django import forms
from django.forms import HiddenInput

from duties.models import Duty
from .models import Rentreceived
from chart_of_accounts.models import ChartOfAccount


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld


class RentreceivedForm(forms.ModelForm):
    class Meta:
        model = Rentreceived
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RentreceivedForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['bank'].empty_label = 'Select a bank'
        self.fields['bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['status'].widget = HiddenInput()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)


from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from products.models import Product


class ProductResource(resources.ModelResource):
    class Meta:
        model = Product


class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource
    list_display = ('product_name',
              'product_type',
              'units_per_box',
              'purchase_price_per_box',
              'sale_price',
              'product_unit')
    search_fields = ('product_name',)


admin.site.register(Product, ProductAdmin)

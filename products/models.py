from decimal import Decimal

from django.db import models

from nps.models import ModelWithTimeStamp


class Product(ModelWithTimeStamp):
    pcs = 0
    pkt = 1
    nos = 2
    litre = 3
    kg = 4
    grams = 5
    dozen = 6

    PRODUCT_UNITS = (
        (pcs, 'pcs'),
        (pkt, 'pkt'),
        (nos, 'nos'),
        (litre, 'litre'),
        (kg, 'kg'),
        (grams, 'grams'),
        (dozen, 'dozen'),
    )
    LUBRICANT = 0
    LUBRICANTS = 1
    UNIFORM = 2
    BONJOUR = 3
    FUELS = 4
    SPECIAL_FUELS = 5

    PRODUCT_TYPE = (
        ('', 'Select product type'),
        (LUBRICANT, 'Lubricant(single)'),
        (LUBRICANTS, 'Lubricants'),
        (UNIFORM, 'Uniform'),
        (BONJOUR, 'Bonjour'),
        (FUELS, 'Fuels'),
        (SPECIAL_FUELS, 'Special Fuels')
    )
    product_name = models.CharField(max_length=50, verbose_name='Product name', unique=True)
    units_per_box = models.IntegerField(verbose_name="Units per box", default=1)
    purchase_price_per_box = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Purchase price/box' )
    sale_price = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Sale price' )

    purchase_price = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True,default=Decimal('0.000'), verbose_name='Purchase price')
    ccc_price = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True, default=Decimal('0.000'))

    product_type = models.SmallIntegerField(verbose_name='Product type', choices=PRODUCT_TYPE, null=True, blank=True)
    product_unit = models.SmallIntegerField(choices=PRODUCT_UNITS, null=True, blank=True, verbose_name="Units")

    def __str__(self):
        return self.product_name

//calculations function declaratioon

function Calculations(rowID) {
    //row calculations
    var sum = 0;
    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });
    $('#id_net_amount').val(sum);

}


function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};

$(document).ready(function () {
        // FETCHING TRANSITION SALE
    duty_id = $('.duty').val();
    $.ajax({

        url: "/sale_transition",
        data: {duty: duty_id},
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $("#sale_transition").html(data.html_sale_transition_table);

        }
    });
    // FETCHING TRANSITION SALE END
    $(".expense_account, #id_duty_person").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });
    // $(".delete-me").css({"pointer-events": "none"}).children().removeClass('btn-danger');
    $(".add-row").hide(); //hide add row button
    $("#id_net_amount").prop('readonly', true);

    // tl_no select change action


    $(document).on('focus', '.amount, #id_duty_person, .expense_account', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });


    $(document).on('blur', '.amount, #id_duty_person, .expense_account', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });


    $(document).on('click', ".amount", function () {
        this.select();
    });


    $(document).on("keyup", ".amount", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });

    $(document).on("change", ".expense_account", function () {
        var trid;
        var p_id;
        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        if (Obj.find_product(p_id) == undefined) {
            $(".dynamic-form-add .add-row").click();
            $('.expense_account').chosen({
                search_contains: true,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
            //calculations function called
            Calculations(trid);
            window.scrollBy(0, 60);
            $(".product").chosen({
                search_contains: true,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
            // to disable fields
            $(".expense_account").not(':last').prop("disabled", true).trigger("chosen:updated");
            Obj.add_row(trid, p_id);
            // enable disable
            $(".amount:last").prop('readonly', true);
            $("#" + trid + " .amount").prop('readonly', false);
            // enable disable end
        } else {
            //calculations function called

            sweetAlert({
                title: "Oops!",
                text: "You have already selected this account!",
                type: "error"
            });
            Calculations(Obj.find_product(p_id).row);
            $(".expense_account:last")[0].selectedIndex = 0;
            $(".expense_account:last").trigger("chosen:updated");
        }
    });

    // delete object of row from Obj
    $('.delete-row').click(function () {
        var trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        Calculations();
        Obj.remove_row(trid);
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".expense_account").prop("disabled", false).trigger("chosen:updated");
        // var TF = $('#id_fuelpurchases_set-TOTAL_FORMS').val();
        // $('#id_fuelpurchases_set-TOTAL_FORMS').val(TF - 1);
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs


});

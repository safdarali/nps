$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-t5_income").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-t5_income .modal-content").html(data.html_form);
                 $("#id_bank,#id_t5_income_account").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                     $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#t5_income-table tbody").html(data.html_t5_income_list);
                    $("#modal-t5_income").modal("hide");
                     $("#total_t5_incomes").html(data.total_t5_incomes);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-t5_income .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-t5_income").click(loadForm);
    $("#modal-t5_income").on("submit", ".js-t5_income-create-form", saveForm);

    // Update book
    $("#t5_income-table").on("click", ".js-update-t5_income", loadForm);
    $("#modal-t5_income").on("submit", ".js-t5_income-update-form", saveForm);

    // Delete book
    $("#t5_income-table").on("click", ".js-delete-t5_income", loadForm);
    $("#modal-t5_income").on("submit", ".js-t5_income-delete-form", saveForm);

});


$(function () {
     var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-permissions").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-permissions .modal-content").html(data.html_form);
            }
        });
    };

    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube-sale-table tbody").html(data.html_latest_lube_sale);
                    $("#modal-delete").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    // Delete
    $("#lube-sale-table").on("click", ".js-delete-latest-lube_sale", load_delete_Form);
    $("#duty-table").on("submit", ".js-delete-latest-lube_sale-form", save_delete_Form);

     $(".js-permissions").click(loadForm);
      // Update book
    $("#bank_deposition-table").on("click", ".js-update-bank_deposition", loadForm);
    // Delete book
    $("#bank_deposition-table").on("click", ".js-delete-bank_deposition", loadForm);
});




$(function () {

    /* Functions */
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-club_card_income").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-club_card_income .modal-content").html(data.html_form);
                     //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
                $("#id_club_card_income_type, #id_club_card_income_unit").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#club_card_income-table tbody").html(data.html_club_card_income_list);
                    $("#modal-club_card_income").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-club_card_income .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-club_card_income").click(loadForm);
    $("#modal-club_card_income").on("submit", ".js-club_card_income-create-form", saveForm);

    // Update book
    $("#club_card_income-table").on("click", ".js-update-club_card_income", loadForm);
    $("#modal-club_card_income").on("submit", ".js-club_card_income-update-form", saveForm);

    // Delete book
    $("#club_card_income-table").on("click", ".js-delete-club_card_income", loadForm);
    $("#modal-club_card_income").on("submit", ".js-club_card_income-delete-form", saveForm);

});
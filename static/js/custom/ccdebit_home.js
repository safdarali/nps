$(function () {


    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-ccdebit").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-ccdebit .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_ccdebits").prop('readonly', true);
                $("#id_product, #id_customer").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                $('#id_date').datetimepicker({
        format: "Y-m-d h:m:s",
        formatTime: 'h:i a',
        onShow: function (ct) {

        },
        timepicker: true
    });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#ccdebit-table tbody").html(data.html_ccdebit_list);
                    $("#total_ccdebits").html(data.total_ccdebits);
                    $("#modal-ccdebit").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-ccdebit .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-ccdebit").click(loadForm);
    $("#modal-ccdebit").on("submit", ".js-ccdebit-create-form", saveForm);

    // Update book
    $("#ccdebit-table").on("click", ".js-update-ccdebit", loadForm);
    $("#modal-ccdebit").on("submit", ".js-ccdebit-update-form", saveForm);

    // Delete book
    $("#ccdebit-table").on("click", ".js-delete-ccdebit", loadForm);
    $("#modal-ccdebit").on("submit", ".js-ccdebit-delete-form", saveForm);

});
$(function () {


    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-hybrid_customer_cash_received").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-hybrid_customer_cash_received .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_bank, #id_customer, #id_paymentmethod, #id_duty").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                // payment method
                $("#id_bank").prop('disabled', true).trigger("chosen:updated");
                $("#id_clubcard_tax").prop('readonly', true);
                $('#id_paymentmethod').change(function () {
                    if ($(this).val() == 1) {
                        $("#id_bank").prop('disabled', true).trigger("chosen:updated");
                        $("#id_clubcard_tax").prop('readonly', true);
                    } else if ($(this).val() == 2) {
                        $("#id_bank").prop("disabled", false).trigger("chosen:updated");
                        $("#id_clubcard_tax").prop('readonly', true);
                    } else if ($(this).val() == 3) {
                        $("#id_clubcard_tax").prop('readonly', false);
                        $("#id_bank").prop('disabled', true).trigger("chosen:updated");
                    }
                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#hybrid_customer_cash_received-table tbody").html(data.html_hybrid_customer_cash_received_list);
                    $("#total_hybrid_customer_cash_receiveds").html(data.total_hybrid_customer_cash_receiveds);
                    $("#modal-hybrid_customer_cash_received").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-hybrid_customer_cash_received .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-hybrid_customer_cash_received").click(loadForm);
    $("#modal-hybrid_customer_cash_received").on("submit", ".js-hybrid_customer_cash_received-create-form", saveForm);

    // Update book
    $("#hybrid_customer_cash_received-table").on("click", ".js-update-hybrid_customer_cash_received", loadForm);
    $("#modal-hybrid_customer_cash_received").on("submit", ".js-hybrid_customer_cash_received-update-form", saveForm);

    // Delete book
    $("#hybrid_customer_cash_received-table").on("click", ".js-delete-hybrid_customer_cash_received", loadForm);
    $("#modal-hybrid_customer_cash_received").on("submit", ".js-hybrid_customer_cash_received-delete-form", saveForm);

});
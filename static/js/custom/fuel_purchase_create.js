//calculations function declaratioon
function Calculations() {
    //row calculations
    var sum = 0;
    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });
    $('#id_net_amount').val(parseFloat(sum).toFixed(3));
}


function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};

$(document).ready(function () {
    $("#id_supplier, .product, #id_tanklorry, #id_tl_decanted_by, #id_tl_cleared_by").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });
    $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
    // $(".delete-me").css({"pointer-events": "none"}).children().removeClass('btn-danger');
    $(".add-row").hide(); //hide add row button
    $("#id_net_amount, #id_tl_reporting_time, #id_tl_decantation_time, #id_tl_checkout_time, #id_tl_carriage_name, #id_tl_driver_name, #id_tl_driver_phone").prop('readonly', true);


    //timepicker
    $('#id_tl_reporting_time, #id_tl_decantation_time, #id_tl_checkout_time').mdtimepicker({

        // format of the time value (data-time attribute)
        timeFormat: 'hh:mm:ss.000',

        // format of the input value
        format: 'h:mm tt',

        // theme of the timepicker
        // 'red', 'purple', 'indigo', 'teal', 'green'
        theme: 'blue',

        // determines if input is readonly
        readOnly: false,

        // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
        hourPadding: false

    });

    // tank lorry modal opening
    $(".btn_new_tl").click(function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-tl").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-tl .modal-content").html(data.html_form);
            }
        });


    });
    // tank lorry save

    var saveTLForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#modal-tl").modal("hide");
                    var html = '<option value="' + data.tl_id + '">' + data.tl_no + '</option>';
                    $('select[name="tanklorry"]').append(html);
                    $("#id_tanklorry").val(data.tl_id).trigger("chosen:updated");
                    $("#id_tl_carriage_name").val(data.tl_carriage_name);
                    $("#id_tl_driver_phone").val(data.tl_driver_phone);
                    $("#id_tl_driver_name").val(data.tl_driver_name);

                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-tl .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };
    $("#modal-tl").on("submit", ".js-tl-create-form", saveTLForm);
    // tl_no select change action


    $("#id_tanklorry").change(function () {
        var tanklorry;
        tanklorry = $(this).val();
        $.ajax({
            url: '/tank_lorries/get_tl_info',
            data: {tanklorry: tanklorry},
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $("#id_tl_carriage_name").val(data.tl_carriage_name);
                $("#id_tl_driver_phone").val(data.tl_driver_phone);
                $("#id_tl_driver_name").val(data.tl_driver_name);
            },
            error: function (data) {
                //message
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Some error occurred!',
                    showConfirmButton: true,
                });
                $("#id_tl_carriage_name").val('');
                $("#id_tl_driver_phone").val('');
                $(this).focus();
            }
        });

    });
    // current element bg color

    $(document).on('focus', '#id_date, #id_supplier, .qty_purchased, .product, .price, .tl_chamber, .tl_dip, tl_dip_observed, .tank, .previous_dip, .current_dip, .previous_stock, .purchased_stock, .sale_during_decantation, .current_dip_stock, .stock_difference, .physical_stock, .amount, #id_supplier_invoice_no, #id_date, #id_tl_reporting_time,#id_tl_decantation_time,#id_tl_checkout_time ', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });


    $(document).on('blur', '#id_date, #id_supplier, .qty_purchased, .product, .price, .tl_chamber, .tl_dip, tl_dip_observed,.tank, .previous_dip, .current_dip, .previous_stock, .purchased_stock, .sale_during_decantation, .current_dip_stock, .stock_difference, .physical_stock, .amount, #id_supplier_invoice_no,#id_date, #id_tl_reporting_time,#id_tl_decantation_time,#id_tl_checkout_time ', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });


    $(document).on('click', ".qty_purchased, .price", function () {
        this.select();
    });

    var tank;
    var milli_short = 0;
    var previous_dip;
    var current_dip;
    var previous_stock = 0.0;
    var purchased_stock = 0.0;
    var sale_during_decantation = 0.0;
    var current_dip_stock = 0.0;
    var stock_difference = 0.0;
    var physical_stock = 0.0;
    var trid;
    var product_id;
    var tl_dip = 0;
    var tl_dip_observed = 0;
    var sum = 0;
    $(document).on('keyup change', '.previous_dip', function () {
        trid = $(this).parents("tr").attr("id");
        previous_dip = $(this).val();
        tank = $("#" + trid + " .tank").val();
        if (tank == "") {
            $("#" + trid + " .tank").focus();
            //message
            swal({
                position: 'center',
                type: 'error',
                title: 'Select required tank',
                showConfirmButton: true,
            });
        } else if (previous_dip > 4) {

            $.ajax({
                url: '/fuel_purchases/get_chart',
                data: {tank: tank, previous_dip: previous_dip},
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#" + trid + " .previous_stock").val(data.litre);
                    previous_stock = $("#" + trid + " .previous_stock").val() || 0;
                    purchased_stock = $("#" + trid + " .purchased_stock").val() || 0;
                    current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
                    sale_during_decantation = $("#" + trid + " .sale_during_decantation").val() || 0;
                    physical_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
                    $("#" + trid + " .current_dipless_stock").val(physical_stock);
                    stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
                    $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
                },
                error: function (data) {
                    $(".current_dip_stock").val(0);
                    swal({
                        title: "Oops!",
                        text: "No any data found!",
                        type: "error"
                    });
                }
            });

        } else {
            $("#" + trid + " .previous_stock").val(0);
            previous_stock = $("#" + trid + " .previous_stock").val() || 0;
            purchased_stock = $("#" + trid + " .purchased_stock").val() || 0;
            current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
            sale_during_decantation = $("#" + trid + " .sale_during_decantation").val() || 0;
            physical_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
            $("#" + trid + " .current_dipless_stock").val(physical_stock);
            stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
            $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
        }

    });

    //tank change action
    $(document).on('change', '.tank', function () {
        trid = $(this).parents("tr").attr("id");
        tank = $(this).val();
        previous_dip = $("#" + trid + " .previous_dip").val();
        $.ajax({
            url: '/fuel_purchases/get_chart',
            data: {tank: tank, previous_dip: previous_dip},
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $("#" + trid + " .previous_stock").val(data.litre);
                $("#" + trid + " .previous_dip").focus();
                previous_stock = $("#" + trid + " .previous_stock").val() || 0;
                purchased_stock = $("#" + trid + " .purchased_stock").val() || 0;
                current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
                sale_during_decantation = $("#" + trid + " .sale_during_decantation").val() || 0;
                physical_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
                $("#" + trid + " .current_dipless_stock").val(physical_stock);
                stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
                $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
            },
            error: function (data) {
                $("#" + trid + " .previous_stock").val(0);
            }
        });

    });
    //purchased stock

    $(document).on('keyup', '.purchased_stock', function () {
        trid = $(this).parents("tr").attr("id");
        previous_stock = $("#" + trid + " .previous_stock").val() || 0;
        purchased_stock = $(this).val() || 0;
        current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
        sale_during_decantation = $("#" + trid + " .sale_during_decantation").val() || 0;
        physical_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
        $("#" + trid + " .current_dipless_stock").val((physical_stock).toFixed(1));
        stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
        $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
    });
    //dip after decantation

    $(document).on('keyup change', '.current_dip', function () {
        trid = $(this).parents("tr").attr("id");
        current_dip = $(this).val();
        tank = $("#" + trid + " .tank").val();
        if (tank == "") {
            $("#" + trid + " .tank").focus();
            //message
            swal({
                position: 'center',
                type: 'error',
                title: 'Select required tank',
                showConfirmButton: true,
            });
        } else if (current_dip > 4) {
            $.ajax({
                url: '/fuel_purchases/get_chart',
                data: {tank: tank, previous_dip: current_dip},
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#" + trid + " .current_dip_stock").val(data.litre);
                    previous_stock = $("#" + trid + " .previous_stock").val() || 0;
                    purchased_stock = $("#" + trid + " .purchased_stock").val() || 0;
                    current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
                    sale_during_decantation = $("#" + trid + " .sale_during_decantation").val() || 0;
                    stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
                    $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
                },
                error: function (data) {
                    $("#" + trid + " .current_dip_stock").val(0);
                    swal({
                        title: "Oops!",
                        text: "No any data found!",
                        type: "error"
                    });
                }
            });

        } else {
            $("#" + trid + " .current_dip_stock").val(0);
            previous_stock = $("#" + trid + " .previous_stock").val() || 0;
            purchased_stock = $("#" + trid + " .purchased_stock").val() || 0;
            current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
            sale_during_decantation = $("#" + trid + " .sale_during_decantation").val() || 0;
            stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
            $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
        }
    });
    //sale during decantations

    $(document).on('keyup change', '.sale_during_decantation', function () {
        trid = $(this).parents("tr").attr("id");
        previous_stock = $("#" + trid + " .previous_stock").val() || 0;
        purchased_stock = $("#" + trid + " .purchased_stock").val() || 0;
        current_dip_stock = $("#" + trid + " .current_dip_stock").val() || 0;
        sale_during_decantation = $(this).val() || 0;
        stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
        $("#" + trid + " .stock_difference").val((stock_difference).toFixed(1));
    });

    //millisho calculation


    $(document).on('keyup change', '.tl_dip', function () {
        trid = $(this).parents("tr").attr("id");
        tl_dip = $(this).val() || 0;
        tl_dip_observed = $("#" + trid + " .tl_dip_observed").val() || 0;
        milli_short = tl_dip - tl_dip_observed;
        $("#" + trid + " .milli_short").val(milli_short);
    });

    $(document).on('keyup change', '.tl_dip_observed', function () {
        trid = $(this).parents("tr").attr("id");
        tl_dip_observed = $(this).val() || 0;
        tl_dip = $("#" + trid + " .tl_dip").val() || 0;
        milli_short = tl_dip - tl_dip_observed;
        $("#" + trid + " .milli_short").val(milli_short);
    });

    $(document).on("change", ".product", function () {

        product_id = $(this).val();
        trid = $(this).parents("tr").attr("id");
        //adding new row
        $(".dynamic-form-add .add-row").click();
        //calculations function called
        window.scrollBy(0, 60);
        $(".product").chosen({
            search_contains: true,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });

        Obj.add_row(trid, product_id);
        // $(".delete-me:last").css({"pointer-events": "none"}).children().removeClass('btn-danger');
        // $(".delete-me").not(':last').css({"pointer-events": "auto"}).children().addClass('btn-danger');
        //to change bg color of last product and barcode
        $(".chosen-single").css({'background-color': 'rgb(250,250,250)'});
        $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
        $(".product").not(':last').prop("disabled", true).trigger("chosen:updated");
        $(".amount, .sale_during_decantation, .current_dip, .purchased_stock, .previous_dip, .tl_chamber, .tl_dip, .tl_dip_observed").attr('readonly', false);
        $(".amount:last, .sale_during_decantation:last,.current_dip:last, .purchased_stock:last,.previous_dip:last, .tl_chamber:last,.tl_dip:last, .tl_dip_observed:last").attr('readonly', true);
        Calculations();
    });

    // delete object of row from Obj
    $('.delete-row').click(function () {
        trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        Calculations();
        Obj.remove_row(trid);
    });

    $(document).on("keyup", ".amount", function () {
        Calculations();
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        var TF = $('#id_fuelpurchases_set-TOTAL_FORMS').val();
        $('#id_fuelpurchases_set-TOTAL_FORMS').val(TF - 1);
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

  $('#id_date, #id_invoice_date').datetimepicker({
        format: "Y-m-d h:m:s",
        formatTime: 'h:i a',
        onShow: function (ct) {

        },
        timepicker: true
    });

    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

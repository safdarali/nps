// My custom code

$('.formset_row').formset({
    addText: '<i class="fa fa-plus btn btn-sm btn-primary" aria-hidden="true"></i>',
    deleteText: '<i class="fa fa-trash-o btn btn-sm btn-danger" aria-hidden="true"></i>',
    prefix: 'uniformorders_set'
});

// current element bg color

$(function () {
    $(document).on('focus', '.product', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '.product', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('focus', '.qty_ordered', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});
$(function () {
    $(document).on('blur', '.qty_ordered', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('focus', '#id_amount_paid', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});
$(function () {
    $(document).on('blur', '#id_amount_paid', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('focus', '.price', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});
$(function () {
    $(document).on('blur', '.price', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('focus', '#id_date', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});
$(function () {
    $(document).on('blur', '#id_date', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('focus', '#id_invoice_amount', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});
$(function () {
    $(document).on('blur', '#id_invoice_amount', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});


//calculations function declaratioon
function Calculations(rowID) {
    //row calculations
    var qty_ordered;
    var sum = 0;
    var invoice_amount;
    var balance;
    var amount_paid;
    var price;
    var net;
    var previous_balance;

    qty_ordered = $("#" + rowID + " .qty_ordered").val();
    price = $("#" + rowID + " .price").val();
    net = parseInt(qty_ordered) * parseFloat(price).toFixed(3);

    $("#" + rowID + " .net").val(parseFloat(net).toFixed(3));
    $(".net").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });

    //sum of net amount on detail form end
    $('#id_amount_paid').val(parseFloat(sum).toFixed(3));
    previous_balance = $('#id_previous_balance').val();
    amount_paid = $('#id_amount_paid').val();
    invoice_amount = $('#id_invoice_amount').val();
    balance = (parseFloat(amount_paid) + parseFloat(previous_balance)) - parseFloat(invoice_amount);
    $('#id_balance').val(parseFloat(balance).toFixed(3));

}

// on keypup calculations
function SubCalculations(rowID) {
    var qtysold;
    var price;
    var d_p;
    var d;
    var net;
    d_p = $("#" + rowID + " .discount").val();
    qtysold = $("#" + rowID + " .qtysold").val();
    price = $("#" + rowID + " .price").val();
    net = parseInt(qtysold) * parseFloat(price).toFixed(3);
    d = net * (d_p / 100) || 0.000;
    $("#" + rowID + " .discount_cur").val(parseFloat(d).toFixed(3));

}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {
    // add row arrays in Obj
    var row_id;
    var product_id;
    $(".net").not(':last').each(function () {
        row_id = $(this).parents("tr").attr("id");
        product_id = $("#" + row_id + " .product").val();
        Obj.add_row(row_id, product_id);
    });
    // end arrays adding
    $(".add-row").hide(); //hide add row button
    $("#id_balance").prop('readonly', true);
    $("#id_previous_balance").prop('readonly', true);
    $("#id_amount_paid").prop('readonly', true);
    $('.delete-row').not(':last').css('display', 'none');
    $("#id_balance").css({'background-color': 'rgb(236, 239, 244)', 'border-color': '#838383'});
    $("#id_supplier").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});
    $(".product").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});
    $(".bank").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});

    $(document).on("click", ".price:last, .qty_ordered:last, .net:last", function () {
        if ($('.add-row').css('display') == 'none') {
            $(this).prop('readonly', true);
        }
    });

    $(document).on("change", ".product", function () {
        var p_id;
        var trid;
        var qty_ordered = 2;
        var price;
        var prePopulate;
        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        $.ajax({
            url: '/uniform_orders/get_price?id=' + p_id,
            type: 'GET',
            success: function (data) {
                if (Obj.find_product(p_id) == undefined) {
                    prePopulate = $.parseJSON(data);
                    $("#" + trid + " .qty_ordered").val(qty_ordered);
                    $("#" + trid + " .price").val(prePopulate[0].price);
                    //calculations function called
                    Calculations(trid);
                    //adding new row
                    $(".dynamic-form-add .add-row").click();
                    window.scrollBy(0, 60);
                    $(".product").chosen({
                        search_contains: true,
                        no_results_text: "Oops, nothing found!",
                        width: "100%"
                    });
                    // to disable fields
                    $(".net").prop('readonly', true);
                    $(".product").not(':last').prop("disabled", true).trigger("chosen:updated");
                    //to change bg color of last product and barcode
                    $(".chosen-single").css({'background-color': 'rgb(250,250,250)'});
                    $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
                    Obj.add_row(trid, p_id);
                    // enable disable
                    $(".price:last").prop('readonly', true);
                    $(".qty_ordered:last").prop('readonly', true);
                    $("#" + trid + " .price").prop('readonly', false);
                    $("#" + trid + " .qty_ordered").prop('readonly', false);
                    // enable disable end
                } else {
                    qty_ordered = $("#" + Obj.find_product(p_id).row + " .qty_ordered").val();
                    $("#" + Obj.find_product(p_id).row + " .qty_ordered").val(parseInt(qty_ordered) + 1);

                    //calculations function called
                    Calculations(Obj.find_product(p_id).row);
                    $(".product:last").val('').trigger("chosen:updated");
                }

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });

    });
    // delete object of row from Obj
    $('.delete-row').click(function () {
        var sum = 0;
        var trid = $(this).parents("tr").attr("id");
        var p_id = $("#" + trid + " .product").val();
        $("#" + trid + " .net").removeClass('net');
        $(".net").each(function () {
            sum += parseFloat($(this).val()) || 0;
        });
        //sum of net amount on detail form end
        $('#id_amount_paid').val(parseFloat(sum).toFixed(3));
        var previous_balance = $('#id_previous_balance').val();
        var amount_paid = $('#id_amount_paid').val();
        var invoice_amount = $('#id_invoice_amount').val();
        var balance = (parseFloat(amount_paid) + parseFloat(previous_balance)) - parseFloat(invoice_amount);
        $('#id_balance').val(parseFloat(balance).toFixed(3));
        Obj.remove_row(trid);
        var new_trid = trid - 1;
        $("#" + new_trid + " .delete-row").css('display', 'block');
         $(".add-row:last").show();
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });
    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });

    // balance calculation
    $(document).on("keyup", "#id_invoice_amount", function () {
        var amount_paid = 0;
        var invoice_amount = 0;
        var balance = 0;
        var previous_balance = 0;
        previous_balance = $('#id_previous_balance').val();
        amount_paid = $('#id_amount_paid').val();
        invoice_amount = $('#id_invoice_amount').val();
        balance = (parseFloat(amount_paid) + parseFloat(previous_balance)) - parseFloat(invoice_amount);
        $('#id_balance').val(parseFloat(balance).toFixed(3));

    });

    //price and quantity multiplication
    $(document).on("keyup", ".qty_ordered", function () {
        var trid = $(this).parents("tr").attr("id");
        SubCalculations(trid);
        Calculations(trid);
    });

    //price calculations
    $(document).on("keyup", ".price", function () {
        var trid = $(this).parent().parent("tr").attr("id");
        var qtysold;
        var price;
        var d_p;
        var d;
        var net;
        d_p = $("#" + trid + " .discount").val();
        qtysold = $("#" + trid + " .qtysold").val();
        price = $("#" + trid + " .price").val();
        net = parseInt(qtysold) * parseFloat(price).toFixed(3);
        d = net * (d_p / 100) || 0.000;
        $("#" + trid + " .discount_cur").val(parseFloat(d).toFixed(3));

        Calculations(trid);

        var p_id = $("#" + trid + " .product").val();

        $.ajax({
            url: '/sorders/soprice?id=' + p_id,
            type: 'GET',
            success: function (data) {

                var prePopulate = $.parseJSON(data);
                var pp = prePopulate[0].purchase_price;
                if (parseFloat(price) < parseFloat(pp)) {
                    sweetAlert({
                        title: "Oops!",
                        text: "Selling price can't be less than purchase price !",
                        type: "error"
                    });

                    d_p = $("#" + trid + " .discount").val();
                    qtysold = $("#" + trid + " .qtysold").val();
                    price = $("#" + trid + " .price").val();
                    net = parseInt(qtysold) * parseFloat(price).toFixed(3);
                    d = net * (d_p / 100) || 0.000;
                    $("#" + trid + " .discount_cur").val(parseFloat(d).toFixed(3));

                    Calculations(trid);
                }

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });

    });

    // customer balance
    $(document).on("change", "#id_supplier", function () {
        var balance;
        var amount_paid;
        var invoice_amount;
        var previous_balance;
        var prePopulate;
        id = $(this).val();

        $.ajax({
            url: '/uniform_orders/previous_balance?id=' + id,
            type: 'GET',
            success: function (data) {
                prePopulate = $.parseJSON(data);
                previous_balance = prePopulate[0].balance;
                $('#id_previous_balance').val(previous_balance);
                amount_paid = $('#id_amount_paid').val();
                invoice_amount = $('#id_invoice_amount').val();
                balance = (parseFloat(previous_balance) + parseFloat(amount_paid)) - parseFloat(invoice_amount);
                $('#id_balance').val(balance);
            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any data found!",
                    type: "error"
                });
            }
        });
    });


});

$(function () {
    var sales_tax;
    var extra_tax;
    var further_tax;
    var sum_taxes;
    /* Functions */

    $(document).on('keyup', '#id_sales_tax, #id_extra_tax, #id_further_tax', function () {
        sales_tax = parseFloat($('#id_sales_tax').val()) ||0;
        extra_tax =  parseFloat($('#id_extra_tax').val()) ||0;
        further_tax =  parseFloat($('#id_further_tax').val()) ||0;
        sum_taxes =  sales_tax + extra_tax + further_tax;
        $('#id_sum_taxes').val(sum_taxes);
    });

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-tax").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-tax .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_taxes").prop('readonly', true);
                $("#id_product").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#button_create_tax").html(data.tax);
                    $("#tax-table tbody").html(data.html_tax_list);
                    $("#modal-tax").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-tax .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-tax").click(loadForm);
    $("#modal-tax").on("submit", ".js-tax-create-form", saveForm);

    // Update book
    $("#tax-table").on("click", ".js-update-tax", loadForm);
    $("#modal-tax").on("submit", ".js-tax-update-form", saveForm);

    // Delete book
    $("#tax-table").on("click", ".js-delete-tax", loadForm);
    $("#modal-tax").on("submit", ".js-tax-delete-form", saveForm);

});
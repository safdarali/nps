$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-bank_expense").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-bank_expense .modal-content").html(data.html_form);
                $("#id_bank, #id_supplier, #id_fee_paying_month").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });

                $('#id_amount').keyup(function () {
                    var from_bank = $('#id_from_bank').val();
                    if (from_bank == '') {
                        $(this).val('');
                        //message
                        swal({
                            position: 'center',
                            type: 'success',
                            title: 'Select a bank!',
                            showConfirmButton: true,
                        })
                    }
                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#bank_expense-table tbody").html(data.html_bank_expense_list);
                    $("#modal-bank_expense").modal("hide");
                    $("#total_bank_expense").html(data.total_bank_expense);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-bank_expense .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-bank_expense").click(loadForm);
    $("#modal-bank_expense").on("submit", ".js-bank_expense-create-form", saveForm);

    // Update book
    $("#bank_expense-table").on("click", ".js-update-bank_expense", loadForm);
    $("#modal-bank_expense").on("submit", ".js-bank_expense-update-form", saveForm);

    // Delete book
    $("#bank_expense-table").on("click", ".js-delete-bank_expense", loadForm);
    $("#modal-bank_expense").on("submit", ".js-bank_expense-delete-form", saveForm);

});
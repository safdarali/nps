// current element bg color

$(function () {
    $(document).on('focus', '.account, .amount', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '.account, .amount', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

function Calculations(rowID) {
    //row calculations
    var sum = 0;
    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });

    $('#id_net_amount').val(parseFloat(sum).toFixed(3));
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {

    $(".add-row").hide(); //hide add row button
    // $(".delete-me").css({"pointer-events": "none"}).children().removeClass('btn-danger');
    ; //hide add row button
    $(".amount, #id_net_amount").attr('readonly', true); //hide add row button
    $(".account").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});
    $("#id_duty_person, .product").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });


    //discounts in currency calculations
    $(document).on("keyup", ".amount", function () {
        var trid;
        trid = $(this).parents("tr").attr("id");

        Calculations(trid);
        // check on qtysold
    });
    // FETCHING TRANSITION SALE
    duty_id = $('.duty').val();
    $.ajax({

        url: "/sale_transition",
        data: {duty: duty_id},
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $("#sale_transition").html(data.html_sale_transition_table);
            
        }
    });
    // FETCHING TRANSITION SALE END


    $(document).on("change", ".account", function () {
        var trid;
        var account_id;
        account_id = $(this).val();
        trid = $(this).parents("tr").attr("id");
        // if (Obj.find_product(account_id) == undefined) {
            //adding new row
            $(".dynamic-form-add .add-row").click();

            //calculations function called
            Calculations(trid);
            window.scrollBy(0, 60);
            $(".account, .product").chosen({
                search_contains: true,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
            $(".chosen-single").css({'background-color': 'rgb(250,250,250)'});
            $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
            Obj.add_row(trid, account_id);
            // $(".delete-me").show(); //hide add row button
        // $(".delete-me").css({"pointer-events": "auto"}).children().addClass('btn-danger');
        // $(".delete-me:last").css({"pointer-events": "none"}).children().removeClass('btn-danger');
            $(".amount").attr('readonly', false);
            $(".amount:last").attr('readonly', true);
            $(".account").not(':last').prop("disabled", true).trigger("chosen:updated");

        // }
        // else {
        //     //calculations function called
        //
        //     sweetAlert({
        //         title: "Oops!",
        //         text: "You have already selected this account!",
        //         type: "error"
        //     });
        //     Calculations(Obj.find_product(account_id).row);
        //     $(".account:last")[0].selectedIndex = 0;
        //     $(".account:last").trigger("chosen:updated");
        //
        // }

    });


    // delete object of row from Obj
    $('.delete-row').click(function () {
        var trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        Calculations(trid);
        Obj.remove_row(trid);
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".account").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });
    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_payment_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });


    //price and quantity multiplication
    $(document).on("keyup", ".qty_sold", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });

    //price calculations
    $(document).on("keyup", ".amount", function () {
        var trid = $(this).parent().parent("tr").attr("id");
        Calculations(trid);

    });


    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

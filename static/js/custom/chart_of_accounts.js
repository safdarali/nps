$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-chart_of_account").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-chart_of_account .modal-content").html(data.html_form);
                $("#id_subaccount, #id_account_type,#id_is_account_active,#id_user, #id_ccc_type").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //on loac account type checking
                account_type =  $('#id_account_type').val();
                  if (account_type == 8 || account_type == 3) {
                        $('#id_phone_number, #id_address, #id_ccc_type,#id_user').parent().hide('slow');
                         $('#id_purchase_price, #id_sale_price, #id_ccc_price').parent().show('slow');
                    }
                    else if(account_type==1 || account_type==2 || account_type==5 || account_type==6 ){
                       $('#id_purchase_price, #id_sale_price, #id_ccc_price, #id_ccc_type,#id_user').parent().hide('slow');
                       $('#id_phone_number, #id_address').parent().show('slow');
                    }
                    else if(account_type==7){
                          $('#id_purchase_price, #id_sale_price, #id_ccc_price, #id_ccc_type').parent().hide('slow');
                       $('#id_phone_number, #id_address,#id_user').parent().show('slow');
                    }
                    else if(account_type==16){
                       $('#id_purchase_price, #id_sale_price, #id_ccc_price').parent().hide('slow');
                       $('#id_phone_number, #id_address, #id_ccc_type,#id_user').parent().show('slow');
                    }else{
                        $('#id_purchase_price, #id_sale_price, #id_ccc_price,#id_phone_number, #id_address, #id_ccc_type,#id_user').parent().hide('slow');
                    }
                 //on load account type checking end
                $('#id_account_type').change(function () {
                    id = $(this).val();
                    if (id == 8 || id == 3) {
                        $('#id_phone_number, #id_address, #id_ccc_type,#id_user').parent().hide('slow');
                         $('#id_purchase_price, #id_sale_price, #id_ccc_price').parent().show('slow');
                    }
                    else if(id==1 || id==2 || id==5 || id==6){
                       $('#id_purchase_price, #id_sale_price, #id_ccc_price, #id_ccc_type, #id_user').parent().hide('slow');
                       $('#id_phone_number, #id_address').parent().show('slow');
                    }
                    else if(id==16){
                       $('#id_purchase_price, #id_sale_price, #id_ccc_price').parent().hide('slow');
                       $('#id_phone_number, #id_address, #id_ccc_type, #id_user').parent().show('slow');
                    }
                    else if(id==7){
                        $('#id_purchase_price, #id_sale_price, #id_ccc_price, #id_ccc_type').parent().hide('slow');
                       $('#id_phone_number, #id_address, #id_user').parent().show('slow');
                    }
                    else{
                        $('#id_purchase_price, #id_sale_price, #id_ccc_price,#id_phone_number, #id_address, #id_ccc_type').parent().hide('slow');
                    }

                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#chart_of_account-table tbody").html(data.html_chart_of_account_list);
                    $("#modal-chart_of_account").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-chart_of_account .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create
    $(".js-create-chart_of_account").click(loadForm);
    $("#modal-chart_of_account").on("submit", ".js-chart_of_account-create-form", saveForm);

    // Update
    $("#chart_of_account-table").on("click", ".js-update-chart_of_account", loadForm);
    $("#modal-chart_of_account").on("submit", ".js-chart_of_account-update-form", saveForm);

    // Delete
    $("#chart_of_account-table").on("click", ".js-delete-chart_of_account", loadForm);
    $("#modal-chart_of_account").on("submit", ".js-chart_of_account-delete-form", saveForm);

});
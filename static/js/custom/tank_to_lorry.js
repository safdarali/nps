$(function () {


    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-tank_stock_transaction").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-tank_stock_transaction .modal-content").html(data.html_form);

                $("#id_lorry").change(function () {
                    var url = "/load_chambers";  // get the url of the `load_cities` view
                    var lorry_id = $(this).val();  // get the selected country ID from the HTML input

                    $.ajax({                       // initialize an AJAX request
                        url: url,                    // set the url of the request (= localhost:8000/hr/ajax/load-cities/)
                        data: {
                            'lorry_id': lorry_id       // add the country id to the GET parameters
                        },
                        success: function (data) {   // `data` is the return of the `load_cities` view function
                            $("#id_chamber").html(data);  // replace the contents of the city input with the data that came from the server
                        }
                    });

                });


            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#un_posted_tank_to_lorry-table tbody").html(data.html_tank_to_lorry);
                    $("#modal-tank_stock_transaction").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-tank_stock_transaction .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-tank_to_lorry_create").click(loadForm);
    $("#modal-tank_stock_transaction").on("submit", ".js-tank_to_lorry-form", saveForm);

    // Update book
    $("#un_posted_tank_to_lorry-table").on("click", ".js-update-tank_to_lorry", loadForm);
    $("#modal-tank_stock_transaction").on("submit", ".js-tank_to_lorry_update-form", saveForm);

    // Delete book
    $("#un_posted_tank_to_lorry-table").on("click", ".js-delete-tank_to_lorry", loadForm);
    $("#modal-tank_stock_transaction").on("submit", ".js-tank_to_lorry-delete-form", saveForm);

});

$(function () {
    $(document).on('click', 'input[type=text], input[type=number]', function () {
        this.select();
    });
});
$(function () {
    $(document).on('focus', '#id_lorry_previous_dip, #id_lorry_previous_stock, #id_lorry_final_dip,#id_lorry_final_stock,#id_lorry_shifted_stock,#id_tank_previous_dip,#id_tank_previous_stock,#id_tank_final_dip,#id_tank_final_stock,#id_lorry, #id_chamber,#id_product, #id_tank', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_lorry_previous_dip, #id_lorry_previous_stock, #id_lorry_final_dip,#id_lorry_final_stock,#id_lorry_shifted_stock,#id_tank_previous_dip,#id_tank_previous_stock,#id_tank_final_dip,#id_tank_final_stock,#id_lorry, #id_chamber,#id_product, #id_tank', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

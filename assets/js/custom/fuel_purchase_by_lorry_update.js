// current element bg color
    $(function () {
    $(document).on('focus', '.chamber_dip_purchased, .amount, .purchased_stock, .chamber_dip_received, .supplier_invoice_no', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '.chamber_dip_purchased, .amount, .purchased_stock, .chamber_dip_received, .supplier_invoice_no', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});


function Calculations() {
    //row calculations
    var sum = 0;

    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });


$('#id_net_amount').val(parseFloat(sum).toFixed(3));
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {


    $(".add-row").hide(); //hide add row button
    $("#id_amount, #id_net_amount").prop('readonly', true);
    $("#id_supplier, #id_lorry, #id_driver_name, .product").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });

    $(document).on("change", "#id_lorry", function () {
        var lorry_id;
        lorry_id = $(this).val();
        $.ajax({
            url: '/fuel_purchases_by_lorry/get_lorry?id=' + lorry_id,
            type: 'GET',
            beforeSend: function () {
                $('.delete-row').click();
            },
            success: function (data) {
                // setting values of forms elements in tr
                $('#id_fuel_purchases_by_lorry-TOTAL_FORMS').val(data.chamber_count - data.chamber_count);
                // for adding rows at start

                for (var i = 0; i < data.chamber_count; i++) {
                    $(".dynamic-form-add .add-row").click();
                }
                var n = 0;
                Obj.arr = [];
                $(".lorry_chamber").each(function () {
                    $(this).parents("tr").attr("id", n);
                    $(this).val(data.chambers[n]);
                    Obj.add_row(n, data.chambers[n]);
                    n = n + 1;
                });
                $('.lorry_chamber').attr('disabled', true);
                $(".product").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any chamber  in this lorry !",
                    type: "error"
                });
            }
        });


    });
$(document).on("keyup", ".amount", function () {
        Calculations();
    });
    $('.delete-row').on('click', function () {
        $(".table tbody tr").not(":last").remove();
    });
    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $('.lorry_chamber').attr('disabled', false);
        $('#id_form_save').submit();
    });
    // make product list enable

    // move amount value to right
    $('#id_amount').addClass('text-right');

    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $(document).on('keyup', '.chamber_dip_purchased', function () {
        var chamber_dip_purchased = 0;
        var chamber_dip_received = 0;
        trid = $(this).parents("tr").attr("id");
        chamber_dip_purchased = $(this).val();
        chamber_dip_received = $("#" + trid + " .chamber_dip_received").val();
        $("#" + trid + " .milli_short").val(chamber_dip_purchased - chamber_dip_received);
        var lorry = $("#id_lorry").val();
        var chamber = $("#" + trid + " .lorry_chamber").val();
        if (lorry == "") {
            //message
            swal({
                position: 'center',
                type: 'error',
                title: 'Select a lorry first',
                showConfirmButton: true,
            });

        }
    });

    $(document).on('keyup', '.chamber_dip_received', function () {
        var chamber_dip_purchased = 0;
        var chamber_dip_received = 0;
        var trid = $(this).parents("tr").attr("id");
        chamber_dip_purchased = $("#" + trid + " .chamber_dip_purchased").val();
        chamber_dip_received = $(this).val();
        $("#" + trid + " .milli_short").val(chamber_dip_purchased - chamber_dip_received);
        var lorry_chamber_id = $("#" + trid + " .lorry_chamber").val();
        var lorry_id = $("#id_lorry").val();
        console.log(lorry_id);
        if (lorry_id != "") {

            $.ajax({
                url: '/fuel_purchases_by_lorry/get_chart',
                data: {lorry_id: lorry_id, lorry_chamber_id: lorry_chamber_id, chamber_dip_received:chamber_dip_received},
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#" + trid + " .purchased_stock").val(data.litre);
                }
            });
        }else {
             //message
            swal({
                position: 'center',
                type: 'error',
                title: 'Select a lorry first',
                showConfirmButton: true,
            });
        }
    });

});

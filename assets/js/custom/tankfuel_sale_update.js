// current element bg color

$(function () {
    $(document).on('focus', '#id_date, #id_duty_person, .qty_sold, .current_reading,.test_reading', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_date, #id_duty_person, .qty_sold, .current_reading, .test_reading', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

function Calculations(rowID) {
    //row calculations
    var sum_sold = 0;
    var previous_stock;
    var stock_received;
    var current_stock;
    var qty_sold;
    previous_stock = $("#" + rowID + " .previous_stock").val();
    stock_received = $("#" + rowID + " .stock_received").val();
    current_stock = $("#" + rowID + " .current_stock").val();
    qty_sold = (parseInt(previous_stock) + parseInt(stock_received)) - parseInt(current_stock);
    $("#" + rowID + " .qty_sold").val(qty_sold);

    $(".qty_sold").each(function () {
        sum_sold += parseFloat($(this).val()) || 0;
    });
    $('#id_net_qty_sold').val(sum_sold);
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {
     $(".delete-me").css({"pointer-events": "none"});
    // add row arrays in Obj
    var row_id;
    var tank_id;
    $(".tank").not(':last').each(function () {
        row_id = $(this).parents("tr").attr("id");
        tank_id = $("#" + row_id + " .tank").val();
        Obj.add_row(row_id, tank_id);
    });
    // end arrays adding
    $('.tank').attr('disabled', true);
    $('.current_dip:last').attr('disabled', true);
    $('.current_stock, .previous_dip, .previous_stock, .stock_received, .qty_sold').attr('readonly', true);
    $(".add-row").hide(); //hide add row button
    $(" #id_net_qty_sold").prop('readonly', true);
    $("#id_duty_person, #id_product").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });

    //discounts in currency calculations
    $(document).on("keyup", ".current_reading", function () {
        var trid;
        trid = $(this).parents("tr").attr("id");

        Calculations(trid);
        // check on qtysold
    });


//for fetching chart values
    $(function () {
        var tank;
        var dip;
        $(document).on('keyup change', '.current_dip', function () {
            trid = $(this).parents("tr").attr("id");
            tank = $("#" + trid + " .tank").val();
            dip = $(this).val();
            if (dip > 4) {

                $.ajax({
                    url: '/tankfuels_sales/get_chart',
                    data: {tank: tank, current_dip: dip},
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $("#" + trid + " .current_stock").val(data.litre);
                        Calculations(trid);

                    },
                    error: function (data) {
                        //message
                        $("#" + trid + ".current_stock").val(0);
                        $("#" + trid + ".current_dip").val(0);
                        swal({
                                title: "Oops!",
                                text: "No any data found!",
                                type: "error"
                            }
                        );
                    }
                });

            }
        });
    });


    $(document).on("change", "#id_product", function () {
        var p_id;
        var date;
        date = $('#id_date').val();
        p_id = $(this).val();
        $.ajax({
            url: '/tankfuels_sales/get_previous_dip?id=' + p_id + '&date=' + date,
            type: 'GET',
            beforeSend: function () {
                $('.delete-row').click();
            },
            success: function (data) {
                // setting values of forms elements in tr
                $('#id_tankfuelsales_set-TOTAL_FORMS').val(data.tank_count - data.tank_count);
                // for adding rows at start

                for (var i = 0; i < data.tank_count; i++) {
                    $(".dynamic-form-add .add-row").click();
                }
                // to remove extra tr rows
                // $(".table").find("tr:gt(" + data.meter_count+1 + ")").remove();
                // add row arrays in Obj
                // to reassign attributes of rows
                var n = 0;
                Obj.arr = [];
                $(".tank").each(function () {
                    $(this).parents("tr").attr("id", n);
                    $("#" + n + " .previous_dip").val(data.previous_dip[n] || 0);
                    $("#" + n + " .previous_stock").val(data.previous_stock[n] || 0);
                    $("#" + n + " .stock_received").val(data.purchase[n].purchased_stock || 0);
                    $(this).val(data.tanks[n]);
                    Obj.add_row(n, data.tanks[n]);
                    n = n + 1;
                });
                $('.current_dip:last').attr('disabled', false);
                $('.tank').attr('disabled', true);
                $('.current_stock, .previous_dip, .previous_stock, .stock_received, .qty_sold').attr('readonly', true);

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });


    });

    // $('.delete-row').click(function () {
    //     var trid = $(this).parents("tr").attr("id");
    //     $("#" + trid + " .qty_sold").removeClass('qty_sold');
    //     Calculations(trid);
    //     Obj.remove_row(trid);
    // });
    $('.delete-row').on('click', function () {
        $(".table tbody tr").not(":last").remove();
    });
    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $('.tank').attr('disabled', false);
        $('.current_dip:last').attr('disabled', false);
        $('#id_form_save').submit();
    });
    // make product list enable

    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_payment_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });


    //price and quantity multiplication
    $(document).on("keyup", ".qty_sold", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });


    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal("show");
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

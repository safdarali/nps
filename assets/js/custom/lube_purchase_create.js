// current element bg color

$(function () {
    $(document).on('focus', '#id_date, #id_invoice_date, #id_supplier, .box_purchased, .product, .box_price', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_date, #id_invoice_date, #id_supplier, .box_purchased, .product, .box_price', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('click', ".box_purchased, .box_price", function () {
        this.select();
    });
});


// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {
    //calculations function declaratioon
    function Calculations(rowID) {
        //row calculations
        var amount_with_taxes;
        var amount_with_all_taxes;
        var qty_purchased;
        var sum = 0;
        var price;
        var net;
        var sum_tax = 0;
        var sum_without_tax = 0;
        var net_taxes = 0;
        var three_taxes = 0;
        var wh_tax = 0;

        qty_purchased = $("#" + rowID + " .box_purchased").val();
        price = $("#" + rowID + " .box_price").val();
        net = parseInt(qty_purchased) * parseFloat(price).toFixed(3);
        $("#" + rowID + " .amount_without_tax").val(net);
        three_taxes = (net * parseFloat((sum_taxes) / 100)).toFixed(3);
        amount_with_taxes = (net + parseFloat(three_taxes)).toFixed(3);
        wh_tax = (amount_with_taxes * (parseFloat(withholding_tax) / 100)).toFixed(3);
        net_taxes = (parseFloat(three_taxes) + parseFloat(wh_tax)).toFixed(3);
        amount_with_all_taxes = (parseFloat(amount_with_taxes) + parseFloat(wh_tax)).toFixed(3);
        $("#" + rowID + " .amount").val(amount_with_all_taxes);
        $("#" + rowID + " .tax_amount").val(net_taxes);
        $(".amount").each(function () {
            sum += parseFloat($(this).val()) || 0;
        });
        $('#id_net_amount').val(parseFloat(sum).toFixed(3));

        $(".tax_amount").each(function () {
            sum_tax += parseFloat($(this).val()) || 0;
        });
        $('#id_tax_net_amount').val(parseFloat(sum_tax).toFixed(3));

        $(".amount_without_tax").each(function () {
            sum_without_tax += parseFloat($(this).val()) || 0;
        });
        $('#id_net_amount_without_tax').val(parseFloat(sum_without_tax).toFixed(3));

    }


    function incrementRow(r) {
        var sum = 1;
        var row = parseInt(r);
        sum = sum + row;
        return sum
    }

    // fetch tax
    var p_id;
    var trid;
    var qty_purchased = 1;
    var prePopulate;
    var withholding_tax;
    var sum_taxes;


    $.ajax({
        url: '/lubes_purchases/fetch_tax',
        type: 'GET',
        success: function (data) {
            prePopulate = $.parseJSON(data);
            sum_taxes = prePopulate[0].sum_taxes;
            withholding_tax = prePopulate[0].withholding_tax;
            $('#sum_taxes').val(sum_taxes);
            $('#withholding_tax').val(withholding_tax);
        }
        ,
        error: function (data) {
            sweetAlert({
                title: "Oops!",
                text: "No any tax found!",
                type: "error"
            });
        }
    });

    sum_taxes = parseFloat($('#sum_taxes').val());
    withholding_tax = parseFloat($('#withholding_tax').val());


    $(".add-row").hide(); //hide add row button
    $(".amount, .tax_amount, .amount_without_tax, #id_net_amount, #id_tax_net_amount, .net_amount_without_tax").prop('readonly', true);
    $("#id_supplier, .product, #id_bank").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });

    $(document).on("click", ".price:last, .qty_ordered:last, .net:last", function () {
        if ($('.add-row').css('display') == 'none') {
            $(this).prop('readonly', true);
        }
    });

    $(document).on("change", ".product", function () {

        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        $.ajax({
            url: '/lubes_purchases/get_price?id=' + p_id,
            type: 'GET',
            success: function (data) {
                if (Obj.find_product(p_id) == undefined) {
                    prePopulate = $.parseJSON(data);
                    $("#" + trid + " .box_price").val(prePopulate[0].price);
                        //calculations
                    Calculations(trid);
                    //calculations end
                    //adding new row
                    $(".dynamic-form-add .add-row").click();
                    window.scrollBy(0, 60);
                    $(".product").chosen({
                        search_contains: true,
                        no_results_text: "Oops, nothing found!",
                        width: "100%"
                    });
                    // to disable fields
                    $(".amount, .tax_amount, .amount_without_tax").prop('readonly', true);
                    $(".product").not(':last').prop("disabled", true).trigger("chosen:updated");
                    //to change bg color of last product and barcode
                    $(".chosen-single").css({'background-color': 'rgb(250,250,250)'});
                    $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
                    Obj.add_row(trid, p_id);
                    // enable disable
                    $(".box_price:last").prop('readonly', true);
                    $(".box_purchased:last").prop('readonly', true);
                    $("#" + trid + " .box_price").prop('readonly', false);
                    $("#" + trid + " .box_purchased").prop('readonly', false);
                    // enable disable end
                } else {
                    //calculations
                    Calculations(Obj.find_product(p_id).row);
                    //calculations end
                    $(".product:last").val('').trigger("chosen:updated");
                }

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });

    });
    // delete object of row from Obj
    $('.delete-row').click(function () {
        var sum = 0;
        var trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        $("#" + trid + " .tax_amount").removeClass('tax_amount');
        $("#" + trid + " .amount_without_tax").removeClass('amount_without_tax');
        Calculations(trid);
        Obj.remove_row(trid);
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".product").prop("disabled", false).trigger("chosen:updated");
        // var TF = $('#id_lubepurchases_set-TOTAL_FORMS').val();
        // $('#id_lubepurchases_set-TOTAL_FORMS').val(TF - 1);
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });
    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_invoice_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: true
    });


    //price and quantity multiplication
    $(document).on("keyup", ".box_purchased", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });

    $(document).on("keyup", ".box_price", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });

    //price calculations
    $(document).on("keyup", ".price", function () {
        var trid = $(this).parent().parent("tr").attr("id");
        Calculations(trid);

    });


    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

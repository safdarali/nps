// current element bg color

$(function () {
    $(document).on('focus', '#id_date, .duty_person, .duty_at', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_date, .duty_person, .duty_at', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});


$(function () {
    $(".duty_person, .duty_at").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});
    $('#id_date').datetimepicker({
        format: "Y-m-d",
        onShow: function (ct) {

        },
        timepicker: false
    });

    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $('#id_form_save').submit();
    });


    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#duty-table tbody").html(data.html_duty_list);
                    $("#modal-delete").modal("hide");
                    $("#total_duties").html(data.total_duties);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    $('.js-duty_summery').on('click', function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-duty-summery").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-duty-summery .modal-content").html(data.html_duty_summery);
            }
        });

    });


    // Delete
    $("#duty-table").on("click", ".js-delete-duty", load_delete_Form);
    $("#duty-table").on("click", ".js-delete-latest-duty", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-duty-form", save_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-latest-duty-form", save_delete_Form);

});


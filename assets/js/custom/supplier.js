$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-supplier").modal("show");
            },
            success: function (data) {
                $("#modal-supplier .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#supplier-table tbody").html(data.html_supplier_list);
                    $("#modal-supplier").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-supplier .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-supplier").click(loadForm);
    $("#modal-supplier").on("submit", ".js-supplier-create-form", saveForm);

    // Update book
    $("#supplier-table").on("click", ".js-update-supplier", loadForm);
    $("#modal-supplier").on("submit", ".js-supplier-update-form", saveForm);

    // Delete book
    $("#supplier-table").on("click", ".js-delete-supplier", loadForm);
    $("#modal-supplier").on("submit", ".js-supplier-delete-form", saveForm);

});
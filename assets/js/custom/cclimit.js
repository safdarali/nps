$(function () {
    // var sales_;
    // var extra_discount;
    // var further_discount;
    // var sum_discounts;
    // /* Functions */
    //
    // $(document).on('keyup', '#id_sales_cclimit, #id_extra_cclimit, #id_further_cclimit', function () {
    //     sales_cclimit = parseFloat($('#id_sales_cclimit').val()) ||0;
    //     extra_cclimit =  parseFloat($('#id_extra_cclimit').val()) ||0;
    //     further_cclimit =  parseFloat($('#id_further_cclimit').val()) ||0;
    //     sum_cclimits =  sales_cclimit + extra_cclimit + further_cclimit;
    //     $('#id_sum_cclimits').val(sum_cclimits);
    // });

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-cclimit").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-cclimit .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_cclimits").prop('readonly', true);
                $("#id_product, #id_customer").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#cclimit-table tbody").html(data.html_cclimit_list);
                    $("#modal-cclimit").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-cclimit .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-cclimit").click(loadForm);
    $("#modal-cclimit").on("submit", ".js-cclimit-create-form", saveForm);

    // Update book
    $("#cclimit-table").on("click", ".js-update-cclimit", loadForm);
    $("#modal-cclimit").on("submit", ".js-cclimit-update-form", saveForm);

    // Delete book
    $("#cclimit-table").on("click", ".js-delete-cclimit", loadForm);
    $("#modal-cclimit").on("submit", ".js-cclimit-delete-form", saveForm);

});
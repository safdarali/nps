$(function () {

    /* Functions */
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-fuel_vehicle_income").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-fuel_vehicle_income .modal-content").html(data.html_form);
                $("#id_vehicle").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                     //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
                $("#id_fuel_vehicle_income_type, #id_fuel_vehicle_income_unit").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#fuel_vehicle_income-table tbody").html(data.html_fuel_vehicle_income_list);
                    $("#modal-fuel_vehicle_income").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-fuel_vehicle_income .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-fuel_vehicle_income").click(loadForm);
    $("#modal-fuel_vehicle_income").on("submit", ".js-fuel_vehicle_income-create-form", saveForm);

    // Update book
    $("#fuel_vehicle_income-table").on("click", ".js-update-fuel_vehicle_income", loadForm);
    $("#modal-fuel_vehicle_income").on("submit", ".js-fuel_vehicle_income-update-form", saveForm);

    // Delete book
    $("#fuel_vehicle_income-table").on("click", ".js-delete-fuel_vehicle_income", loadForm);
    $("#modal-fuel_vehicle_income").on("submit", ".js-fuel_vehicle_income-delete-form", saveForm);

});
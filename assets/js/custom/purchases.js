// current element bg color

$(function () {
    $(document).on('focus', '#id_date, #id_tl_carriage_name, #id_tl_driver_phone, #id_supplier, #id_supplier_invoice_no, #id_product, #id_tl_no, #id_tl_reporting_time, #id_tl_decantation_time, #id_tl_chamber, #id_tl_dip, #id_tl_dip_observed, #id_milli_short, #id_tl_decanted_by, #id_tl_cleared_by, #id_tank, #id_previous_dip, #id_previous_stock, #id_purchased_stock, #id_current_dipless_stock, #id_current_dip, #id_current_dip_stock, #id_sale_during_decantation, #id_amount', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_date, #id_tl_carriage_name, #id_tl_driver_phone, #id_supplier, #id_supplier_invoice_no, #id_product, #id_tl_no, #id_tl_reporting_time, #id_tl_decantation_time, #id_tl_chamber, #id_tl_dip, #id_tl_dip_observed, #id_milli_short, #id_tl_decanted_by, #id_tl_cleared_by, #id_tank, #id_previous_dip, #id_previous_stock, #id_purchased_stock, #id_current_dipless_stock, #id_current_dip, #id_current_dip_stock, #id_sale_during_decantation, #id_amount', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('click', "#id_previous_dip, #id_purchased_stock, #id_sale_during_decantation, #id_amount, #id_current_dip", function () {
        this.select();
    });
});


//purchased stock calculations
$(function () {
    var previous_stock = 0.0;
    var purchased_stock = 0.0;
    var current_dipless_stock = 0.0;
    var sale_during_decantation = 0.0;
    var current_dip_stock = 0.0;
    var stock_difference = 0.0;
    $(document).on('keyup', '#id_purchased_stock', function () {
        previous_stock = $('#id_previous_stock').val();
        purchased_stock = $(this).val();
        current_dip_stock = $("#id_current_dip_stock").val();
        sale_during_decantation = $("#id_sale_during_decantation").val();
        current_dipless_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
        $('#id_current_dipless_stock').val((current_dipless_stock).toFixed(1));
        stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
        $('#id_stock_difference').val((stock_difference).toFixed(1));
    });
});

//for fetching chart values
$(function () {
    var tank;
    var dip;
    var previous_stock = 0.0;
    var purchased_stock = 0.0;
    var current_dipless_stock = 0.0;
    var current_dip_stock = 0.0;
    var stock_difference = 0.0;
    var sale_during_decantation = 0.0;
    $(document).on('keyup change', '#id_previous_dip', function () {
        tank = $('#id_tank').val();
        dip = $('#id_previous_dip').val();
        if (tank == "") {
            $('#id_tank').focus();
            //message
            swal({
                position: 'center',
                type: 'error',
                title: 'Select required tank',
                showConfirmButton: true,
            });
        } else if (dip > 4) {

            $.ajax({
                url: '/fuel_purchases/get_chart',
                data: {tank: tank, previous_dip: dip},
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#id_previous_stock").val(data.litre);
                    previous_stock = $('#id_previous_stock').val();
                    purchased_stock = $('#id_purchased_stock').val();
                    current_dip_stock = $("#id_current_dip_stock").val();
                    sale_during_decantation = $("#id_sale_during_decantation").val();
                    current_dipless_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
                    $('#id_current_dipless_stock').val((current_dipless_stock).toFixed(1));
                    stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
                    $('#id_stock_difference').val((stock_difference).toFixed(1));
                },
                error: function (data) {
                    $("#id_previous_stock").val(0);
                    //message
                    swal({
                        title: "Oops!",
                        text: "No any data found!",
                        type: "error"
                    });
                }
            });

        }
    });
});

$(function () {
    var tank;
    var dip;
    var previous_stock = 0.0;
    var purchased_stock = 0.0;
    var current_dipless_stock = 0.0;
    var sale_during_decantation = 0.0;
    var current_dip_stock = 0.0;
    var stock_difference = 0.0;
    $(document).on('change', '#id_tank', function () {
        tank = $(this).val();
        dip = $('#id_previous_dip').val();
        $.ajax({
            url: '/fuel_purchases/get_chart',
            data: {tank: tank, previous_dip: dip},
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $("#id_previous_stock").val(data.litre);
                $('#id_previous_dip').focus();
                previous_stock = $('#id_previous_stock').val();
                purchased_stock = $("#id_purchased_stock").val();
                current_dip_stock = $("#id_current_dip_stock").val();
                sale_during_decantation = $("#id_sale_during_decantation").val();
                current_dipless_stock = (parseFloat(previous_stock)) + (parseFloat(purchased_stock));
                $('#id_current_dipless_stock').val(current_dipless_stock);
                stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
                $('#id_stock_difference').val((stock_difference).toFixed(1));
            },
            error: function (data) {
                $("#id_previous_stock").val(0);
            }
        });

    });
});

$(function () {
    var tank;
    var dip;
    var previous_stock = 0.0;
    var purchased_stock = 0.0;
    var sale_during_decantation = 0.0;
    var current_dip_stock = 0.0;
    var stock_difference = 0.0;
    $(document).on('keyup change', '#id_current_dip', function () {
        dip = $(this).val();
        tank = $('#id_tank').val();
        if (tank == "") {
            $('#id_tank').focus();
            //message
            swal({
                position: 'center',
                type: 'error',
                title: 'Select required tank',
                showConfirmButton: true,
            });
        } else if (dip > 4) {
            $.ajax({
                url: '/fuel_purchases/get_chart',
                data: {tank: tank, previous_dip: dip},
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#id_current_dip_stock").val(data.litre);
                    previous_stock = $('#id_previous_stock').val();
                    purchased_stock = $("#id_purchased_stock").val();
                    current_dip_stock = $("#id_current_dip_stock").val();
                    sale_during_decantation = $("#id_sale_during_decantation").val();
                    stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
                    $('#id_stock_difference').val((stock_difference).toFixed(1));
                },
                error: function (data) {
                    $("#id_current_dip_stock").val(0);
                    swal({
                        title: "Oops!",
                        text: "No any data found!",
                        type: "error"
                    });
                }
            });

        }
    });
});

$(function () {
    var previous_stock = 0.0;
    var purchased_stock = 0.0;
    var sale_during_decantation = 0.0;
    var current_dip_stock = 0.0;
    var stock_difference = 0.0;
    $(document).on('keyup', '#id_sale_during_decantation', function () {
        previous_stock = $('#id_previous_stock').val();
        purchased_stock = $("#id_purchased_stock").val();
        current_dip_stock = $("#id_current_dip_stock").val();
        sale_during_decantation = $(this).val();
        stock_difference = ((parseFloat(current_dip_stock)) + (parseFloat(sale_during_decantation))) - (parseFloat(previous_stock)) - (parseFloat(purchased_stock));
        $('#id_stock_difference').val((stock_difference).toFixed(1));
    });
});


$(function () {

    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    /* Functions */


    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-purchase").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-purchase .modal-content").html(data.html_form);

                //date picker
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: false
                });
                //timepicker
                $('#id_tl_reporting_time, #id_tl_decantation_time, #id_tl_checkout_time').mdtimepicker({

                    // format of the time value (data-time attribute)
                    timeFormat: 'hh:mm:ss.000',

                    // format of the input value
                    format: 'h:mm tt',

                    // theme of the timepicker
                    // 'red', 'purple', 'indigo', 'teal', 'green'
                    theme: 'blue',

                    // determines if input is readonly
                    readOnly: true,

                    // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
                    hourPadding: false

                });
                //millisho calculation
                var tl_dip = 0;
                var tl_dip_observed = 0;

                $('#id_tl_dip_observed').keyup(function () {
                    tl_dip = $('#id_tl_dip').val();
                    tl_dip_observed = $(this).val();
                    var ms = tl_dip - tl_dip_observed;
                    $('#id_milli_short').val(ms);
                });

                $('#id_tl_dip').keyup(function () {
                    tl_dip = $(this).val();
                    tl_dip_observed = $('#id_tl_dip_observed').val();
                    var ms = tl_dip - tl_dip_observed;
                    $('#id_milli_short').val(ms);
                });
                // tl creation modal opening

                $("#id_tanklorry, #id_supplier, #id_tank, #id_product, #id_tl_decanted_by, #id_tl_cleared_by").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                // tank lorry modal opening
                $(".btn_new_tl").click(function () {
                    var btn = $(this);
                    $.ajax({
                        url: btn.attr("data-url"),
                        type: 'get',
                        dataType: 'json',
                        beforeSend: function () {
                            $("#modal-tl").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                        },
                        success: function (data) {
                            $("#modal-tl .modal-content").html(data.html_form);
                        }
                    });


                });
                // tank lorry save

                var saveTLForm = function () {
                    var form = $(this);
                    $.ajax({
                        url: form.attr("action"),
                        data: form.serialize(),
                        type: form.attr("method"),
                        dataType: 'json',
                        success: function (data) {
                            if (data.form_is_valid) {
                                $("#modal-tl").modal("hide");
                                var html = '<option value="' + data.tl_id + '">' + data.tl_no + '</option>';
                                $('select[name="tanklorry"]').append(html);
                                $("#id_tanklorry").val(data.tl_id).trigger("chosen:updated");
                                $("#id_tl_carriage_name").val(data.tl_carriage_name);
                                $("#id_tl_driver_phone").val(data.tl_driver_phone);

                                //message
                                swal({
                                    position: 'center',
                                    type: 'success',
                                    title: 'Your work has been saved',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            } else {
                                $("#modal-tl .modal-content").html(data.html_form);
                            }
                        }
                    });
                    return false;
                };
                $("#modal-tl").on("submit", ".js-tl-create-form", saveTLForm);
                // tl_no select change action


                $("#id_tanklorry").change(function () {
                    var tanklorry;
                    tanklorry = $(this).val();
                    $.ajax({
                        url: '/tank_lorries/get_tl_info',
                        data: {tanklorry: tanklorry},
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            $("#id_tl_carriage_name").val(data.tl_carriage_name);
                            $("#id_tl_driver_phone").val(data.tl_driver_phone);
                        },
                        error: function (data) {
                            //message
                            swal({
                                position: 'center',
                                type: 'error',
                                title: 'Some error occurred!',
                                showConfirmButton: true,
                            });
                             $("#id_tl_carriage_name").val('');
                            $("#id_tl_driver_phone").val('');
                            $(this).focus();
                        }
                    });

                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#fuel_purchase-table tbody").html(data.html_fuel_purchases_list);
                    $("#modal-purchase").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-purchase .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#fuel_purchase-table tbody").html(data.html_fuel_purchases_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Create
    $(".js-create-purchase").click(loadForm);

    $("#modal-purchase").on("submit", ".js-purchase-create-form", saveForm);

    // Update
    $("#fuel_purchase-table").on("click", ".js-update-fuel_purchase", loadForm);
    $("#modal-purchase").on("submit", ".js-delete-fuel_purchase-form", saveForm);

    // Delete
    $("#fuel_purchase-table").on("click", ".js-delete-fuel_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-fuel_purchase-form", save_delete_Form);

});


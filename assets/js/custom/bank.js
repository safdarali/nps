$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-bank").modal("show");
            },
            success: function (data) {
                $("#modal-bank .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#bank-table tbody").html(data.html_bank_list);
                    $("#modal-bank").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-bank .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-bank").click(loadForm);
    $("#modal-bank").on("submit", ".js-bank-create-form", saveForm);

    // Update book
    $("#bank-table").on("click", ".js-update-bank", loadForm);
    $("#modal-bank").on("submit", ".js-bank-update-form", saveForm);

    // Delete book
    $("#bank-table").on("click", ".js-delete-bank", loadForm);
    $("#modal-bank").on("submit", ".js-bank-delete-form", saveForm);

});
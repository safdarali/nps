
// current element bg color

$(function () {
    $(document).on('focus', '#id_date, #id_supplier, .qty_purchased, .product, .price', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_date, #id_supplier, .qty_purchased, .product, .price', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

$(function () {
    $(document).on('click', ".qty_purchased, .price", function () {
        this.select();
    });
});


//calculations function declaratioon
function Calculations(rowID) {
    //row calculations
    var qty_purchased;
    var sum = 0;
    var price;
    var net;

    qty_purchased = $("#" + rowID + " .qty_purchased").val();
    price = $("#" + rowID + " .price").val();
    net = parseInt(qty_purchased) * parseFloat(price).toFixed(3);

    $("#" + rowID + " .net").val(parseFloat(net).toFixed(3));
    $(".net").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });

    //sum of net amount on detail form end
    $('#id_amount').val(parseFloat(sum).toFixed(3));

}


function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {
    $(".add-row").hide(); //hide add row button
    $("#id_amount").prop('readonly', true);
    $("#id_supplier, .product, #id_bank").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});

    $(document).on("click", ".price:last, .qty_ordered:last, .net:last", function () {
        if ($('.add-row').css('display') == 'none') {
            $(this).prop('readonly', true);
        }
    });

    $(document).on("change", ".product", function () {
        var p_id;
        var trid;
        var qty_purchased = 1;
        var prePopulate;
        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        $.ajax({
            url: '/lubes_purchases/get_price?id=' + p_id,
            type: 'GET',
            success: function (data) {
                if (Obj.find_product(p_id) == undefined) {
                    prePopulate = $.parseJSON(data);
                    $("#" + trid + " .price").val(prePopulate[0].price);
                    //calculations function called
                    Calculations(trid);
                    //adding new row
                    $(".dynamic-form-add .add-row").click();
                    window.scrollBy(0, 60);
                    $(".product").chosen({
                        search_contains: true,
                        no_results_text: "Oops, nothing found!",
                        width: "100%"
                    });
                    // to disable fields
                    $(".net").prop('readonly', true);
                    $(".product").not(':last').prop("disabled", true).trigger("chosen:updated");
                    //to change bg color of last product and barcode
                    $(".chosen-single").css({'background-color': 'rgb(250,250,250)'});
                    $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
                    Obj.add_row(trid, p_id);
                    // enable disable
                    $(".price:last").prop('readonly', true);
                    $(".qty_purchased:last").prop('readonly', true);
                    $("#" + trid + " .price").prop('readonly', false);
                    $("#" + trid + " .qty_purchased").prop('readonly', false);
                    // enable disable end
                } else {
                    qty_purchased = $("#" + Obj.find_product(p_id).row + " .qty_purchased").val();
                    $("#" + Obj.find_product(p_id).row + " .qty_purchased").val(parseInt(qty_purchased) + 1);

                    //calculations function called
                    Calculations(Obj.find_product(p_id).row);
                    $(".product:last").val('').trigger("chosen:updated");
                }

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });

    });
    // delete object of row from Obj
    $('.delete-row').click(function () {
        var sum = 0;
        var trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .net").removeClass('net');
        Calculations(trid);
        Obj.remove_row(trid);
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });
    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_payment_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });


    //price and quantity multiplication
    $(document).on("keyup", ".qty_purchased", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });

    //price calculations
    $(document).on("keyup", ".price", function () {
        var trid = $(this).parent().parent("tr").attr("id");
        Calculations(trid);

    });




     var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal("show");
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


   // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

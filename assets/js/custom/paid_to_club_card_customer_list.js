$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-paid_to_club_card_customer").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-paid_to_club_card_customer .modal-content").html(data.html_form);
                 $("#id_bank,#id_customer,#id_paid_to_club_card_customer_account").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                     $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#paid_to_club_card_customer-table tbody").html(data.html_paid_to_club_card_customer_list);
                    $("#modal-paid_to_club_card_customer").modal("hide");
                     $("#total_paid_to_club_card_customers").html(data.total_paid_to_club_card_customers);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-paid_to_club_card_customer .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-paid_to_club_card_customer").click(loadForm);
    $("#modal-paid_to_club_card_customer").on("submit", ".js-paid_to_club_card_customer-create-form", saveForm);

    // Update book
    $("#paid_to_club_card_customer-table").on("click", ".js-update-paid_to_club_card_customer", loadForm);
    $("#modal-paid_to_club_card_customer").on("submit", ".js-paid_to_club_card_customer-update-form", saveForm);

    // Delete book
    $("#paid_to_club_card_customer-table").on("click", ".js-delete-paid_to_club_card_customer", loadForm);
    $("#modal-paid_to_club_card_customer").on("submit", ".js-paid_to_club_card_customer-delete-form", saveForm);

});
$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-tl").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-tl .modal-content").html(data.html_form);
                }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#tl-table tbody").html(data.html_tl_list);
                    $("#modal-tl").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-tl .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-tl").click(loadForm);
    $("#modal-tl").on("submit", ".js-tl-create-form", saveForm);

    // Update book
    $("#tl-table").on("click", ".js-update-tl", loadForm);
    $("#model-tl").on("submit", ".js-tl-update-form", saveForm);

    // Delete book
    $("#tl-table").on("click", ".js-delete-tl", loadForm);
    $("#modal-tl").on("submit", ".js-tl-delete-form", saveForm);

});
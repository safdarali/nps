from django.db.models import Sum, F
from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.generic import ListView
from easy_pdf.views import PDFTemplateView

from cash.models import SupplierPayment, BankDeposition, CreditSalem
from chart_of_accounts.models import ChartOfAccount, MainAccount, SubAccount
from lorry_stock.models import LorryStock, LorryStockHistory
from products.models import Product
from purchases.models import LubesStock
from rent_received.models import Rentreceived
from sales.models import TanksLedger
from tanks.models import Tank
from nps.custom_decorators.permission_required import permission_required
from .forms import LedgerForm

from general_journal.models import GeneralJournal, TankGeneralJournal, GeneralJournal


def reports_home(request):
    return render(request, 'reports/reports_home.html')


def trial_balance(request):
    data = dict()
    if request.method == 'GET':
        tbs = GeneralJournal.objects.values('chartofaccount_id', 'chartofaccount__account_name').annotate(
            total_debit=Sum('debit'), total_credit=Sum('credit')).order_by('chartofaccount_id')
        sum_debit = tbs.aggregate(sum_debit=Sum('total_debit'))
        sum_credit = tbs.aggregate(sum_credit=Sum('total_credit'))
        data['html_trial_balance'] = render_to_string('reports/includes/trial_balance.html',
                                                      {'tbs': tbs, 'sum_debit': sum_debit, 'sum_credit': sum_credit})
    return JsonResponse(data)


def ledger_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if request.POST.get('account_name', None) is not None:
            ac_id = request.POST.get('account_name', None)
            start_date = request.POST.get('start_date', None)
            end_date = request.POST.get('end_date', None)
            account = ChartOfAccount.objects.get(id=ac_id)
            data['form_is_valid'] = True
            previous_balance = GeneralJournal.objects.filter(chartofaccount_id=ac_id, duty__date__lt=start_date).aggregate(
                difference=Sum('debit') - Sum('credit'))
            if previous_balance['difference'] is not None:
                pb = previous_balance['difference']
            else:
                pb = 0

            tbs = GeneralJournal.objects.filter(chartofaccount_id=ac_id,
                                                duty__date__range=(start_date, end_date)).values('duty__date','duty__date', 'voucher_type',
                                                                                                 'voucher_number',
                                                                                                 'naration',
                                                                                                 'debit',
                                                                                                 'credit').annotate(
                difference=F('debit') - F('credit'))
            balance = pb
            for t in tbs:
                balance = balance + t['difference']
                t.update({'balance': balance})

            data['html_ledger'] = render_to_string('reports/includes/ledger.html',
                                                   {'tbs': tbs, 'account_id': ac_id,
                                                    'account_name': account.account_name, 'start_date': start_date,
                                                    'end_date': end_date, 'previous_balance': pb})
        else:
            data['form_is_valid'] = False

    else:
        context = {'form': ChartOfAccount.objects.all()}
        data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tank_ledger_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if request.POST.get('tank_name', None) is not None:
            tank_id = request.POST.get('tank_name', None)
            start_date = request.POST.get('start_date', None)
            end_date = request.POST.get('end_date', None)
            tank = Tank.objects.get(id=tank_id)
            data['form_is_valid'] = True

            tank_ledgers = TanksLedger.objects.filter(tank_id=tank_id,duty__date__range=(start_date, end_date)).values('duty__date').\
                annotate(opening_stock_sum=Sum('opening_stock'),received_stock_sum=Sum('received_stock'),meter_sale_sum=Sum('meter_sale'),test_reading_sum=Sum('test_reading'),dip_sum=Sum('dip'),physical_stock_sum=Sum('physical_stock'), total_stock=Sum('opening_stock') +Sum('received_stock'),
                         closing_stock=(Sum('opening_stock') + Sum('received_stock'))-Sum('meter_sale'),net_book_stock=((Sum('opening_stock') + Sum('received_stock'))-Sum('meter_sale')) + Sum('test_reading'),
                         net_sale=Sum('meter_sale') - Sum('test_reading'), gain_loss=Sum('physical_stock')-(Sum('opening_stock') + Sum('received_stock')-Sum('meter_sale')+Sum('test_reading')))

            mtd = 0
            for tl in tank_ledgers:
                mtd = mtd + tl['gain_loss']
                tl.update({'mtd': mtd})

            data['html_ledger'] = render_to_string('reports/includes/tank_ledger.html',
                                                   {'tank_ledgers': tank_ledgers, 'account_id': tank_id,
                                                    'account_name': tank.tank_name, 'start_date': start_date,
                                                    'end_date': end_date})
        else:
            data['form_is_valid'] = False

    else:
        context = {'form': Tank.objects.all()[:3]}
        data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tank_ledger(request):
    if request.method == 'POST':
        form = request.POST
    else:
        form = Tank.objects.all()[:3]
    return tank_ledger_form(request, form, 'reports/includes/tank_ledger_param_form.html')

def tank_four_and_five_ledger(request):
    if request.method == 'POST':
        form = request.POST
    else:
        form = Product.objects.filter(product_type=5)
    return tank_four_and_five_ledger_form(request, form, 'reports/includes/tank_four_and_five_ledger_param_form.html')


def agency_customer_invoice(request):
    if request.method == 'POST':
        form = request.POST
    else:
        form = ChartOfAccount.objects.filter(account_type=7)
    return agency_customer_invoice_form(request, form, 'reports/includes/agency_customer_invoice_param_form.html')


def agency_customer_invoice_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if request.POST.get('customer', None) is not None:
            ac_id = request.POST.get('customer', None)
            start_date = request.POST.get('start_date', None)
            end_date = request.POST.get('end_date', None)
            account = ChartOfAccount.objects.get(id=ac_id)
            data['form_is_valid'] = True
            csm = CreditSalem.objects.filter(duty__date__range=(start_date, end_date),
                                                       creditsales__debit_account=ac_id).values('duty__date','creditsales__product__product_name','creditsales__qty_sold','creditsales__discount_amount','creditsales__amount')
            totals = csm.aggregate(total_net_amount=Sum('creditsales__amount'),
                                  total_net_discount_amount=Sum('creditsales__discount_amount'))
            data['html_ledger'] = render_to_string('reports/includes/agency_customer_invoice.html',
                                                   {'csm': csm, 'account_name': account.account_name,
                                                    'account_id': account.id, 'start_date': start_date,
                                                    'end_date': end_date,
                                                    'total_net_amount': totals['total_net_amount'],
                                                    'total_net_discount_amount': totals['total_net_discount_amount']})
        else:
            data['form_is_valid'] = False

    else:
        context = {'form': ChartOfAccount.objects.filter(account_type=7)}
        data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def tank_four_and_five_ledger_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if request.POST.get('product_name', None) is not None:
            ac_id = request.POST.get('product_name', None)
            start_date = request.POST.get('start_date', None)
            end_date = request.POST.get('end_date', None)
            account = Product.objects.get(id=ac_id)
            data['form_is_valid'] = True
            previous_balance = TankGeneralJournal.objects.filter(product_id=ac_id, date__lt=start_date).aggregate(
                difference=Sum('debit') - Sum('credit'))
            if previous_balance['difference'] is not None:
                pb = previous_balance['difference']
            else:
                pb = 0

            tbs = TankGeneralJournal.objects.filter(product_id=ac_id,
                                                    duty__date__range=(start_date, end_date)).values('duty__date',
                                                                                                     'voucher_type',
                                                                                                     'voucher_number',
                                                                                                     'naration',
                                                                                                     'debit',
                                                                                                     'credit').annotate(
                difference=F('debit') - F('credit'))
            balance = pb
            for t in tbs:
                balance = balance + t['difference']
                t.update({'balance': balance})

            data['html_ledger'] = render_to_string('reports/includes/tank_four_and_five_ledger.html',
                                                   {'tbs': tbs, 'account_id': ac_id,
                                                    'account_name': account.product_name, 'start_date': start_date,
                                                    'end_date': end_date, 'previous_balance': pb})
        else:
            data['form_is_valid'] = False

    else:
        context = {'form': Product.objects.filter(product_type=5)}
        data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def expenses_list_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        start_date = request.POST.get('start_date', None)
        end_date = request.POST.get('end_date', None)
        if start_date is not None and end_date is not None:
            data['form_is_valid'] = True
            previous_balance = GeneralJournal.objects.filter(chartofaccount__subaccount__mainaccount=5,
                                                             duty__date__lt=start_date).aggregate(
                difference=Sum('debit') - Sum('credit'))
            if previous_balance['difference'] is not None:
                pb = previous_balance['difference']
            else:
                pb = 0

            tbs = GeneralJournal.objects.filter(chartofaccount__subaccount__mainaccount=5,
                                                duty__date__range=(start_date, end_date)).values('chartofaccount',
                                                                                                 'chartofaccount__account_name').annotate(
                balance=Sum('debit') - Sum('credit')).order_by('chartofaccount')
            total = tbs.aggregate(total=Sum('balance'))
            data['html_expense_list'] = render_to_string('reports/includes/expenses_list.html',
                                                         {'tbs': tbs, 'start_date': start_date, 'end_date': end_date,
                                                          'previous_balance': pb, 'total': total})
        else:
            data['form_is_valid'] = False

    else:
        data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


def expenses_list(request):
    form = request.POST
    return expenses_list_form(request, form, 'reports/includes/expense_param_form.html')


def head_account_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if request.POST.get('sub_account_name', None) is not None:
            ac_id = request.POST.get('sub_account_name', None)
            start_date = request.POST.get('start_date', None)
            end_date = request.POST.get('end_date', None)
            account = SubAccount.objects.get(id=ac_id)
            data['form_is_valid'] = True
            previous_balance = GeneralJournal.objects.filter(chartofaccount__subaccount=ac_id,
                                                             duty__date__lt=start_date).aggregate(
                difference=Sum('debit') - Sum('credit'))
            if previous_balance['difference'] is not None:
                pb = previous_balance['difference']
            else:
                pb = 0

            tbs = GeneralJournal.objects.filter(chartofaccount__subaccount=ac_id,
                                                duty__date__range=(start_date, end_date)).values('duty__date', 'voucher_type',
                                                                                                 'voucher_number',
                                                                                                 'naration',
                                                                                                 'debit',
                                                                                                 'credit').annotate(
                difference=F('debit') - F('credit'))
            balance = pb
            for t in tbs:
                balance = balance + t['difference']
                t.update({'balance': balance})

            data['html_head_account'] = render_to_string('reports/includes/ledger.html',
                                                         {'tbs': tbs, 'account_id': ac_id,
                                                          'account_name': account.get_sub_account_name_display,
                                                          'start_date': start_date,
                                                          'end_date': end_date, 'previous_balance': pb})
        else:
            data['form_is_valid'] = False

    else:
        context = {'form': SubAccount.objects.all()}
        data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def customers_outstanding_amounts(request):
    data = dict()
    if request.method == 'GET':
        tbs = GeneralJournal.objects.filter(chartofaccount__account_type=7).values('chartofaccount',
                                                                                   'chartofaccount__account_name').annotate(
            balance=Sum('debit') - Sum('credit')).order_by('chartofaccount')
        total = tbs.aggregate(total=Sum('balance'))
        data['html_head_account'] = render_to_string('reports/includes/customers_outstanding_amounts.html',
                                                     {'tbs': tbs, 'total': total})
    return JsonResponse(data)


def lorries_stocks(request):
    data = dict()
    if request.method == 'GET':
        lorry_stock = LorryStock.objects.all()
        data['lorry_stock'] = render_to_string('reports/includes/lorries_stocks.html',
                                                     {'lorry_stock': lorry_stock})
    return JsonResponse(data)


def lorries_stocks_history(request):
    data = dict()
    if request.method == 'GET':
        lorry_stock_history = LorryStockHistory.objects.all()
        data['lorry_stock_history'] = render_to_string('reports/includes/lorries_stocks_history.html',
                                                     {'lorry_stock_history': lorry_stock_history})
    return JsonResponse(data)


def suppliers_payable_amounts(request):
    data = dict()
    if request.method == 'GET':
        tbs = GeneralJournal.objects.filter(chartofaccount__account_type=1).values('chartofaccount',
                                                                                   'chartofaccount__account_name').annotate(
            balance=Sum('debit') - Sum('credit')).order_by('chartofaccount')
        total = tbs.aggregate(total=Sum('balance'))
        data['html_head_account'] = render_to_string('reports/includes/suppliers_payable_amounts.html',
                                                     {'tbs': tbs, 'total': total})
    return JsonResponse(data)


def ledger(request):
    if request.method == 'POST':
        form = request.POST
    else:
        form = ChartOfAccount.objects.all()
    return ledger_form(request, form, 'reports/includes/ledger_param_form.html')


def cc_ledger(request):
    if request.method == 'POST':
        form = request.POST
    else:
        form = ChartOfAccount.objects.filter(cccustomertype__in=[1, 2]) | ChartOfAccount.objects.filter(id=40070001)
    return cc_ledger_form(request, form, 'reports/includes/cc_ledger_param_form.html')


def cc_ledger_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if request.POST.get('account_name', None) is not None:
            ac_id = request.POST.get('account_name', None)
            start_date = request.POST.get('start_date', None)
            end_date = request.POST.get('end_date', None)
            account = ChartOfAccount.objects.get(id=ac_id)
            data['form_is_valid'] = True
            previous_balance = GeneralJournal.objects.filter(chartofaccount_id=ac_id, duty__date__lt=start_date).aggregate(
                difference=Sum('debit') - Sum('credit'))
            if previous_balance['difference'] is not None:
                pb = previous_balance['difference']
            else:
                pb = 0

            tbs = GeneralJournal.objects.filter(chartofaccount_id=ac_id,
                                                duty__date__range=(start_date, end_date)).values('duty__date', 'voucher_type',
                                                                                                 'voucher_number',
                                                                                                 'naration',
                                                                                                 'debit',
                                                                                                 'credit').annotate(
                difference=F('debit') - F('credit'))
            balance = pb
            for t in tbs:
                balance = balance + t['difference']
                t.update({'balance': balance})

            data['html_ledger'] = render_to_string('reports/includes/cc_ledger.html',
                                                   {'tbs': tbs, 'account_id': ac_id,
                                                    'account_name': account.account_name, 'start_date': start_date,
                                                    'end_date': end_date, 'previous_balance': pb})
        else:
            data['form_is_valid'] = False
    else:
        context = {'form': ChartOfAccount.objects.filter(cccustomertype__in=[1, 2]) | ChartOfAccount.objects.filter(
            id__in=[50010001, 50010003, 40070001])}
        data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def head_account(request):
    if request.method == 'POST':
        form = request.POST
    else:
        form = SubAccount.objects.all()
    return head_account_form(request, form, 'reports/includes/ledger_param_form.html')


class LubesStockHistory(ListView):
    model = LubesStock
    template_name = 'reports/lubes_stock_histories.html'
    context_object_name = 'lubes_stock_histories'

    def get_queryset(self):
        queryset = super(LubesStockHistory, self).get_queryset()
        product_id = self.request.GET.get('product_id')
        if product_id is not None and product_id != '0':
            queryset = LubesStock.objects.filter(product_id=product_id)
        else:
            queryset = LubesStock.objects.all()
        return queryset

    def get_context_data(self, **kwargs):
        context = super(LubesStockHistory, self).get_context_data(**kwargs)
        pl = Product.objects.filter(product_type=1)
        l = []
        for p in pl:
            l.append(LubesStock.objects.filter(product_id=p.id).last())
        context['latest_stocks'] = l
        context['products_list'] = pl
        return context


def suppliers_payments_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        start_date = request.POST.get('start_date', None)
        end_date = request.POST.get('end_date', None)
        if start_date is not None and end_date is not None:
            data['form_is_valid'] = True
            previous_balance = SupplierPayment.objects.filter(date__lt=start_date).aggregate(balance=Sum('amount'))
            if previous_balance['balance'] is not None:
                pb = previous_balance['balance']
            else:
                pb = 0

            tbs = SupplierPayment.objects.filter(date__date__range=(start_date, end_date)).values(
                'supplier__account_name', 'duty__date', 'bank__account_name', 'payment_for', 'amount').order_by('duty__date')
            total = tbs.aggregate(total=Sum('amount'))
            data['html_suppliers_payment_list'] = render_to_string('reports/includes/suppliers_payments.html',
                                                                   {'tbs': tbs, 'start_date': start_date,
                                                                    'end_date': end_date, 'previous_balance': pb,
                                                                    'total': total})
        else:
            data['form_is_valid'] = False

    else:
        data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


def suppliers_payments(request):
    form = request.POST
    return suppliers_payments_form(request, form, 'reports/includes/suppliers_payments_param_form.html')


def bank_depositions_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        start_date = request.POST.get('start_date', None)
        end_date = request.POST.get('end_date', None)
        if start_date is not None and end_date is not None:
            data['form_is_valid'] = True
            previous_balance = BankDeposition.objects.filter(duty__date__lt=start_date).aggregate(balance=Sum('amount'))
            if previous_balance['balance'] is not None:
                pb = previous_balance['balance']
            else:
                pb = 0

            tbs = BankDeposition.objects.filter(duty__date__range=(start_date, end_date)).values('bank__account_name',
                                                                                                 'duty__date',
                                                                                                 'amount').order_by(
                'duty__date')
            total = tbs.aggregate(total=Sum('amount'))
            data['html_bank_deposition_list'] = render_to_string('reports/includes/bank_depositions.html',
                                                                 {'tbs': tbs, 'start_date': start_date,
                                                                  'end_date': end_date, 'previous_balance': pb,
                                                                  'total': total})
        else:
            data['form_is_valid'] = False

    else:
        data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


def bank_depositions(request):
    form = request.POST
    return bank_depositions_form(request, form, 'reports/includes/bank_depositions_param_form.html')


def rent_received_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        start_date = request.POST.get('start_date', None)
        end_date = request.POST.get('end_date', None)
        if start_date is not None and end_date is not None:
            data['form_is_valid'] = True
            previous_balance = Rentreceived.objects.filter(rent_receiving_date__lt=start_date).aggregate(
                balance=Sum('rent_amount'))
            if previous_balance['balance'] is not None:
                pb = previous_balance['balance']
            else:
                pb = 0

            tbs = Rentreceived.objects.filter(rent_receiving_date__date__range=(start_date, end_date)).values(
                'bank__account_name', 'rent_cheque_no', 'rent_receiving_date', 'rent_receiving_month',
                'rent_bank_deposition_date', 'rent_amount').order_by('rent_receiving_date')
            total = tbs.aggregate(total=Sum('rent_amount'))
            data['html_rent_received_list'] = render_to_string('reports/includes/rent_received.html',
                                                               {'tbs': tbs, 'start_date': start_date,
                                                                'end_date': end_date, 'previous_balance': pb,
                                                                'total': total})
        else:
            data['form_is_valid'] = False

    else:
        data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


def rent_received(request):
    form = request.POST
    return rent_received_form(request, form, 'reports/includes/rent_received_param_form.html')


from django import forms
from chart_of_accounts.models import ChartOfAccount, MainAccount, SubAccount


class LedgerForm(forms.ModelForm):
    account_name = forms.ModelChoiceField(queryset = ChartOfAccount.objects.all())

    class Meta:
        model = ChartOfAccount
        fields = '__all__'


class HeadAccountForm(forms.ModelForm):
    main_account_name = forms.ModelChoiceField(queryset = MainAccount.objects.all())

    class Meta:
        model = SubAccount
        fields = '__all__'
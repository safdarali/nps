from django.urls import path
from reports.views import reports_home, trial_balance, ledger, head_account, LubesStockHistory, \
    customers_outstanding_amounts, \
    suppliers_payable_amounts, expenses_list, suppliers_payments, bank_depositions, rent_received,\
    tank_ledger, tank_four_and_five_ledger, agency_customer_invoice, cc_ledger, lorries_stocks, lorries_stocks_history

urlpatterns = [
    path('reports', reports_home, name='reports_home'),
    path('reports/trial_balance', trial_balance, name='trial_balance'),
    path('reports/expenses_list', expenses_list, name='expenses_list'),
    path('reports/suppliers_payments', suppliers_payments, name='suppliers_payments'),
    path('reports/bank_depositions', bank_depositions, name='bank_depositions'),
    path('reports/rent_received', rent_received, name='rent_received'),
    path('reports/ledger', ledger, name='ledger'),
    path('reports/cc_ledger', cc_ledger, name='cc_ledger'),
    path('reports/tank_ledger', tank_ledger, name='tank_ledger'),
    path('reports/tank_four_and_five_ledger', tank_four_and_five_ledger, name='tank_four_and_five_ledger'),
    path('reports/agency_customer_invoice', agency_customer_invoice, name='agency_customer_invoice'),
    path('reports/head_account', head_account, name='head_account'),
    path('reports/customers_outstanding_amounts', customers_outstanding_amounts, name='customers_outstanding_amounts'),
    path('reports/lorries_stocks', lorries_stocks, name='lorries_stocks'),
    path('reports/lorries_stocks_history', lorries_stocks_history, name='lorries_stocks_history'),
    path('reports/suppliers_payable_amounts', suppliers_payable_amounts, name='suppliers_payable_amounts'),
    path('reports/lubes_stock_histories', LubesStockHistory.as_view(),name='lubes_stock_histories'),
]
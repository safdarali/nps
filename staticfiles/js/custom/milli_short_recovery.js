$(function () {
    var loadForm = function () {
        var sum = 0;
        var percentage = 0;
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-milli_short_recovery_detail").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-milli_short_recovery_detail .modal-content").html(data.milli_short_recovery_detail);
                $('.total_amount').each(function () {
                    sum += parseInt($(this).val());
                });
                $('.sum_total_amount').val(sum);
                $('.percent').keyup(function () {
                    percentage = parseFloat($(this).val() / 100) * parseFloat($('.sum_total_amount').val());
                    $('.percentage').val(parseFloat(percentage).toFixed(3));
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            beforeSend: function () {
                $("#modal-milli_short_recovery_detail").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $('#modal-milli_short_recovery_detail').modal('hide');
                location.reload();
            },
            error: function () {
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Your work not saved',
                    showConfirmButton: true,
                    timer: 1500
                })
            }
        });
        return false;
    };
    $(".js-show_milli_short_recovery_detail").click(loadForm);
    $("#modal-milli_short_recovery_detail").on("submit", ".js-milli_short_recovery_update", saveForm);
});
$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-meter").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-meter .modal-content").html(data.html_form);
                 $("#id_tank, #id_product").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#meter-table tbody").html(data.html_meter_list);
                    $("#modal-meter").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-meter .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-meter").click(loadForm);
    $("#modal-meter").on("submit", ".js-meter-create-form", saveForm);

    // Update book
    $("#meter-table").on("click", ".js-update-meter", loadForm);
    $("#modal-meter").on("submit", ".js-meter-update-form", saveForm);

    // Delete book
    $("#meter-table").on("click", ".js-delete-meter", loadForm);
    $("#modal-meter").on("submit", ".js-meter-delete-form", saveForm);

});
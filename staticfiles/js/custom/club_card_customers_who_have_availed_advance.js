$(function () {
    var loadForm = function () {
        var sum = 0;
        var percentage = 0;
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-advance_given_detail").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-advance_given_detail .modal-content").html(data.advance_availed_detail);
                $('.expense_given').each(function () {
                    sum += parseInt($(this).val());
                });
                $('.sum_expense_given').val(sum);
                $('.percent').keyup(function () {
                    percentage = parseFloat($(this).val() / 100) * parseFloat($('.sum_expense_given').val());
                    $('.percentage').val(parseFloat(percentage).toFixed(3));
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            beforeSend: function () {
                $("#modal-advance_given_detail").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $('#modal-advance_given_detail').modal('hide');
                location.reload();
            },
            error: function () {
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Your work not saved',
                    showConfirmButton: true,
                    timer: 1500
                })
            }
        });
        return false;
    };
    $(".js-show_advance_given_detail").click(loadForm);
    $("#modal-advance_given_detail").on("submit", ".js-advance_given_update", saveForm);
});
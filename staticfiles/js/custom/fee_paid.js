$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-fee_paid").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-fee_paid .modal-content").html(data.html_form);
                 $("#id_bank, #id_supplier, #id_fee_paying_month").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#fee_paid-table tbody").html(data.html_fee_paid_list);
                    $("#modal-fee_paid").modal("hide");
                     $("#total_fee_paid").html(data.total_fee_paids);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-fee_paid .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-fee_paid").click(loadForm);
    $("#modal-fee_paid").on("submit", ".js-fee_paid-create-form", saveForm);

    // Update book
    $("#fee_paid-table").on("click", ".js-update-fee_paid", loadForm);
    $("#modal-fee_paid").on("submit", ".js-fee_paid-update-form", saveForm);

    // Delete book
    $("#fee_paid-table").on("click", ".js-delete-fee_paid", loadForm);
    $("#modal-fee_paid").on("submit", ".js-fee_paid-delete-form", saveForm);

});
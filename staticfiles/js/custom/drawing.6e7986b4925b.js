$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-drawing").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-drawing .modal-content").html(data.html_form);
                 $("#id_bank, #id_supplier, #id_fee_paying_month").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#drawing-table tbody").html(data.html_drawing_list);
                    $("#modal-drawing").modal("hide");
                     $("#total_drawing").html(data.total_drawing);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-drawing .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-drawing").click(loadForm);
    $("#modal-drawing").on("submit", ".js-drawing-create-form", saveForm);

    // Update book
    $("#drawing-table").on("click", ".js-update-drawing", loadForm);
    $("#modal-drawing").on("submit", ".js-drawing-update-form", saveForm);

    // Delete book
    $("#drawing-table").on("click", ".js-delete-drawing", loadForm);
    $("#modal-drawing").on("submit", ".js-drawing-delete-form", saveForm);

});
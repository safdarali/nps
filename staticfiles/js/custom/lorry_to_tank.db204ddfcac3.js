$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-Lorry_stock_transaction").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-Lorry_stock_transaction .modal-content").html(data.html_form);

                //  $("#id_bank, #id_supplier, #id_fee_paying_month").chosen({
                //     search_contains: true,
                //     no_results_text: "Oops, nothing found!",
                //     width: "100%"
                // });
                //for date select

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#un_posted_lorry_to_tank-table tbody").html(data.html_lorry_to_tank);
                    $("#modal-Lorry_stock_transaction").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-Lorry_stock_transaction .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-lorry_to_tank_create").click(loadForm);
    $("#modal-Lorry_stock_transaction").on("submit", ".js-lorry_to_tank-form", saveForm);

    // Update book
    $("#drawing-table").on("click", ".js-update-drawing", loadForm);
    $("#modal-drawing").on("submit", ".js-drawing-update-form", saveForm);

    // Delete book
    $("#drawing-table").on("click", ".js-delete-drawing", loadForm);
    $("#modal-drawing").on("submit", ".js-drawing-delete-form", saveForm);

});
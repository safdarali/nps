$(function () {

    /* Functions */

    // var loadForm = function () {
    //     var btn = $(this);
    //     $.ajax({
    //         url: btn.attr("data-url"),
    //         type: 'get',
    //         dataType: 'json',
    //         beforeSend: function () {
    //             $("#modal-product").modal({
    //                 backdrop: 'static',
    //                 keyboard: false
    //             });
    //         },
    //         success: function (data) {
    //             $("#modal-product .modal-content").html(data.html_form);
    //         }
    //     });
    // };

    // var TrialBalance = function () {
    //     $.ajax({
    //         url: '/reports/trial_balance',
    //         type: 'GET',
    //         dataType: 'json',
    //         success: function (data) {
    //             $("#report-data").html(data.html_trial_balance);
    //             //message
    //             swal({
    //                 position: 'center',
    //                 type: 'success',
    //                 title: 'Your work has been saved',
    //                 showConfirmButton: false,
    //                 timer: 1500
    //             })
    //
    //         }
    //     });
    //     return false;
    // };


    /* Binding */

    // Create book
    $(".js-create-product").click(loadForm);
    $("#modal-product").on("submit", ".js-product-create-form", saveForm);

    // Update book
    $("#product-table").on("click", ".js-update-product", loadForm);
    $("#modal-product").on("submit", ".js-product-update-form", saveForm);

    // Delete book
    $("#product-table").on("click", ".js-delete-product", loadForm);
    $("#modal-product").on("submit", ".js-product-delete-form", saveForm);




});


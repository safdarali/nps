$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-uniform_order").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-uniform_order .modal-content").html(data.html_form);
                //for date select
                $('#id_rent_bank_deposition_date, #id_rent_receiving_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: false
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#uniform_order-table tbody").html(data.html_uniform_order_list);
                    $("#modal-uniform_order").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-uniform_order .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-uniform_order").click(loadForm);
    $("#modal-uniform_order").on("submit", ".js-uniform_order-create-form", saveForm);

    // Update book
    $("#uniform_order-table").on("click", ".js-update-uniform_order", loadForm);
    $("#modal-uniform_order").on("submit", ".js-uniform_order-update-form", saveForm);

    // Delete book
    $("#uniform_order-table").on("click", ".js-delete-uniform_order", loadForm);
    $("#modal-uniform_order").on("submit", ".js-uniform_order-delete-form", saveForm);

});
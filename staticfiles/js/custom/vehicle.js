$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-vehicle").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-vehicle .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#vehicle-table tbody").html(data.html_vehicle_list);
                    $("#modal-vehicle").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-vehicle .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-vehicle").click(loadForm);
    $("#modal-vehicle").on("submit", ".js-vehicle-create-form", saveForm);

    // Update book
    $("#vehicle-table").on("click", ".js-update-vehicle", loadForm);
    $("#modal-vehicle").on("submit", ".js-vehicle-update-form", saveForm);

    // Delete book
    $("#vehicle-table").on("click", ".js-delete-vehicle", loadForm);
    $("#modal-vehicle").on("submit", ".js-vehicle-delete-form", saveForm);

});
$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-bank_deposition").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-bank_deposition .modal-content").html(data.html_form);
                $("#id_bank, #id_deposition_for").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#bank_deposition-table tbody").html(data.html_bank_deposition_list);
                    $("#modal-bank_deposition").modal("hide");
                     $("#total_bank_deposition").html(data.total_bank_depositions);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-bank_deposition .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-bank_deposition").click(loadForm);
    $("#modal-bank_deposition").on("submit", ".js-bank_deposition-create-form", saveForm);

    // Update book
    $("#bank_deposition-table").on("click", ".js-update-bank_deposition", loadForm);
    $("#modal-bank_deposition").on("submit", ".js-bank_deposition-update-form", saveForm);

    // Delete book
    $("#bank_deposition-table").on("click", ".js-delete-bank_deposition", loadForm);
    $("#modal-bank_deposition").on("submit", ".js-bank_deposition-delete-form", saveForm);

});
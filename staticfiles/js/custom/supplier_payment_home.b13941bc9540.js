$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-supplier_payment").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-supplier_payment .modal-content").html(data.html_form);
                 $("#id_supplier, #id_bank,#id_payment_for").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#supplier_payment-table tbody").html(data.html_supplier_payment_list);
                    $("#modal-supplier_payment").modal("hide");
                     $("#total_supplier_payment").html(data.total_supplier_payments);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-supplier_payment .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-supplier_payment").click(loadForm);
    $("#modal-supplier_payment").on("submit", ".js-supplier_payment-create-form", saveForm);

    // Update book
    $("#supplier_payment-table").on("click", ".js-update-supplier_payment", loadForm);
    $("#modal-supplier_payment").on("submit", ".js-supplier_payment-update-form", saveForm);

    // Delete book
    $("#supplier_payment-table").on("click", ".js-delete-supplier_payment", loadForm);
    $("#modal-supplier_payment").on("submit", ".js-supplier_payment-delete-form", saveForm);

});
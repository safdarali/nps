$(function () {

    /* Functions */
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-t4_income").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-t4_income .modal-content").html(data.html_form);
                     //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
                $("#id_t4_income_type, #id_t4_income_unit").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#t4_income-table tbody").html(data.html_t4_income_list);
                    $("#modal-t4_income").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-t4_income .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-t4_income").click(loadForm);
    $("#modal-t4_income").on("submit", ".js-t4_income-create-form", saveForm);

    // Update book
    $("#t4_income-table").on("click", ".js-update-t4_income", loadForm);
    $("#modal-t4_income").on("submit", ".js-t4_income-update-form", saveForm);

    // Delete book
    $("#t4_income-table").on("click", ".js-delete-t4_income", loadForm);
    $("#modal-t4_income").on("submit", ".js-t4_income-delete-form", saveForm);

});
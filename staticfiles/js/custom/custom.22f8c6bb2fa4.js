$(document).ready(function () {
    $('.user-menu').mouseenter(function () {
        $('.dropdown-menu-right').show();
        $('.dropdown-menu-right').mouseleave(function () {
            $(this).hide();
        });

    });
// on all invoice forms
    $("#id_amount").prop('readonly', true);
    $("#id_previous_balance").prop('readonly', true);
    $("#id_net_amount").prop('readonly', true);

});

$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-extra_swaping_return").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-extra_swaping_return .modal-content").html(data.html_form);
                 $("#id_bank, #id_supplier, #id_fee_paying_month").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#extra_swaping_return-table tbody").html(data.html_extra_swaping_return_list);
                    $("#modal-extra_swaping_return").modal("hide");
                     $("#total_extra_swaping_return").html(data.total_extra_swaping_return);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-extra_swaping_return .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-extra_swaping_return").click(loadForm);
    $("#modal-extra_swaping_return").on("submit", ".js-extra_swaping_return-create-form", saveForm);

    // Update book
    $("#extra_swaping_return-table").on("click", ".js-update-extra_swaping_return", loadForm);
    $("#modal-extra_swaping_return").on("submit", ".js-extra_swaping_return-update-form", saveForm);

    // Delete book
    $("#extra_swaping_return-table").on("click", ".js-delete-extra_swaping_return", loadForm);
    $("#modal-extra_swaping_return").on("submit", ".js-extra_swaping_return-delete-form", saveForm);

});
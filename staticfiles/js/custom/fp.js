$(function () {


    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-fp").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-fp .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_fps").prop('readonly', true);
                $("#id_product, #id_supplier, #id_duty, #id_decanted_by, #id_tank").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                // add tank in  list
                $("#id_product").change(function () {
                    var url = $(".js-fp-create-form").attr("data-tank-url");
                    var productId = $(this).val();

                    $.ajax({
                        url: url,
                        data: {
                            'product': productId
                        },
                        success: function (data) {
                            $("#id_tank").html(data);
                            $("#id_tank").trigger("chosen:updated");
                        }
                    });

                });
                // tank end
                // for date set
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    formatTime: 'h:i a',
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
                // for date set end
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#fp-table tbody").html(data.html_fp_list);
                    $("#modal-fp").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-fp .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-fp").click(loadForm);
    $("#modal-fp").on("submit", ".js-fp-create-form", saveForm);

    // Update book
    $("#fp-table").on("click", ".js-update-fp", loadForm);
    $("#modal-fp").on("submit", ".js-fp-update-form", saveForm);

    // Delete book
    $("#fp-table").on("click", ".js-delete-fp", loadForm);
    $("#modal-fp").on("submit", ".js-fp-delete-form", saveForm);

});
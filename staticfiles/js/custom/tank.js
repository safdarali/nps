$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-tank").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-tank .modal-content").html(data.html_form);
                 $("#id_product").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#tank-table tbody").html(data.html_tank_list);
                    $("#modal-tank").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-tank .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-tank").click(loadForm);
    $("#modal-tank").on("submit", ".js-tank-create-form", saveForm);

    // Update book
    $("#tank-table").on("click", ".js-update-tank", loadForm);
    $("#modal-tank").on("submit", ".js-tank-update-form", saveForm);

    // Delete book
    $("#tank-table").on("click", ".js-delete-tank", loadForm);
    $("#modal-tank").on("submit", ".js-tank-delete-form", saveForm);

});
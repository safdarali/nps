//calculations function declaratioon

function Calculations() {
    //row calculations
    var sum = 0;
    var price = 0;
    var amount = 0;
    var net_amount = 0;
    var qty_sold = 0;
    var orig_qty_sold = 0;
    var orig_net_amount = 0;
    var fuel_equivalent = 0;
    var clubcard_tax = 0;
    var sum_cc_tax_equivalent = 0;
    price = $(".price").val();
    qty_sold = $(".qty_sold").val();
    amount = price * qty_sold;
    $(".amount").val(parseFloat(amount).toFixed(3));

    clubcard_tax = $('#id_clubcard_tax').val();

    $('#id_net_amount').val(parseFloat(amount).toFixed(3));
    net_amount = $('#id_net_amount').val();
    fuel_equivalent = $("#id_fuel_equivalent").val();
    sum_cc_tax_equivalent = parseFloat(fuel_equivalent) + parseFloat(clubcard_tax);
    $("#id_orig_net_amount").val(parseFloat(net_amount - sum_cc_tax_equivalent).toFixed(3));
    orig_net_amount = $("#id_orig_net_amount").val();
    $("#id_orig_qty_sold").val(parseInt(orig_net_amount / price));
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};

$(document).ready(function () {
    $("#id_supplier, .product, #id_tanklorry, #id_tl_decanted_by, #id_tl_cleared_by, #id_company_name, #id_clubcardvehicle, #id_duty_person").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });
    // $(".delete-me").css({"pointer-events": "none"}).children().removeClass('btn-danger');
    $(".add-row").hide(); //hide add row button
    $("#id_net_amount, #id_tl_carriage_name, #id_tl_driver_name, #id_tl_driver_phone, #id_orig_net_amount, #id_orig_qty_sold, .amount").prop('readonly', true);


    //timepicker
    $('#id_tl_reporting_time, #id_tl_decantation_time, #id_tl_checkout_time').mdtimepicker({

        // format of the time value (data-time attribute)
        timeFormat: 'hh:mm:ss.000',

        // format of the input value
        format: 'h:mm tt',

        // theme of the timepicker
        // 'red', 'purple', 'indigo', 'teal', 'green'
        theme: 'blue',

        // determines if input is readonly
        readOnly: false,

        // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
        hourPadding: false

    });

    //selecting customer types form db
    $("#id_company_name").change(function () {
        var company;
        var cccustomertype;
        company = $(this).val();
        $.ajax({
            url: '/clubcard_sales/get_company_info',
            data: {company: company},
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                cccustomertype = data.cccustomertype;
                if (cccustomertype == 1) {
                    $('#id_expense_given').prop('readonly', true);
                    $('#id_received_amount').prop('readonly', false);
                    $('#id_slip_no').prop('readonly', false);
                    $('#id_clubcard_tax').prop('readonly', false);
                } else if (cccustomertype == 2 || cccustomertype == 3) {
                    $('#id_slip_no').prop('readonly', true);
                    $('#id_received_amount').prop('readonly', true);
                    $('#id_clubcard_tax').prop('readonly', true);
                    $('#id_fuel_equivalent').prop('readonly', true);
                    $('#id_expense_given').prop('readonly', false);
                }
            },
            error: function (data) {
                //message
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Some error occurred!',
                    showConfirmButton: true,
                });
            }
        });
    });
    //selecting customer types form db end
    // tank lorry modal opening
    $(".btn_new_tl").click(function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-tl").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-tl .modal-content").html(data.html_form);
                $(".company_name-ccc").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });


    });
    // tank lorry save

    var saveTLForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#modal-tl").modal("hide");
                    $("#id_company_name").val(data.company_name).trigger("chosen:updated");
                    $('#id_clubcardvehicle').append("<option value='" + data.vehicle_value + "'>" + data.vehicle_display + "</option>");
                    $("#id_clubcardvehicle").val(data.vehicle_value).trigger("chosen:updated");
                    $("#driver_name").val(data.driver_name);
                    $("#driver_phone").val(data.driver_phone);
                    $("#driver_cnic").val(data.driver_cnic);
                    $('#camera_button').css({'display': 'block'});
                    $('#driver_photo>img').remove();
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-tl .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };
    $("#modal-tl").on("submit", ".js-clubcard-vehicle-create-form", saveTLForm);
    // tl_no select change action


    $("#id_tanklorry").change(function () {
        var tanklorry;
        tanklorry = $(this).val();
        $.ajax({
            url: '/tank_lorries/get_tl_info',
            data: {tanklorry: tanklorry},
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $("#id_tl_carriage_name").val(data.tl_carriage_name);
                $("#id_tl_driver_phone").val(data.tl_driver_phone);
                $("#id_tl_driver_name").val(data.tl_driver_name);
            },
            error: function (data) {
                //message
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Some error occurred!',
                    showConfirmButton: true,
                });
                $("#id_tl_carriage_name").val('');
                $("#id_tl_driver_phone").val('');
                $(this).focus();
            }
        });

    });
    // current element bg color

    $(document).on('focus', '#id_date, #id_supplier, .qty_purchased, .product, .price, .tl_chamber, .tl_dip, tl_dip_observed, .tank, .previous_dip, .current_dip, .previous_stock, .purchased_stock, .sale_during_decantation, .current_dip_stock, .stock_difference, .physical_stock, .amount, #id_supplier_invoice_no, #id_date, #id_tl_reporting_time,#id_tl_decantation_time,#id_tl_checkout_time ', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });


    $(document).on('blur', '#id_date, #id_supplier, .qty_purchased, .product, .price, .tl_chamber, .tl_dip, tl_dip_observed,.tank, .previous_dip, .current_dip, .previous_stock, .purchased_stock, .sale_during_decantation, .current_dip_stock, .stock_difference, .physical_stock, .amount, #id_supplier_invoice_no,#id_date, #id_tl_reporting_time,#id_tl_decantation_time,#id_tl_checkout_time ', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });


    $(document).on('click', ".qty_purchased, .price", function () {
        this.select();
    });


    $(document).on("keyup", ".qty_sold", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });
    $(document).on("keyup", "#id_clubcard_tax", function () {
        if ($(this).val() == '') {
            $(this).val(0)
        }
        Calculations();
    });

    $(document).on("keyup", "#id_fuel_equivalent", function () {

        if ($(this).val() == '') {
            $(this).val(0)
        }
        Calculations();
    });

    $(document).on("change", ".product", function () {
        var p_id;
        var trid;
        var qty_sold;
        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        $.ajax({
            url: '/clubcard_sales/get_ccc_price?id=' + p_id,
            type: 'GET',
            success: function (data) {
                if (Obj.find_product(p_id) == undefined) {
                    $("#" + trid + " .price").val(data.ccc_price);
                    $("#" + trid + " .qty_present").val(data.available_stock);
                    //adding new row
                    // $(".dynamic-form-add .add-row").click();
                    //calculations function called
                    Calculations(trid);
                    window.scrollBy(0, 60);
                    $(".product").chosen({
                        search_contains: true,
                        no_results_text: "Oops, nothing found!",
                        width: "100%"
                    });
                    // to disable fields
                    $(".amount").prop('readonly', true);
                    $(".product").not(':last').prop("disabled", true).trigger("chosen:updated");
                    Obj.add_row(trid, p_id);
                    // enable disable
                    $(".price:last").prop('readonly', true);
                    $(".qty_sold:last").prop('readonly', true);
                    $(".discount:last").prop('readonly', true);
                    $(".qty_present").prop('readonly', true);
                    $("#" + trid + " .price").prop('readonly', false);
                    $("#" + trid + " .qty_sold").prop('readonly', false);
                    $("#" + trid + " .discount").prop('readonly', false);
                    // enable disable end
                } else {
                    // qty_sold = $("#" + Obj.find_product(p_id).row + " .qty_sold").val();
                    // $("#" + Obj.find_product(p_id).row + " .qty_sold").val(parseInt(qty_sold) + 1);

                    //calculations function called
                    Calculations(Obj.find_product(p_id).row);
                    // $(".product:last").val('').trigger("chosen:updated");
                }

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });

    });

    // delete object of row from Obj
    $('.delete-row').click(function () {
        trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        $("#" + trid + " .discount_amount").removeClass('discount_amount');
        Calculations();
        Obj.remove_row(trid);
    });

    //price calculations
    $(document).on("keyup", ".price", function () {
        var trid = $(this).parent().parent("tr").attr("id");
        Calculations(trid);

    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".product").prop("disabled", false).trigger("chosen:updated");
        // var TF = $('#id_fuelpurchases_set-TOTAL_FORMS').val();
        // $('#id_fuelpurchases_set-TOTAL_FORMS').val(TF - 1);
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_invoice_date').datetimepicker({
        format: "Y-m-d h:m:s",
        formatTime: 'h:i a',
        onShow: function (ct) {

        },
        timepicker: true
    });

    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

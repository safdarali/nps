// current element bg color

$(function () {
    $(document).on('focus', '#id_date, #id_duty_person, .qty_sold, .discount, .price', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur','#id_date, #id_duty_person, .qty_sold, .discount, .price', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

function Calculations(rowID) {
    //row calculations
    var qty_sold;
    var sum = 0;
    var price;
    var amount;
    var discount;
    var new_price;
    var net_discount = 0;
    var qty_present;
    qty_present = $("#" + rowID + " .qty_present").val();
    qty_sold = $("#" + rowID + " .qty_sold").val();
    price = $("#" + rowID + " .price").val();
    discount = $("#" + rowID + " .discount").val();
    new_price = price - discount;
    amount = parseInt(qty_sold) * parseFloat(new_price).toFixed(3);
    $("#" + rowID + " .amount").val(parseFloat(amount).toFixed(3));
    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });
    $(".discount").each(function () {
        net_discount += parseFloat($(this).val()) * $(this).parent().parent().find("td .qty_sold").val();
    });
    $('#id_net_amount').val(parseFloat(sum).toFixed(3));
    $('#id_net_discount').val(parseFloat(net_discount).toFixed(3));

    if ((discount == '') || (parseInt(discount) >= price)) {
        var net_discount = 0;
        var sum = 0;
        sweetAlert({
            title: "Oops!",
            text: "Discount can't be less than zero or equal to or greater than sale price !",
            type: "error"
        });
        $("#" + rowID + " .discount").val(0).select();
        qty_sold = $("#" + rowID + " .qty_sold").val();
        price = $("#" + rowID + " .price").val();
        discount = $("#" + rowID + " .discount").val();
        new_price = price - discount;
        amount = parseInt(qty_sold) * parseFloat(new_price).toFixed(3);
        $("#" + rowID + " .amount").val(parseFloat(amount).toFixed(3));
        $(".amount").each(function () {
            sum += parseFloat($(this).val()) || 0;
        });
        $(".discount").each(function () {
            net_discount += parseFloat($(this).val()) * $(this).parent().parent().find("td .qty_sold").val();
        });
        $('#id_net_amount').val(parseFloat(sum).toFixed(3));
        $('#id_net_discount').val(parseFloat(net_discount).toFixed(3));

    }
    // check on qtysold
    if (parseInt(qty_sold) > parseInt(qty_present)) {
           var net_discount = 0;
            var sum = 0;
        sweetAlert({
                title: "Oops!",
                text: "Selling qty is greater than present in stock !",
                type: "error"
            }, function () {
                $(".product:last").focus();
            }
        );

        $("#" + rowID + " .qty_sold").val(qty_present);
        qty_sold = $("#" + rowID + " .qty_sold").val();
        price = $("#" + rowID + " .price").val();
        discount = $("#" + rowID + " .discount").val();
        new_price = price - discount;
        amount = parseInt(qty_sold) * parseFloat(new_price).toFixed(3);
        $("#" + rowID + " .amount").val(parseFloat(amount).toFixed(3));
        $(".amount").each(function () {
            sum += parseFloat($(this).val()) || 0;
        });
        $(".discount").each(function () {
            net_discount += parseFloat($(this).val()) * $(this).parent().parent().find("td .qty_sold").val();
        });
        $('#id_net_amount').val(parseFloat(sum).toFixed(3));
        $('#id_net_discount').val(parseFloat(net_discount).toFixed(3));

    }
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {

    $(".add-row").hide(); //hide add row button
    $("#id_amount, #id_net_discount, #id_net_amount").prop('readonly', true);
    $("#id_duty_person, .product").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });

    $(document).on("click", ".price:last, .qty_ordered:last, .net:last", function () {
        if ($('.add-row').css('display') == 'none') {
            $(this).prop('readonly', true);
        }
    });

    //discounts in currency calculations
    $(document).on("keyup", ".discount", function () {
        var trid;
        trid = $(this).parents("tr").attr("id");

        Calculations(trid);
        // check on qtysold
    });


    $(document).on("change", ".product", function () {
        var p_id;
        var trid;
        var qty_sold;
        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        $.ajax({
            url: '/lubes_sales/get_price?id=' + p_id,
            type: 'GET',
            success: function (data) {
                if (Obj.find_product(p_id) == undefined) {
                    $("#" + trid + " .price").val(data.price);
                    $("#" + trid + " .qty_present").val(data.available_stock);
                    //adding new row
                    $(".dynamic-form-add .add-row").click();
                     //calculations function called
                    Calculations(trid);
                    window.scrollBy(0, 60);
                    $(".product").chosen({
                        search_contains: true,
                        no_results_text: "Oops, nothing found!",
                        width: "100%"
                    });
                    // to disable fields
                    $(".amount").prop('readonly', true);
                    $(".product").not(':last').prop("disabled", true).trigger("chosen:updated");
                    //to change bg color of last product and barcode
                    $(".chosen-single").css({'background-color': 'rgb(250,250,250)'});
                    $(".chosen-single:last").css({'background-color': 'rgb(200,250,200)'});
                    Obj.add_row(trid, p_id);
                    // enable disable
                    $(".price:last").prop('readonly', true);
                    $(".qty_sold:last").prop('readonly', true);
                    $(".discount:last").prop('readonly', true);
                    $(".qty_present").prop('readonly', true);
                    $("#" + trid + " .price").prop('readonly', false);
                    $("#" + trid + " .qty_sold").prop('readonly', false);
                    $("#" + trid + " .discount").prop('readonly', false);
                    // enable disable end
                } else {
                    qty_sold = $("#" + Obj.find_product(p_id).row + " .qty_sold").val();
                    $("#" + Obj.find_product(p_id).row + " .qty_sold").val(parseInt(qty_sold) + 1);

                    //calculations function called
                    Calculations(Obj.find_product(p_id).row);
                    $(".product:last").val('').trigger("chosen:updated");
                }

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });

    });
    // delete object of row from Obj
    // delete object of row from Obj
    $('.delete-row').click(function () {
        var trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        Calculations(trid);
        Obj.remove_row(trid);
        var new_trid = trid - 1;
        $("#" + new_trid + " .delete-row").css('display', 'block');
        $(".add-row:last").show();
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".product").prop("disabled", false).trigger("chosen:updated");
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });
    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_payment_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });


    //price and quantity multiplication
    $(document).on("keyup", ".qty_sold", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });

    //price calculations
    $(document).on("keyup", ".price", function () {
        var trid = $(this).parent().parent("tr").attr("id");
        Calculations(trid);

    });


    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

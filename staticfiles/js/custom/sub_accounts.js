$(function () {

    /* Functions */

    var loadSubAccountForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-sub_account").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-sub_account .modal-content").html(data.html_form);
                $("#id_sub_account_name, #id_mainaccount").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});
            }
        });
    };

    var saveSubAccountForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#sub_account-table tbody").html(data.html_sub_account_list);
                    $("#modal-sub_account").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-sub_account .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };

    /* Binding */

    // Create subaccount
    $(".js-create-sub_account").click(loadSubAccountForm);
    $("#modal-sub_account").on("submit", ".js-sub_account-create-form", saveSubAccountForm);

    // Update subaccount
    $("#sub_account-table").on("click", ".js-update-sub_account", loadSubAccountForm);
    $("#modal-sub_account").on("submit", ".js-sub_account-update-form", saveSubAccountForm);

    // Delete subaccount
    $("#sub_account-table").on("click", ".js-delete-sub_account", loadSubAccountForm);
    $("#modal-sub_account").on("submit", ".js-sub_account-delete-form", saveSubAccountForm);

});
$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-expense").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-expense .modal-content").html(data.html_form);
                 $("#id_bank,#id_expense_account").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                     $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#expense-table tbody").html(data.html_expense_list);
                    $("#modal-expense").modal("hide");
                     $("#total_expenses").html(data.total_expenses);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-expense .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-expense").click(loadForm);
    $("#modal-expense").on("submit", ".js-expense-create-form", saveForm);

    // Update book
    $("#expense-table").on("click", ".js-update-expense", loadForm);
    $("#modal-expense").on("submit", ".js-expense-update-form", saveForm);

    // Delete book
    $("#expense-table").on("click", ".js-delete-expense", loadForm);
    $("#modal-expense").on("submit", ".js-expense-delete-form", saveForm);

});
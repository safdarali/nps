$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-main_account").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-main_account .modal-content").html(data.html_form);
                $("#id_main_account_name").chosen({search_contains: true, no_results_text: "Oops, nothing found!", width: "100%"});
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#main_account-table tbody").html(data.html_main_account_list);
                    $("#modal-main_account").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-main_account .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create
    $(".js-create-main_account").click(loadForm);
    $("#modal-main_account").on("submit", ".js-main_account-create-form", saveForm);

    // Update
    $("#main_account-table").on("click", ".js-update-main_account", loadForm);
    $("#modal-main_account").on("submit", ".js-main_account-update-form", saveForm);

    // Delete
    $("#main_account-table").on("click", ".js-delete-main_account", loadForm);
    $("#modal-main_account").on("submit", ".js-main_account-delete-form", saveForm);

});
$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-bank_to_bank_transfer").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-bank_to_bank_transfer .modal-content").html(data.html_form);
                 $("#id_bank, #id_supplier, #id_fee_paying_month").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#bank_to_bank_transfer-table tbody").html(data.html_bank_to_bank_transfer_list);
                    $("#modal-bank_to_bank_transfer").modal("hide");
                     $("#total_bank_to_bank_transfer").html(data.total_bank_to_bank_transfer);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-bank_to_bank_transfer .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-bank_to_bank_transfer").click(loadForm);
    $("#modal-bank_to_bank_transfer").on("submit", ".js-bank_to_bank_transfer-create-form", saveForm);

    // Update book
    $("#bank_to_bank_transfer-table").on("click", ".js-update-bank_to_bank_transfer", loadForm);
    $("#modal-bank_to_bank_transfer").on("submit", ".js-bank_to_bank_transfer-update-form", saveForm);

    // Delete book
    $("#bank_to_bank_transfer-table").on("click", ".js-delete-bank_to_bank_transfer", loadForm);
    $("#modal-bank_to_bank_transfer").on("submit", ".js-bank_to_bank_transfer-delete-form", saveForm);

});
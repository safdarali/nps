$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal_clubcard_vehicle").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal_clubcard_vehicle .modal-content").html(data.html_form);

            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#clubcard_vehicle-table tbody").html(data.html_clubcard_vehicle_list);
                    $("#modal_clubcard_vehicle").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-clubcard_vehicle .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-clubcard_vehicle").click(loadForm);
    $("#modal_clubcard_vehicle").on("submit", ".js-clubcard_vehicle-create-form", saveForm);

    // Update book
    $("#clubcard_vehicle-table").on("click", ".js-update-clubcard_vehicle", loadForm);
    $("#modal_clubcard_vehicle").on("submit", ".js-clubcard_vehicle-update-form", saveForm);

    // Delete book
    $("#clubcard_vehicle-table").on("click", ".js-delete-clubcard_vehicle", loadForm);
    $("#modal_clubcard_vehicle").on("submit", ".js-clubcard_vehicle-delete-form", saveForm);

});
$(function () {
    // var sales_;
    // var extra_discount;
    // var further_discount;
    // var sum_discounts;
    // /* Functions */
    //
    // $(document).on('keyup', '#id_sales_ccdebit, #id_extra_ccdebit, #id_further_ccdebit', function () {
    //     sales_ccdebit = parseFloat($('#id_sales_ccdebit').val()) ||0;
    //     extra_ccdebit =  parseFloat($('#id_extra_ccdebit').val()) ||0;
    //     further_ccdebit =  parseFloat($('#id_further_ccdebit').val()) ||0;
    //     sum_ccdebits =  sales_ccdebit + extra_ccdebit + further_ccdebit;
    //     $('#id_sum_ccdebits').val(sum_ccdebits);
    // });

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-ccdebit").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-ccdebit .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_ccdebits").prop('readonly', true);
                $("#id_product, #id_customer").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#ccdebit-table tbody").html(data.html_ccdebit_list);
                    $("#modal-ccdebit").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-ccdebit .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-ccdebit").click(loadForm);
    $("#modal-ccdebit").on("submit", ".js-ccdebit-create-form", saveForm);

    // Update book
    $("#ccdebit-table").on("click", ".js-update-ccdebit", loadForm);
    $("#modal-ccdebit").on("submit", ".js-ccdebit-update-form", saveForm);

    // Delete book
    $("#ccdebit-table").on("click", ".js-delete-ccdebit", loadForm);
    $("#modal-ccdebit").on("submit", ".js-ccdebit-delete-form", saveForm);

});
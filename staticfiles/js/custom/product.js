$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-product").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-product .modal-content").html(data.html_form);
                  $("#id_product_type, #id_product_unit").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#product-table tbody").html(data.html_product_list);
                    $("#modal-product").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-product .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-product").click(loadForm);
    $("#modal-product").on("submit", ".js-product-create-form", saveForm);

    // Update book
    $("#product-table").on("click", ".js-update-product", loadForm);
    $("#modal-product").on("submit", ".js-product-update-form", saveForm);

    // Delete book
    $("#product-table").on("click", ".js-delete-product", loadForm);
    $("#modal-product").on("submit", ".js-product-delete-form", saveForm);

});
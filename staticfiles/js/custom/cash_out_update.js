// current element bg color

$(function () {
    $(document).on('focus', '.qty_sold, .debit_account, .product', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', 'qty_sold, .debit_account, .product', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

function Calculations(rowID) {
    //row calculations
    var sum = 0;
    var sum_da = 0;
    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });
    $(".discount_amount").each(function () {
        sum_da += parseFloat($(this).val()) || 0;
    });
    $('#id_net_amount').val(parseFloat(sum).toFixed(3));
    $('#id_net_discount_amount').val(parseFloat(sum_da).toFixed(3));
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {

    $(document).on("keyup", ".qty_sold", function () {
        var p_id;
        var trid;
        var qty_sold;
        var sale_price;
        var amount = 0;
        var discount_amount = 0;
        var discount = 0;
        var discounted_price = 0;
        var total_nozzle_rate_amount =0;
        trid = $(this).parents("tr").attr("id");
        p_id = $("#" + trid + " .product").val();
        qty_sold = $(this).val();
        $.ajax({
            url: '/cash_out/get_price',
            type: 'GET',
            data: {'p_id': p_id },
            success: function (data) {
                discount = $("#" + trid + " .discount").val();
                total_nozzle_rate_amount = data.sale_price * qty_sold;
                discount_amount = discount * qty_sold;
                discounted_price = data.sale_price - discount;
                amount = qty_sold * discounted_price;
                $("#" + trid + " .nozzle_rate_amount").val(parseFloat(total_nozzle_rate_amount).toFixed(3));
                $("#" + trid + " .discount_amount").val(parseFloat(discount_amount).toFixed(3));
                $("#" + trid + " .amount").val(parseFloat(amount).toFixed(3));

                Calculations(trid);
            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No data found!",
                    type: "error"
                });
            }
        });

    });

    $(document).on("change", ".product", function () {
        var p_id;
        var c_id;
        var trid;
        var qty_sold;
        var sale_price;
        var amount = 0;
        var discount_amount = 0;
        var discounted_price = 0;
        trid = $(this).parents("tr").attr("id");
        p_id = $(this).val();
        c_id = $("#" + trid + " .debit_account").val();
        qty_sold = $("#" + trid + " .qty_sold").val();
        $.ajax({
            url: '/cash_out/get_discount',
            type: 'GET',
            data: {'p_id': p_id, 'c_id': c_id},
            success: function (data) {
                $("#" + trid + " .discount").val(data.discount);
                discount_amount = data.discount * qty_sold;
                discounted_price = data.sale_price - data.discount;
                amount = qty_sold * discounted_price;
                $("#" + trid + " .discount_amount").val(discount_amount);
                $("#" + trid + " .amount").val(amount);

                Calculations(trid);
            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No data found!",
                    type: "error"
                });
            }
        });

    });

    $(".add-row").hide(); //hide add row button
    $(".amount, #id_net_amount, #id_net_discount_amount, .discount, .discount_amount, .nozzle_rate_amount").attr('readonly', true); //hide add row button
    $(".debit_account, .product").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });
    $("#id_duty_person").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });


    //discounts in currency calculations
    $(document).on("keyup", ".amount", function () {
        var trid;
        trid = $(this).parents("tr").attr("id");

        Calculations(trid);
        // check on qtysold
    });


    $(document).on("change", ".debit_account", function () {
        var trid;
        var account_id
        account_id = $(this).val();
        trid = $(this).parents("tr").attr("id");

        //adding new row
        $(".dynamic-form-add .add-row").click();

        //calculations function called
        Calculations(trid);
        window.scrollBy(0, 60);
        $(".debit_account, .product").chosen({
            search_contains: true,
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
        Obj.add_row(trid, account_id);
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $(".product:last").prop("disabled", true).trigger("chosen:updated");
        $(".debit_account").not(':last').prop("disabled", true).trigger("chosen:updated");
        $(".amount, .discount, .discount_amount").attr('readonly', true);
    });


    // delete object of row from Obj
    $('.delete-row').click(function () {
        var trid = $(this).parents("tr").attr("id");
        $("#" + trid + " .amount").removeClass('amount');
        $("#" + trid + " .discount_amount").removeClass('discount_amount');
        Calculations(trid);
        Obj.remove_row(trid);
    });


    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
        $(".debit_account").prop("disabled", false).trigger("chosen:updated");
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".product").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });

    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });



    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

$(function () {
    // var sales_;
    // var extra_discount;
    // var further_discount;
    // var sum_discounts;
    // /* Functions */
    //
    // $(document).on('keyup', '#id_sales_discount, #id_extra_discount, #id_further_discount', function () {
    //     sales_discount = parseFloat($('#id_sales_discount').val()) ||0;
    //     extra_discount =  parseFloat($('#id_extra_discount').val()) ||0;
    //     further_discount =  parseFloat($('#id_further_discount').val()) ||0;
    //     sum_discounts =  sales_discount + extra_discount + further_discount;
    //     $('#id_sum_discounts').val(sum_discounts);
    // });

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-discount").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-discount .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_discounts").prop('readonly', true);
                $("#id_product, #id_customer").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#discount-table tbody").html(data.html_discount_list);
                    $("#modal-discount").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-discount .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-discount").click(loadForm);
    $("#modal-discount").on("submit", ".js-discount-create-form", saveForm);

    // Update book
    $("#discount-table").on("click", ".js-update-discount", loadForm);
    $("#modal-discount").on("submit", ".js-discount-update-form", saveForm);

    // Delete book
    $("#discount-table").on("click", ".js-delete-discount", loadForm);
    $("#modal-discount").on("submit", ".js-discount-delete-form", saveForm);

});
$(function () {

    /* Functions */

    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-rent_received").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-rent_received .modal-content").html(data.html_form);
                 $("#id_bank, #id_rent_receiving_month").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                //for date select
                $('#id_rent_bank_deposition_date, #id_rent_receiving_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    onShow: function (ct) {

                    },
                    timepicker: false
                });
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#rent_received-table tbody").html(data.html_rent_received_list);
                    $("#modal-rent_received").modal("hide");
                     $("#total_rent_received").html(data.total_rent_receiveds);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else {
                    $("#modal-rent_received .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-rent_received").click(loadForm);
    $("#modal-rent_received").on("submit", ".js-rent_received-create-form", saveForm);

    // Update book
    $("#rent_received-table").on("click", ".js-update-rent_received", loadForm);
    $("#modal-rent_received").on("submit", ".js-rent_received-update-form", saveForm);

    // Delete book
    $("#rent_received-table").on("click", ".js-delete-rent_received", loadForm);
    $("#modal-rent_received").on("submit", ".js-rent_received-delete-form", saveForm);

});
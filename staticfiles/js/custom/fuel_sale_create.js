// current element bg color

$(function () {
    $(document).on('focus', '#id_date, #id_duty_person, .qty_sold, .current_reading,.test_reading', function () {
        $(this).css({'background-color': 'rgb(200,250,200)'});
    });
});

$(function () {
    $(document).on('blur', '#id_date, #id_duty_person, .qty_sold, .current_reading, .test_reading', function () {
        $(this).css({'background-color': 'rgb(255,255,255)'});
    });
});

function Calculations(rowID) {
    //row calculations
    var sum = 0;
    var sum_sold = 0;
    var sum_test = 0;
    var price;
    var amount;
    var previous_reading;
    var current_reading;
    var test_reading;
    var qty_sold;
    current_reading = $("#" + rowID + " .current_reading").val();
    previous_reading = $("#" + rowID + " .previous_reading").val();
    test_reading = $("#" + rowID + " .test_reading").val();
    price = $("#" + rowID + " .price").val();
    qty_sold = parseInt(current_reading) - (parseInt(previous_reading) + parseInt(test_reading));
    $("#" + rowID + " .qty_sold").val(qty_sold);
    amount = parseFloat(price * qty_sold).toFixed(3);
    $("#" + rowID + " .amount").val(amount);

    $(".amount").each(function () {
        sum += parseFloat($(this).val()) || 0;
    });
    $(".qty_sold").each(function () {
        sum_sold += parseFloat($(this).val()) || 0;
    });
    $(".test_reading").each(function () {
        sum_test += parseFloat($(this).val()) || 0;
    });

    $('#id_net_amount').val(parseFloat(sum).toFixed(3));
    $('#id_net_qty_sold').val(sum_sold);
    $('#id_net_test_reading').val(sum_test);
}

function incrementRow(r) {
    var sum = 1;
    var row = parseInt(r);
    sum = sum + row;
    return sum
}

// object definition
var Obj = {
    arr: [],
    subObj: {},
    add_row: function (row, p_id) {
        this.subObj = {};
        this.subObj.row = row;
        this.subObj.p_id = p_id;
        this.arr.push(this.subObj);
    },
    find_product: function (p_id) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].p_id == p_id) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    find_row: function (row) {
        l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                return this.arr[i];
            }
        }
        return undefined;
    },
    remove_row: function (row) {
        var l = this.arr.length;
        for (var i = 0; i < l; i++) {
            if (this.arr[i].row == row) {
                this.arr.splice(i, 1);
                break;
            }
        }

    }
};


$(document).ready(function () {

    $(".add-row").hide(); //hide add row button
    $("#id_amount, #id_net_discount, #id_net_amount, #id_net_qty_sold, #id_net_test_reading").prop('readonly', true);
    $("#id_duty_person, #id_product").chosen({
        search_contains: true,
        no_results_text: "Oops, nothing found!",
        width: "100%"
    });

    //discounts in currency calculations
    $(document).on("keyup", ".current_reading, .test_reading", function () {
        var trid;
        trid = $(this).parents("tr").attr("id");

        Calculations(trid);
        // check on qtysold
    });


    $(document).on("change", "#id_product", function () {
        var p_id;
        p_id = $(this).val();
        $.ajax({
            url: '/fuels_sales/get_price?id=' + p_id,
            type: 'GET',
            beforeSend: function () {
              $('.delete-row').click();
            },
            success: function (data) {
                // setting values of forms elements in tr
                $('#id_fuelsales_set-TOTAL_FORMS').val(data.meter_count - data.meter_count);
                // for adding rows at start

                for (var i = 0; i < data.meter_count; i++) {
                    $(".dynamic-form-add .add-row").click();
                }
                // to remove extra tr rows
                // $(".table").find("tr:gt(" + data.meter_count+1 + ")").remove();
                // add row arrays in Obj
                // to reassign attributes of rows
                var n = 0;
                Obj.arr = [];
                $(".meter").each(function () {
                    $(this).parents("tr").attr("id", n);
                    $("#" + n + " .price").val(data.price);
                    $("#" + n + " .previous_reading").val(data.current_readings[n]);
                    $(this).val(data.meters[n]);
                    Obj.add_row(n, data.meters[n]);
                    n = n + 1;
                });
                $('.meter').attr('disabled', true);
                // $('.previous_reading, .qty_sold, .price, .amount').attr('readonly', true);

            },
            error: function (data) {
                sweetAlert({
                    title: "Oops!",
                    text: "No any product in stock under this product ID!",
                    type: "error"
                });
            }
        });


    });

$('.delete-row').on('click', function () {
     $(".table tbody tr").not(":last").remove();
});
    // disable scroll on date
    $('#id_date').bind("mousewheel", function () {
        return false;
    });
    // make product list enable
    $(document).on("click", "#id_btn_save", function () {
         $('.meter').attr('disabled', false);
        $('#id_form_save').submit();
    });
    // make product list enable
    $(document).on("click", "#updateSaleOrder", function () {
        $(".meter").not(':last').prop("disabled", false).trigger("chosen:updated");
        $('#updatesaleorder').submit();
    });
    // move amount value to right
    $('#id_amount').addClass('text-right');
    //hide last tr
    // $(".dynamic-form-add").hide();

    //select value on click
    $(function () {
        $(document).on('click', 'input[type=text], input[type=number]', function () {
            this.select();
        });
    });
    //to stop scrol on date inputs

    $('#id_date, #id_payment_date').datetimepicker({
        format: "Y-m-d h:m:s",
        onShow: function (ct) {

        },
        timepicker: false
    });


    //price and quantity multiplication
    $(document).on("keyup", ".qty_sold", function () {
        var trid = $(this).parents("tr").attr("id");
        Calculations(trid);
    });


    var load_delete_Form = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-delete").modal("show");
            },
            success: function (data) {
                $("#modal-delete .modal-content").html(data.html_form);
            }
        });
    };

    var save_delete_Form = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#lube_purchase-table tbody").html(data.html_lubes_purchase_list);
                    $("#modal-delete").modal("hide");
                    $("#total_purchases").html(data.total_purchases);
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-delete .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    // Delete
    $("#lube_purchase-table").on("click", ".js-delete-lube_purchase", load_delete_Form);
    $("#modal-delete").on("submit", ".js-delete-lube_purchase-form", save_delete_Form);
});

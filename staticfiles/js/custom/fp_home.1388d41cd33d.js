$(function () {


    var loadForm_home = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-fp-home").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            success: function (data) {
                $("#modal-fp-home .modal-content").html(data.html_form);
                $(document).on('click', 'input[type=text], input[type=number]', function () {
                    this.select();
                });
                $("#id_sum_fps").prop('readonly', true);
                $("#id_product, #id_supplier, #id_duty, #id_decanted_by, #id_tank").chosen({
                    search_contains: true,
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                // add tank in  list
                $("#id_product").change(function () {
                    var url = $(".js-fp-create-form").attr("data-tank-url");
                    var productId = $(this).val();

                    $.ajax({
                        url: url,
                        data: {
                            'product': productId
                        },
                        success: function (data) {
                            $("#id_tank").html(data);
                            $("#id_tank").trigger("chosen:updated");
                        }
                    });

                });
                // tank end
                   // for date set
                $('#id_date').datetimepicker({
                    format: "Y-m-d h:m:s",
                    formatTime: 'h:i a',
                    onShow: function (ct) {

                    },
                    timepicker: true
                });
                // for date set end
            }
        });
    };

    var saveForm_home = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.form_is_valid) {
                    $("#fps_count").html(data.fps_count);
                    $("#fps_not_posted").html(data.not_posted);
                    $("#modal-fp-home").modal("hide");
                    //message
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    $("#modal-fp-home .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    };


    /* Binding */

    // Create book
    $(".js-create-fp-home").click(loadForm_home);
    $("#modal-fp-home").on("submit", ".js-fp-create-form", saveForm_home);

});
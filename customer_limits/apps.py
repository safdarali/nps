from django.apps import AppConfig


class CclimitsConfig(AppConfig):
    name = 'customer_limits'

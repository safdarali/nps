from django.shortcuts import render, get_object_or_404

from nps.custom_decorators.permission_required import permission_required
from .models import CustomerLimit
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import CustomerLimitForm


def customer_limit_list(request):
    customer_limit = CustomerLimit.objects.all()
    return render(request, 'customer_limits/customer_limit_list.html', {'customer_limits': customer_limit})


def save_customer_limit_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            customer_limit = CustomerLimit.objects.all()
            data['html_customer_limit_list'] = render_to_string(
                'customer_limits/includes/partial_customer_limit_list.html', {'customer_limits': customer_limit})
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('customer_limits.add_customer_limit')
def customer_limit_create(request):
    if request.method == 'POST':
        form = CustomerLimitForm(request.POST)
    else:
        form = CustomerLimitForm()
    return save_customer_limit_form(request, form, 'customer_limits/includes/partial_customer_limit_create.html')


@permission_required('customer_limits.change_cclimit')
def customer_limit_update(request, pk):
    customer_limit = get_object_or_404(CustomerLimit, pk=pk)
    if request.method == 'POST':
        form = CustomerLimitForm(request.POST, instance=customer_limit)
    else:
        form = CustomerLimitForm(instance=customer_limit)
    return save_customer_limit_form(request, form, 'customer_limits/includes/partial_customer_limit_update.html')


@permission_required('customer_limits.delete_customer_limit')
def customer_limit_delete(request, pk):
    customer_limit = get_object_or_404(CustomerLimit, pk=pk)
    data = dict()
    if request.method == 'POST':
        customer_limit.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        customer_limit = CustomerLimit.objects.all()
        data['html_customer_limit_list'] = render_to_string('customer_limits/includes/partial_customer_limit_list.html',
                                                            {
                                                                'customer_limits': customer_limit
                                                            })
    else:
        context = {'customer_limit': customer_limit}
        data['html_form'] = render_to_string('customer_limits/includes/partial_customer_limit_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)

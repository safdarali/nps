from django.urls import path
from .views import customer_limit_list, \
    customer_limit_create, customer_limit_update, customer_limit_delete

urlpatterns = [
    path('customer_limits/', customer_limit_list, name='customer_limit_list'),
    path('customer_limits/create', customer_limit_create, name='customer_limit_create'),
    path('customer_limits/<int:pk>/update', customer_limit_update, name='customer_limit_update'),
    path('customer_limits/<int:pk>/delete', customer_limit_delete, name='customer_limit_delete'),
]
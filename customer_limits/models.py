from decimal import Decimal

from django.db import models

# Create your models here.
from chart_of_accounts.models import ChartOfAccount
from nps.models import ModelWithTimeStamp


class CustomerLimit(ModelWithTimeStamp):
    customer = models.OneToOneField(ChartOfAccount, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Customer')
    limit = models.DecimalField(max_digits=15, decimal_places=3, null=False, blank=False,default=Decimal('0.000'),verbose_name='Limit')


    def __str__(self):
        return self.customer
from django import forms

from chart_of_accounts.models import ChartOfAccount
from .models import CustomerLimit


class CustomerLimitForm(forms.ModelForm):
    class Meta:
        model = CustomerLimit
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CustomerLimitForm, self).__init__(*args, **kwargs)
        self.fields['customer'].empty_label = 'Select a club card customer'
        self.fields['customer'].queryset = ChartOfAccount.objects.filter(cccustomertype=2)
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)

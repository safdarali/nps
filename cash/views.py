from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from chart_of_accounts.models import ChartOfAccount
from discounts.models import Discount
from duties.models import Duty
from expenses.forms import ExpenseForm
from products.models import Product
from nps.custom_decorators.permission_required import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from datetime import datetime
from django.views.generic import ListView, CreateView, UpdateView, DetailView, DeleteView
from django.shortcuts import get_object_or_404, redirect, render
from django.http import JsonResponse
from django.template.loader import render_to_string
from general_journal.models import GeneralJournal, TankGeneralJournal
from sales.models import LubeSalem, FuelSalem, SaleTransition
from .forms import CreditSalemForm, CreditSalesForm, CreditSaleFormSet, \
    SupplierPaymentForm, \
    BankDepositionForm, FuelVehicleIncomeForm, NetSalemForm, NetSalesFormSet, RecoverysFormSet, RecoverymForm
from .models import CreditSalem, CreditSales, \
    SupplierPayment, BankDeposition, FuelVehicleIncome, NetSalem, NetSales, Recoverym, Recoverys
from django.db import transaction
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld.id


@login_required
def NetSaleDelete(request, pk):
    cash_in = get_object_or_404(NetSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        cash_in.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        cash_ins = NetSalem.objects.all()
        data['html_cash_in_list'] = render_to_string('cash/includes/partial_cash_in_list.html', {
            'cash_ins': cash_ins
        })
        data['total_cash_in'] = NetSalem.objects.count()
    else:
        context = {'cash_in': cash_in}
        data['html_form'] = render_to_string('cash/includes/partial_cash_in_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


@login_required
def LatestNetSaleDelete(request, pk):
    cash_in = get_object_or_404(NetSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        cash_in.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        d = NetSalem.objects.latest('id')
        data['html_cash_in_list'] = render_to_string('cash/includes/partial_cash_in_latest.html', {
            'cash_in': d
        })
        data['total_cash_ins'] = NetSalem.objects.count()
    else:
        context = {'cash_in': cash_in}
        data['html_form'] = render_to_string('cash/includes/partial_latest_cash_in_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class NetSaleList(ListView, LoginRequiredMixin):
    template_name = 'cash/cash_in_list.html'
    context_object_name = 'cash_ins'
    model = NetSalem

    @method_decorator(permission_required('cash.list_netsale'))
    def dispatch(self, *args, **kwargs):
        return super(NetSaleList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NetSaleList, self).get_context_data(**kwargs)
        context['total_cash_ins'] = NetSalem.objects.count()
        return context


class NetSaleCreate(CreateView, LoginRequiredMixin):
    template_name = 'cash/includes/cash_in_create.html'
    model = NetSalem
    form_class = NetSalemForm

    @method_decorator(permission_required('cash.add_netsalem'))
    def dispatch(self, *args, **kwargs):
        return super(NetSaleCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(NetSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_context_data(self, **kwargs):
        data = super(NetSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['cash_in'] = NetSalesFormSet(self.request.POST)
            data['total_cash_ins'] = NetSalem.objects.count()
        else:
            data['cash_in'] = NetSalesFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['cash_in']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(NetSaleCreate, self).form_valid(form)


class LatestNetSaleCreate(CreateView, LoginRequiredMixin):
    template_name = 'cash/includes/cash_in_create.html'
    model = NetSalem
    form_class = NetSalemForm

    def get_form_kwargs(self):
        kwargs = super(LatestNetSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest')

    def get_context_data(self, **kwargs):
        data = super(LatestNetSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['cash_in'] = NetSalesFormSet(self.request.POST)
            data['total_cash_ins'] = NetSalem.objects.count()
        else:
            data['cash_in'] = NetSalesFormSet()
        return data

    def form_valid(self, form):
        date = form.cleaned_data['date']
        context = self.get_context_data()
        invoices = context['cash_in']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(LatestNetSaleCreate, self).form_valid(form)


class NetSaleUpdate(UpdateView, LoginRequiredMixin):
    model = NetSalem
    form_class = NetSalemForm
    template_name = 'cash/includes/cash_in_update.html'
    success_url = '/cash_ins'

    def get_form_kwargs(self):
        kwargs = super(NetSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = NetSalemForm
        form = self.get_form(form_class)
        cash_in = NetSalesFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, cash_in=cash_in))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = NetSalemForm
        form = self.get_form(form_class)
        cash_in = NetSalesFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and cash_in.is_valid():
            return self.form_valid(form, cash_in)
        return self.form_invalid(form, cash_in)

    def form_valid(self, form, cash_in):
        self.object = form.save()
        cash_in.instance = self.object
        cash_in.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, cash_in):
        return self.render_to_response(self.get_context_data(form=form, cash_in=cash_in))


class LatestNetSaleUpdate(UpdateView, LoginRequiredMixin):
    model = NetSalem
    form_class = NetSalemForm
    template_name = 'cash/includes/cash_in_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_form_kwargs(self):
        kwargs = super(LatestNetSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = NetSalemForm
        form = self.get_form(form_class)
        cash_in = NetSalesFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, cash_in=cash_in))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = NetSalemForm
        form = self.get_form(form_class)
        cash_in = NetSalesFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and cash_in.is_valid():
            return self.form_valid(form, cash_in)
        return self.form_invalid(form, cash_in)

    def form_valid(self, form, cash_in):
        duty = form.cleaned_data['duty']
        self.object = form.save()
        cash_in.instance = self.object
        id = form.instance.id
        cash_in.save()
        net_amount = form.cleaned_data['net_amount']
        cig = NetSales.objects.filter(netsalem_id=id)
        petrol_amount = 0
        diesel_amount = 0
        # general journal entry
        for i in cig:
            if i.account is not None:
                if i.product_id == 1:
                    petrol_amount += i.amount
                elif i.product_id == 2:
                    diesel_amount += i.amount
                general_journal_2 = GeneralJournal(chartofaccount_id=i.account.id, duty_id=duty.id,
                                                   voucher_type='CS', voucher_number=id, debit=0,
                                                   credit=i.amount, naration='By Cash In Hand Account')
                general_journal_2.save()
        general_journal_1 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id,
                                           voucher_type='CS', voucher_number=id,
                                           debit=net_amount, credit=0, naration='To Counter Sale Account')
        general_journal_1.save()
        # general journal entry end
        # UPDATE SALE TRANSITION
        sale_transition = SaleTransition.objects.filter(duty_id=duty.id)
        for st in sale_transition:
            if st.product_id == 1:
                st_obj = SaleTransition.objects.get(id=st.id)
                st_obj.allocated = st_obj.allocated + petrol_amount
                st_obj.balance = st_obj.balance - petrol_amount
                st_obj.save()
            elif st.product_id == 2:
                st_obj = SaleTransition.objects.get(id=st.id)
                st_obj.allocated = st_obj.allocated + diesel_amount
                st_obj.balance = st_obj.balance - diesel_amount
                st_obj.save()
        # STATUS UPDATE
        nsem = NetSalem.objects.get(id=id)
        nsem.status = 2
        nsem.save()
        return HttpResponseRedirect(self.get_success_url())


class NetSaleDetail(DetailView, LoginRequiredMixin):
    template_name = 'cash/includes/cash_in_detail.html'
    context_object_name = 'cash_in'
    model = NetSalem


class NetSaleLatest(ListView, LoginRequiredMixin):
    template_name = 'duties/cash_in_latest.html'
    context_object_name = 'cash_in'
    model = NetSalem

    def get_queryset(self):
        queryset = super(NetSaleLatest, self).get_queryset()
        queryset = queryset.latest('id')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(NetSaleLatest, self).get_context_data(**kwargs)
        context['total_cash_ins'] = NetSalem.objects.count()
        return context


class LatestNetSaleDelete(DeleteView, LoginRequiredMixin):
    template_name = 'cash/includes/partial_latest_fuel_sale_delete.html'
    model = FuelSalem


class LatestNetSaleDelete(DeleteView, LoginRequiredMixin):
    template_name = 'cash/includes/partial_latest_cash_in_delete.html'
    model = NetSalem

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        gj = GeneralJournal.objects.filter(voucher_number=self.object.id, voucher_type='DUTY-CI')
        gj.delete()
        self.object.delete()

        return HttpResponseRedirect(success_url)


# cash out

# RECOVERY
@login_required
def RecoveryDelete(request, pk):
    recovery = get_object_or_404(Recoverym, pk=pk)
    data = dict()
    if request.method == 'POST':
        recovery.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        recoverys = Recoverym.objects.all()
        data['html_recovery_list'] = render_to_string('cash/includes/partial_recovery_list.html', {
            'recoverys': recoverys
        })
        data['total_recovery'] = Recoverym.objects.count()
    else:
        context = {'recovery': recovery}
        data['html_form'] = render_to_string('cash/includes/partial_recovery_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


@login_required
def LatestRecoveryDelete(request, pk):
    recovery = get_object_or_404(Recoverym, pk=pk)
    data = dict()
    if request.method == 'POST':
        recovery.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        d = Recoverym.objects.latest('id')
        data['html_recovery_list'] = render_to_string('cash/includes/partial_recovery_latest.html', {
            'recovery': d
        })
        data['total_recoverys'] = Recoverym.objects.count()
    else:
        context = {'recovery': recovery}
        data['html_form'] = render_to_string('cash/includes/partial_latest_recovery_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class RecoveryList(ListView, LoginRequiredMixin):
    template_name = 'cash/recovery_list.html'
    context_object_name = 'recoverys'
    model = Recoverym

    @method_decorator(permission_required('cash.list_Recovery'))
    def dispatch(self, *args, **kwargs):
        return super(RecoveryList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RecoveryList, self).get_context_data(**kwargs)
        context['total_recoverys'] = Recoverym.objects.count()
        return context


class RecoveryCreate(CreateView, LoginRequiredMixin):
    template_name = 'cash/includes/recovery_create.html'
    model = Recoverym
    form_class = RecoverymForm

    @method_decorator(permission_required('cash.add_Recoverym'))
    def dispatch(self, *args, **kwargs):
        return super(RecoveryCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(RecoveryCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_context_data(self, **kwargs):
        data = super(RecoveryCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['recovery'] = RecoverysFormSet(self.request.POST)
            data['total_recoverys'] = Recoverym.objects.count()
        else:
            data['recovery'] = RecoverysFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['recovery']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(RecoveryCreate, self).form_valid(form)


class LatestRecoveryCreate(CreateView, LoginRequiredMixin):
    template_name = 'cash/includes/recovery_create.html'
    model = Recoverym
    form_class = RecoverymForm

    def get_form_kwargs(self):
        kwargs = super(LatestRecoveryCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest')

    def get_context_data(self, **kwargs):
        data = super(LatestRecoveryCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['recovery'] = RecoverysFormSet(self.request.POST)
            data['total_recoverys'] = Recoverym.objects.count()
        else:
            data['recovery'] = RecoverysFormSet()
        return data

    def form_valid(self, form):
        date = form.cleaned_data['date']
        context = self.get_context_data()
        invoices = context['recovery']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()

        return super(LatestRecoveryCreate, self).form_valid(form)


class RecoveryUpdate(UpdateView, LoginRequiredMixin):
    model = Recoverym
    form_class = RecoverymForm
    template_name = 'cash/includes/recovery_update.html'
    success_url = '/recoverys'

    def get_form_kwargs(self):
        kwargs = super(RecoveryUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = RecoverymForm
        form = self.get_form(form_class)
        recovery = RecoverysFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, recovery=recovery))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = RecoverymForm
        form = self.get_form(form_class)
        recovery = RecoverysFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and recovery.is_valid():
            return self.form_valid(form, recovery)
        return self.form_invalid(form, recovery)

    def form_valid(self, form, recovery):
        self.object = form.save()
        recovery.instance = self.object
        recovery.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, recovery):
        return self.render_to_response(self.get_context_data(form=form, recovery=recovery))


class LatestRecoveryUpdate(UpdateView, LoginRequiredMixin):
    model = Recoverym
    form_class = RecoverymForm
    template_name = 'cash/includes/recovery_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_form_kwargs(self):
        kwargs = super(LatestRecoveryUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = RecoverymForm
        form = self.get_form(form_class)
        recovery = RecoverysFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, recovery=recovery))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = RecoverymForm
        form = self.get_form(form_class)
        recovery = RecoverysFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and recovery.is_valid():
            return self.form_valid(form, recovery)
        return self.form_invalid(form, recovery)

    def form_valid(self, form, recovery):
        duty = form.cleaned_data['duty']
        self.object = form.save()
        recovery.instance = self.object
        id = form.instance.id
        recovery.save()
        net_amount = form.cleaned_data['net_amount']
        cig = Recoverys.objects.filter(recoverym_id=id)
        naration = ''
        # general journal entry
        for i in cig:
            naration += f'{i.account},'
            general_journal_2 = GeneralJournal(chartofaccount_id=i.account.id, duty_id=duty.id,
                                                   voucher_type='CS', voucher_number=id, debit=0,
                                                   credit=i.amount, naration='By Cash In Hand Account')
            general_journal_2.save()
        general_journal_1 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id,
                                           voucher_type='CS', voucher_number=id,
                                           debit=net_amount, credit=0, naration=f'To {naration}')
        general_journal_1.save()
        # general journal entry end

        nsem = Recoverym.objects.get(id=id)
        nsem.status = 2
        nsem.save()
        return HttpResponseRedirect(self.get_success_url())


class RecoveryDetail(DetailView, LoginRequiredMixin):
    template_name = 'cash/includes/recovery_detail.html'
    context_object_name = 'recovery'
    model = Recoverym


class RecoveryLatest(ListView, LoginRequiredMixin):
    template_name = 'duties/recovery_latest.html'
    context_object_name = 'recovery'
    model = Recoverym

    def get_queryset(self):
        queryset = super(RecoveryLatest, self).get_queryset()
        queryset = queryset.latest('id')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(RecoveryLatest, self).get_context_data(**kwargs)
        context['total_recoverys'] = Recoverym.objects.count()
        return context


class LatestRecoveryDelete(DeleteView, LoginRequiredMixin):
    template_name = 'cash/includes/partial_latest_fuel_sale_delete.html'
    model = FuelSalem


class LatestRecoveryDelete(DeleteView, LoginRequiredMixin):
    template_name = 'cash/includes/partial_latest_recovery_delete.html'
    model = Recoverym

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        gj = GeneralJournal.objects.filter(voucher_number=self.object.id, voucher_type='DUTY-CI')
        gj.delete()
        self.object.delete()

        return HttpResponseRedirect(success_url)

# RECOVERY END
class GetDiscount(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        p_id = request.GET.get('p_id')
        c_id = request.GET.get('c_id')
        if p_id is not None and c_id is not None:
            try:
                discount = Discount.objects.filter(customer_id=c_id, product_id=p_id)[0]
                data['discount'] = discount.discount
            except:
                data['discount'] = 0
            try:
                sale_price = Product.objects.get(id=p_id)
                data['sale_price'] = sale_price.sale_price
            except:
                data['sale_price'] = 0
        return JsonResponse(data)


class GetPrice(ListView):
    def get(self, request, *args, **kwargs):
        data = dict()
        p_id = request.GET.get('p_id')
        if p_id is not None:
            try:
                sale_price = Product.objects.get(id=p_id)
                data['sale_price'] = sale_price.sale_price
            except:
                data['sale_price'] = 0
        return JsonResponse(data)


@login_required
def CreditSaleDelete(request, pk):
    cash_out = get_object_or_404(CreditSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        cash_out.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        cash_outs = CreditSalem.objects.all()
        data['html_cash_out_list'] = render_to_string('cash/includes/partial_cash_out_list.html', {
            'cash_outs': cash_outs
        })
        data['total_cash_out'] = CreditSalem.objects.count()
    else:
        context = {'cash_out': cash_out}
        data['html_form'] = render_to_string('cash/includes/partial_cash_out_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


@login_required
def LatestCreditSaleDelete(request, pk):
    cash_out = get_object_or_404(CreditSalem, pk=pk)
    data = dict()
    if request.method == 'POST':
        cash_out.delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        d = CreditSalem.objects.latest('id')
        data['html_cash_out_list'] = render_to_string('cash/includes/partial_cash_out_latest.html', {
            'cash_out': d
        })
        data['total_cash_outs'] = CreditSalem.objects.count()
    else:
        context = {'cash_out': cash_out}
        data['html_form'] = render_to_string('cash/includes/partial_latest_cash_out_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class CreditSaleList(ListView, LoginRequiredMixin):
    template_name = 'cash/cash_out_list.html'
    context_object_name = 'cash_outs'
    model = CreditSalem

    @method_decorator(permission_required('cash.list_creditsale'))
    def dispatch(self, *args, **kwargs):
        return super(CreditSaleList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreditSaleList, self).get_context_data(**kwargs)
        context['total_cash_outs'] = CreditSalem.objects.count()
        return context


class CreditSaleCreate(CreateView, LoginRequiredMixin):
    template_name = 'cash/includes/cash_out_create.html'
    model = CreditSalem
    form_class = CreditSalemForm

    @method_decorator(permission_required('cash.add_creditsalem'))
    def dispatch(self, *args, **kwargs):
        return super(CreditSaleCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(CreditSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_context_data(self, **kwargs):
        data = super(CreditSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['cash_out'] = CreditSaleFormSet(self.request.POST)
            data['total_cash_outs'] = CreditSalem.objects.count()
        else:
            data['cash_out'] = CreditSaleFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        invoices = context['cash_out']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                id = form.instance.id
                invoices.user = self.request.user
                invoices.save()

        return super(CreditSaleCreate, self).form_valid(form)


class LatestCreditSaleCreate(CreateView, LoginRequiredMixin):
    template_name = 'cash/includes/cash_out_create.html'
    model = CreditSalem
    form_class = CreditSalemForm

    def get_form_kwargs(self):
        kwargs = super(LatestCreditSaleCreate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': self.object.pk})

    def get_context_data(self, **kwargs):
        data = super(LatestCreditSaleCreate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['cash_out'] = CreditSaleFormSet(self.request.POST)
            data['total_cash_outs'] = CreditSalem.objects.count()
        else:
            data['cash_out'] = CreditSaleFormSet()
        return data

    def form_valid(self, form):
        date = form.cleaned_data['date']
        context = self.get_context_data()
        invoices = context['cash_out']
        with transaction.atomic():
            self.object = form.save()
            if invoices.is_valid():
                invoices.instance = self.object
                invoices.user = self.request.user
                invoices.save()
                # general journal entry
                for i in invoices:
                    if i.account is not None:
                        if i.account.account_type == 10:
                            debit_naration = 'Cash Counter sale'
                            crdit_naration = ''
                            voucher_type = 'CS'
                        else:
                            debit_naration = 'Cash received'
                            crdit_naration = ''
                            voucher_type = 'CR'
                        general_journal_1 = GeneralJournal(chartofaccount_id=i.account, date=date,
                                                           voucher_type=voucher_type, voucher_number=i.id,
                                                           debit=i.amount, credit=0, naration=debit_naration)
                        general_journal_1.save()
                        general_journal_2 = GeneralJournal(chartofaccount_id=i.account, date=date,
                                                           voucher_type=voucher_type, voucher_number=i.id, debit=0,
                                                           credit=i.amount, naration=crdit_naration)
                        general_journal_2.save()
                        # general journal entry end
        return super(LatestCreditSaleCreate, self).form_valid(form)


class CreditSaleUpdate(UpdateView, LoginRequiredMixin):
    model = CreditSalem
    form_class = CreditSalemForm
    template_name = 'cash/includes/cash_out_update.html'
    success_url = '/cash_outs'

    @method_decorator(permission_required('cash.change_creditsalem'))
    def dispatch(self, *args, **kwargs):
        return super(CreditSaleUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(CreditSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = CreditSalemForm
        form = self.get_form(form_class)
        cash_out = CreditSaleFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, cash_out=cash_out))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = CreditSalemForm
        form = self.get_form(form_class)
        cash_out = CreditSaleFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and cash_out.is_valid():
            return self.form_valid(form, cash_out)
        return self.form_invalid(form, cash_out)

    def form_valid(self, form, cash_out):
        self.object = form.save()
        cash_out.instance = self.object
        cash_out.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, cash_out):
        return self.render_to_response(self.get_context_data(form=form, cash_out=cash_out))


class LatestCreditSaleUpdate(UpdateView, LoginRequiredMixin):
    model = CreditSalem
    form_class = CreditSalemForm
    template_name = 'cash/includes/cash_out_update.html'

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def get_form_kwargs(self):
        kwargs = super(LatestCreditSaleUpdate, self).get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = CreditSalemForm
        form = self.get_form(form_class)
        cash_out = CreditSaleFormSet(instance=self.object)
        return self.render_to_response(self.get_context_data(form=form, cash_out=cash_out))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = CreditSalemForm
        form = self.get_form(form_class)
        cash_out = CreditSaleFormSet(self.request.POST, instance=self.object)

        if form.is_valid() and cash_out.is_valid():
            return self.form_valid(form, cash_out)
        return self.form_invalid(form, cash_out)

    def form_valid(self, form, cash_out):
        duty = form.cleaned_data['duty']
        net_discount_amount = form.cleaned_data['net_discount_amount']
        self.object = form.save()
        cash_out.instance = self.object
        id = form.instance.id
        cash_out.save()
        cig = CreditSales.objects.filter(creditsalem_id=id)
        petrol_amount = 0
        diesel_amount = 0
        # general journal entry
        for i in cig:
            if i.debit_account is not None and i.product is not None:
                if i.product_id == 1:
                    petrol_amount += i.amount
                elif i.product_id == 2:
                    diesel_amount += i.amount
                voucher_type = 'CO'
                if i.product.product_type == 4 or i.product.product_type == 5:
                    general_journal_1 = GeneralJournal(chartofaccount_id=i.debit_account.id, duty_id=duty.id,
                                                       voucher_type=voucher_type, voucher_number=id, debit=i.amount,
                                                       credit=0, naration=f"To {i.product}, {i.qty_sold}L")
                    general_journal_1.save()
                    general_journal_2 = GeneralJournal(
                        chartofaccount=ChartOfAccount.objects.filter(product_id=i.product.id, subaccount_id=4001)[0],
                        duty_id=duty.id, voucher_type=voucher_type, voucher_number=id, debit=0, credit=i.nozzle_rate_amount,
                        naration=f"By {i.debit_account}")
                    general_journal_2.save()

                    if i.discount_amount > 0:
                        general_journal_4 = GeneralJournal(chartofaccount_id=50090004, duty_id=duty.id,
                                                           voucher_type=voucher_type, voucher_number=id,
                                                           debit=i.discount_amount, credit=0, naration=f"To {i.product}, {i.qty_sold}L, {i.debit_account}")
                        general_journal_4.save()
            # general journal entry end
            # UPDATE SALE TRANSITION
            sale_transition = SaleTransition.objects.filter(duty_id=duty.id)
            for st in sale_transition:
                if st.product_id == 1:
                    st_obj = SaleTransition.objects.get(id=st.id)
                    st_obj.allocated = st_obj.allocated + petrol_amount
                    st_obj.balance = st_obj.balance - petrol_amount
                    st_obj.save()
                elif st.product_id == 2:
                    st_obj = SaleTransition.objects.get(id=st.id)
                    st_obj.allocated = st_obj.allocated + diesel_amount
                    st_obj.balance = st_obj.balance - diesel_amount
                    st_obj.save()
        csem = CreditSalem.objects.get(id=id)
        csem.status = 2
        csem.save()

        return HttpResponseRedirect(self.get_success_url())


class CreditSaleDetail(DetailView, LoginRequiredMixin):
    template_name = 'cash/includes/cash_out_detail.html'
    context_object_name = 'cash_out'
    model = CreditSalem


class CreditSaleLatest(ListView, LoginRequiredMixin):
    template_name = 'duties/cash_out_latest.html'
    context_object_name = 'cash_out'
    model = CreditSalem

    def get_queryset(self):
        queryset = super(CreditSaleLatest, self).get_queryset()
        queryset = queryset.latest('id')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(CreditSaleLatest, self).get_context_data(**kwargs)
        context['total_cash_outs'] = CreditSalem.objects.count()
        return context


class LatestCreditSaleDelete(DeleteView, LoginRequiredMixin):
    template_name = 'cash/includes/partial_latest_fuel_sale_delete.html'
    model = FuelSalem

    @method_decorator(permission_required('cash.delete_creditsalem'))
    def dispatch(self, *args, **kwargs):
        return super(LatestCreditSaleDelete, self).dispatch(*args, **kwargs)


class LatestCash_outDelete(DeleteView, LoginRequiredMixin):
    template_name = 'cash/includes/partial_latest_cash_out_delete.html'
    model = CreditSalem

    def get_success_url(self):
        return reverse_lazy('duty_latest', kwargs={'random_duty': get_latest_duty_date()})

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        gj = GeneralJournal.objects.filter(voucher_number=self.object.id, voucher_type='DUTY-CO')
        gj.delete()
        self.object.delete()

        return HttpResponseRedirect(success_url)


# SUPLIER PAYMENT

def supplier_payment_home(request):
    total_supplier_payment = SupplierPayment.objects.count()
    return render(request, 'cash/supplier_payment_home.html', {'total_supplier_payments': total_supplier_payment})


@permission_required('cash.list_supplierpayment')
def supplier_payment_list(request):
    supplier_payments = SupplierPayment.objects.all()
    total_supplier_payment = SupplierPayment.objects.count()
    return render(request, 'cash/supplier_payment_list.html',
                  {'supplier_payments': supplier_payments, 'total_supplier_payments': total_supplier_payment})


def save_supplier_payment_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            data['total_supplier_payments'] = SupplierPayment.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_supplier_payment_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # general journal entry
            id = form.instance.id
            amount = form.cleaned_data['amount']
            supplier = form.cleaned_data['supplier']
            if form.cleaned_data['bank'] is not None:
                credit_ac = form.cleaned_data['bank'].id
                bank_name = ChartOfAccount.objects.get(id=credit_ac).account_name
                vourcher_type = 'SPCP/B'
            else:
                credit_ac = 30010001
                bank_name = ChartOfAccount.objects.get(id=30010001).account_name
                vourcher_type = 'SPCP'
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=supplier.id, duty_id=duty.id, voucher_type=vourcher_type,
                                               voucher_number=id, debit=amount, credit=0, naration=f'To {bank_name} account')
            # SP supplier payment
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=credit_ac, duty_id=duty.id, voucher_type=vourcher_type,
                                               voucher_number=id, debit=0, credit=amount,
                                               naration=f'By {supplier} account')
            general_journal_2.save()
            # general journal entry end
            sp = SupplierPayment.objects.get(id=id)
            sp.status = 2
            sp.save()
            data['form_is_valid'] = True
            supplier_payments = SupplierPayment.objects.all()
            data['html_supplier_payment_list'] = render_to_string('cash/includes/partial_supplier_payment_list.html', {
                'supplier_payments': supplier_payments
            })
            data['total_supplier_payments'] = SupplierPayment.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('cash.add_supplierpayment')
def supplier_payment_create(request):
    if request.method == 'POST':
        form = SupplierPaymentForm(request.POST)
    else:
        form = SupplierPaymentForm()
    return save_supplier_payment_form(request, form, 'cash/includes/partial_supplier_payment_create.html')


@permission_required('cash.change_supplierpayment')
def supplier_payment_update(request, pk):
    supplier_payment = get_object_or_404(SupplierPayment, pk=pk)
    if request.method == 'POST':
        form = SupplierPaymentForm(request.POST, instance=supplier_payment)
    else:
        form = SupplierPaymentForm(instance=supplier_payment)
    return update_supplier_payment_form(request, form, 'cash/includes/partial_supplier_payment_update.html')


@permission_required('cash.delete_supplierpayment')
def supplier_payment_delete(request, pk):
    supplier_payment = get_object_or_404(SupplierPayment, pk=pk)
    data = dict()
    if request.method == 'POST':
        supplier_payment.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='SP').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        supplier_payments = SupplierPayment.objects.all()
        data['html_supplier_payment_list'] = render_to_string('cash/includes/partial_supplier_payment_list.html', {
            'supplier_payments': supplier_payments
        })
        data['total_supplier_payments'] = SupplierPayment.objects.count()
    else:
        context = {'supplier_payment': supplier_payment}
        data['html_form'] = render_to_string('cash/includes/partial_supplier_payment_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# BANK DEPOSITION

def bank_deposition_home(request):
    total_bank_deposition = BankDeposition.objects.count()
    return render(request, 'cash/bank_deposition_home.html', {'total_bank_depositions': total_bank_deposition})


@permission_required('cash.list_bankdeposition')
def bank_deposition_list(request):
    bank_depositions = BankDeposition.objects.all()
    total_bank_deposition = BankDeposition.objects.count()
    return render(request, 'cash/bank_deposition_list.html',
                  {'bank_depositions': bank_depositions, 'total_bank_depositions': total_bank_deposition})


def save_bank_deposition_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            latest_duty = Duty.objects.latest('id')
            data['form_is_valid'] = True
            data['total_bank_depositions'] = BankDeposition.objects.count()
            bank_depositions = BankDeposition.objects.filter(duty=latest_duty)
            data['html_bank_deposition_list'] = render_to_string('cash/includes/partial_bank_deposition_list.html', {
                'bank_depositions': bank_depositions
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_bank_deposition_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            latest_duty = Duty.objects.latest('id')
            id = form.instance.id
            amount = form.cleaned_data['amount']
            bank = form.cleaned_data['bank'].id
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=bank, duty_id=duty.id, voucher_type='BD',
                                               voucher_number=id,
                                               debit=amount, credit=0, naration='To cash account')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id, voucher_type='BD',
                                               voucher_number=id,
                                               debit=0, credit=amount, naration='By bank account')
            general_journal_2.save()
            # general journal entry end
            bd = BankDeposition.objects.get(id=id)
            bd.status = 2
            bd.save()
            data['form_is_valid'] = True
            bank_depositions = BankDeposition.objects.filter(duty=latest_duty)
            data['html_bank_deposition_list'] = render_to_string('cash/includes/partial_bank_deposition_list.html', {
                'bank_depositions': bank_depositions
            })
            data['total_bank_depositions'] = BankDeposition.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@permission_required('cash.add_bankdeposition')
def bank_deposition_create(request):
    if request.method == 'POST':
        form = BankDepositionForm(request.POST)
    else:
        form = BankDepositionForm()
    return save_bank_deposition_form(request, form, 'duties/includes/partial_bank_deposition_create.html')


@permission_required('cash.change_bankdeposition')
def bank_deposition_update(request, pk):
    bank_deposition = get_object_or_404(BankDeposition, pk=pk)
    if request.method == 'POST':
        form = BankDepositionForm(request.POST, instance=bank_deposition)
    else:
        form = BankDepositionForm(instance=bank_deposition)
    return update_bank_deposition_form(request, form, 'duties/includes/partial_bank_deposition_update.html')


@permission_required('cash.delete_bankdeposition')
def bank_deposition_delete(request, pk):
    bank_deposition = get_object_or_404(BankDeposition, pk=pk)
    data = dict()
    if request.method == 'POST':
        bank_deposition.delete()
        latest_duty = Duty.objects.latest('id')
        # GeneralJournal.objects.filter(voucher_number=pk, voucher_type='BD').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        bank_depositions = BankDeposition.objects.filter(duty=latest_duty)
        data['html_bank_deposition_list'] = render_to_string('cash/includes/partial_bank_deposition_list.html', {
            'bank_depositions': bank_depositions
        })
        data['total_bank_depositions'] = BankDeposition.objects.count()
    else:
        context = {'bank_deposition': bank_deposition}
        data['html_form'] = render_to_string('cash/includes/partial_bank_deposition_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


# fuel vehicle income
def fuel_vehicle_income_home(request):
    total_fuel_vehicle_income = FuelVehicleIncome.objects.count()
    return render(request, 'fuel_vehicle_incomes/fuel_vehicle_income_home.html',
                  {'total_fuel_vehicle_incomes': total_fuel_vehicle_income})


def fuel_vehicle_income_list(request):
    fuel_vehicle_incomes = FuelVehicleIncome.objects.all()
    return render(request, 'fuel_vehicle_incomes/fuel_vehicle_income_list.html',
                  {'fuel_vehicle_incomes': fuel_vehicle_incomes})


def save_fuel_vehicle_income_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            id = form.instance.id
            amount = form.cleaned_data['amount']
            vehicle = form.cleaned_data['vehicle'].id
            duty = form.cleaned_data['duty']
            general_journal_1 = GeneralJournal(chartofaccount_id=30010001, duty_id=duty.id, voucher_type='FVI',
                                               voucher_number=id,
                                               debit=amount, credit=0, naration='To fuel vehicle income')
            general_journal_1.save()
            general_journal_2 = GeneralJournal(chartofaccount_id=vehicle, duty_id=duty.id, voucher_type='FVI',
                                               voucher_number=id,
                                               debit=0, credit=amount, naration='By cash in hand account')
            general_journal_2.save()
            # general journal entry end
            data['form_is_valid'] = True
            fuel_vehicle_incomes = FuelVehicleIncome.objects.all()
            data['html_fuel_vehicle_income_list'] = render_to_string(
                'fuel_vehicle_incomes/includes/partial_fuel_vehicle_income_list.html', {
                    'fuel_vehicle_incomes': fuel_vehicle_incomes
                })
            data['total_fuel_vehicle_incomes'] = FuelVehicleIncome.objects.count()
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def update_fuel_vehicle_income_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            id = form.instance.id
            # general journal entry
            amount = form.cleaned_data['amount']
            vehicle = form.cleaned_data['vehicle'].id
            date = form.cleaned_data['date']
            g1 = GeneralJournal.objects.filter(voucher_type='FVI', voucher_number=id).order_by('id').first()
            g1.debit = amount
            g1.date = date
            g1.save()
            g2 = GeneralJournal.objects.filter(voucher_type='FVI', voucher_number=id).order_by('id').last()
            g2.credit = amount
            g2.chartofaccount_id = vehicle
            g2.date = date
            g2.save()
            # general journal entry end
            data['form_is_valid'] = True
            fuel_vehicle_incomes = FuelVehicleIncome.objects.all()
            data['html_fuel_vehicle_income_list'] = render_to_string(
                'fuel_vehicle_incomes/includes/partial_fuel_vehicle_income_list.html', {
                    'fuel_vehicle_incomes': fuel_vehicle_incomes
                })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


# @permission_required('cash.add_fuel_vehicle_income')
def fuel_vehicle_income_create(request):
    if request.method == 'POST':
        form = FuelVehicleIncomeForm(request.POST)
    else:
        form = FuelVehicleIncomeForm()
    return save_fuel_vehicle_income_form(request, form,
                                         'fuel_vehicle_incomes/includes/partial_fuel_vehicle_income_create.html')


# @permission_required('fuel_vehicle_incomes.change_fuel_vehicle_income')
def fuel_vehicle_income_update(request, pk):
    fuel_vehicle_income = get_object_or_404(FuelVehicleIncome, pk=pk)
    if request.method == 'POST':
        form = FuelVehicleIncomeForm(request.POST, instance=fuel_vehicle_income)
    else:
        form = FuelVehicleIncomeForm(instance=fuel_vehicle_income)
    return update_fuel_vehicle_income_form(request, form,
                                           'fuel_vehicle_incomes/includes/partial_fuel_vehicle_income_update.html')


# @permission_required('fuel_vehicle_incomes.delete_fuel_vehicle_income')
def fuel_vehicle_income_delete(request, pk):
    fuel_vehicle_income = get_object_or_404(FuelVehicleIncome, pk=pk)
    data = dict()
    if request.method == 'POST':
        fuel_vehicle_income.delete()
        GeneralJournal.objects.filter(voucher_number=pk, voucher_type='FVI').delete()
        data['form_is_valid'] = True  # This is just to play along with the existing code
        fuel_vehicle_incomes = FuelVehicleIncome.objects.all()
        data['html_fuel_vehicle_income_list'] = render_to_string(
            'fuel_vehicle_incomes/includes/partial_fuel_vehicle_income_list.html', {
                'fuel_vehicle_incomes': fuel_vehicle_incomes
            })
    else:
        context = {'fuel_vehicle_income': fuel_vehicle_income}
        data['html_form'] = render_to_string('fuel_vehicle_incomes/includes/partial_fuel_vehicle_income_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)



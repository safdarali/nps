from django.db import models
from decimal import Decimal
from django.conf import settings
from chart_of_accounts.models import ChartOfAccount
from duties.models import DutyMen, Duty
from django.utils import timezone

from nps.models import ModelWithTimeStamp
from products.models import Product
from .choices import *


class NetSalem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True)
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Net amount')
    net_discount_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Net discount amount')
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True)

    class Meta:
        permissions = (('list_netsalem', 'Can list netsalem'),)


class NetSales(ModelWithTimeStamp):
    netsalem = models.ForeignKey(NetSalem, on_delete=models.CASCADE, null=True, blank=True)
    account = models.ForeignKey(ChartOfAccount, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Account')
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, blank=True, default=None,verbose_name='Product')
    qty_sold = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, verbose_name='Qty sold', default=Decimal('0.000'))
    price = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Price / litre')
    nozzle_rate_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Nozzle/Amount')
    discount = models.DecimalField(max_digits=8, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Discount/L')
    discount_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False,default=Decimal('0.000'), verbose_name='Discount amount')
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Amount')

    class Meta:
        default_permissions = ()
        # unique_together = ('account','product',)

    def __str__(self):
        return self.account


class Recoverym(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True)
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Net amount')
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True)

    class Meta:
        permissions = (('list_recoverym', 'Can list recoverym'),)


class Recoverys(ModelWithTimeStamp):
    recoverym = models.ForeignKey(Recoverym, on_delete=models.CASCADE, null=True, blank=True)
    account = models.ForeignKey(ChartOfAccount, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Account')
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Amount')

    class Meta:
        default_permissions = ()
        # unique_together = ('account','product',)

    def __str__(self):
        return self.account


class CreditSalem(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    duty_person = models.ForeignKey(DutyMen, on_delete=models.CASCADE, null=False, blank=False)
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True)
    net_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Net amount')
    net_discount_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Net discount amount')
    status = models.SmallIntegerField(verbose_name='Status', choices=STATUS, default=1, null=True, blank=True)
    class Meta:
        permissions = (('list_creditsalem', 'Can list creditsalem'),)

    def __str__(self):
        return "%s" % self.duty.date


class CreditSales(ModelWithTimeStamp):
    creditsalem = models.ForeignKey(CreditSalem, on_delete=models.CASCADE, null=True, blank=True)
    debit_account = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=False, blank=True, verbose_name='Customer', related_name='debit_account')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Product')
    qty_sold = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, verbose_name='Qty sold', default=Decimal('0.000'))
    price = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Price / litre')
    nozzle_rate_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Nozzle/Amount')
    discount = models.DecimalField(max_digits=8, decimal_places=3, null=False, blank=False, default=Decimal('0.000'),verbose_name='Discount/L')
    discount_amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Discount amount')
    amount = models.DecimalField(max_digits=20, decimal_places=3, null=False, blank=False, default=Decimal('0.000'), verbose_name='Amount')

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.debit_account.account_name


class SupplierPayment(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )
    supplier = models.ForeignKey(ChartOfAccount, on_delete=models.SET_NULL, null=True, related_name='sp_supplier')
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Duty')
    bank = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, blank=True, default=None, related_name='sp_bank')
    payment_for = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, blank=True, default=None, verbose_name='Payment for')
    amount = models.DecimalField(max_digits=15, decimal_places=3, verbose_name='Amount')
    description = models.TextField(verbose_name='Description', null=True, blank=True, )
    cheque_no = models.CharField(max_length=20, verbose_name='Cheque #', null=True, blank=True)
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1, null=True, blank=True)

    class Meta:
        permissions = (('list_supplierpayment', 'Can list supplierpayment'),)


class BankDeposition(ModelWithTimeStamp):
    STATUS = (
        (1, 'not posted'),
        (2, 'posted')
    )

    DEPOSITION_FOR = (
        (1, 'Duty Cash'),
        (2, 'Recovery'),
        (3, 'Advance Received')
    )
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Duty')
    bank = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=False, blank=False, related_name='bd_bank')
    deposition_for = models.SmallIntegerField(verbose_name='Deposition for?', choices=DEPOSITION_FOR, default=1)
    amount = models.DecimalField(max_digits=15, decimal_places=3, verbose_name='Amount')
    description = models.TextField(verbose_name='Description', null=True, blank=True, )
    slip_no = models.CharField(max_length=20, verbose_name='Slip #', null=True, blank=True)
    status = models.SmallIntegerField(verbose_name='', choices=STATUS, default=1, null=True, blank=True)

    class Meta:
        permissions = (('list_bankdeposition', 'Can list bankdeposition'),)


class FuelVehicleIncome(ModelWithTimeStamp):
    duty = models.ForeignKey(Duty, on_delete=models.CASCADE, null=True, blank=True)
    vehicle = models.ForeignKey(ChartOfAccount, on_delete=models.CASCADE, null=True, blank=True, default=None, related_name='vehicle')
    amount = models.DecimalField(max_digits=15, decimal_places=3, verbose_name='Amount')

    class Meta:
        permissions = (('list_fuel_vehicle_income', 'Can list fuel_vehicle_income'),)





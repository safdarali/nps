from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import BankDeposition, SupplierPayment, CreditSales


class BankDepositionResource(resources.ModelResource):
    class Meta:
        model = BankDeposition


class BankDepositionAdmin(ImportExportModelAdmin):
    resource_class = BankDepositionResource
    list_display = ('duty', 'bank', 'amount','status')


admin.site.register(BankDeposition, BankDepositionAdmin)


class SupplierPaymentResource(resources.ModelResource):
    class Meta:
        model = SupplierPayment


class SupplierPaymentAdmin(ImportExportModelAdmin):
    resource_class = SupplierPaymentResource
    list_display = ('supplier', 'created_at', 'duty','bank','payment_for','amount','status')


admin.site.register(SupplierPayment, SupplierPaymentAdmin)


class CreditSalesResource(resources.ModelResource):
    class Meta:
        model = CreditSales


class CreditSalesAdmin(ImportExportModelAdmin):
    resource_class = CreditSalesResource
    list_display = ('debit_account', 'product','qty_sold','discount','discount_amount','amount')


admin.site.register(CreditSales, CreditSalesAdmin)


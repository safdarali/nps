# Generated by Django 3.0.7 on 2020-10-25 17:21

from decimal import Decimal
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cash', '0006_auto_20201025_2208'),
    ]

    operations = [
        migrations.AddField(
            model_name='netsalem',
            name='net_discount_amount',
            field=models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Net discount amount'),
        ),
    ]

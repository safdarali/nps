# Generated by Django 3.0.7 on 2020-10-20 02:50

from decimal import Decimal
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('duties', '0001_initial'),
        ('products', '0001_initial'),
        ('chart_of_accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CreditSaleAndExpensem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('net_amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Net amount')),
                ('net_discount_amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Net discount amount')),
                ('status', models.SmallIntegerField(blank=True, choices=[(1, 'not posted'), (2, 'posted')], default=1, null=True, verbose_name='Status')),
                ('duty', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty')),
                ('duty_person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='duties.DutyMen')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'permissions': (('list_creditsaleandexpensem', 'Can list creditsaleandexpensem'),),
            },
        ),
        migrations.CreateModel(
            name='NetSalem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('net_amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Invoice amount')),
                ('status', models.SmallIntegerField(blank=True, choices=[(1, 'not posted'), (2, 'posted')], default=1, null=True, verbose_name='Status')),
                ('duty', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty')),
                ('duty_person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='duties.DutyMen')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'permissions': (('list_netsalem', 'Can list netsalem'),),
            },
        ),
        migrations.CreateModel(
            name='SupplierPayment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('amount', models.DecimalField(decimal_places=3, max_digits=15, verbose_name='Amount')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description')),
                ('cheque_no', models.CharField(blank=True, max_length=20, null=True, verbose_name='Cheque #')),
                ('status', models.SmallIntegerField(blank=True, choices=[(1, 'not posted'), (2, 'posted')], default=1, null=True, verbose_name='')),
                ('bank', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sp_bank', to='chart_of_accounts.ChartOfAccount')),
                ('duty', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty', verbose_name='Duty')),
                ('payment_for', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='chart_of_accounts.ChartOfAccount', verbose_name='Payment for')),
                ('supplier', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='sp_supplier', to='chart_of_accounts.ChartOfAccount')),
            ],
            options={
                'permissions': (('list_supplierpayment', 'Can list supplierpayment'),),
            },
        ),
        migrations.CreateModel(
            name='FuelVehicleIncome',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('amount', models.DecimalField(decimal_places=3, max_digits=15, verbose_name='Amount')),
                ('duty', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty')),
                ('vehicle', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vehicle', to='chart_of_accounts.ChartOfAccount')),
            ],
            options={
                'permissions': (('list_fuel_vehicle_income', 'Can list fuel_vehicle_income'),),
            },
        ),
        migrations.CreateModel(
            name='CreditSaleAndExpenses',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('qty_sold', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=10, verbose_name='Qty sold')),
                ('nozzle_rate_amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Nozzle/Amount')),
                ('discount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=8, verbose_name='Discount/L')),
                ('discount_amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Discount amount')),
                ('amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Amount')),
                ('creditsaleandexpensem', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cash.CreditSaleAndExpensem')),
                ('debit_account', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='debit_account', to='chart_of_accounts.ChartOfAccount', verbose_name='Customer')),
                ('product', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.Product', verbose_name='Product')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='BankDeposition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('deposition_for', models.SmallIntegerField(choices=[(1, 'Duty Cash'), (2, 'Recovery'), (3, 'Advance Received')], default=1, verbose_name='Deposition for?')),
                ('amount', models.DecimalField(decimal_places=3, max_digits=15, verbose_name='Amount')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description')),
                ('slip_no', models.CharField(blank=True, max_length=20, null=True, verbose_name='Slip #')),
                ('status', models.SmallIntegerField(blank=True, choices=[(1, 'not posted'), (2, 'posted')], default=1, null=True, verbose_name='')),
                ('bank', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bd_bank', to='chart_of_accounts.ChartOfAccount')),
                ('duty', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='duties.Duty', verbose_name='Duty')),
            ],
            options={
                'permissions': (('list_bankdeposition', 'Can list bankdeposition'),),
            },
        ),
        migrations.CreateModel(
            name='NetSales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('updated_at', models.DateTimeField(auto_now=True, db_index=True)),
                ('amount', models.DecimalField(decimal_places=3, default=Decimal('0.000'), max_digits=20, verbose_name='Amount')),
                ('account', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='chart_of_accounts.ChartOfAccount', verbose_name='Account')),
                ('netsalem', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cash.NetSalem')),
                ('product', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.Product', verbose_name='Product')),
            ],
            options={
                'default_permissions': (),
                'unique_together': {('account', 'product')},
            },
        ),
    ]

from django.urls import path
from .views import CreditSaleList, CreditSaleUpdate, \
    LatestCash_outDelete, LatestCreditSaleCreate, CreditSaleDelete, \
    LatestCreditSaleUpdate, CreditSaleDetail, CreditSaleCreate, supplier_payment_list, \
    supplier_payment_home, supplier_payment_create, \
    supplier_payment_update, supplier_payment_delete, bank_deposition_list, bank_deposition_home, \
    bank_deposition_create, bank_deposition_update, bank_deposition_delete, \
    fuel_vehicle_income_list, fuel_vehicle_income_create, fuel_vehicle_income_update, fuel_vehicle_income_delete, \
    fuel_vehicle_income_home, GetDiscount, GetPrice, NetSaleList, NetSaleCreate, NetSaleDetail, NetSaleUpdate, \
    LatestNetSaleUpdate, NetSaleDelete, LatestNetSaleCreate, LatestNetSaleDelete, RecoveryList, RecoveryCreate, \
    RecoveryDetail, RecoveryUpdate, LatestRecoveryUpdate, RecoveryDelete, LatestRecoveryCreate, LatestRecoveryDelete

urlpatterns = [

    path('cash_in', NetSaleList.as_view(), name='cash_in_list'),
    path('cash_in/create', NetSaleCreate.as_view(), name='cash_in_create'),
    path('cash_in/<int:pk>/detail', NetSaleDetail.as_view(), name="cash_in_detail"),
    path('cash_in/<int:pk>/update', NetSaleUpdate.as_view(), name="cash_in_update"),
    path('cash_in/latest/<int:pk>/update', LatestNetSaleUpdate.as_view(), name="latest_cash_in_update"),
    path('cash_in/<int:pk>/delete', NetSaleDelete, name="cash_in_delete"),
    path('cash_in/latest/create', LatestNetSaleCreate, name="latest_cash_in_create"),
    path('cash_in/<int:pk>/latest/delete', LatestNetSaleDelete.as_view(), name="latest_cash_in_delete"),

    path('recovery', RecoveryList.as_view(), name='recovery_list'),
    path('recovery/create', RecoveryCreate.as_view(), name='recovery_create'),
    path('recovery/<int:pk>/detail', RecoveryDetail.as_view(), name="recovery_detail"),
    path('recovery/<int:pk>/update', RecoveryUpdate.as_view(), name="recovery_update"),
    path('recovery/latest/<int:pk>/update', LatestRecoveryUpdate.as_view(), name="latest_recovery_update"),
    path('recovery/<int:pk>/delete', RecoveryDelete, name="recovery_delete"),
    path('recovery/latest/create', LatestRecoveryCreate, name="latest_recovery_create"),
    path('recovery/<int:pk>/latest/delete', LatestRecoveryDelete.as_view(), name="latest_recovery_delete"),

    path('cash_out', CreditSaleList.as_view(), name='cash_out_list'),
    path('cash_out/create', CreditSaleCreate.as_view(), name='cash_out_create'),
    path('cash_out/<int:pk>/detail', CreditSaleDetail.as_view(), name="cash_out_detail"),
    path('cash_out/<int:pk>/update', CreditSaleUpdate.as_view(), name="cash_out_update"),
    path('cash_out/latest/<int:pk>/update', LatestCreditSaleUpdate.as_view(), name="latest_cash_out_update"),
    path('cash_out/<int:pk>/delete', CreditSaleDelete, name="cash_out_delete"),
    path('cash_out/latest/create', LatestCreditSaleCreate, name="latest_cash_out_create"),
    path('cash_out/<int:pk>/latest/delete', LatestCash_outDelete.as_view(), name="latest_cash_out_delete"),
    path('cash_out/get_discount', GetDiscount.as_view(), name="get_discount"),
    path('cash_out/get_price', GetPrice.as_view(), name="get_price"),

    path('supplier_payment/', supplier_payment_list, name='supplier_payment_list'),
    path('supplier_payment/home', supplier_payment_home, name='supplier_payment_home'),
    path('supplier_payment/create', supplier_payment_create, name='supplier_payment_create'),
    path('supplier_payment/<int:pk>/update', supplier_payment_update, name='supplier_payment_update'),
    path('supplier_payment/<int:pk>/delete', supplier_payment_delete, name='supplier_payment_delete'),

    path('bank_deposition/', bank_deposition_list, name='bank_deposition_list'),
    path('bank_deposition/home', bank_deposition_home, name='bank_deposition_home'),
    path('bank_deposition/create', bank_deposition_create, name='bank_deposition_create'),
    path('bank_deposition/<int:pk>/update', bank_deposition_update, name='bank_deposition_update'),
    path('bank_deposition/<int:pk>/delete', bank_deposition_delete, name='bank_deposition_delete'),

    path('fuel_vehicle_income/', fuel_vehicle_income_list, name='fuel_vehicle_income_list'),
    path('fuel_vehicle_income/create', fuel_vehicle_income_create, name='fuel_vehicle_income_create'),
    path('fuel_vehicle_income/home', fuel_vehicle_income_home, name='fuel_vehicle_income_home'),
    path('fuel_vehicle_income/<int:pk>/update', fuel_vehicle_income_update, name='fuel_vehicle_income_update'),
    path('fuel_vehicle_income/<int:pk>/delete', fuel_vehicle_income_delete, name='fuel_vehicle_income_delete'),

]

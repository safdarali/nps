from django import forms
from django.db.models import Q
from django.forms import HiddenInput, Textarea
from django.forms.models import inlineformset_factory
from chart_of_accounts.models import ChartOfAccount
from duties.models import Duty, DutyMen
from products.models import Product
from .models import NetSalem, NetSales, CreditSalem, CreditSales, \
    SupplierPayment, BankDeposition, FuelVehicleIncome, Recoverys, Recoverym


def get_latest_duty_date():
    ld = Duty.objects.latest('id')
    return ld


class NetSalesForm(forms.ModelForm):
    class Meta:
        model = NetSales
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(NetSalesForm, self).__init__(*args, **kwargs)
        if self.instance.account_id:
            self.fields['account'].queryset = ChartOfAccount.objects.filter(id=self.instance.account_id)
            self.fields['account'].empty_label = 'Select an Account'
            self.fields['product'].queryset = Product.objects.filter(product_type__in=[0, 4, 5])
            self.fields['product'].empty_label = 'Select a product'
        else:
            self.fields['account'].queryset = ChartOfAccount.objects.filter(account_type=7)
            self.fields['account'].empty_label = 'Select an Account'
            self.fields['product'].queryset = Product.objects.filter(product_type__in=[0, 4, 5])
            self.fields['product'].empty_label = 'Select a product'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


NetSalesFormSet = inlineformset_factory(NetSalem, NetSales, form=NetSalesForm, extra=1)


class NetSalemForm(forms.ModelForm):
    class Meta:
        model = NetSalem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(NetSalemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class RecoverysForm(forms.ModelForm):
    class Meta:
        model = Recoverys
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RecoverysForm, self).__init__(*args, **kwargs)
        if self.instance.account_id:
            self.fields['account'].queryset = ChartOfAccount.objects.filter(id=self.instance.account_id)
            self.fields['account'].empty_label = 'Select an Account'
        else:
            self.fields['account'].queryset = ChartOfAccount.objects.filter(account_type=7)
            self.fields['account'].empty_label = 'Select an Account'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


RecoverysFormSet = inlineformset_factory(Recoverym, Recoverys, form=RecoverysForm, extra=1)


class RecoverymForm(forms.ModelForm):
    class Meta:
        model = Recoverym
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(RecoverymForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class CreditSalesForm(forms.ModelForm):
    class Meta:
        model = CreditSales
        fields = '__all__'

    def clean_product(self):
        product = self.cleaned_data.get('product', None)
        if product is None:
            raise forms.ValidationError('Product not selected!')
        return product

    def __init__(self, *args, **kwargs):
        super(CreditSalesForm, self).__init__(*args, **kwargs)
        if self.instance.debit_account_id and self.instance.product_id:
            self.fields['debit_account'].queryset = ChartOfAccount.objects.filter(id=self.instance.debit_account_id)
            self.fields['product'].queryset = Product.objects.filter(product_type__in=[0, 4, 5])
            self.fields['debit_account'].empty_label = 'Select an Account'
            self.fields['product'].empty_label = 'Select a product'
        else:
            self.fields['debit_account'].queryset = ChartOfAccount.objects.filter(account_type=7)
            self.fields['product'].queryset = Product.objects.filter(product_type__in=[0, 4, 5])
            self.fields['debit_account'].empty_label = 'Select an Account'
            self.fields['product'].empty_label = 'Select a product'
        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


CreditSaleFormSet = inlineformset_factory(CreditSalem, CreditSales,
                                                    form=CreditSalesForm, extra=1)


class CreditSalemForm(forms.ModelForm):
    class Meta:
        model = CreditSalem
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(CreditSalemForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['duty_person'].empty_label = None
        self.fields['duty_person'].queryset = DutyMen.objects.filter(duty=duty)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)


class SupplierPaymentForm(forms.ModelForm):
    class Meta:
        model = SupplierPayment
        fields = '__all__'
        widgets = {
            'description': Textarea(attrs={'rows': 4, 'cols': 30}),
        }

    def __init__(self, *args, **kwargs):
        super(SupplierPaymentForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['supplier'].empty_label = 'Select a supplier'
        self.fields['supplier'].queryset = ChartOfAccount.objects.filter(account_type=1)
        self.fields['bank'].empty_label = 'Select a bank'
        self.fields['bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['status'].widget = HiddenInput()
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['payment_for'].empty_label = None
        self.fields['payment_for'].queryset = ChartOfAccount.objects.filter(account_type__in=[8, 13])


class BankDepositionForm(forms.ModelForm):
    class Meta:
        model = BankDeposition
        fields = '__all__'
        widgets = {
            'description': Textarea(attrs={'rows': 4, 'cols': 30}),
        }

    def __init__(self, *args, **kwargs):
        super(BankDepositionForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        self.fields['bank'].empty_label = 'Select a bank'
        self.fields['bank'].queryset = ChartOfAccount.objects.filter(account_type=6)
        self.fields['duty'].empty_label = None
        self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)
        self.fields['status'].widget = HiddenInput()


class FuelVehicleIncomeForm(forms.ModelForm):
    class Meta:
        model = FuelVehicleIncome
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(FuelVehicleIncomeForm, self).__init__(*args, **kwargs)
        duty = get_latest_duty_date()
        if self.instance.vehicle_id:
            self.fields['vehicle'].queryset = ChartOfAccount.objects.filter(id=self.instance.vehicle_id)
            self.fields['vehicle'].empty_label = 'Select vehicle'
        else:
            self.fields['vehicle'].queryset = ChartOfAccount.objects.filter(account_type=15)
            self.fields['vehicle'].empty_label = 'Select vehicle'

        if self.instance.duty_id:
            self.fields['duty'].empty_label = None
            self.fields['duty'].queryset = Duty.objects.filter(date=self.instance.duty_id)
        else:
            self.fields['duty'].empty_label = None
            self.fields['duty'].queryset = Duty.objects.filter(date=duty.date)

        for myField in self.fields:
            self.fields[myField].widget.attrs['class'] = "form-control %s" % (myField)
